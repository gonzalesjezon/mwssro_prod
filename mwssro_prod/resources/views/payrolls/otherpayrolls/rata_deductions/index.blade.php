@@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Search</b></h5>
<!-- 				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							<div class="col-md-6" style="padding-left: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
									<option value=""></option>
								</select>
							</div>
							<div class="col-md-6" style="padding-right: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
									<option value=""></option>
								</select>
							</div>
						</td>

					</tr>
				</table> -->
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="pei">
			<div class="row">
				<div class="col-md-12">
					<label style="font-weight: 600;font-size: 15px; margin-left: 20px;" >&nbsp;&nbsp;{{ $title }}</label>
					<div class="sub-panel">
						{!! $controller->datatable() !!}
					</div>
				</div>
			</div>
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
					<div class="button-wrapper" style="position: relative;top: -20px;margin-left: 7px;">
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newEmployeeStatus" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editEmployeeStatus" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus" id="saveEmployeeStatus"><i class="fa fa-save"></i> Save</a>

						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-form="myform" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"id="cancelEmployeeStatus"> Cancel</a>
					</div>
						<h5 id="employee_name" style="margin-left: 5px;font-weight: bold"></h5>
						<hr>
						<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="monetization_id" id="monetization_id">
							<input type="hidden" name="employee_id" id="employee_id">
							<input type="hidden" name="salary_grade_id" id="salary_grade_id">
							<input type="hidden" name="position_id" id="position_id">

							<div class="col-md-3">
								<div class="form-group newEmployeeStatus">
									<label> Salary</label>
									<input type="text" name="basic_amount" id="basic_amount" class="form-control onlyNumber" readonly>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group newEmployeeStatus">
									<label> Number of Days</label>
									<input type="text" name="number_of_days" id="number_of_days" class="form-control onlyNumber" maxlength="2">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group newEmployeeStatus">
									<label>Factor Rate</label>
									<input type="text" name="factor_rate" id="factor_rate" class="form-control onlyNumber" value="0.0481927">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	@endsection

@section('js-logic1')
<!-- <script src="{{ asset('js/payroll-vue/benefits.js') }} "></script> -->
<script type="text/javascript">
	$(document).ready(function(){
		var table = $('#table').DataTable();
	});

</script>
@endsection

