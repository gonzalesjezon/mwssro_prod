
<table class="table table-responsive datatable" id="tbl_initial_salary">
	<thead>
		<tr>
			<th>Adjustment Amount</th>
			<th>Deduction Amount</th>
			<th>Total</th>
			<th>Period</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody class="text-right">

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_initial_salary').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_initial_salary tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	        $(this).removeClass('selected');
	        clear_form_elements('myForm4');

	        id 							= $(this).data('id');
			employee_id 				= $(this).data('employee_id');
			old_basic_pay_amount 		= $(this).data('old_basic_pay_amount');
			new_basic_pay_amount 		= $(this).data('new_basic_pay_amount');
			salary_adjustment_amount 	= $(this).data('salary_adjustment_amount');
			gsisContAmount 				= $(this).data('gsis_cont_amount');
			philhealthContAmount 		= $(this). data('philhealth_cont_amount');
			// providentFundAmount 		= $(this).data('provident_fund_amount');
			wTaxAmount 					= $(this).data('wtax_amount');
			first_deduction_amount 		= $(this).data('first_deduction_amount');
			second_deduction_amount 	= $(this).data('second_deduction_amount');
			third_deduction_amount 		= $(this).data('third_deduction_amount');
			fourth_deduction_amount 	= $(this).data('fourth_deduction_amount');
			transaction_date 			= $(this).data('transaction_date');
			date_from 					= $(this).data('date_from');
			date_to 					= $(this).data('date_to');

			adjustmentAmount = parseFloat(new_basic_pay_amount) - parseFloat(old_basic_pay_amount);

			// totalDeductionAmount = parseFloat(gsisContAmount) + parseFloat(providentFundAmount) + parseFloat(wTaxAmount) + parseFloat(philhealthContAmount);

			totalDeductionAmount = parseFloat(gsisContAmount) + parseFloat(wTaxAmount) + parseFloat(philhealthContAmount);

			netAmount = parseFloat(adjustmentAmount) - parseFloat(totalDeductionAmount);

			new_rate_amount = (new_basic_pay_amount !== 0) ? commaSeparateNumber(parseFloat(new_basic_pay_amount).toFixed(2)) : '';
			old_rate_amount = (old_basic_pay_amount !== 0) ? commaSeparateNumber(parseFloat(old_basic_pay_amount).toFixed(2)) : '';
			adjustment_amount = (adjustmentAmount !== 0) ? commaSeparateNumber(parseFloat(adjustmentAmount).toFixed(2)) : '';
			gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '';
			philhealth_cont_amount = (philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthContAmount).toFixed(2)) : '';
			// pf_cont_amount = (providentFundAmount !== 0) ? commaSeparateNumber(parseFloat(providentFundAmount).toFixed(2)) : '';
			tax_amount = (wTaxAmount !== 0) ? commaSeparateNumber(parseFloat(wTaxAmount).toFixed(2)) : '';
			total_deduction_amount = (totalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '';
			net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';
			first_amount = (first_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(first_deduction_amount).toFixed(2)) : '';
			second_amount = (second_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(second_deduction_amount).toFixed(2)) : '';
			third_amount = (third_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(third_deduction_amount).toFixed(2)) : '';
			fourth_amount = (fourth_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(fourth_deduction_amount).toFixed(2)) : '';

			$('#transaction_id').val(id);
			$('#employee_id').val(employee_id);
			$('#transaction_date').val(transaction_date).trigger('change');
			$('#date_from').val(date_from);
			$('#date_to').val(date_to);
			$('#new_rate_amount').val(new_rate_amount);
			$('#old_rate_amount').val(old_rate_amount);
			$('#adjustment_amount').val(adjustment_amount);
			$('#gsis_cont_amount').val(gsis_cont_amount);
			$('#philhealth_cont_amount').val(philhealth_cont_amount);
			// $('#pf_cont_amount').val(pf_cont_amount);
			$('#tax_amount').val(tax_amount);
			$('#total_deduction_amount').val(total_deduction_amount);
			$('#net_amount').val(net_amount);
			$('#first_amount').val(first_amount);
			$('#second_amount').val(second_amount);
			$('#third_amount').val(third_amount);
			$('#fourth_amount').val(fourth_amount);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}
	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }

	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
