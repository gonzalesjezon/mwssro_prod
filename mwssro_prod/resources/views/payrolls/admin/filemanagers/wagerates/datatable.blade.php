<div class="col-md-12">
	<table class="table datatable" id="wagetable">
		<thead>
			<tr>
				<th>Wage Region</th>
				<th>Wage Rate</th>
				<th>Effectivity Date</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->wage_region }}</td>
					<td>{{ $value->wage_rate }}</td>
					<td>{{ $value->effectivity_date }}</td>
				</tr>
				@endforeach
			
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#wagetable').DataTable({
		'dom':'<lf<t>pi>',
		"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	});

	$('#wagetable tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

	$('#wagetable tr').on('click',function(){
		var id = 0;	
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#wage_region').val(data.wage_region);
				$('#wage_rate').val(data.wage_rate);
				$('#effectivity_date').val(data.effectivity_date);
				
				$('#for_update').val(data.id);
			}
		})
	});

})
</script>
