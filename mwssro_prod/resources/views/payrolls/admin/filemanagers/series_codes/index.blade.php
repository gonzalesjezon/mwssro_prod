@extends('app-filemanager')

@section('filemanager-content')

<div class="panel panel-default">
	<div class="panel-heading">{!! $title !!}</div>
	<div class="panel-body">

		<div class="row p-2">
			<div class="col-md-12">
				<a href="{{ route('series_codes.create') }}" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Create</a>
			</div>
		</div>

		<div class="row p-4">
			<div class="col-md-12">
				<table class="table" id="table">
					<thead>
						<tr>
							<th>Code</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($series as $item)
						<tr>
							<td>{!! $item->code !!}</td>
							<td>{!! $item->description !!}</td>
							<td style="width: 20%;" class="text-center">
								<div class="row form-group">
									<div class="col-md-6">
										<a class="btn btn-xs btn-success" href="{{ route('series_codes.edit',['id' => $item->id]) }}"><i class="fa fa-edit"></i> Edit</a>
									</div>
									<div class="col-md-6">
										<form action="{{ url($module_prefix.'/series_codes/'.$item->id) }}" method="POST">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" name="_method" value="DELETE">
											<button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Delete record?')"><i class="fa fa-trash"></i> Delete</button>
										</form>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>


@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){
		var table = $('#table').DataTable();

		if(`{{ Session::get('success') }}` != ""){
			swal({  title: `{{ Session::get('success') }}`,
					text: '',
					type: "success",
					icon: 'success',
				});
		}
	});
</script>

@endsection