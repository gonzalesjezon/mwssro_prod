@extends('app-filemanager')

@section('filemanager-content')

<div class="panel panel-default">
	<div class="panel-heading">{!! $title !!}</div>
	<div class="panel-body">
		
		<div class="row p-4">
			<div class="col-md-12">
				@include('payrolls.admin.filemanagers.series_codes._form')
			</div>
		</div>

	</div>
</div>

@endsection

@section('js-logic2')

<script type="text/javascript">
	$(document).ready(function(){

		if(`{{ Session::get('success') }}` != ""){
			swal({  title: `{{ Session::get('success') }}`,
					text: '',
					type: "success",
					icon: 'success',
				});
		}

	})
</script>

@endsection