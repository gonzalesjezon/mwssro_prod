
<form method="POST" action="{{ url($module_prefix.'/'.$module ) }}" id="form">

<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="id" value="{{ @$series->id }}">
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>Code</label>
			<input type="text" name="code" class="form-control form-control-sm" placeholder="Enter code" required value="{{ @$series->code }}">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>Description</label>
			<input type="text" name="description" class="form-control form-control-sm" placeholder="Enter description" required value="{{ @$series->description }}">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-4 text-right">
		<div class="form-group">
			<button class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Save</button>
		</div>
	</div>
</div>


</form>
