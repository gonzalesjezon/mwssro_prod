@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3" style="margin-top: 30px;">
			<div class="search-btn">
				<span>Search</span>
				<!-- <a class="btn btn-xs btn-danger btnfilter" style="float: right;line-height: 16px;margin-bottom: 3px;" ><i class="fa fa-filter"></i>Filter</a> -->
			</div>
			<div >
				<input type="text" name="filter_search" class="form-control search1">
			</div>
			<br>
			<div class="sub-panelnamelist ">
				{!! $controller->show() !!}
			</div>
		</div>
		<div class="col-md-9">
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
					<label id="employee_name" style="    position: absolute;top: 10px;left: 8px;"></label>
					{!! $controller->showLoansHistory() !!}
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
var tLoan = $('#tbl_loaninfo').DataTable();
	// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#year').val(_Year);

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#month').val(_Month);
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});

$('.select2').select2();

// DATE PICKER
$('.datepicker').datepicker({
	dateFormat:'yy-mm-dd'
});


$(document).on('click','#namelist tr',function(){

	employee_id = $(this).data('empid');
	$('#employee_id').val(employee_id);
	fullname = $(this).data('fullname');
	$('#employee_name').text(fullname);

	$.ajax({
		url:base_url+module_prefix+module+'/getLoansHistory',
		data:{
			'employee_id':employee_id
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			tLoan.clear().draw();
			if(data.loanshistory.length !== 0){

				generateLoanInfoTable(data)
			}

		}
	});
});



var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {"q":$('.search1').val(),'limit':$(".limit").val()},
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});

// # loan Info Table
function generateLoanInfoTable(data){

	$.each(data.loanshistory,function(k,v){

		effectivity_date = (v) ? v.loan_date_granted : '';
		loan_name = (v.loans) ? v.loans.name : '';
		total_loan = (v.loan_totalamount) ? v.loan_totalamount : 0;
		total_balance = (v.loan_totalbalance) ? v.loan_totalbalance : 0;
		amortization = (v.loan_amortization) ? v.loan_amortization : 0;
		date_start = (v.loan_date_started) ? v.loan_date_started : '';
		date_end = (v.loan_date_end) ? v.loan_date_end : '';
		status = (v.loan_date_terminated) ? 'Terminated' : 'Active';
		year = (v.year) ? v.year : '';
		month = (v.month) ? v.month : '';

		total_loan = (total_loan !== 0) ? commaSeparateNumber(parseFloat(total_loan).toFixed(2)) : '';
		total_balance = (total_balance !== 0) ? commaSeparateNumber(parseFloat(total_balance).toFixed(2)) : '';
		amortization = (amortization !== 0) ? commaSeparateNumber(parseFloat(amortization).toFixed(2)) : '';

		tLoan.row.add( [
			// effectivity_date,
        	loan_name,
        	total_loan,
        	total_balance,
        	amortization,
        	year,
        	month,
        	status

        ]).draw( false );


	});

}

})
</script>
@endsection
