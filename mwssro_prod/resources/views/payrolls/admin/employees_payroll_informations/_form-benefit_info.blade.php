<div class="col-md-12">
	<div class="box-1 button-style-wrapper" >
		<a class="btn btn-xs btn-info btn-savebg btn_new" id="newBenefit" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit"><i class="fa fa-save"></i> New</a>
			<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" id="editBenefit"><i class="fa fa-edit"></i> Edit</a>

			<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="form3" id="saveBenefit"><i class="fa fa-save"></i> Save</a>

			<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="myform3" id="cancelBenefit"> Cancel</a>
	</div>
	<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeBenefitinfo')}}" onsubmit="return false" id="form3" class="myform3" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="formcontent">
			<table class="table borderless">
				<tr>
					<td>
						<span>Select Benefits</span>
					</td>
					<td>
						<select class="form-control font-style2 newBenefit" name="benefit_id" id="benefit_id">
							<option value=""></option>
							@foreach($benefit as $value)
							<option data-amount="{{ $value->amount }}"  value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</td>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td>
						<span>Amount</span>
					</td>
					<td>
						<input type="text" name="benefit_amount" id="benefit_amount" class="form-control font-style2 onlyNumber newBenefit">
					</td>
					<td colspan="2"></td>
					<td class="text-right">
						<label class="font-style2" style="color:#000;font-weight: normal;position: relative;top: 20px;">
							<input type="checkbox" name="terminated" id="chk_benefit_terminated" data-picker="benefit_info_date_terminated" class="uncheked">
							Terminated
						</label>
					</td>
				</tr>
				<tr>
					<td>Date</td>
					<td>
						<div class="col-md-6">
							<input type="text" name="start_date" id="benefit_info_start_date" class="form-control font-style2  newBenefit datepicker" placeholder="Start Date">

						</div>
						<div class="col-md-6">
							<input type="text" name="end_date" id="benefit_info_end_date" class="form-control font-style2  newBenefit datepicker" placeholder="End Date">
						</div>
					</td>
					<td></td>
					<td class="text-right">Date Terminated</td>
					<td>
						<input type="text" name="date_terminated" id="benefit_info_date_terminated" class="form-control font-style2 datepicker" disabled>
					</td>
				</tr>
			</table>
		</div>
		<input type="hidden" name="id" id="benefit_info_id">
		<input type="hidden" name="employee_id" class="employee_id">
	</form>

</div>