<div class="nonplantilla">
	<div class="col-md-12">
		<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
			<a class="btn btn-xs btn-info btn-savebg btn_new" id="newNonPlantilla" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"><i class="fa fa-save"></i> New</a>

			<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editNonPlantilla" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"><i class="fa fa-save"></i> Edit</a>

			<a class="btn btn-xs btn-info btn_save_nonplantilla hidden" data-form="form" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla" id="saveNonPlantilla"><i class="fa fa-save"></i> Save</a>

			<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-form="myform" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"id="cancelNonPlantilla"> Cancel</a>
		</div>


		<input type="hidden" name="jo_employeeinfo_id" id="jo_employeeinfo_id">
		<input type="hidden" name="jo_annual_rate_amount" id="jo_annual_rate_amount">
		<input type="hidden" name="jo_tax_amount_one" id="jo_tax_amount_one">
		<input type="hidden" name="jo_tax_amount_two" id="jo_tax_amount_two">
		<div class="col-md-4">
			<div class="border-style02">
				<table class="table borderless">
					<tr>
						<td>
							<span>Employee Status</span>
						</td>
						<td>
							<input type="text" name="jo_employee_status" id="jo_employee_status" class="form-control font-style2 "  readOnly>
						</td>
					</tr>
					<tr>
						<td>
							<span>Employee No.</span>
						</td>
						<td>
							<input type="text" name="jo_employee_no" id="jo_employee_no" class="form-control font-style2" readOnly>
						</td>
					</tr>
					<tr>
						<td>
							<span>BP No.</span>
						</td>
						<td>
							<input type="text" name="jo_bp_no" id="jo_bp_no" class="form-control font-style2 newNonPlantilla">
						</td>
					</tr>
				</table>
			</div>
			<div class="border-style02 margintop-25">
				<table class="table borderless">
					<tr>
						<td>
							<span>Position Item No.</span>
						</td>
						<td>
							<input type="text" name="jo_position_itemno" id="jo_position_itemno" class="form-control font-style2" readOnly>
						</td>
					</tr>
					<tr>
						<td>
							<span>Position</span>
						</td>
						<td>
							<input type="text" name="jo_position" id="jo_position" class="form-control font-style2" readOnly>
						</td>
					</tr>
					<tr>
						<td>
							<span>Office</span>
						</td>
						<td>
							<input type="text" name="jo_office" id="jo_office" class="form-control font-style2 _input" readOnly>
						</td>
					</tr>
					<tr>
						<td>
							<span>Division</span>
						</td>
						<td>
							<input type="text" name="jo_division" id="jo_division" class="form-control font-style2 _input" readOnly>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="col-md-4">
			<div class="border-style2">
				<table class="table borderless">
					<tr class="text-center hidden">
						<td>
							<span>Daily Rate</span>
						</td>
						<td>
							<input type="text" name="job_order_daily_rate" class="form-control font-style2 onlyNumber newNonPlantilla" id="job_order_daily_rate" placeholder="0.00" />
						</td>
					</tr>
					<tr>
						<td>
							<span>Monthly Rate</span>
						</td>
						<td>
							<input type="text" name="jo_monthly_rate_amount" class="form-control font-style2 onlyNumber newNonPlantilla" id="jo_monthly_rate_amount" placeholder="0.00"/>
						</td>
					</tr >
					<tr class="hidden">
						<td>
							<span>Annual Rate</span>
						</td>
						<td>
							<input type="text" name="jo_annual_rate_amount" class="form-control font-style2 onlyNumber" id="jo_annual_rate_amount" placeholder="0.00" readonly />
						</td>
					</tr>
				</table>
			</div>
			<div class="border-style2" style="margin-top: -30px;">
				<table class="table borderless">
					<tr class="text-center">
						<td colspan="2"><span>Tax Policy</span></td>
						<!-- <td><span>Contributions</span></td> -->
					</tr>
					<tr class="text-center">
						<td colspan="2">
							<select class="font-style2 form-control newNonPlantilla customselect" id="jo_taxpolicy_id" name="jo_taxpolicy_id">
								<option value=""></option>
								@foreach($jo_tax_policy as $value)
								<option data-taxrate="{{ $value->job_grade_rate }}" value="{{ $value->id }}">{{ $value->policy_name }}</option>
								@endforeach
							</select>
						</td>
					<!-- 	<td class="hidden">
							<input type="text" name="jo_tax_amount" id="jo_tax_amount" class="form-control font-style2 onlyNumber" readOnly>
						</td> -->
					</tr>
					<tr class="text-center">
						<td colspan="2">
							<select class="font-style2 form-control newNonPlantilla customselect" id="jo_taxpolicy_two_id" name="jo_taxpolicy_two_id">
								<option value=""></option>
								@foreach($jo_tax_policy_two as $value)
								<option data-taxrate="{{ $value->job_grade_rate }}" value="{{ $value->id }}">{{ $value->policy_name }}</option>
								@endforeach
							</select>
						</td>
					<!-- 	<td class="hidden">
							<input type="text" name="jo_tax_amount_two" id="jo_tax_amount_two" class="form-control font-style2 onlyNumber" readOnly>
						</td> -->
					</tr>
				</table>
			</div>
			<div class="border-style2 margintop-25">
				<table class="table borderless">
					<tr>
						<td class="text-center" colspan="2"><span>For Job Order/Contractual</span></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<span>Assumption to Duty</span>
						</td>
						<td>
							<input type="text" class="form-control font-style2 newNonPlantilla " id="jo_assumption_to_duty" name="jo_assumption_to_duty" readOnly />
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<span>End of Contract</span>
						</td>
						<td>
							<input type="text" class="form-control font-style2 newNonPlantilla " id="jo_end_of_contract" name="jo_end_of_contract" readOnly />
						</td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="col-md-4">
			<div class="border-style2 ">
				<table class="table borderless newNonPlantilla">
					<tr>
						<td><span>TIN</span></td>
						<td>
							<input type="text" name="jo_tax_id_number" id="jo_tax_id_number" class="form-control font-style2 newNonPlantilla">
						</td>
					</tr>
					<tr>
						<td><span>ATM No</span></td>
						<td>
							<input type="text" name="jo_atm_no" id="jo_atm_no" class="form-control font-style2 newNonPlantilla">
						</td>
					</tr>
					<tr>
						<td>
							<span>Bank</span>
						</td>
						<td>
							<select class="form-control font-style2 customselect newNonPlantilla"  id="jo_bank_id" name="jo_bank_id">
								<option value=""></option>
								@foreach($bank as $value)
									<option data-jobankbranch="{{ $value->branch_name }}" value="{{ $value->id }}"  >{{ $value->name }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<span>Bank Branch</span>
						</td>
						<td>
							<input type="text" class="form-control font-style2 customselect" id="jo_bank_branch" name="jo_bank_branch" readonly />
						</td>
					</tr>
				</table>
			</div>
			<div class="border-style2 margintop-25 newNonPlantilla">
				<table class="table borderless">
					<tr>
						<td nowrap>
								PHIC Contri
						</td>
						<td>
							<input type="text" class="form-control font-style2 onlyNumber newNonPlantilla" id="phic_amount" name="phic_amount" />
						</td>
					</tr>
				</table>
			</div>

		</div>
	</div>
</div>