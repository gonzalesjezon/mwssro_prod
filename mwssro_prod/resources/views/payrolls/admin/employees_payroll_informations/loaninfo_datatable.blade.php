<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_loaninfo">
		<thead>
			<tr>
				<th>Loan Name</th>
				<th>Total Loan</th>
				<th>Loan Balance</th>
				<th>Amortization</th>
				<th>Date Start</th>
				<th>Date End</th>
				<th>Action</th>
				<!-- <th>For Year</th>
				<th>For Month</th> -->
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_loaninfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_loaninfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        clear_form_elements('myForm4');

	        id 	 					= $(this).data('id');
			employee_id 	 		= $(this).data('employee_id');
			loan_id 		 		= $(this).data('loan_id');
			loan_amount		 		= $(this).data('loan_amount');
			loan_total_amount		= $(this).data('loan_total_amount');
			loan_total_balance		= $(this).data('loan_total_balance');
			start_date		 		= $(this).data('start_date');
			end_date		 		= $(this).data('end_date');
			loan_date_granted	 	= $(this).data('loan_date_granted');
			date_terminated			= $(this).data('date_terminated');
			terminated	 			= $(this).data('terminated');

			$('#loan_info_id').val(id)
			$('#loan_id').val(loan_id);
			$('#loan_amount').val(loan_amount);
			$('#loan_total_amount').val(loan_total_amount);
			$('#loan_total_balance').val(loan_total_balance);
			$('#loan_start_date').val(start_date);
			$('#loan_end_date').val(end_date);
			$('#loan_date_granted').val(loan_date_granted);
			$('#loan_info_date_terminated').val(date_terminated);

			if(terminated == 1){
				$('#chk_loan_terminated').prop('checked',true);
			}else{
				$('#chk_loan_terminated').prop('checked',false);
			}

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}
	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
