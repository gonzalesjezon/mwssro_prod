<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_benefitinfo">
		<thead>
			<tr>
				<th>Start Date</th>
				<th>Benefits</th>
				<th>Amount</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody >

		</tbody>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#tbl_benefitinfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_benefitinfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');


	        id 					= $(this).data('id');
			employee_id 		= $(this).data('employee_id');
			benefit_amount 		= $(this).data('benefit_amount');
			name				= $(this).data('name');
			benefit_id 			= $(this).data('benefit_id');
			start_date 	 		= $(this).data('start_date');
			end_date 	 		= $(this).data('end_date');
			date_terminated 	= $(this).data('date_terminated');
			terminated 			= $(this).data('terminated');


			$('#benefit_info_id').val(id);
			$('.employee_id').val(employee_id)
			$('#benefit_amount').val(benefit_amount);
			$('#benefit_id').val(benefit_id);
			$('#benefit_info_start_date').val(start_date);
			$('#benefit_info_end_date').val(end_date);
			$('#chk_benefit_terminated').val(terminated);
			$('#benefit_info_date_terminated').val(date_terminated);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
