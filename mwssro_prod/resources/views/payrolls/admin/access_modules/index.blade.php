@extends('app-front')

@section('content')
<style type="text/css">
.table thead>tr>td{
	font-size: 12px !important;
}

.table tbody>tr>td{
	font-size: 12px !important;
}

.tr td{
	border-bottom: none;
}
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
</style>
<div style="height: 50px;"></div>

@if ($message = Session::get('success'))
<div class="row">
    <div class="col-md-12">
        <div role="alert" class="alert alert-contrast alert-success alert-dismissible">
            <div class="message">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                    <span aria-hidden="true" class="fas fa-times"></span>
                </button>
                <strong>Good!</strong> {{ $message }}
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<a href="{{ route('access_modules.create') }}" class="btn btn-xs btn-info btn-savebg"><i class="fa fa-plus"></i> Create</a>
		</div>
	</div>
</div>

<table class="table"  id="table">
	<thead>
		<tr>
			<th>Access Name</th>
			<th>Date Created</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody class="text-center">
		@foreach($access_modules as $value)
		<tr>
			<td>{{ $value->access_name}}</td>
			<td>{{ date('F d, Y H:i:s A',strtotime($value->created_at)) }}</td>
			<td style="width: 15%;">
				<div class="row form-group">
					<div class="col-md-6">
						<a class="btn btn-xs btn-success" href="{{ route('access_modules.edit',['id' => $value->id]) }}"><i class="fa fa-edit"></i> Edit</a>
					</div>
					<div class="col-md-6">
						<form action="{{ url($module_prefix.'/access_modules/'.$value->id) }}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="_method" value="DELETE">
							<button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Delete record?')"><i class="fa fa-trash"></i> Delete</button>
						</form>
					</div>
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>



@endsection

@section('js-logic1')
<!-- <script src="{{ asset('js/payroll-vue/benefits.js') }} "></script> -->
<script type="text/javascript">
	$(document).ready(function(){
		var table = $('#table').DataTable();
	});

</script>
@endsection

