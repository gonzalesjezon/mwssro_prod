
<div class="border-style2">
	<form method="post" action="{{ url($module_prefix.'/'.$module.'/storeDeductionInfoTransaction')}}" id="formDeduction" onsubmit="return false" class="myformDeduction">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="employee_id" class="employee_id">
		<input type="hidden" name="year" class="year">
		<input type="hidden" name="month" class="month">
		<div class="panel p-2">
			<div class="panel-body">
				<div class="row">
					<div class="form-group">
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDeductionInfo" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editDeductionInfo" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="formDeduction" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo" id="saveDeductionInfo"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-form="myform" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"id="cancelDeductionInfo"> Cancel</a>
						</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<label>Deduction</label>
						<div class="form-group newDeductionInfo">
							<select class="form-control form-control-sm" id="deduction_id" name="deduction_id">
								<option value=""></option>
								@foreach($deductions as $key => $value)
								<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<label>Select pay period</label>
						<div class="form-group newDeductionInfo">
							<select class="form-control form-control-sm" id="pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">1st Half</option>
								<option value="secondhalf">2nd Half</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
							<label>Amount</label>
							<div class="form-group newDeductionInfo">
								<input type="text" name="deduction_amount" id="deduction_amount" class="form-control onlyNumber">
							</div>
					</div>
				</div>

				
		</div>
		</div>
	</form>
</div>