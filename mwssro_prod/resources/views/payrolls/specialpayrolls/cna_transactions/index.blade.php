@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr>
						<td>
								@include('payrolls.includes._months-year')
						</td>
					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="check" id="w" value="w/">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="check" id="wo" value="w/o">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9">
			<label style="font-weight: 600;font-size: 15px;padding-left: 5px;" id="d-name"></label>
			<div class="sub-panel">
				{!! $controller->showCNADatatable() !!}
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-4">
					<div class="panel">
							<div class="panel-body">
								<div class=" button-style-wrapper2">
								<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_saveadjust"><i class="fa fa-save"></i> Add</a>
								<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
							</div>

							<hr>

							<div class="form-group">
								<label>Add Amount</label>
								<input type="text" name="amount" id="amount" class="form-control" placeholder="add amount">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#table').DataTable();
	number_of_actual_work = 0;
    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();

    $('#select_month').trigger('change');
		$('#select_year').trigger('change');

    $('#wo').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	$('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _check = ""
    $('input[type=radio][name=check]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{

    		if (this.value == 'w/') {
    			$('#btn_process').prop('disabled',true);
    			_check = 'w/';

    		}
    		else if (this.value == 'w/o') {
    			$('#btn_process').prop('disabled',false);
    			_check = 'w/o';

    		}
    		$('.btnfilter').trigger('click');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    	console.log(_listId);
    });

    var id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');
    	$('#d-name').text($(this).data('employee'));

    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getCNAInfo',
    			data:{
    				'employee_id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){
    				console.log(data);

    				table.clear().draw();
    				if(data.length !== 0){
	    				clear_form_elements('benefits-content');

	    				cnaAmount = 0;
	    				$.each(data,function(k,v){

	    					cnaAmount = (v.amount) ? v.amount : 0;

	    				});

	    				cna_amount = (cnaAmount) ? commaSeparateNumber(parseFloat(cnaAmount).toFixed(2)) : '';
	    				total = (cnaAmount) ? commaSeparateNumber(parseFloat(cnaAmount).toFixed(2)) : '';

						table.row.add( [
							cna_amount,
									'',
							total

						]).draw( false );
    				}
				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'check':_check,
					'year':_Year,
					'month':_Month
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process',function(){
	let amount = $('#amount').val();
	if(_listId.length == 0 || amount == ""){
		swal("No selectd employee or <br> No Amount inputted!", "", "warning");
	}else{
		swal({
			title: "Process CNA?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processCNA();
			}else{
				return false;
			}
		});
	}

});

$.processCNA= function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processCNA',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'amount':$('#amount').val()
		},
		type:'POST',
		beforeSend:function(){
			$('#btn_process').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
		},
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					$('#btn_process').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
					_listId = [];
					$('.btnfilter').trigger('click');
				}else{

				}
			}
	});
}

$(document).on('click','#delete',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete CNA?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteCNA();
			}else{
				return false;
			}
		});
	}
})

$.deleteCNA = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteCNA',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	'_year':_Year,
			   	'_month':_Month,
			   	'_check':_check },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});



});


</script>
@endsection