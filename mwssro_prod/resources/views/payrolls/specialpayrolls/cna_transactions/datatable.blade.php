<div class="col-md-12">
	<table class="table table-responsive datatable" id="table">
		<thead>
			<tr >
				<th>CNA Amount</th>
				<th>Tax Withheld</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody class="text-right">
		</tbody>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#table').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#table tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
