<div class="panel panel-default formAddDeduction mt-2">
	<div class="panel-body">
		<div class="benefits-content style-box2">
			<div class="col-md-4">
				<div class="button-group mb-4 pt-2">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebanks"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save hidden submitme" ><i class="fa fa-save"></i> Save</a>
					<!-- <a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a> -->
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
				<form method="post" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="employee_id" id="employee_id">
					<input type="hidden" name="id" id="rata_id">
					<input type="hidden" name="year" id="year">
					<input type="hidden" name="month" id="month">
					<input type="hidden" name="pay_period" id="selected_period">
					<div class="form-group">
						<label>Select Deductions</label>
						<select class="form-control" name="deduction_id" id="deduction_id">
							<option></option>
							@foreach($deductions as $key => $value)
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label>Deduction Amount</label>
						<input type="text" name="deduction_amount" id="deduction_amount" class="form-control onlyNumber">
					</div>
				</form>
			</div>
		
	</div>
 </div>
</div>
