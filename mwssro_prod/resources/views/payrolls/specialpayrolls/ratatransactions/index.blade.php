@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless pb-0 mb-0" style="border:none;font-weight: bold">
						<td>
								@include('payrolls.includes._months-year')
							<div class="row pl-1 pr-1">
								<div class="col-md-12">
									<span>Select Pay Period</span>
									<div class="form-group">
										<select class="form-control font-style2 select2" id="pay_period">
											<option></option>
											<option value="firsthalf">First Half</option>
											<option value="secondhalf">Second Half</option>
										</select>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_rata" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_rata" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="wrata" value="wrata">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="worata" value="worata">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9 pt-4" id="rata">
			<label style="font-weight: 600;font-size: 15px;" class="pl-2">
				<span id="employee_name"></span>
			</label>
			<div class="row pl-4 pr-4">
				<div class="col-md-12">
					<div class="tab-container">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#rataTable">RATA</a></li>
							<li><a href="#deductionTable" >Deduction</a></li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane fade in active" id="rataTable">
								<br>
								{!! $controller->showRataDatatable() !!}
							</div>

							<div class="tab-pane fade in" id="deductionTable">
								<br>
								{!! $controller->showDatatable() !!}

								@include('payrolls.specialpayrolls.ratatransactions._form-one')
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblRata = $('#tbl_rata').DataTable();
	var tblDeduction = $('#tbl_deduction').DataTable();
	number_of_actual_work = 0;

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

	var _bool = false;
	var _Year;
	var _Month;
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();
		if(_bool == true){
			$('.btnfilter').trigger('click');
		}

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
		if(_bool == true){
			$('.btnfilter').trigger('click');
		}
	})

    $('.select2').select2();

    $('#select_month').trigger('change');
	$('#select_year').trigger('change');

    $('#worata').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	$('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	// $('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });

    $(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});

    $('.nav-tabs a').click(function(){
			$(this).tab('show');
		});


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkrata = ""
    $('input[type=radio][name=chk_wrata]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{


    		if (this.value == 'wrata') {
    			$('#btn_process_rata').prop('disabled',true);
    			_bool = true;
    			_checkrata = 'wrata';

    		}
    		else if (this.value == 'worata') {
    			$('#btn_process_rata').prop('disabled',false);
    			_checkrata = 'worata';

    		}
    		$('._searchname').trigger('keyup');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}

    });

    var number_of_leave = 0;
    var no_of_actual_work;
    $(document).on('keyup','#no_of_leave_field',function(){

    	number_of_leave = $(this).val();

    	no_of_actual_work = (number_of_leave) ? (number_of_actual_work - number_of_leave) : (number_of_actual_work + number_of_leave);

    	$('#no_of_actual_work').val(no_of_actual_work);
    })

    $(document).on('change','#leave_type',function(){
    	if($(this).val()){
    		$('.benefits-content :input').attr('disabled',false);
    	}else{
    		$('.benefits-content :input').attr('disabled',true);
    		$('.leave-field').attr('disabled',false);
    	}
    });
    var representation_amount = 0;
    var transportation_amount = 0;
    var rata_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');
    	$('#employee_name').text($(this).data('empname'))

    	let payPeriod = $('#pay_period').find(':selected').val();
    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getRataInfo',
    			data:{
    				'id':employee_id,
    				'year':_Year,
    				'month':_Month,
    				'pay_period':payPeriod
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){

    				clear_form_elements('benefits-content');
    				$('#tbl_leave').html('');
    				rata_id = 0;
    				if(data.rata !== null){

	    				rata_id = data.rata.id;
	    				employee_id = data.rata.employee_id;
	    				number_of_actual_work = (data.rata.number_of_actual_work) ? data.rata.number_of_actual_work : data.rata.number_of_work_days;
	    				$('#no_of_actual_work').val(number_of_actual_work);
	    				$('#no_of_work_days').val(data.rata.number_of_work_days);

	    				arr = [];
	    				leave_type = [];
	    				number_of_leave = [];
	    				$.each(data.leave,function(k,v){
	    					arr += '<tr>';
	    					arr += '<td>'+v.leave_date+'</td>';

	    					arr += '<td>'+v.leave_type+'</td>';
	    					arr += '</tr>';

	    				})
	    				$('#tbl_leave').html(arr);
	    				item = '';
	    				$.each(data.leave_list,function(k,v){
	    					item += v+', ('+k+')';
	    				});


	    				percentage = (data.rata.percentage_of_rata_value) ? (data.rata.percentage_of_rata_value * 100) : '100';

						// <!-- GENERATE RATA TABLE --!>

						tblRata.clear().draw();

						representation_amount = (data.rata.representation_amount) ? data.rata.representation_amount : 0;
						transportation_amount = (data.rata.transportation_amount) ? data.rata.transportation_amount : 0;
						total_rata = (parseFloat(representation_amount) + parseFloat(transportation_amount));

						tblRata.row.add( [
							representation_amount,
							transportation_amount,
							item,
							number_of_actual_work,
							percentage+'%',
							representation_amount,
							transportation_amount,
							total_rata.toFixed(2)

							]).draw( false );

							tblRata.rows(0).nodes().to$().attr("data-id", rata_id);
				      tblRata.rows(0).nodes().to$().attr("data-employee_id", employee_id);


				        $('#rata_id').val(rata_id);
				        $('#employee_id').val(employee_id);
				        $("#year").val(_Year);
				        $("#month").val(_Month);
				        $("#selected_period").val(payPeriod);
    				}

    				tblDeduction.clear().draw();
    				if(data.rata_deduction.length > 0)
    				{
    					generate_deduction(data.rata_deduction);
    				}
				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

 
function generate_deduction(data)
{
	tblDeduction.clear().draw();
	$.each(data,function(k,v){

		name = (v.deductions.name) ? v.deductions.name : '';
		amount = (v.deduction_amount) ? v.deduction_amount : 0;

		tblDeduction.row.add([
			name,
			amount,
			'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteDeduction" data-deduction_id="'+v.id+'" data-year="'+v.year+'" data-month="'+v.month+'" data-employee_id="'+v.employee_id+'" data-pay_period="'+v.pay_period+'"><i class="fa fa-trash"></i> Delete</a>'
		]).draw( false );

		tblDeduction.rows(k).nodes().to$().attr("data-id", v.id);
		tblDeduction.rows(k).nodes().to$().attr("data-deduction_id", v.deduction_id);
		tblDeduction.rows(k).nodes().to$().attr("data-deduction_amount", v.deduction_amount);
		tblDeduction.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
		tblDeduction.rows(k).nodes().to$().attr("data-pay_period", v.pay_period);
		tblDeduction.rows(k).nodes().to$().attr("data-year", v.year);
		tblDeduction.rows(k).nodes().to$().attr("data-month", v.month);

	});
}

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'limit':$(".limit").val(),
					'check_rata':_checkrata,
					'year':_Year,
					'month':_Month,
					'pay_period':$('#pay_period').find(':selected').val()
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");
				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);
				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});


$(document).on('click','#btn_process_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processRata();
			}else{
				return false;
			}
		});
	}

});

$.processRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'rata_id':rata_id,
			'employee_id':employee_id,
			'pay_period':$('#pay_period').find(':selected').val()
		},
		type:'POST',
		beforeSend:function(){
			$('#btn_process_rata').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
		},
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					$('#btn_process_rata').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
					$('._searchname').trigger('keyup');
				}else{

				}
			}
	});
}

$(document).on('click','#btn_save_rata',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.saveRata();
	}

});

$.saveRata = function(){
	$.ajax({
		type:'POST',
		url:base_url+module_prefix+module+'/storeRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'employee_id':employee_id,
			'rata_id':rata_id,
			'rata':{
				'no_of_actual_work':$('#no_of_actual_work').val(),
				'no_of_work_days':$('#no_of_work_days').val(),
				'no_of_used_vehicles':$('#no_of_used_vehicles').val(),
				'percentage_of_rata':$('#percentage_of_rata :selected').text(),
				'percentage_of_rata_value':$('#percentage_of_rata :selected').val(),
				'representation_amount':representation_amount,
				'transportation_amount':transportation_amount,
				'year':_Year,
				'month':_Month
			},
			'leave':{
				'inclusive_leave_date':$('#inclusive_leave_date').val(),
				'leave_type':$('#leave_type').val(),
				'no_of_leave_field':$('#no_of_leave_field').val(),
			}

		},
		success:function(data){
			par = JSON.parse(data);

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				}).then(function(){

					$('.benefits-content :input').prop('disabled',true);
					clear_form_elements('benefits-content');
					$('#tbl_leave').html('');
					$('.btn_save').addClass('hidden');
					$('.btn_new').addClass('hidden');
					$('.btn_edit').removeClass('hidden');

					employee_id = ''
					rata_id = '';
					number_of_actual_work = (par.rata.rata.number_of_actual_work) ? par.rata.rata.number_of_actual_work : par.rata.number_of_work_days;
					$('#no_of_actual_work').val(number_of_actual_work);
					$('#no_of_work_days').val(par.rata.rata.number_of_work_days);

					arr = [];
					leave_type = [];
					number_of_leave = [];
					$.each(par.rata.leave,function(k,v){
						arr += '<tr>';
						arr += '<td>'+v.leave_date+'</td>';

						arr += '<td>'+v.leave_type+'</td>';
						arr += '</tr>';

					})
					$('#tbl_leave').html(arr);
					item = '';
					$.each(par.rata.leave_list,function(k,v){
						item += v+', ('+k+')';
					});
						// GENERATE TR FOR BENEFITS TAB

						percentage = (par.rata.rata.percentage_of_rata_value) ? (par.rata.rata.percentage_of_rata_value * 100) : '100';

						tblRata.clear().draw();

						representation_amount = (par.rata.rata.representation_amount) ? par.rata.rata.representation_amount : 0;
						transportation_amount = (par.rata.rata.transportation_amount) ? par.rata.rata.transportation_amount : 0;
						total_rata = (parseFloat(representation_amount) + parseFloat(transportation_amount));

						tblRata.row.add( [
							representation_amount,
							transportation_amount,
							item,
							number_of_actual_work,
							percentage+'%',
							representation_amount,
							transportation_amount,
							total_rata.toFixed(2)

							]).draw( false );
					});


			}else{
				swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-warning",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})
			}
		}
	});
}

$(document).on('click','#delete_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteRata();
				tblRata.clear().draw();
			}else{
				return false;
			}
		});
	}
})

$.deleteRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteRata',
		data:{
			'empid':_listId,
			'year':_Year,
			'month':_Month,
			'_token':"{{ csrf_token() }}",
			'pay_period':$('#pay_period').find(':selected').val()
		},
		type:'post',
		beforeSend:function(){
			$('#delete_rata').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_rata').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('._searchname').trigger('keyup');
			}
		}
	});
}
var btn;
/*serialize All form ON SUBMIT*/
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);

	$("#form").ajaxForm({
		beforeSend:function(){

		},
		success:function(data){
			par  =  JSON.parse(data);
			if(par.status){

				swal({  title: par.response,
						text: '',
						type: "success",
						icon: 'success',

					}).then(function(){

						// window.location.href = base_url+module_prefix+module;
						// clear_form_elements('myform')

						$('.btn_cancel').trigger('click');
						generate_deduction(par.data);

					});

			}else{

				swal({  title: par.response,
						text: '',
						type: "error",
						icon: 'error',

					});

			}

			btn.button('reset');
		},
		error:function(data){
			$error = data.responseJSON;
			/*reset popover*/
			$('input[type="text"], select').popover('destroy');

			/*add popover*/
			block = 0;
			$(".error-msg").remove();
			$.each($error,function(k,v){
				var messages = v.join(', ');
				msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
				$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
				if(block == 0){
					$('html, body').animate({
				        scrollTop: $('.err-'+k).offset().top - 250
				    }, 500);
				    block++;
				}
			})
			$('.saving').replaceWith(btn);
		},
		always:function(){
			setTimeout(function(){
					$('.saving').replaceWith(btn);
				},300)
		}
	}).submit();

});

// ======================================================= //
// ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
	loan_id 			= $(this).data('deduction_id');
	year 					= $(this).data('year');
	month 				= $(this).data('month');
	employee_id 	= $(this).data('employee_id');
	pay_period 		= $(this).data('pay_period');
	function_name = $(this).data('function_name')

	if(loan_id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':loan_id,
						'year':year,
						'month':month,
						'employee_id':employee_id,
						'pay_period':pay_period,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						})

						generate_deduction(res.data)

					}

				})
			}else{
				return false;
			}
		});
	}
})

});


</script>
@endsection