<table class="table table-responsive datatable" id="tbl_deduction">
	<thead>
		<tr >
			<th>Deduction</th>
			<th class="text-right">Amount</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_deduction').DataTable({
	 	'dom':'<lf<t>pi>',
	 	'responsive': true,
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_deduction tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        employee_id = $(this).data('employee_id');
	        id 					= $(this).data('id');
	        deduction_id = $(this).data('deduction_id');
	        year 				= $(this).data('year');
	        month 			= $(this).data('month');
	        pay_period 	= $(this).data('pay_period');
	        deduction_amount 	= $(this).data('deduction_amount');

	        $('#employee_id').val(employee_id);
	        $('#rata_id').val(id);
	        $('#deduction_id').val(deduction_id);
	        $('#year').val(year);
	        $('#month').val(month);
	        $('#pay_period').val(pay_period);
	        $('#deduction_amount').val(deduction_amount);


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );


})
</script>
