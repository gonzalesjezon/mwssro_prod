<form id="form" action="{{ url($module_prefix.'/'.$module)}}" method="post" onsubmit="return false" class="myform">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="button-wrapper mb-3" id="editForm">
	<a class="btn btn-xs btn-info btn-editbg btn_edit" id="editOvertime" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-btnedit="editOvertime" data-btnsave="saveOvertime"><i class="fa fa-save"></i> Edit</a>

	<a class="btn btn-xs btn-info btn-savebg btn_save save_overtime hidden submitme" data-form="form" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-btnedit="editOvertime" data-btnsave="saveOvertime" id="saveOvertime"><i class="fa fa-save"></i> Save</a>
	<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-form="myform" data-btnedit="editOvertime" data-btnsave="saveOvertime"id="cancelOvertime"> Cancel</a>
</div>

<input type="hidden" name="id" id="overtime_id">
<input type="hidden" name="employee_id" id="employee_id">
<input type="hidden" name="from_date" class="from_date">
<input type="hidden" name="to_date" class="to_date">
<input type="hidden" name="year" class="year">
<input type="hidden" name="month" class="month">

<table class="table borderless">
		<tr class="text-center">
			<td>Type of OT</td>
			<td>OT Rate</td>
			<td>No of Hrs.</td>
			<td>OT Hrs.</td>
			<td>Amount</td>
		</tr>
		<tr>
			<td >Regular Day (1.25%)</td>
			<td>
				<input type="text" name="regular_ot_rate" id="regular_ot_rate" class="form-control font-style2 ot_rate" readonly>
			</td>
			<td class="clear-input" >
				<input type="text" name="regular_no_hrs" id="regular_no_hrs" class="form-control regular font-style2 isNumber editOvertime" data-input-one="regular_ot_hrs" data-input-two="regular_ot_rate" data-input-three="regular_ot_amount" data-rate="1.25">
			</td>
			<td class="clear-input">
				<input type="text" name="regular_ot_hrs" id="regular_ot_hrs" class="form-control font-style2 isNumber" readonly>
			</td>
			<td class="clear-input">
				<input type="text" name="regular_ot_amount" id="regular_ot_amount" class="form-control font-style2 ot_amount" readonly placeholder="0.00">
			</td>
		</tr>
		<tr>
			<td>Weekend (1.50%)</td>
			<td>
				<input type="text" name="weekend_ot_rate" id="weekend_ot_rate" class="form-control font-style2 ot_rate" readonly>
			</td>
			<td class="clear-input">
				<input type="text" name="weekend_no_hrs" id="weekend_no_hrs" class="form-control font-style2 regular isNumber editOvertime" data-input-one="weekend_ot_hrs" data-input-two="weekend_ot_rate" data-input-three="weekend_ot_amount" data-rate="1.50">
			</td>
			<td class="clear-input">
				<input type="text" name="weekend_ot_hrs" id="weekend_ot_hrs" class="form-control font-style2 isNumber" readonly>
			</td>
			<td class="clear-input">
				<input type="text" name="weekend_ot_amount" id="weekend_ot_amount" class="form-control font-style2 ot_amount" readonly placeholder="0.00">
			</td>
		</tr>
		<tr>
			<td nowrap>Regular Holiday (1.50%)</td>
			<td>
				<input type="text" name="holiday_ot_rate" id="holiday_ot_rate" class="form-control font-style2 ot_rate" readonly>
			</td>
			<td>
				<input type="text" name="holiday_no_hrs" id="holiday_no_hrs" class="form-control font-style2 regular isNumber editOvertime" data-input-one="holiday_ot_hrs" data-input-two="holiday_ot_rate" data-rate="1.50" data-input-three="holiday_ot_amount">
			</td>
			<td class="clear-input">
				<input type="text" name="holiday_ot_hrs" id="holiday_ot_hrs" class="form-control font-style2 isNumber" readonly>
			</td>
			<td class="clear-input">
				<input type="text" name="holiday_ot_amount" id="holiday_ot_amount" class="form-control font-style2 ot_amount" readonly placeholder="0.00">
			</td>
		</tr>

		<tr>
			<td colspan="3"></td>
			<td>Total</td>
			<td>
				<input type="text" name="ot_total" id="ot_total" class="form-control font-style2 isNumber" readonly placeholder="0.00">
			</td>
		</tr>

		<tr>
			<td colspan="3"></td>
			<td>W/tax</td>
			<td>
				<input type="text" name="ot_tax" id="ot_tax" class="form-control font-style2 onlyNumber" placeholder="0.00">
			</td>
		</tr>

		<tr>
			<td colspan="3"></td>
			<td>Net Amount</td>
			<td>
				<input type="text" name="ot_net" id="ot_net" class="form-control font-style2 onlyNumber" readonly placeholder="0.00">
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<div class="col-md-12">
					<span>OT Covered Period:</span>
					<div class="form-group">
						<textarea name="ot_covered_period" id="ot_covered_period" rows="5" cols="50"></textarea>
					</div>
				</div>
			</td>
			<td colspan="2"></td>
		</tr>
	</table>

	</form>