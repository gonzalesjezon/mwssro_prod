@extends('app-front')
@section('content')
<div class="col-md-12">
	<div class="col-md-3">
		<div style="height: 50px;"></div>
		<div>
			<h5 ><b>Filter Employee By</b></h5>
			<table class="table borderless" style="border:none;font-weight: bold">
				<tr>
					<td>
						@include('payrolls.includes._months-year')
						<div class="row">
							<div class="col-md-6">
								<span>Covered</span>
								<div class="form-group">
									<input type="text" name="from_date" id="from_date" class="form-control font-style2 datepicker">
								</div>
							</div>
							<div class="col-md-6">
								<span>&nbsp;</span>
								<div class="form-group">
									<input type="text" name="to_date" id="to_date" class="form-control font-style2 datepicker">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<span>Employee Status</span>
								<div class="form-group">
									<select class="form-control font-style2" id="status" name="status">
										<option value=""></option>
										<option value="plantilla">Plantilla</option>
										<option value="nonplantilla">Non Plantilla</option>
									</select>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<div class="col-md-4">
					<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				</div>
				<div class="col-md-8 text-right">
					<button class="btn btn-xs btn-info process_overtime" id="process_overtime" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_overtime" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>

				</div>
				<div class="search-btn">
					<div class="col-md-12">
						<span >Search</span>
						<span class="radiobut-style radio-inline " style="margin-left: 70px;">
							<input type="radio" name="chk_overtime" id="wovertime" value="w/">
							W/ OT
						</span>

						<span class="radiobut-style radio-inline ">
							<input type="radio" name="chk_overtime" id="woovertime" value="w/o">
							W/Out OT
						</span>
					</div>
				</div>
			<div >
				<input type="text" name="filter_search" class="form-control _searchname">
			</div>
			<br>
			<br>
			<div class="namelist" style="position: relative;top: -25px;">
				{!! $controller->show() !!}
			</div>

		</div>
	</div>
	<div class="col-md-9">
		<div style="height: 50px;"></div>
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="newSummary-name">
					<label id="d-name" style="text-transform: uppercase;"></label>
				</div>
			</div>
			<div class="col-md-6">

			</div>
		</div>
		<div class="tab-container">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#">Overtime</a></li>
			</ul>
		</div>
		<div class="tab-content">
			<div class="col-md-12">
				<div class="tab-pane">
					<div class="border-style2">
						@include('payrolls.overtimepay._form')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="employee_id" id="employee_id">
<input type="hidden" name="overtime_id" id="overtime_id">
@endsection
@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
var _Year;
var _Month;
var _bool = false;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})

$('.editOvertime').prop('readonly',true);
$('.select2').select2();

$('#select_year').trigger('change');
$('#select_month').trigger('change');

$('.isNumber').keypress(function (event) {
    return isNumber(event, this)
});

$('.datepicker').datepicker({
	dateFormat:'yy-mm-dd'
});

$('#from_date').change(function(){
	$('#to_date').val($(this).val());
	$('.from_date').val($(this).val());
});

$('#to_date').change(function(){
	$('.to_date').val($(this).val());
});


//  SEPARATE NUMBER IN COMMA
$(".onlyNumber").keyup(function(){
	amount  = $(this).val();
	if(amount == 0){
		$(this).val('');
	}else{
		plainAmount = amount.replace(/\,/g,'')
		$(this).val(commaSeparateNumber(plainAmount));
	}
});

var list_id = [];
// CHECK EMPLOYEE WITH OR W/ OUT OVERTIME
check_overtime = ""
 $('input[type=radio][name=chk_overtime]').change(function() {
 	if(!_Year){
 		swal({
		  	title: 'Select year and month first',
		  	type: "warning",
		  	showCancelButton: false,
		  	confirmButtonClass: "btn-warning",
		  	confirmButtonText: "OK",
		  	closeOnConfirm: false
		})
		$(this).prop('checked',false);
 	}else{
		if (this.value == 'w/') {
			$('#process_overtime').prop('disabled',true);
			$('#editForm').removeClass('hidden');
			_bool = true;
        	check_overtime = this.value;
        	list_id
        }
        else if (this.value == 'w/o') {
        	$('#process_overtime').prop('disabled',false);
        	$('#editForm').addClass('hidden');
            check_overtime = this.value;
            list_id
        }
        $('.btnfilter').trigger('click');
 	}
});

// GET SELECTED ID AND PUSH IT INTO ARRAY

$(document).on('click','.emp_select',function(){
	empid = $(this).val();
	index = $(this).data('key');
	if(!_Year && !_Month && !list_id){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
		$('.emp_select').prop('checked',false);
	}else{
		if($(this).is(':checked')){
			list_id[index] =  empid;

		}else{
			
		}
	}
	
});

$(document).on('click','#check_all',function(){
	if(!_Year && !_Month){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
		$('#check_all').prop('checked',false);
	}else{
		if ($(this).is(':checked')) {
	        $('.emp_select').prop('checked', 'checked');

	        $('.emp_select:checked').each(function(){
	        	list_id.push($(this).val())
	        });

	    } else {
	        $('.emp_select').prop('checked', false)
	        list_id = [];
	    }
	}
});


// GET EMPLOYEE OVERTIME PAY DETAILS
$(document).on('click','#namelist tr',function(){
	employee_id = $(this).data('empid');
	$('#d-name').text($(this).data('employee'));

	if(_Year && _Month){
		$.ajax({
			url:base_url+module_prefix+module+'/getOvertimeInfo',
			data:{
				'employee_id':employee_id,
				'year':_Year,
				'month':_Month,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);
				$('#net_overtime').text('')
				clear_form_elements('editOvertime');
				clear_form_elements('clear-input');
				$('#overtime_id').val('');
				$('.year').val(_Year);
				$('.month').val(_Month);
				$('#employee_id').val(employee_id);

				monthlyRate = 0;
				if(data.plantilla !== null){
					monthlyRate = (data.plantilla.monthly_rate_amount) ? data.plantilla.monthly_rate_amount : 0;
					otRate 	 		 = (Number(monthlyRate) / 22) / 8;

					$('.ot_rate').val(otRate.toFixed(3));
				}

				if(data.nonplantilla !== null){
					// monthlyRate = (data.nonplantilla.monthly_rate_amount) ? data.nonplantilla.monthly_rate_amount : 0;
					// otRate 	 		 = (Number(monthlyRate) / 22) / 8;

					// $('.ot_rate').val(otRate.toFixed(3));

				}

				if(data.overtimepay){
					overtimeId = data.overtimepay.id;

					regularNoHrs = (data.overtimepay.regular_no_hrs) ? data.overtimepay.regular_no_hrs : 0;
					weekendNoHrs = (data.overtimepay.weekend_no_hrs) ? data.overtimepay.weekend_no_hrs : 0;
					holidayNoHrs = (data.overtimepay.holiday_no_hrs) ? data.overtimepay.holiday_no_hrs : 0;
					otTax = (data.overtimepay.ot_tax) ? data.overtimepay.ot_tax : 0;
					otCovered = (data.overtimepay.ot_covered_period) ? data.overtimepay.ot_covered_period : '';

					$('#overtime_id').val(overtimeId);
					$('#ot_covered_period').val(otCovered);
					$('#regular_no_hrs').val(regularNoHrs).trigger('keyup');
					$('#weekend_no_hrs').val(weekendNoHrs).trigger('keyup');
					$('#holiday_no_hrs').val(holidayNoHrs).trigger('keyup');
					$('#ot_tax').val(otTax).trigger('keyup');

				}
			}
		});
	}else{
		swal({
			title: 'Select year and month first',
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
	}
});

$(document).on('click','#process_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processOvertime();
			}else{
				return false;
			}
		});
	}
});


// SAVE COMPUTED OVERTIME PAY
$.processOvertime = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/storeOvertimeInfo',
		type:'POST',
		data:{
			'_token':"{{ csrf_token() }}",
			'employee_id':list_id,
			'year':_Year,
			'month':_Month,
			'status':status
		},
		beforeSend:function(){
        	$('#process_overtime').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data)
			swal({
			  	title: par.response,
			  	type: "success",
			  	showCancelButton: false,
			  	confirmButtonClass: "btn-success",
			  	confirmButtonText: "Yes",
			  	closeOnConfirm: false
			});
			$('#process_overtime').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
			clear_form_elements('editOvertime');
			$('#employee_id').val('');
			$('#overtime_id').val('');
			$('#editOvertime').removeClass('hidden');
			$('#saveOvertime').addClass('hidden');
			$('#cancelOvertime').addClass('hidden');
			$('.editOvertime').prop('readonly',true);
			list_id = [];
			$('.btnfilter').trigger('click')
		}
	});
}

var status;
$('#status').change(function(){
	status = $(this).find(':selected').val();
	$('.btnfilter').trigger('click');
})


// FILTER BY
var timer;
$(document).on('click','.btnfilter',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	'year':_Year,
			   	'month':_Month,
			   	'check_overtime':check_overtime,
			   	'status':status
			   },
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});


$(document).on('click','#delete_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteOvertime();
			}else{
				return false;
			}
		});
	}
});

$.deleteOvertime = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteOvertime',
		data:{'empid':list_id,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
        	$('#delete_overtime').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(response){
			par = JSON.parse(response)
			if(par.status){
				swal({
				  title: par.response,
				  type: "success",
				  showCancelButton: false,
				  confirmButtonClass: "btn-success",
				  confirmButtonText: "OK",
				  closeOnConfirm: false
				});

				list_id = [];
				$('#delete_overtime').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
				$('.btnfilter').trigger('click');
				clear_form_elements('myForm');
				$("#overtime_pay").text(0.00)
			}
		}
	})
}

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			   	'year':_Year,
			   	'month':_Month,
			   	'check_overtime':check_overtime,
			   	'status':status
			},
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnedit+' :input').attr("readonly",false);
	$('.'+btnedit).attr('readonly',false);
	$('#'+btnedit).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	$('#overtime_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnedit+' :input').attr("readonly",true);
	$('.'+btnedit).attr('readonly',true);
	// $('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).removeClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	clear_form_elements('editOvertime');
	$('.error-msg').remove();

});

$('.regular').keyup(function(){
		multiplier 	= $(this).val();
		nameOtHrs 	= $(this).data('input-one');
		nameOtRate  = $(this).data('input-two');
		nameOtAmount = $(this).data('input-three');
		rate 				= $(this).data('rate');

		otHrs = Number(rate) * Number(multiplier);
		$('#'+nameOtHrs).val(otHrs.toFixed(3));

		otRate = $('#'+nameOtRate).val();
		otAmount = Number(otRate) * Number(otHrs);
		ot_amount = (otAmount) ? commaSeparateNumber(Number(otAmount).toFixed(2)) : '0.00';
		$('#'+nameOtAmount).val(ot_amount);

		var total = 0;
		$("[class*='ot_amount']").each(function (k,v) {
			var name  = $(this).attr("name");
			amount 		= $('#'+name).val();
			total += Number(amount.replace(/\,/g,''));
		});

		total_amount = (total) ? commaSeparateNumber(Number(total).toFixed(2)) : '0.00';
		$('#ot_total').val(total_amount);
		$('#ot_net').val(total_amount);
});

$('#ot_tax').keyup(function(){
	tax = $(this).val();

	let otTotal = $('#ot_total').val();
	otNet = Number(otTotal.replace(/\,/g,'')) - Number(tax);

	total_amount = (otNet) ? commaSeparateNumber(Number(otNet).toFixed(2)) : '0.00';
	$('#ot_net').val(total_amount);
});


$(document).off('click',".submitme").on('click',".submitme",function(){
		btn = $(this);
		form = $(this).data('form');

		$("#"+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							clear_form_elements('myform')
							$('.editOvertime').prop('readonly',true);
							$('.btn_cancel').trigger('click');

						});
				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});




});
</script>
@endsection