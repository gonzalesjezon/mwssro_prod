@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row pl-4 pr-4">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<span>&nbsp;</span>
						<div class="form-group">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;">
	       		<div class="row">
	       			<div class="col-md-8">
	       				<img src="{{url('/images/mwssreportheader.png')}}" style="height: 50px;margin-top: 20px;">
	       				<h5><b>{!! $title !!}</b></h5> <br>
	       				<span>OFFICE/DEPARTMENT:    </span> <br>
	       				<span>Payment of allowance for the period <span id="month_year"></span> in accordance with RA No. 7323 dated 30 March 1992</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table">
							<thead class="text-center" style="font-weight: bold;">
								 <tr>
								 	<td rowspan="2"style="line-height: 20px;">#</td>
								 	<td rowspan="2"style="line-height: 20px;">Name</td>
								 	<td rowspan="2"style="line-height: 20px;">Date Started</td>
								 	<td rowspan="2"style="line-height: 20px;">Designation</td>
								 	<td rowspan="2"style="line-height: 20px;">Allowance <br> Per Day</td>
								 	<td rowspan="2"style="line-height: 20px;">Total Amount <br> Allowance</td>
								 	<td colspan="3" "style="line-height: 20px;">Less (Undertime/Absences)</td>
								 	<td rowspan="2" >TOTAL Abs./Und.</td>
								 	<td rowspan="2"style="line-height: 20px;">Add Overtime</td>
								 	<td rowspan="2"style="line-height: 20px;">NET Amount <br> (Allowance)</td>
								 </tr>
								 <tr>
								 	<td><span>Days(Amt)</span></td>
								 	<td><span>Hrs(Amt)</span></td>
								 	<td><span>Min(Amt)</span></td>
								 </tr>
							</thead>
							<tfoot>
							 	<tr>
							 		<td colspan="4"  style="border: none">Prepared By</td>
							 		<td colspan="4"  style="border: none">Verified By</td>
							 		<td colspan="4"  style="border: none">Certified Correct</td>
							 	</tr>
							 	<tr>
							 		<td class="text-left" colspan="4" style="border: none">
							 			<b><span>Theresa V. Makiling</span></b> <br>
							 			<span>Finance Officer</span>
							 		</td>
							 		<td class="text-left" colspan="4"  style="border: none">
							 			<b><span>Marie Claudeline M. Tenorio</span></b> <br>
							 			<span>IRM Officer </span>
							 		</td>
							 		<td class="text-left" colspan="4"  style="border: none">
							 			<b><span>IRM Officer </span></b> <br>
							 			<span>Chief Corporate Accountant</span>
							 		</td>
							 	</tr>
							 </tfoot>
							<tbody id="tbl_body">
							</tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);
					if(data.transaction.length !== 0){
						arr = [];
						var percentOne;
						var percentTwo;
						var	total_monthly_compensation_amount = 0;
						var total_half_monthly_compensation_amount = 0;
						var total_less_amount = 0;
						var total_compensation_amount = 0;
						var total_percentTwo = 0;
						var total_net_amount = 0;
						var total_basic_pay = 0;
						$.each(data.transaction,function(k,v){

							lastname 		= (v.employees.lastname) ? v.employees.lastname+',' : '';
							firstname 	= (v.employees.firstname) ? v.employees.firstname : '';
							middlename 	= (v.employees.middlename) ? v.employees.middlename : '';
							middlename 	= (middlename) ? middlename.substring(-1,1) + '.' : '';
							fullname = astname +' '+firstname+' '+middlename;
							employee_status = v.employee_status.name;
							daily_amount = (v.employeeinfo) ? v.employeeinfo.daily_rate_amount : 0;
							actual_basicpay = (v.actual_basicpay) ? v.actual_basicpay : 0;

							absence = (v.total_absences !== 0) ? v.total_absences : 0;
							undertime = (v.total_undertime !== 0) ? v.total_undertime : 0;
							less_amount = (absence + undertime);

							total_less_amount += parseFloat(less_amount);
							total_basic_pay += parseFloat(actual_basicpay);


							daily_amount = (daily_amount !== 0) ? commaSeparateNumber(parseFloat(daily_amount).toFixed(2)) : '';
							actual_basicpay = (actual_basicpay !== 0) ? commaSeparateNumber(parseFloat(actual_basicpay).toFixed(2)) : '';
							absence = (absence !== 0) ? commaSeparateNumber(parseFloat(absence).toFixed(2)) : '';
							undertime = (undertime !== 0) ? commaSeparateNumber(parseFloat(undertime).toFixed(2)) : '';
							less_amount = (less_amount !== 0) ? commaSeparateNumber(parseFloat(less_amount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td>'+(k+1)+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td></td>';
							arr += '<td class="text-center">'+employee_status+'</td>';
							arr += '<td class="text-right">'+daily_amount+'</td>';
							arr += '<td class="text-right">'+actual_basicpay+'</td>';
							arr += '<td class="text-right">'+absence+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right">'+actual_basicpay+'</td>';
							arr += '</tr>';

						});
							total_basic_pay = (total_basic_pay !== 0) ? commaSeparateNumber(parseFloat(total_basic_pay).toFixed(2)) : '';

							arr += '<tr style="font-weight:bold;">';
							arr += '<td></td>';
							arr += '<td>Total</td>';
							arr += '<td></td>';
							arr += '<td></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right">'+total_basic_pay+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right">'+total_basic_pay+'</td>';
							arr += '</tr>';


						$('#tbl_body').html(arr);
						days = daysInMonth(_Month,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod =  months[_Month]+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);
						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}

	$('#print').on('click',function(){
		$('#reports').printThis();
	})
})
</script>
@endsection