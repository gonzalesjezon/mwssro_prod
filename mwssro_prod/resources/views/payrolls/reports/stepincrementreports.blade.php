@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row pl-4 pr-4">
					<div class="col-md-12">
						<span><b>Employee Name</b></span>
					</div>
				</div>
				<div class="row pl-4 pr-4 mb-2">
					<div class="col-md-6">
						<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
							<option value=""></option>
							@foreach($employeeinfo as $value)
							<option value="{{ $value->id }}">{{ strtoupper($value->lastname) }} {{ strtoupper($value->firstname) }} {{ strtoupper($value->middlename) }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row pl-4 pr-4">
					<div class="col-md-12">
						<span><b>Transaction Date</b></span>
					</div>
				</div>
				<div class="row pl-4 pr-4 mb-2">
					<div class="col-md-6">
						<select class="form-control select2" name="month" id="select_month">
							<option value=""></option>
						</select>
					</div>
				</div>
				@include('payrolls.reports.includes._signatory-two')
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#prnModal" id="preview">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body" id="reports" style="margin-left: 15px; margin-right: 15px;width: 760px;margin: auto;font-family: Book Antiqua !important;">
	       		<div class="row">
	       			<div class="col-xs-12">
						Philippine Institute for Development Studies	<br>
						Step Increment -   <span id="fullname"></span> <br>
						For the period <span id="covered_period"></span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 50px;">
	       			<div class="col-xs-12">Basic Pay</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;"> Per Notice of Step Increment Due to Length of Service</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span id="new_basic_amount"></span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">   Per Notice of Step Increment Due to Length of Service</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span id="old_basic_amount">
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-9">
	       				<span >Salary Adjustment</span>
	       			</div>
	       			<div class="col-xs-3 text-right" style="border-bottom: 2px solid #333; border-top: 1px solid #333;">
	       				<span  id="salary_adj_amount">
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 25px;">
	       			<div class="col-xs-6">
	       				<span >Gross</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span >Basic Pay</span>
	       			</div>
	       			<div class="col-xs-4 text-right">
	       				<span id="basic_pay">
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-12">Deductions:</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">GSIS Contribution</span>
	       			</div>
	       			<div class="col-xs-4 text-right">
	       				<span id="gsis_cont_amount">
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">Provident Fund Contribution</span>
	       			</div>
	       			<div class="col-xs-4 text-right">
	       				<span id="pf_cont_amount" >
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">Withholding Tax</span>
	       			</div>
	       			<div class="col-xs-4 text-right">
	       				<span id="wtax_amount">
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span >Total Deduction</span>
	       			</div>
	       			<div class="col-xs-4 text-right" style="border-bottom: 1px solid #333;border-top: 1px solid #333;">
	       				<span  id="total_deduction_amount">
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span >Net Amount Due</span>
	       			</div>
	       			<div class="col-xs-4 text-right" style="border-bottom: 2px solid #333;">
	       				<span  id="net_amount">
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">Certified Correct:</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-xs-6">
	       				<span class="signatory_one font-weight-bold"></span> <br>
	       				<span class="position_one"></span>
	       			</div>
	       			<div class="col-xs-6">
	       				<span class="signatory_two font-weight-bold"></span> <br>
	       				<span class="position_two"></span>
	       			</div>
	       		</div>
	       </div>
	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	  // GENERATE MONTH
	  var months = {
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December'
	}


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	/* SIGNATORY */
	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

		$.ajax({
			url:base_url+module_prefix+module+'/getStepIncrements',
			data:{
				'id':_empid,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);
				arr = [];

				if(data.length !== 0){
					$.each(data,function(k,v){
						arr += '<option value="'+v.transaction_date+'">'+v.transaction_date+'</option>';
					})

					$('#select_month').html(arr).trigger('change');
				}

			}
		})

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	$.ajax({
		url:base_url+module_prefix+module+'/show',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
			'emp_type':emp_type,
			'emp_status':emp_status,
			'category':category,
			'searchby':searchby,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			firstname = data.employees.firstname;
			lastname = data.employees.lastname;
			middlename = data.employees.middlename;

			fullname = lastname+' '+firstname+' '+middlename

			oldBasicAmount = (data.old_basic_pay_amount) ? data.old_basic_pay_amount : 0;
			newBasicAmount = (data.new_basic_pay_amount) ? data.new_basic_pay_amount : 0;
			salaryAdjAmount = (data.salary_adjustment_amount) ? data.salary_adjustment_amount : 0;
			gsisContAmount = (data.gsis_cont_amount) ? data.gsis_cont_amount : 0;
			philhealthContAmount = (data.philhealth_cont_amount) ? data.philhealth_cont_amount : 0;
			pfContAmount = (data.provident_fund_amount) ? data.provident_fund_amount : 0;
			wTaxAmount = (data.wtax_amount) ? data.wtax_amount : 0;

			salaryAdjAmountTwo = (2.5/22 * parseFloat(salaryAdjAmount));

			totalDeductionAmount = parseFloat(gsisContAmount) + parseFloat(philhealthContAmount) + parseFloat(pfContAmount) + parseFloat(wTaxAmount);

			netAmount = parseFloat(salaryAdjAmountTwo) - parseFloat(totalDeductionAmount);

			old_basic_amount = (oldBasicAmount !== 0) ? commaSeparateNumber(parseFloat(oldBasicAmount).toFixed(2)) : '';
			new_basic_amount = (newBasicAmount !== 0) ? commaSeparateNumber(parseFloat(newBasicAmount).toFixed(2)) : '';
			salary_adj_amount = (salaryAdjAmount !== 0) ? commaSeparateNumber(parseFloat(salaryAdjAmount).toFixed(2)) : '';
			gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '';
			philhealth_cont_amount = (philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthContAmount).toFixed(2)) : '';
			pf_cont_amount = (pfContAmount !== 0) ? commaSeparateNumber(parseFloat(pfContAmount).toFixed(2)) : '';
			wtax_amount = (wTaxAmount !== 0) ? commaSeparateNumber(parseFloat(wTaxAmount).toFixed(2)) : '';

			total_deduction_amount = (totalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '';
			salary_adj_amount_two = (salaryAdjAmountTwo !== 0) ? commaSeparateNumber(parseFloat(salaryAdjAmountTwo).toFixed(2)) : '';
			net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';

			$('#old_basic_amount').text(old_basic_amount);
			$('#new_basic_amount').text(new_basic_amount);
			$('#salary_adj_amount').text(salary_adj_amount);
			$('#gsis_cont_amount').text(gsis_cont_amount);
			$('#philhealth_cont_amount').text(philhealth_cont_amount);
			$('#pf_cont_amount').text(pf_cont_amount);
			$('#wtax_amount').text(wtax_amount);
			$('#total_deduction_amount').text(total_deduction_amount);
			$('#salary_adj_amount_two').text(salary_adj_amount_two);
			$('#fullname').text(fullname);
			$('#net_amount').text(net_amount);

			dateSplitOne = data.date_from.split('-');
			dateSplitTwo = data.date_to.split('-');

			covered_period = months[parseInt(dateSplitTwo[1])]+' '+dateSplitOne[2]+' - '+dateSplitTwo[2]+' '+dateSplitTwo[0];
			$('#covered_period').text(covered_period);


			$('#btnModal').trigger('click');
		}
	})


});
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection