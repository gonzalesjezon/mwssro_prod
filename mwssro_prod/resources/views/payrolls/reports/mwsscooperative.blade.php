@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait2.css') }}">
<style type="text/css">

	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}

	.vertical-align-middle{
		vertical-align: middle !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				@include('payrolls.reports.includes._transmital-signatory')
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 960px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table table2" style="border: none !important;">
									<thead>
										<tr>
											<td colspan="5" style="border: none !important;">
												<div class="row">
							       			<div class="col-md-12 text-center" >
													@include('payrolls.reports.includes._header') 
													<span class="font-weight-bold">GENERAL PAYROLL</span>  <br>
													For the month of <span id="pay_period"></span>		<br>	<br>
													<span class="font-weight-bold">MWSS-RO Multi-Purpose Cooperative</span>	<br>
													Schedule of Remittance
							       			</div>
							       		</div>
											</td>
										</tr>
										<tr class="text-center" style="font-weight: bold;">
											<td style="width: 10px;"></td>
											<td style="width: 25em !important;">Name</td>
											<td style="width: 10em !important;" >Capital <br> Contribution</td>
											<td style="width: 14em !important;">Loan</td>
											<td style="width: 10em !important">Total</td>
										</tr>
									</thead>
									<tfoot style="border: none !important;">
										<tr>
											<td style="border: none;" colspan="5" nowrap>
												<div class="row text-center mb-8">
													<div class="col-md-4">Prepared by:</div>
													<div class="col-md-4">Checked & Verified:</div>
													<div class="col-md-4">Approved:</div>
												</div>
												<div class="row text-center">
													<div class="col-md-4">
														<span class="signatory_one font-weight-bold"></span> <br>
										    		<span class="position_one"></span>
													</div>
													<div class="col-md-4">
														<span class="signatory_two font-weight-bold"></span> <br>
										    		<span class="position_two"></span>
										    	</div>
													<div class="col-md-4">
														<span class="signatory_three font-weight-bold"></span> <br>
										    		<span class="position_three"></span>
													</div>
												</div>
											</td>
										</tr>
									</tfoot>
									 <tbody id="tbl_body">
									 </tbody>
								</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					if(data.length !== 0){

						arr = [];


						total_capital_amount = 0;
						total_loan_amount = 0;
						net_amount = 0
						$.each(data,function(k,v){
							capital_amount = 0;
							loan_amount = 0;

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees.middlename) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';

							fullname = lastname+' '+firstname+' '+middlename;

							capital_amount = (v.loan_transaction !== null) ? v.loan_transaction.amount : 0;
							total_capital_amount += parseFloat(capital_amount);

							loan_amount = (v.deduction_transaction !== null) ? v.deduction_transaction.amount : 0;
							total_loan_amount += parseFloat(loan_amount);

							total_amount = (parseFloat(loan_amount) + parseFloat(capital_amount));
							net_amount += parseFloat(total_amount);
							capital_amount = (capital_amount) ? commaSeparateNumber(parseFloat(capital_amount).toFixed(2)) : '';
							loan_amount = (loan_amount) ? commaSeparateNumber(parseFloat(loan_amount).toFixed(2)) : '';
							total_amount = (total_amount) ? commaSeparateNumber(parseFloat(total_amount).toFixed(2)) : '';
							arr += '<tr>';
							arr += '<td>'+(k+1)+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td class="text-right">'+loan_amount+'</td>';
							arr += '<td colspan="1" class="text-right">'+capital_amount+'</td>';
							arr += '<td class="text-right">'+total_amount+'</td>';
							arr += '</tr>';

						});
						total_capital_amount = (total_capital_amount) ? commaSeparateNumber(parseFloat(total_capital_amount).toFixed(2)) : '';
						total_loan_amount = (total_loan_amount) ? commaSeparateNumber(parseFloat(total_loan_amount).toFixed(2)) : '';
						net_amount = (net_amount) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';
						arr += '<tr>';
						arr += '<td></td>'
						arr += '<td><b>Total</b></td>'
						arr += '<td class="text-right"><b>'+total_loan_amount+'</b></td>'
						arr += '<td class="text-right"><b>'+total_capital_amount+'</b></td>'
						arr += '<td class="text-right"><b>'+net_amount+'</b></td>'
						arr += '</tr>';


						$('#tbl_body').html(arr);

						$('#pay_period').text(months[_Month]+' '+_Year)

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection