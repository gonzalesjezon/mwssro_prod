@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		
   				<table class="table" style="border: none !important;">
   					<thead style="border: none !important;">
   						<tr>
   							<td colspan="13" style="border:none !important;">
   								<div class="row">
				       			<div class="col-md-12 text-center">
				       				<span>
				       					LIST OF EMPLOYEES  DEDUCTED LOANS  <br>
				       					FOR THE MONTH OF <span class="covered_date"></span>
				       				</span>
				       			</div>
				       		</div>
   							</td>
   						</tr>
   						<tr class="text-center" style="font-weight: bold;">
   							<td rowspan="3">#</td>
   							<td rowspan="3">Name</td>
   							<td rowspan="3">Position</td>
   							<td colspan="9">Type of Loan</td>
   							<td rowspan="3">Total Loans</td>
   						</tr>
   						<tr class="text-center" style="font-weight: bold;">
   							<td colspan="4">GSIS</td>
   							<td colspan="2">Pagibig</td>
   							<td rowspan="2">LB MOBILE</td>
   							<td rowspan="2">MEMABAI LOAN</td>
   							<td rowspan="2">PROVIDENT LOAN</td>
   						</tr>
   						<tr class="text-center" style="font-weight: bold;">
   							<td>CONSO LOAN</td>
   							<td>POLICY LOAN</td>
   							<td>EMERG LOAN</td>
   							<td>EDUC LOAN</td>
   							<td>HDMF HOUSING</td>
   							<td>PIB MPL</td>
   						</tr>
   					</thead>
   					<tbody id="tbl_content"></tbody>
   				</table>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _payPeriod;
	var _semiPayPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'year':year,
				'month':month,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data !== null){
					arr = [];
					ctr = 0;

					netconsoLoanAmount  = 0;
					netpolicyLoanAmount = 0;
					netemergLoan 		= 0;
					neteducLoan 		= 0;
					nethdmfLoan 		= 0;
					netmplLoan 			= 0;
					netlbMobile 		= 0;
					netmembaiLoan 		= 0;
					netprovidentLoan 	= 0;
					netTotalLoans 		= 0;
					$.each(data,function(k,v){

						consoLoanAmount  = 0;
						policyLoanAmount = 0;
						emergLoan 		 = 0;
						educLoan 		 = 0;
						hdmfLoan 		 = 0;
						mplLoan 		 = 0;
						lbMobile 		 = 0;
						membaiLoan 		 = 0;
						providentLoan 	 = 0;
						totalLoans 		 = 0;

						var employee_number;
						var fullname;
						var position;

						employee_number = (v.employee_number !== null) ? v.employee_number : '';
						firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
						lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
						middlename 			= (v.employees) ? v.employees.middlename : '';
						middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
						fullname = lastname+' '+firstname+' '+middlename;

						position = (v.positions !== null) ? v.positions.Name : '';


						$.each(v.loaninfo_transaction,function(key,val){


							if(val.loans.code === 'CSL'){
								consoLoanAmount = val.amount;
							}

							if(val.loans.code === 'PL'){
								policyLoanAmount = val.amount;
							}

							if(val.loans.code === 'EL'){
								emergLoan = val.amount;
							}

							if(val.loans.code === 'EA'){
								educLoan = val.amount;
							}

							if(val.loans.code === 'HHL'){
								hdmfLoan = val.amount;
							}

							if(val.loans.code === 'MPL'){
								mplLoan = val.amount;
							}

							if(val.loans.code === 'LB'){
								lbMobile = val.amount;
							}

							if(val.loans.code === 'ML'){
								membaiLoan = val.amount;
							}

							if(val.loans.code === 'PFL'){
								providentLoan = val.amount;
							}

						totalLoans =	parseFloat(consoLoanAmount) + parseFloat(policyLoanAmount) + parseFloat(emergLoan) + parseFloat(educLoan) + parseFloat(hdmfLoan) + parseFloat(mplLoan) + parseFloat(lbMobile) + parseFloat(membaiLoan) + parseFloat(providentLoan);

						});

						netconsoLoanAmount += parseFloat(consoLoanAmount);
						netpolicyLoanAmount += parseFloat(policyLoanAmount);
						netemergLoan += parseFloat(emergLoan);
						neteducLoan += parseFloat(educLoan);
						nethdmfLoan += parseFloat(hdmfLoan);
						netmplLoan += parseFloat(mplLoan);
						netlbMobile += parseFloat(lbMobile);
						netmembaiLoan += parseFloat(membaiLoan);
						netprovidentLoan += parseFloat(providentLoan);
						netTotalLoans += parseFloat(totalLoans);

						conso_loan = (consoLoanAmount !== 0) ? commaSeparateNumber(parseFloat(consoLoanAmount).toFixed(2)) : '';
						provident_loan = (providentLoan !== 0) ? commaSeparateNumber(parseFloat(providentLoan).toFixed(2)) : '';
						membai_loan = (membaiLoan !== 0) ? commaSeparateNumber(parseFloat(membaiLoan).toFixed(2)) : '';
						lb_loan = (lbMobile !== 0) ? commaSeparateNumber(parseFloat(lbMobile).toFixed(2)) : '';
						mpl_loan = (mplLoan !== 0) ? commaSeparateNumber(parseFloat(mplLoan).toFixed(2)) : '';
						hdmf_loan = (hdmfLoan !== 0) ? commaSeparateNumber(parseFloat(hdmfLoan).toFixed(2)) : '';
						emerg_loan = (emergLoan !== 0) ? commaSeparateNumber(parseFloat(emergLoan).toFixed(2)) : '';
						ea_loan = (educLoan !== 0) ? commaSeparateNumber(parseFloat(educLoan).toFixed(2)) : '';
						policy_loan = (policyLoanAmount !== 0) ? commaSeparateNumber(parseFloat(policyLoanAmount).toFixed(2)) : '';
						total_loan = (totalLoans !== 0) ? commaSeparateNumber(parseFloat(totalLoans).toFixed(2)) : '';

						arr += '<tr class="text-right">';
						arr += '<td class="text-center">'+employee_number+'</td>';
						arr += '<td class="text-left" nowrap>'+fullname+'</td>';
						arr += '<td nowrap class="text-left">'+position+'</td>';
						arr += '<td class="text-right">'+conso_loan+'</td>'; // consoloan
						arr += '<td class="text-right">'+policy_loan+'</td>';
						arr += '<td class="text-right">'+emerg_loan+'</td>';
						arr += '<td class="text-right">'+ea_loan+'</td>';
						arr += '<td class="text-right">'+hdmf_loan+'</td>';
						arr += '<td class="text-right">'+mpl_loan+'</td>';
						arr += '<td class="text-right">'+lb_loan+'</td>';
						arr += '<td class="text-right">'+membai_loan+'</td>';
						arr += '<td class="text-right">'+provident_loan+'</td>';
						arr += '<td class="text-right">'+total_loan+'</td>';
						arr += '</tr>';
						ctr++;

					});
					ctr = 0;

					netconsoLoanAmount = (netconsoLoanAmount) ? commaSeparateNumber(parseFloat(netconsoLoanAmount).toFixed(2)) : '';
					netpolicyLoanAmount = (netpolicyLoanAmount) ? commaSeparateNumber(parseFloat(netpolicyLoanAmount).toFixed(2)) : '';
					netemergLoan = (netemergLoan) ? commaSeparateNumber(parseFloat(netemergLoan).toFixed(2)) : '';
					neteducLoan = (neteducLoan) ? commaSeparateNumber(parseFloat(neteducLoan).toFixed(2)) : '';
					nethdmfLoan = (nethdmfLoan) ? commaSeparateNumber(parseFloat(nethdmfLoan).toFixed(2)) : '';
					netmplLoan = (netmplLoan) ? commaSeparateNumber(parseFloat(netmplLoan).toFixed(2)) : '';
					netlbMobile = (netlbMobile) ? commaSeparateNumber(parseFloat(netlbMobile).toFixed(2)) : '';
					netmembaiLoan = (netmembaiLoan) ? commaSeparateNumber(parseFloat(netmembaiLoan).toFixed(2)) : '';
					netprovidentLoan = (netprovidentLoan) ? commaSeparateNumber(parseFloat(netprovidentLoan).toFixed(2)) : '';
					netTotalLoans = (netTotalLoans) ? commaSeparateNumber(parseFloat(netTotalLoans).toFixed(2)) : '';

					arr += '<tr class="text-right" style="font-weight:bold;">';
					arr += '<td>TOTAL</td>';
					arr += '<td class="text-left"></td>';
					arr += '<td class="text-center"></td>';
					arr += '<td class="text-right">'+netconsoLoanAmount+'</td>';
					arr += '<td class="text-right">'+netpolicyLoanAmount+'</td>';
					arr += '<td class="text-right">'+netemergLoan+'</td>';
					arr += '<td class="text-right">'+neteducLoan+'</td>';
					arr += '<td class="text-right">'+nethdmfLoan+'</td>';
					arr += '<td class="text-right">'+netmplLoan+'</td>';
					arr += '<td class="text-right">'+netlbMobile+'</td>';
					arr += '<td class="text-right">'+netmembaiLoan+'</td>';
					arr += '<td class="text-right">'+netprovidentLoan+'</td>';
					arr += '<td class="text-right">'+netTotalLoans+'</td>';

					arr += '</tr>';

					days = daysInMonth(_Month,_Year)

					if(_payPeriod == 'monthly'){
						_coveredPeriod = months[_Month]+' 1-'+days+', '+_Year;
					}else{
						switch(_semiPayPeriod){
							case 'firsthalf':
								_coveredPeriod = months[_Month]+' 1-15, '+_Year;
							break;
							default:
								_coveredPeriod =months[_Month]+' 16-'+days+', '+_Year;
							break;
						}
					}

					$('.covered_date').text(months[_Month]+' '+_Year);

					$('#tbl_content').html(arr);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}


});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
})
</script>
@endsection