@extends('layouts.app-monetizationreports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row pl-4 pr-4">
					<div class="col-md-12">
						<span><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row pl-4 pr-4 mb-2">
					<div class="col-md-6">
						<select class="form-control select2" name="created_at" id="created_at">
							<option value=""></option>
							@foreach($transaction as $key => $value)
							<option value="{{ $value->created_at->format('Y-m-d') }}">{{ $value->created_at->format('Y-m-d') }}</option>
							@endforeach
						</select>
					</div>
				</div>
				@include('payrolls.reports.includes._signatory-one')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
								<table class="table table2" style="border: none !important;">
								<thead style="border:none !important;">
									<tr>
										<td colspan="8" style="border:none !important;">
											<div class="row">
						       			<div class="col-md-12 text-center">
						       				<h4>GENERAL PAYROLL</h4>
						       			</div>
						       		</div>
						       		<div class="row pl-3 pr-3">
						       			<div class="col-md-12 text-left">
						       				<span style="font-weight: bold">OFFICE/DEPARTMENT: </span> <br>
						       				<span>Monetization of leave credits for CY <span id="month_year"></span></span>
						       			</div>
						       		</div>
										</td>
									</tr>
									<tr class="text-center">
										<td></td>
										<td>Name</td>
										<td>Designation</td>
										<td>SG</td>
										<td>Basic Salary</td>
										<td>No. of Days <br> Applied</td>
										<td>Amount</td>
										<td>Land Bank <br> Accnt. No</td>
									</tr>
								</thead>
								<tfoot style="border:none;">
									<tr>
										<td colspan="8" style="border:none;">
											<div class="row">
						       			<div class="col-md-12 text-center">
						       				<span>
						       					I hereby certify that this payroll is correct and that	<br>
												the claims herein stated are for services actually	<br>
												and duly rendered as authorized by management.		 <br><br>
						       				</span>

						       				<span class=" font-weight-bold signatory_one"></span> <br>
											     <span class="position_one"></span>
						       				<br>
						       			</div>
						       		</div>
						       		<div class="row" style="margin-top: 50px;">
						       			<div class="col-md-4" style="padding-left: 30px;">
						       				<span>
							       				Certified:  (1) as to accuracy of computation (2) accounting <br>
												codes and journal entries are proper; (3) properly entered in 	<br>
												the accounting records.
						       				</span>
						       			</div>
						       			<div class="col-md-4 text-center" style="padding-left: 20px;">
						       				<span>
						       					Approved for appropriation and funds being
												available:
						       				</span>
						       			</div>
						       			<div class="col-md-4 text-center" style="padding-left: 20px;">
						       				<span>
						       					<b>APPROVED:</b>
						       				</span>
						       			</div>
						       		</div>
						       		<div class="row" style="margin-top: 30px;">
						       			<div class="col-md-4 text-center">
						       				<span class=" font-weight-bold signatory_two"></span> <br>
											    <span class="position_two"></span>
											RO-18-______

						       			</div>
						       			<div class="col-md-4 text-center">
						       				<span class=" font-weight-bold signatory_three"></span> <br>
											    <span class="position_three"></span>

						       			</div>
						       			<div class="col-md-4 text-center">
						       				<span class=" font-weight-bold signatory_four"></span> <br>
											    <span class="position_four"></span>

						       			</div>
						       		</div>
										</td>
									</tr>
								</tfoot>
								 <tbody id="tbl_body">
								 </tbody>
								</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _createdAt;
	$('.select2').select2();

	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});

	$('#signatory_4').change(function(){
		signatory_four = $(this).find(':selected').text();
		position_four 	= $(this).find(':selected').data('position');

		$('.signatory_four').text(signatory_four);
		$('.position_four').text(position_four);
	});

	$(document).on('change','#created_at',function(){
		_createdAt = "";
		_createdAt = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})



	$(document).on('click','#preview',function(){
		if(!_createdAt){
			swal({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'created_at':_createdAt},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction !== 0){

						arr = [];

						sub_net_amount 	  = 0;
						$.each(data.transaction,function(k,v){
							basic_amount 	 	   = 0;

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							positions = (v.positions !== null) ? v.positions.Name : '';
							salarygrade = (v.salarygrade) ? v.salarygrade.Name : '';
							salary_amount = (v.transaction) ? v.transaction.total_basicpay_amount : 0;
							number_of_days = (v.number_of_days !== null) ? v.number_of_days : 0;
							net_amount = (v.net_amount !== null) ? v.net_amount : 0;
							atmNo = (v.employeeinfo.atm_no) ? v.employeeinfo.atm_no : '';

							sub_net_amount += parseFloat(net_amount);



							salary_amount = (salary_amount !== 0) ? commaSeparateNumber(parseFloat(salary_amount).toFixed(2)) : '';
							net_amount = (net_amount !== 0) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td>'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
							arr += '<td class="text-left">'+positions+'</td>';
							arr += '<td class="text-center">'+salarygrade+'</td>';
							arr += '<td class="text-right">'+salary_amount+'</td>';
							arr += '<td class="text-center">'+number_of_days+'</td>';
							arr += '<td class="text-right">'+net_amount+'</td>';
							arr += '<td class="text-right">'+atmNo+'</td>';
							arr += '</tr>';
						});

						sub_net_amount = (sub_net_amount !== 0) ? commaSeparateNumber(parseFloat(sub_net_amount).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold">';
						arr += '<td></td>';
						arr += '<td class="text-left">TOTAL</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-center"></td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+sub_net_amount+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';


						$('#tbl_body').html(arr);

						// days = daysInMonth(_monthNumber,_Year);

						// if(_payPeriod == 'monthly'){
						// 	_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						// }else{
						// 	switch(_semiPayPeriod){
						// 		case 'firsthalf':
						// 			_coveredPeriod = _Month+' 1-15, '+_Year;
						// 		break;
						// 		default:
						// 			_coveredPeriod =_Month+' 16-'+days+', '+_Year;
						// 		break;
						// 	}
						// }
						year = new Date();

						$('#month_year').text(year.getFullYear());

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection