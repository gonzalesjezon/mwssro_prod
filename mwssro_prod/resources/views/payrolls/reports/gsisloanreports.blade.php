@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		
   				<table class="table table2" style="border: none !important;">
   					<thead style="border: none !important;">
   						<tr>
   							<td style="border:none !important;" colspan="35">
   								<div class="row pl-3 pr-3">
				   					<div class="col-md-12 text-left">
					   					<span style="font-weight: bold;">Remitting Agency</span>	 <br>
					   					<span style="font-weight: bold;">Office Code</span> <br>
					   					<span style="font-weight: bold;">Due Month</span>	<span class="covered_date"></span>
				   					</div>
				       		</div>
   							</td>
   						</tr>
   						<tr class="text-center" style="font-weight: bold;">
   							<td>BPNO</td>
   							<td>LastName</td>
   							<td>FirstName</td>
   							<td>MI</td>
   							<td>PREFIX</td>
   							<td>APPELLATION</td>
   							<td>BirthDate</td>
   							<td>CRN</td>
   							<td>Basic Monthly Salary</td>
   							<td>Effectivity Date</td>
   							<td>PS</td>
   							<td>GS</td>
   							<td>EC</td>
   							<td>CONSOLOAN</td>
   							<td>ECARDPLUS</td>
   							<td>SALARY_LOAN</td>
   							<td>CASH_ADV</td>
   							<td>EMRGYLN</td>
   							<td>EDUC_ASST</td>
   							<td>ELA</td>
   							<td>SOS</td>
   							<td>PLREG</td>
   							<td>PLOPT</td>
   							<td>REL</td>
   							<td>LCH_DCS</td>
   							<td>STOCK_PURCHASE</td>
   							<td>OPT_LIFE</td>
   							<td>CEAP</td>
   							<td>EDU_CHILD</td>
   							<td>GENESIS</td>
   							<td>GENPLUS</td>
   							<td>GENFLEXI</td>
   							<td>GENSPCL</td>
   							<td>HELP</td>
   							<td>GFAL</td>
   						</tr>
   					</thead>
   					<tbody id="tbl_content"></tbody>
   				</table>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

$(document).on('click','#preview',function(){
	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month First!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				if(data !== null){
					arr = [];
					ctr = 0;

					netconsoLoanAmount  = 0;
					netpolicyLoanAmount = 0;
					netemergLoan 		= 0;
					neteducLoan 		= 0;
					nethdmfLoan 		= 0;
					netmplLoan 			= 0;
					netlbMobile 		= 0;
					netmembaiLoan 		= 0;
					netprovidentLoan 	= 0;
					netTotalLoans 		= 0;
					$.each(data,function(k,v){

						consoLoanAmount  = 0;
						policyLoanAmount = 0;
						emergLoan 		 = 0;
						educLoan 		 = 0;
						hdmfLoan 		 = 0;
						mplLoan 		 = 0;
						lbMobile 		 = 0;
						membaiLoan 		 = 0;
						providentLoan 	 = 0;
						ec_amount 		 = 100;

						bp_no = (v.employeeinfo.bp_no !== null) ? v.employeeinfo.bp_no : '';
						crn = (v.employeeinfo.crn !== null && v.employeeinfo.crn !== undefined) ? v.employeeinfo.crn : '';
						firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
						lastname 				= (v.employees.lastname) ? v.employees.lastname : '';
						middlename 			= (v.employees) ? v.employees.middlename : '';
						middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';

						basicAmount = (v.total_basicpay_amount) ? v.total_basicpay_amount : 0;
						effectivity_date = (v.salaryinfo) ? v.salaryinfo.salary_effectivity_date : 0;

						psShareAmount = (v.gsis_ee_share) ? v.gsis_ee_share : 0;
						erShareAmount = (v.gsis_er_share) ? v.gsis_er_share : 0;

						// LOANS


						if(v.loaninfo_transaction.length !== 0){
							$.each(v.loaninfo_transaction,function(key,val){
								if(val.loans){
									switch(val.loans.code){
										case 'CSL':
											consoLoanAmount = (val.amount) ? val.amount : 0;
										break;
										case 'PL':
											policyLoanAmount = (val.amount) ? val.amount : 0;
										break;
										case 'EA':
											educLoan = (val.amount) ? val.amount : 0;
										break;
										case 'EL':
											emergLoan = (val.amount) ? val.amount : 0;
										break;
									}
								}

							});
						}


						basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '0.00';
						ps_share_amount = (psShareAmount) ? commaSeparateNumber(parseFloat(psShareAmount).toFixed(2)) : '0.00';
						er_share_amount = (erShareAmount) ? commaSeparateNumber(parseFloat(erShareAmount).toFixed(2)) : '0.00';

						conso_loan_amount = (consoLoanAmount) ? commaSeparateNumber(parseFloat(consoLoanAmount).toFixed(2)) : '0.00';
						policy_loan_amount = (policyLoanAmount) ? commaSeparateNumber(parseFloat(policyLoanAmount).toFixed(2)) : '0.00';
						educ_loan_amount = (educLoan) ? commaSeparateNumber(parseFloat(educLoan).toFixed(2)) : '0.00';
						emerg_loan_amount = (emergLoan) ? commaSeparateNumber(parseFloat(emergLoan).toFixed(2)) : '0.00';

						arr += '<tr class="text-right">';
						arr += '<td>'+bp_no+'</td>';
						arr += '<td class="text-left" nowrap>'+lastname+'</td>';
						arr += '<td class="text-left" nowrap>'+firstname+'</td>';
						arr += '<td class="text-left" nowrap>'+middlename+'</td>';
						arr += '<td class="text-center"></td>'; // PREFIX
						arr += '<td class="text-center"></td>'; // APPELATION
						arr += '<td class="text-center"></td>'; // BIRTHDATE
						arr += '<td class="text-center">'+crn+'</td>'; // CRN
						arr += '<td class="text-right">'+basic_amount+'</td>';
						arr += '<td class="text-right">'+effectivity_date+'</td>';
						arr += '<td class="text-right">'+ps_share_amount+'</td>';
						arr += '<td class="text-right">'+er_share_amount+'</td>';
						arr += '<td class="text-right">'+ec_amount.toFixed(2)+'</td>';
						arr += '<td class="text-right">'+conso_loan_amount+'</td>'; // CONSO LOAN
						arr += '<td class="text-right"></td>'; // ECARD PLUS
						arr += '<td class="text-right"></td>'; // SALARY LOAN
						arr += '<td class="text-right"></td>'; // CASH ADVANCE
						arr += '<td class="text-right">'+emerg_loan_amount+'</td>'; // EMERGENCY LOAN
						arr += '<td class="text-right">'+educ_loan_amount+'</td>'; // EDUC ASSISTS LOAN
						arr += '<td class="text-right"></td>'; // ELA
						arr += '<td class="text-right"></td>'; // SOS
						arr += '<td class="text-right"></td>'; // PL REG
						arr += '<td class="text-right"></td>'; // PLOPT
						arr += '<td class="text-right"></td>'; // REL
						arr += '<td class="text-right"></td>'; // LCH_DCS
						arr += '<td class="text-right"></td>'; // STOCK PURCHASE
						arr += '<td class="text-right"></td>'; // OPT LIFE
						arr += '<td class="text-right"></td>'; // CEAP
						arr += '<td class="text-right"></td>'; // EDUC CHILD
						arr += '<td class="text-right"></td>'; // GENESIS
						arr += '<td class="text-right"></td>'; // GENSPCL
						arr += '<td class="text-right"></td>'; // HELP
						arr += '<td class="text-right"></td>'; // GFAL
						arr += '<td class="text-right"></td>'; // GFAL
						arr += '<td class="text-right"></td>'; // GFAL

						arr += '</tr>';
						ctr++;

					});
					ctr = 0;


					$('.covered_date').text(months[_Month]+' '+_Year);

					$('#tbl_content').html(arr);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}


});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
})
</script>
@endsection