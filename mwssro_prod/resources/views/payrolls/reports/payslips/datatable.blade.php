<div class="modal fade border0 in" id="prnModal" role="dialog" style="display: block;">
	 <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">
	    <div class="mypanel border0" style="height:100%;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	    </div>
	 </div>
</div>