@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printpayslip.css') }}">
<style type="text/css">
	.table2>tbody>tr>td, .table2>thead>tr>td{
		border: none !important;
		padding:2px !important;
	}

</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td >
				<div class="row" style="margin-left: 0;margin-right: 0;">
					<div class="col-md-12">
						<span>Select Employee</span>
						<div class="form-group">
							<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
								<option value=""></option>
								@foreach($employeeinfo as $value)
								<option value="{{ $value->id }}">{!! $value->getFullName() !!}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._months-year')

				<div class="row pl-4 pr-4">
					<div class="col-md-6">
						<span>Noted By</span>
						<div class="form-group">
							<select class="form-control font-style2 position" id="signatory_1" name="signatory_one">
								<option></option>
								@foreach($employees as $employee)
								<option data-position="{{ @$employee->employeeinformation->positions->Name }}" data-selected="position_one">{!! $employee->getFullName() !!}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" name="position_one" id="position_one" class="form-control">
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg mb-2 btn_save" style="display: inline-block;width: 30%;">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-xs btn-danger" id="preview">Preview</a>
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="padding:0px;width:60%;">
	    <div class="mypanel border0" style="height:100%;width: 100%;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body">
		       <div class="row">
		       		<div class="col-md-12">
		       			<div class="reports" id="reports" style="font-family: Book Antiqua !important;">
		       				<div class="report-header">
		       					<div class="col-md-12 text-center">
		       						<img src="{{url('/images/mwssreportheader.png')}}" style="height: 55px;margin-top: 20px;margin-bottom: 10px;">
		       						<i></i>
		       					</div>
		       				</div>
		       				<div style="border-bottom: 1px solid #e3e3e3; clear: both;"></div>
		       				<div class="reports-content">
		       					<div class="report-title">
		       						<h4 class="report-title">EMPLOYEE PAYSLIP</h4>
		       					</div>
		       					<br><br>
		       					<div class="report-name" style="font-size: 12px;">
		       						<div class="row">
			       						<div class="col-md-2">
			       							Employee Name
			       						</div>
			       						<div class="col-md-1" style="width: 25px;">:</div>
		       							<div class="col-md-4 text-left">
		       								<span id="employee_name"></span>
		       							</div>
		       							<div class="col-md-2">Employee No</div>
		       							<div class="col-md-1"  style="width: 25px;">:</div>
		       							<div class="col-md-2"><span id="employee_number"></span></div>
		       						</div>
		       						<div class="row">
		       							<div class="col-md-2">Designation</div>
		       							<div class="col-md-1"  style="width: 25px;">:</div>
		       							<div class="col-md-4"><span id="designation"></span></div>
		       						</div>
		       						<div class="row">
		       							<div class="col-md-2">Payroll Period</div>
		       							<div class="col-md-1"  style="width: 25px;">:</div>
		       							<div class="col-md-2"><span id="payroll_period"></span></div>
		       						</div>
		       					</div>
		       					<hr>
		       					<div class="row pl-4 pr-4">
		       						<div class="col-md-6">
		       							<table class="table table2 border-0 mb-6">
		       								<tbody class="border-0">
		       									<tr>
		       										<td colspan="2" class="font-weight-bold ">Earnings*</td>
		       									</tr>
		       									<tr><td style="height: 20px;"></td></tr>
		       									<tr>
		       										<td class="font-weight-bold">Basic Salary</td>
		       										<td class="text-right">
		       											<span class="clear" id="basic_salary"></span>
		       										</td>
		       									</tr>

		       									<tr>
		       										<td class="font-weight-bold">PERA</td>
		       										<td class="text-right">
		       											<span class="clear" id="pera_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-overtime" >
		       										<td class="font-weight-bold">Overtime</td>
		       										<td class="text-right">
		       											<span class="clear overtime_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-uniform" >
		       										<td class="font-weight-bold">Uniform Allowance</td>
		       										<td class="text-right">
		       											<span class="clear uniform_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-midyear" >
		       										<td class="font-weight-bold">Mid Year Bonus</td>
		       										<td class="text-right">
		       											<span class="clear midyear_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-yearend" >
		       										<td class="font-weight-bold">Year End Bonus</td>
		       										<td class="text-right">
		       											<span class="clear yearend_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-ra" >
		       										<td class="font-weight-bold">Representation Allowance</td>
		       										<td class="text-right">
		       											<span class="clear ra_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-ta" >
		       										<td class="font-weight-bold">Transportation Allowancee</td>
		       										<td class="text-right">
		       											<span class="clear ta_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-pbb" >
		       										<td class="font-weight-bold">Performance Base Bonus</td>
		       										<td class="text-right">
		       											<span class="clear pbb_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-pei" >
		       										<td class="font-weight-bold">PEI</td>
		       										<td class="text-right">
		       											<span class="clear pei_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-cna" >
		       										<td class="font-weight-bold">CNA</td>
		       										<td class="text-right">
		       											<span class="clear cna_amount"></span>
		       										</td>
		       									</tr>

		       									<tr class="d-none hide-all hide-honoraria" >
		       										<td class="font-weight-bold">Honoraria</td>
		       										<td class="text-right">
		       											<span class="clear honoraria_amount"></span>
		       										</td>
		       									</tr>

		       								</tbody>
		       							</table>

		       						</div>

		       						<div class="col-md-6">
		       							<table class="table table2 border-0">
		       								<thead class="border-0">
		       									<tr>
		       										<td colspan="2" class="font-weight-bold">Deductions</td>
		       									</tr>
		       									<tr><td style="height: 20px;"></td></tr>
		       									<tr>
		       										<td class="font-weight-bold">GSIS</td>
		       										<td class="text-right">
		       											<span class="clear gsis_share"></span>
		       										</td>
		       									</tr>

		       									<tr>
		       										<td class="font-weight-bold">Pagibig</td>
		       										<td class="text-right">
		       											<span class="clear pagibig_share"></span>
		       										</td>
		       									</tr>

		       									<tr>
		       										<td class="font-weight-bold">PHIC</td>
		       										<td class="text-right">
		       											<span class="clear phic_share"></span>
		       										</td>
		       									</tr>

		       									<tr>
		       										<td class="font-weight-bold">PAGIBIG II</td>
		       										<td class="text-right">
		       											<span class="clear mp2_amount"></span>
		       										</td>
		       									</tr>

		       									<tr>
		       										<td class="font-weight-bold">Tax</td>
		       										<td class="text-right">
		       											<span class="clear tax_amount"></span>
		       										</td>
		       									</tr>
		       								</thead>
		       								<tbody id="deduction_lists" class="border-0"></tbody>
		       							</table>
		       						</div>
		       					</div>

		       					<div class="row font-weight-bold pl-4 pr-4 mb-6" style="font-size: 12px;">
		       						<div class="col-md-3">
		       							<span>Total Earnings</span>
		       						</div>
		       						<div class="col-md-3 text-right">
		       							<span id="total_earning" class="clear"></span>
		       						</div>

		       						<div class="col-md-3">
		       							<span>Total Deductions</span>
		       						</div>
		       						<div class="col-md-3 text-right">
		       							<span id="total_deduction" class="clear"></span>
		       						</div>
		       					</div>

		       					<div class="row font-weight-bold pl-4 pr-4 mb-6" style="font-size: 12px;">
		       						<div class="col-md-6"></div>

		       						<div class="col-md-3">
		       							<span>Net Earnings</span>
		       						</div>
		       						<div class="col-md-3 text-right" style="border-bottom: 2px solid #333;">
		       							<span id="net_amount" class="clear"></span>
		       						</div>
		       					</div>

			       			
			       					
			       					<div class="row pl-4 pr-4">
			       						<div class="col-md-4 text-left"><span style="font-size: 12px;"><b>Noted By:</b></span></div>
			       					</div>
			       					<br>
			       					<br><br>
			       					<div class="row pl-4 pr-4">
			       						<div class="col-md-6 text-left" style="margin-left: 15px">
				       						<span class="font-weight-bold signatory_one"></span> <br>
				       						<span class="position_one"></span>
				       					</div>
			       					</div>
			       				</div>
		       				</div>
		       				<br>
		       				<div class="reports-footer">
		       					<div class="col-md-6">
		       						<div class="footer-left-message">
			       						<i>
			       							This is a computer generated document and does
											not require any signature if without alterations
			       						</i>

		       						</div>
		       					</div>
		       				</div>
		       			</div>
		       		</div>
		       </div>
	       </div>

	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	$.ajax({
		url:base_url+module_prefix+module+'/getPayslip',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			$('.hide-all').addClass('d-none');
			$('.clear').text('');
			body = [];

			if(data.plantilla == true){
				if(data.transaction === null){
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}else{

					lastname 		= (data.transaction.employees.lastname) ? data.transaction.employees.lastname + ',' : "";
					firstname 	= (data.transaction.employees.firstname) ? data.transaction.employees.firstname : "";
					middlename 	= (data.transaction.employees.middlename) ? data.transaction.employees.middlename : "";
					middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';

					fullname = lastname+' '+firstname+' '+middlename;

					position = (data.transaction.positions) ? data.transaction.positions.Name : '';
					empno 	 = (data.transaction.employees.employee_number) ? data.transaction.employees.employee_number : '';

					basicAmount = (data.transaction.total_basicpay_amount) ? data.transaction.total_basicpay_amount : 0;
					gsisEE 			= (data.transaction.gsis_ee_share) ? data.transaction.gsis_ee_share : 0;
					phicShare 	= (data.transaction.phic_share) ? data.transaction.phic_share : 0;
					pagibigShare 	= (data.transaction.pagibig_share) ? data.transaction.pagibig_share : 0;
					mp2Amount 	= (data.transaction.mp2_amount) ? data.transaction.mp2_amount : 0;
					mp1Amount 	= (data.transaction.mp1_amount) ? data.transaction.mp1_amount : 0;
					taxAmount 	= (data.transaction.tax_amount) ? data.transaction.tax_amount : 0;
					totalDeduction = (data.transaction.net_deduction) ? data.transaction.net_deduction : 0;

					pagibigShare = Number(pagibigShare) + Number(mp1Amount);

					peraAmount = 0;

					if(data.transaction.benefit_transactions.lenght != 0)
					{
						$.each(data.transaction.benefit_transactions,function(k,v){
							switch(v.benefits.code)
							{
								case 'PERA':
									peraAmount = v.amount;
									break;
								// case 'REP':
								// 	$('.hide-ra').removeClass('d-none');
								// 	raAmount = v.amount;
								// 	break;
								// case 'TRANSPO':
								// 	$('.hide-ta').removeClass('d-none');
								// 	taAmount = v.amount;
								// 	break;
							}
						});
					}

					raAmount 	 = 0;
					taAmount 	 = 0;
					if(data.transaction.rata.lenght != 0)
					{
						$.each(data.transaction.rata,function(k,v){
							raAmount += Number(v.representation_amount);
							taAmount += Number(v.transportation_amount);
						});
					}

					if(raAmount)
					{
						$('.hide-ra').removeClass('d-none');
					}

					if(taAmount)
					{
						$('.hide-ta').removeClass('d-none');
					}

					
					/* DEDUCTIONS*/					
					if(data.transaction.deductioninfo_transaction.lenght != 0)
					{
						$.each(data.transaction.deductioninfo_transaction,function(k,v){
							body += '<tr >';
							body +=	'<td class="font-weight-bold">'+v.deductions.name+'</td>';
							body += '<td class="text-right">'+commaSeparateNumber(Number(v.amount).toFixed(2))+'</td>';
							body += '</tr>';
						});
					}

					/* LOANS*/
					if(data.transaction.loaninfo_transaction.lenght != 0)
					{
						$.each(data.transaction.loaninfo_transaction,function(k,v){
							body += '<tr >';
							body +=	'<td class="font-weight-bold">'+v.loans.name+'</td>';
							body += '<td class="text-right">'+commaSeparateNumber(Number(v.amount).toFixed(2))+'</td>';
							body += '</tr>';
						});
					}


					midYear 				= 0;
					yearEnd 				= 0;
					peiAmount 			= 0;
					pbbAmount 			= 0;
					honorariaAmount = 0;
					cnaAmount 			= 0;
					if(data.transaction.special_payroll.lenght != 0)
					{
						$.each(data.transaction.special_payroll,function(k,v){
							switch(v.status){
								case 'midyearbonus':
									$('.hide-midyear').removeClass('d-none');
									midYear = v.amount;
									break;
								case 'yearendbonus':
									$('.hide-yearend').removeClass('d-none');
									yearEnd = v.amount;
									break;
								case 'pei':
									$('.hide-pei').removeClass('d-none');
									peiAmount = v.amount;
									cnaAmount = (v.cna_amount) ? v.cna_amount : '';
									if(cnaAmount){
										$('.hide-cna').removeClass('d-none');
									}
									break;
								case 'pbb':
									$('.hide-pbb').removeClass('d-none');
									pbbAmount = v.amount;
									break;
								case 'honoraria':
									$('.hide-honoraria').removeClass('d-none');
									honorariaAmount = v.amount;
									break;
							}
						});
					}

					$("#deduction_lists").html(body);

					netEarnings = Number(basicAmount) + Number(peraAmount) + Number(midYear) + Number(yearEnd) + Number(peiAmount) + Number(pbbAmount) + Number(honorariaAmount) + Number(cnaAmount);

					netAmount = Number(netEarnings) - Number(totalDeduction);


					basic_salary 	= (basicAmount) ? commaSeparateNumber(Number(basicAmount).toFixed(2)) : '0.00';
					pera_amount 	= (peraAmount) ? commaSeparateNumber(Number(peraAmount).toFixed(2)) : '0.00';
					ra_amount 		= (raAmount) ? commaSeparateNumber(Number(raAmount).toFixed(2)) : '0.00';
					ta_amount 		= (taAmount) ? commaSeparateNumber(Number(taAmount).toFixed(2)) : '0.00';
					midyear_amount = (midYear) ? commaSeparateNumber(Number(midYear).toFixed(2)) : '0.00';
					yearend_amount = (yearEnd) ? commaSeparateNumber(Number(yearEnd).toFixed(2)) : '0.00';
					pei_amount = (peiAmount) ? commaSeparateNumber(Number(peiAmount).toFixed(2)) : '0.00';
					cna_amount = (cnaAmount) ? commaSeparateNumber(Number(cnaAmount).toFixed(2)) : '0.00';
					pbb_amount = (pbbAmount) ? commaSeparateNumber(Number(pbbAmount).toFixed(2)) : '0.00';
					honoraria_amount = (honorariaAmount) ? commaSeparateNumber(Number(honorariaAmount).toFixed(2)) : '0.00';

					total_earning = (netEarnings) ? commaSeparateNumber(Number(netEarnings).toFixed(2)) : '0.00';

					gsis_share = (gsisEE) ? commaSeparateNumber(Number(gsisEE).toFixed(2)) : '0.00';
					phic_share = (phicShare) ? commaSeparateNumber(Number(phicShare).toFixed(2)) : '0.00';
					pagibig_share = (pagibigShare) ? commaSeparateNumber(Number(pagibigShare).toFixed(2)) : '0.00';
					mp2_amount = (mp2Amount) ? commaSeparateNumber(Number(mp2Amount).toFixed(2)) : '0.00';
					tax_amount = (taxAmount) ? commaSeparateNumber(Number(taxAmount).toFixed(2)) : '0.00';
					total_deduction = (totalDeduction) ? commaSeparateNumber(Number(totalDeduction).toFixed(2)) : '0.00';
					net_amount = (netAmount) ? commaSeparateNumber(Number(netAmount).toFixed(2)) : '0.00';

					$('#employee_name').text(fullname);
					$('#employee_number').text(empno);
					$('#designation').text(position);

					/* EARNINGS*/
					$('#basic_salary').text(basic_salary);
					$('#pera_amount').text(pera_amount);
					$('.ra_amount').text(ra_amount);
					$('.ta_amount').text(ta_amount);

					$('.midyear_amount').text(midyear_amount);
					$('.yearend_amount').text(yearend_amount);
					$('.pei_amount').text(pei_amount);
					$('.cna_amount').text(cna_amount);
					$('.pbb_amount').text(pbb_amount);
					$('.honoraria_amount').text(honoraria_amount);

					$('#total_earning').text(total_earning);

					// Deductions

					$('.gsis_share').text(gsis_share);
					$('.phic_share').text(phic_share);
					$('.pagibig_share').text(pagibig_share);
					$('.mp2_amount').text(mp2_amount);
					$('.tax_amount').text(tax_amount);

					$('#total_deduction').text(total_deduction);
					$('#net_amount').text(net_amount);


					$('#payroll_period').text(months[_Month]+' '+_Year);
					$('#btnModal').trigger('click');

					return false;
				}

			}else{
				if(data.nonplantilla === null){
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}else{
					basic_salary = (data.nonplantilla.total_basicpay_amount) ? data.nonplantilla.total_basicpay_amount : 0.00;
					$('#basic_salary').text(commaSeparateNumber(parseFloat(basic_salary).toFixed(2)));

					position = (data.nonplantilla.positions !== null) ? data.nonplantilla.positions.name : '';
					$('#designation').text(position);

					month = data.nonplantilla.month;
					$('.month').text(month);

					year = data.nonplantilla.year;
					$('.year').text(year);

					lastname 		= (data.nonplantilla.employees.lastname) ? data.nonplantilla.employees.lastname : "";
					firstname 	= (data.nonplantilla.employees.firstname) ? data.nonplantilla.employees.firstname : "";
					middlename 	= (data.nonplantilla.employees.middlename) ? data.nonplantilla.employees.middlename : "";

					fullname = lastname+' '+firstname+' '+middlename;
					$('#employee_name').text(fullname);

					empno = data.employeeinfo.employees.employee_number;
					$('#employee_number').text(empno);

					// office = data.transaction.offices.name;
					// $('#office').text(office);

					// position = (data.transaction.positionitems.position_id !== null) ? data.transaction.positionitems.positions.name : '';
					// $('#position').text(position);

					if(data.overtime !== null){
						overtime = (data.overtime.total_overtime_amount !== null) ? data.overtime.total_overtime_amount : 0;
					}else{
						overtime = 0;
					}

					totalEarnings = (parseFloat(basic_salary) + parseFloat(overtime));
					overtime = (overtime !== 0) ? commaSeparateNumber(parseFloat(overtime).toFixed(2)) : '0.00';
					$('#overtime_amount').text(overtime);
					$('#total_earning_amount').text(commaSeparateNumber(parseFloat(totalEarnings).toFixed(2)));

					taxamount_one = (data.nonplantilla.tax_rate_amount_one) ? data.nonplantilla.tax_rate_amount_one : 0;
					taxamount_two = (data.nonplantilla.tax_rate_amount_two) ? data.nonplantilla.tax_rate_amount_two : 0;

					taxamount = parseFloat(taxamount_one) + parseFloat(taxamount_two);
					$('#wtax_amount').text(commaSeparateNumber(parseFloat(taxamount).toFixed(2)));

					total_deduction_amount = taxamount;

					$('#total_deduction_amount').text(commaSeparateNumber(parseFloat(total_deduction_amount).toFixed(2)));

					netpay = (parseFloat(totalEarnings) - parseFloat(total_deduction_amount));
					$('#net_earning_amount').text(commaSeparateNumber(parseFloat(netpay).toFixed(2)));

					$('#payroll_period').text(months[_Month]+' '+_Year);

					$('#btnModal').trigger('click');
				}

			}



		}
	})


});
$('#print').on('click',function(){
	$('#reports').printThis();
});

	/*VIEW POSTED*/
$(document).on('click','.view',function(){
	
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		_empid = $(this).data('empid');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)

		$('#preview').trigger('click');
	});



/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

$('.position').change(function(){
	name = $(this).find(':selected').data('selected');
	value = $(this).find(':selected').data('position');
	
	$('#'+name).val(value)
});
})
</script>
@endsection