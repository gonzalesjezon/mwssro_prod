@extends('layouts.app-nonplantillareports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	.p-print{
		font-size: 12px !important;
	    color: #333 !important;
	    line-height: 0.6em !important;
	}
	.table>thead>tr>td{
		vertical-align: middle !important;
		padding: 4px !important;
	}

	.table>tbody>tr>td{
		padding: 2px !important;
	}

	@media print{
		.table>thead>tr>td{
			vertical-align: middle !important;
			padding: 4px !important;
		}

		.table>tbody>tr>td{
			padding: 2px !important;
		}
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row pl-4 pr-4">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select class="form-control font-style2" id="semi_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._signatory-one')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg mb-2 btn_save" style="display: inline-block;width: 30%;">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div class="container-fluid mt-8" id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
									<table class="table" style="border: none;">
										 <thead >
										 	<tr>
										 		<td colspan="7" style="border-left: none;border-right: none;border-top: none;">
										 			<div class="row pl-2 pr-2">
									       		<div class="col-md-6">
								       				<h5><b>{!! $title !!}</b></h5>
								       				<h6><b>Payment of compensation for the period of </b> <span id="month_year"></span></h6>
								       			</div>
									       	</div>
										 		</td>
										 	</tr>
											 <tr class="text-center">
											 	<td>#</td>
											 	<td>Name</td>
											 	<td style="width: 130px;">Monthly  Compensation</td>
											 	<td style="width: 130px;">Compensation for the period</td>
											 	<td>LESS <br> Deduction</td>
											 	<td>NET</td>
											 	<td>Land Bank Account No.</td>
											 </tr>
										 </thead>
										<tfoot style="border: none;">
										 	<tr>
										 		<td colspan="7" style="border: none">
										 			<div class="row">
									       			<div class="col-md-12 text-center">
									       				<span>
									       					I hereby certify that this payroll is correct and that	<br>
															the claims herein stated are for services actually	<br>
															and duly rendered as authorized by management.
									       				</span>
									       				<br><br><br>
									       				<span class=" font-weight-bold signatory_one"></span> <br>
									       				<span class="position_one"></span>
									       				<br>
									       			</div>
									       		</div>
									       		<div class="row" style="margin-top: 50px;">
									       			<div class="col-md-4" style="padding-left: 30px;">
									       				<span>
										       				Certified:  (1) as to accuracy of computation (2) accounting <br>
															codes and journal entries are proper; (3) properly entered in 	<br>
															the accounting records.
									       				</span>
									       			</div>
									       			<div class="col-md-4 text-center" style="padding-left: 20px;">
									       				<span>
									       					Approved for appropriation and funds being
															available:
									       				</span>
									       			</div>
									       			<div class="col-md-4 text-center" style="padding-left: 20px;">
									       				<span>
									       					<b>APPROVED:</b>
									       				</span>
									       			</div>
									       		</div>
									       		<div class="row" style="margin-top: 30px;">
									       			<div class="col-md-4 text-center">
									       				<p class="p-print font-weight-bold signatory_two"></p>
													    	<p class="p-print position_two"></p>
													    	<p class="p-print" >RO-18-______</p>

									       			</div>
									       			<div class="col-md-4 text-center">
									       				<p class="p-print font-weight-bold signatory_three"></p>
								      					<p class="p-print position_three"></p>

									       			</div>
									       			<div class="col-md-4 text-center">
									       				<p class="p-print font-weight-bold signatory_four"></p>
								               	<p class="p-print position_four"></p>

									       			</div>
									       		</div>
										 		</td>
										 	</tr>
										 </tfoot>
										 <tbody id="tbl_body">
										 </tbody>
									</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});

	$('#signatory_4').change(function(){
		signatory_four = $(this).find(':selected').text();
		position_four 	= $(this).find(':selected').data('position');

		$('.signatory_four').text(signatory_four);
		$('.position_four').text(position_four);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	$(document).on('change','#select_pay_period',function(){
		_payPeriod = $(this).find(':selected').val()
		$('#pay_period').text(_payPeriod);
	});

	var _semiPayPeriod = "";
	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getGeneralPayrollReport',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':$('#semi_pay_period :selected').val()
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						arr = [];
						subMonthlyComp 		= 0;
						subHalfMonthlyComp 	= 0;
						subLwopAmount 		= 0;
						subCompAmount 		= 0;
						subTaxOne 			= 0;
						subTaxTwo 			= 0;
						subTotalTax 	 	= 0;
						subTotalNet 		= 0;
						$.each(data.transaction,function(k,v){

							lastname   = (v.employees.lastname) ? v.employees.lastname+',' : '';
							firstname  = (v.employees.firstname) ? v.employees.firstname : '';
							middlename = (v.employees.middlename) ? v.employees.middlename : '';
							middlename 	= (middlename) ? middlename.substring(-1,1) + '.' : '';
							fullname   =  lastname+' '+firstname+' '+middlename;

							atmNo = (v.employeeinfo.atm_no !== null) ? v.employeeinfo.atm_no : '';
							monthlyComp    =  (v.monthly_rate_amount) ? v.monthly_rate_amount : 0;
							halfMonthyComp =  (v.gross_pay) ? v.gross_pay : 0;
							netPay = (v.net_pay) ? v.net_pay : 0;

							absence 	= (v.total_absences_amount !== 0) ? v.total_absences_amount : 0;
							undertime 	= (v.total_undertime_amount !== 0) ? v.total_undertime_amount : 0;
							tardiness 	= (v.total_tardiness_amount !== 0) ? v.total_tardiness_amount : 0;
							lwopAmount 	= (Number(absence) + Number(undertime) + Number(tardiness));

							taxRateOne   = (v.employeeinfo.taxpolicy_id) ? v.employeeinfo.tax_policy_one.job_grade_rate : 0;
							taxAmountTwo = (v.tax_rate_amount_two) ? Number(v.tax_rate_amount_two) : 0;
							taxAmountOne = (v.tax_rate_amount_one) ? Number(v.tax_rate_amount_one) : 0;
							phicAmount   = (v.phic_amount) ? Number(v.phic_amount) : 0;


							compAmount 	   = (Number(halfMonthyComp) - Number(lwopAmount));
							totalTaxAmount = Number(taxAmountOne + Number(taxAmountTwo) + Number(phicAmount));
							// totalNet 	   	 = Number(halfMonthyComp) - Number(totalTaxAmount);
							totalNet = netPay;

							subMonthlyComp += Number(monthlyComp);
							subHalfMonthlyComp += Number(halfMonthyComp);
							subLwopAmount += Number(lwopAmount);
							subCompAmount += Number(compAmount);
							subTaxOne += Number(taxAmountOne);
							subTaxTwo += Number(taxAmountTwo);
							subTotalTax += Number(totalTaxAmount)
							subTotalNet += Number(totalNet);

							// 3 % percent tax computation

							monthly_comp = (monthlyComp) ? commaSeparateNumber(Number(monthlyComp).toFixed(2)) : '0.00';
							half_monthly_comp = (halfMonthyComp) ? commaSeparateNumber(Number(halfMonthyComp).toFixed(2)) : '0.00';
							lwop_amount = (lwopAmount) ? commaSeparateNumber(Number(lwopAmount).toFixed(2)) : '0.00';
							compensation_amount = (compAmount) ? commaSeparateNumber(Number(compAmount).toFixed(2)) : '0.00';
							tax_one = (taxAmountOne) ? commaSeparateNumber(Number(taxAmountOne).toFixed(2)) : '0.00';
							tax_two = (taxAmountTwo) ? commaSeparateNumber(Number(taxAmountTwo).toFixed(2)) : '0.00';
							total_tax = (totalTaxAmount) ? commaSeparateNumber(Number(totalTaxAmount).toFixed(2)) : '0.00';
							total_net = (totalNet) ? commaSeparateNumber(Number(totalNet).toFixed(2)) : '0.00';

							arr += '<tr>';
							arr += '<td>'+(k+1)+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td class="text-right">'+monthly_comp+'</td>';
							arr += '<td class="text-right">'+compensation_amount+'</td>';
							arr += '<td class="text-right">'+total_tax+'</td>';
							arr += '<td class="text-right">'+total_net+'</td>';
							arr += '<td class="text-center">'+atmNo+'</td>';

							arr += '</tr>';

						});

						sub_monthly_comp = (subMonthlyComp) ? commaSeparateNumber(Number(subMonthlyComp).toFixed(2))  : '0.00';
						sub_halfmonthly_comp = (subHalfMonthlyComp) ? commaSeparateNumber(Number(subHalfMonthlyComp).toFixed(2))  : '0.00';
						sub_lwop_amount = (subLwopAmount) ? commaSeparateNumber(Number(subLwopAmount).toFixed(2))  : '0.00';
						sub_comp_amount = (subCompAmount) ? commaSeparateNumber(Number(subCompAmount).toFixed(2))  : '0.00';
						sub_tax_one = (subTaxOne) ? commaSeparateNumber(Number(subTaxOne).toFixed(2))  : '0.00';
						sub_tax_two = (subTaxTwo) ? commaSeparateNumber(Number(subTaxTwo).toFixed(2))  : '0.00';
						sub_total_tax = (subTotalTax) ? commaSeparateNumber(Number(subTotalTax).toFixed(2))  : '0.00';
						sub_total_net = (subTotalNet) ? commaSeparateNumber(Number(subTotalNet).toFixed(2))  : '0.00';

						arr += '<tr class="font-weight-bold">';
						arr += '<td></td>';
						arr += '<td >TOTAL</td>';
						arr += '<td class="text-right">'+sub_monthly_comp+'</td>';
						arr += '<td class="text-right">'+sub_comp_amount+'</td>';
						arr += '<td class="text-right">'+sub_total_tax+'</td>';
						arr += '<td class="text-right">'+sub_total_net+'</td>';
						arr += '<td></td>';

						arr += '</tr>';

						$('#tbl_body').html(arr);

						_coveredPeriod = data.From+' to '+data.To+', '+_Year;

						$('#month_year').text(_coveredPeriod);
						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$('#print').on('click',function(){
	$('#reports').printThis();
});

/*VIEW POSTED*/
$(document).on('click','.view',function(){
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)

		$('#semi_pay_period').val(_semiPayPeriod)

		$('#preview').trigger('click');
	});


/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

$('.position').change(function(){
	name = $(this).find(':selected').data('selected');
	value = $(this).find(':selected').data('position');
	
	$('#'+name).val(value)
});

})
</script>
@endsection