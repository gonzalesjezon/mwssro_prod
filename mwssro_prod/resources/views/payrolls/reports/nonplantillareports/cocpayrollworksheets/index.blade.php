@extends('layouts.app-nonplantillareports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	.table>thead>tr>td{
		vertical-align: middle !important;
		padding: 4px !important;
	}

	.table>tbody>tr>td{
		padding: 2px !important;
	}

	@media print{
		.table>thead>tr>td{
			vertical-align: middle !important;
			padding: 4px !important;
		}

		.table>tbody>tr>td{
			padding: 2px !important;
		}
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row pr-4 pl-4">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select class="form-control font-style2" id="semi_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._transmital-signatory')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg mb-2 btn_save" style="display: inline-block;width: 30%;">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px !important;margin: auto !important;">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<h5><b>PAYROLL WORKSHEET</b></h5>
	       				<h6><b>Payment of compensation for the period of </b> <span id="month_year"></span></h6>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table">
							<thead class="text-center" style="font-weight: bold;">
								 <tr>
								 	<td rowspan="2"style="line-height: 20px;">#</td>
								 	<td rowspan="2"style="line-height: 20px;">Name</td>
								 	<td rowspan="2"style="line-height: 20px;">Monthly Compensation</td>
								 	<td rowspan="2"style="line-height: 20px;">1/2 Monthly Compensation</td>
								 	<td rowspan="2"style="line-height: 20px;">Less (Undertime/Absences)</td>
								 	<td rowspan="2"style="line-height: 20px;">Compensation for the period</td>
								 	<td colspan="2" >LESS ITW</td>
								 	<td rowspan="2"style="line-height: 20px;">Philhealth</td>
								 	<td rowspan="2"style="line-height: 20px; width: 70px;">Total Deduction</td>
								 	<td rowspan="2"style="line-height: 20px;">NET</td>
								 	<td rowspan="2"style="line-height: 20px;">Land Bank Account No.</td>
								 </tr>
								 <tr>
								 	<td><span id="percent_one">412-3 (5/10%)</span></td>
								 	<td><span id="percent_two">412-5(3%)</span></td>
								 </tr>
							</thead>
							<tfoot>
							 	<tr class="font-weight-bold">
							 		<td colspan="4"  style="border: none">Prepared By</td>
							 		<td colspan="4"  style="border: none">Verified By</td>
							 		<td colspan="4"  style="border: none">Certified Correct</td>
							 	</tr>
							 	<tr>
							 		<td colspan="12" style="height: 50px;border: none;">&nbsp;</td>
							 	</tr>
							 	<tr>
							 		<td class="text-left" colspan="4" style="border: none">
							 			<span class="font-weight-bold signatory_one"></span> <br>
							 			<span class="position_one"></span>
							 		</td>
							 		<td class="text-left" colspan="4"  style="border: none">
							 			<span class="font-weight-bold signatory_two"></span> <br>
							 			<span class="position_two"></span>
							 		</td>
							 		<td class="text-left" colspan="4"  style="border: none">
							 			<span class="font-weight-bold signatory_three"></span> <br>
							 			<span class="position_three"></span>
							 		</td>
							 	</tr>
							</tfoot>
							<tbody id="tbl_body">
							</tbody>
					</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();


	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getPayrollWorksheetReport',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':$('#semi_pay_period :selected').val()
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.length !== 0){
						arr = [];
						subMonthlyComp 		= 0;
						subHalfMonthlyComp 	= 0;
						subLwopAmount 		= 0;
						subCompAmount 		= 0;
						subTaxOne 			= 0;
						subTaxTwo 			= 0;
						subPhic 			= 0;
						subTotalDeduction 	= 0;
						subTotalNet 		= 0;

						$.each(data.transaction,function(k,v){
							lastname   = (v.employees.lastname) ? v.employees.lastname+',' : '';
							firstname  = (v.employees.firstname) ? v.employees.firstname : '';
							middlename = (v.employees.middlename) ? v.employees.middlename : '';
							middlename 	= (middlename) ? middlename.substring(-1,1) + '.' : '';
							fullname   =  lastname+' '+firstname+' '+middlename;

							atmNo = (v.employeeinfo.atm_no !== null) ? v.employeeinfo.atm_no : '';
							monthlyComp    =  (v.monthly_rate_amount) ? v.monthly_rate_amount : 0;
							halfMonthyComp =  Number(monthlyComp) / 2;
							halfNet = (v.net_pay) ? v.net_pay : 0;

							absence 	= (v.total_absences_amount !== 0) ? v.total_absences_amount : 0;
							undertime 	= (v.total_undertime_amount !== 0) ? v.total_undertime_amount : 0;
							tardiness 	= (v.total_tardiness_amount !== 0) ? v.total_tardiness_amount : 0;
							lwopAmount 	= (Number(absence) + Number(undertime) + Number(tardiness));


							taxRateOne   = (v.employeeinfo.taxpolicy_id) ? v.employeeinfo.tax_policy_one.job_grade_rate : 0;
							taxAmountTwo = (v.tax_rate_amount_two) ? Number(v.tax_rate_amount_two)  : 0;
							taxAmountOne = (v.tax_rate_amount_one) ? Number(v.tax_rate_amount_one)  : 0;
							phicAmount = (v.phic_amount) ? v.phic_amount : 0;

							compAmount 	   = (Number(halfMonthyComp) - Number(lwopAmount));
							totalDeduction = Number(taxAmountOne) + Number(taxAmountTwo) + Number(phicAmount);
							// totalNet 	   = Number(compAmount) - Number(totalDeduction);
							totalNet = halfNet;

							subMonthlyComp += Number(monthlyComp);
							subHalfMonthlyComp += Number(halfMonthyComp);
							subLwopAmount += Number(lwopAmount);
							subCompAmount += Number(compAmount);
							subTaxOne += Number(taxAmountOne);
							subTaxTwo += Number(taxAmountTwo);
							subPhic += Number(phicAmount);
							subTotalDeduction += Number(totalDeduction)
							subTotalNet += Number(totalNet);

							// 3 % percent tax computation

							monthly_comp = (monthlyComp) ? commaSeparateNumber(Number(monthlyComp).toFixed(2)) : '0.00';
							half_monthly_comp = (halfMonthyComp) ? commaSeparateNumber(Number(halfMonthyComp).toFixed(2)) : '0.00';
							lwop_amount = (lwopAmount) ? commaSeparateNumber(Number(lwopAmount).toFixed(2)) : '0.00';
							compensation_amount = (compAmount) ? commaSeparateNumber(Number(compAmount).toFixed(2)) : '0.00';
							tax_one = (taxAmountOne) ? commaSeparateNumber(Number(taxAmountOne).toFixed(2)) : '0.00';
							tax_two = (taxAmountTwo) ? commaSeparateNumber(Number(taxAmountTwo).toFixed(2)) : '0.00';
							phic_amount = (phicAmount) ? commaSeparateNumber(Number(phicAmount).toFixed(2)) : '0.00';
							total_deduction = (totalDeduction) ? commaSeparateNumber(Number(totalDeduction).toFixed(2)) : '0.00';
							total_net = (totalNet) ? commaSeparateNumber(Number(totalNet).toFixed(2)) : '0.00';

							arr += '<tr>';
							arr += '<td>'+(k+1)+'</td>';
							arr += '<td nowrap>'+fullname+'</td>';
							arr += '<td class="text-right">'+monthly_comp+'</td>';
							arr += '<td class="text-right">'+half_monthly_comp+'</td>';
							arr += '<td class="text-right">'+lwop_amount+'</td>';
							arr += '<td class="text-right">'+compensation_amount+'</td>';
							arr += '<td class="text-right">'+tax_two+'</td>';
							arr += '<td class="text-right">'+tax_one+'</td>';
							arr += '<td class="text-right">'+phic_amount+'</td>';
							arr += '<td class="text-right">'+total_deduction+'</td>';
							arr += '<td class="text-right">'+total_net+'</td>';
							arr += '<td class="text-center">'+atmNo+'</td>';

							arr += '</tr>';

						});

						sub_monthly_comp = (subMonthlyComp) ? commaSeparateNumber(Number(subMonthlyComp).toFixed(2))  : '0.00';
						sub_halfmonthly_comp = (subHalfMonthlyComp) ? commaSeparateNumber(Number(subHalfMonthlyComp).toFixed(2))  : '0.00';
						sub_lwop_amount = (subLwopAmount) ? commaSeparateNumber(Number(subLwopAmount).toFixed(2))  : '0.00';
						sub_comp_amount = (subCompAmount) ? commaSeparateNumber(Number(subCompAmount).toFixed(2))  : '0.00';
						sub_tax_one = (subTaxOne) ? commaSeparateNumber(Number(subTaxOne).toFixed(2))  : '0.00';
						sub_tax_two = (subTaxTwo) ? commaSeparateNumber(Number(subTaxTwo).toFixed(2))  : '0.00';
						sub_phic = (subPhic) ? commaSeparateNumber(Number(subPhic).toFixed(2))  : '0.00';
						sub_total_deduction = (subTotalDeduction) ? commaSeparateNumber(Number(subTotalDeduction).toFixed(2))  : '0.00';
						sub_total_net = (subTotalNet) ? commaSeparateNumber(Number(subTotalNet).toFixed(2))  : '0.00';

						arr += '<tr class="font-weight-bold">';
						arr += '<td></td>';
						arr += '<td >TOTAL</td>';
						arr += '<td class="text-right">'+sub_monthly_comp+'</td>';
						arr += '<td class="text-right">'+sub_halfmonthly_comp+'</td>';
						arr += '<td class="text-right">'+sub_lwop_amount+'</td>';
						arr += '<td class="text-right">'+sub_comp_amount+'</td>';
						arr += '<td class="text-right">'+sub_tax_two+'</td>';
						arr += '<td class="text-right">'+sub_tax_one+'</td>';
						arr += '<td class="text-right">'+sub_phic+'</td>';
						arr += '<td class="text-right">'+sub_total_deduction+'</td>';
						arr += '<td class="text-right">'+sub_total_net+'</td>';
						arr += '<td></td>';

						arr += '</tr>';

						$('#tbl_body').html(arr);
						days = daysInMonth(_Month,_Year)

						_coveredPeriod = data.From+' to '+data.To+', '+_Year;

						$('#month_year').text(_coveredPeriod);
						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}

	$('#print').on('click',function(){
		$('#reports').printThis();
	});

	/*VIEW POSTED*/
$(document).on('click','.view', function(){
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)

		$('#semi_pay_period').val(_semiPayPeriod)

		$('#preview').trigger('click');
	});


/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

$('.position').change(function(){
	name = $(this).find(':selected').data('selected');
	value = $(this).find(':selected').data('position');
	
	$('#'+name).val(value)
});
})
</script>
@endsection