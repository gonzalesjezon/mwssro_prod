@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				@include('payrolls.reports.includes._transmital-signatory')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table table2" style="border:none !important;">
									<thead>
										<tr>
											<td style="border:none !important;" colspan="12">
												<div class="row mb-3">
							       			<div class="col-md-12 text-center" >
														<span class="font-weight-bold">Remittance List of Premium Contributions</span> <br>
														For the month of <span id="pay_period"></span>
							       			</div>
							       		</div>
							       		<div class="row pl-3 mb-3">
							       			<div class="col-md-12 font-weight-bold">
							       				<span>Office    :   Metropolitan Waterworks and Sewerage System - Regulatory Office</span> <br>
							       				Address :   3F/MWSI-MWSS Bldg., Katipunan Road, Balara, Quezon City 1105 <br>
							       				Tel. No. :   4358898
							       			</div>
							       		</div>
											</td>
										</tr>
										<tr class="text-center" style="font-weight: bold">
											<td rowspan="2">No</td>
											<td rowspan="2">Name</td>
											<td rowspan="2">BP No.</td>
											<td rowspan="2">Basic Salary</td>
											<td rowspan="2">Position</td>
											<td colspan="2">
												Compulsory Contributions <br>
												for Life & Retirement
											</td>
											<td rowspan="2">
													Employees <br>
													Compensation
											</td>
											<td rowspan="2">Opt. Life Ins. Prem.</td>
											<td rowspan="2">Total</td>
											<td rowspan="2" colspan="2">Remarks</td>
										</tr>
										<tr class="text-center" style="font-weight: bold">
											<td>Personal Share</td>
											<td>Gov't Share</td>
										</tr>
									</thead>
									<tfoot style="border:none;">
										<tr>
											<td style="border:none" colspan="12">
												<div class="row mb-8 text-center">
													<div class="col-md-4 font-weight-bold">
														Prepared  by:
													</div>
													<div class="col-md-4 font-weight-bold">
														Checked & Verified:
													</div>
													<div class="col-md-4 font-weight-bold">
														Approved:
													</div>
												</div>
												<div class="row mb-1 text-center">
													<div class="col-md-4">
														<span class="signatory_one font-weight-bold"></span> <br>
										    		<span class="position_one"></span>
													</div>
													<div class="col-md-4">
														<span class="signatory_two font-weight-bold"></span> <br>
										    		<span class="position_two"></span>
													</div>
													<div class="col-md-4">
														<span class="signatory_three font-weight-bold"></span> <br>
										    		<span class="position_three"></span>
													</div>
												</div>
											</td>
										</tr>
									</tfoot>
									 <tbody id="tbl_body">
									 </tbody>
								</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

$(document).on('click','#preview',function(){

	if(!_Year && !_Month){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{'month':_Month,'year':_Year},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);
				if(data.length !== 0){

					arr = [];

					var total_premium_amount = 0;
					var net_premium_amount = 0;
					var net_employer_share_amount = 0;
					var net_personal_share_amount = 0;
					$.each(data,function(k,v){

						firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
						lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
						middlename 			= (v.employees) ? v.employees.middlename : '';
						middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';

						fullname = lastname+' '+firstname+' '+middlename;
						position = (v.positions) ? v.positions.Name : '';
						bp_number = (v.employeeinfo.bp_no !== null) ? v.employeeinfo.bp_no : '';
						// position  = (v.positionitems !== null) ? v.positionitems.positions.name : '';
						basic_salary_amount  = (v.total_basicpay_amount !== null) ? v.total_basicpay_amount : 0;

						personal_share_amount = (v.gsis_ee_share) ? v.gsis_ee_share : 0;
						employer_share_amount = (v.gsis_er_share) ? v.gsis_er_share : 0;
						total_premium_amount = (parseFloat(personal_share_amount) + parseFloat(employer_share_amount));

						net_personal_share_amount += parseFloat(personal_share_amount);
						net_employer_share_amount += parseFloat(employer_share_amount);
						net_premium_amount += parseFloat(total_premium_amount);




						basic_salary_amount = (basic_salary_amount !== 0) ? commaSeparateNumber(parseFloat(basic_salary_amount).toFixed(2)) : '';
						personal_share_amount = (personal_share_amount !== 0) ? commaSeparateNumber(parseFloat(personal_share_amount).toFixed(2)) : '';
						employer_share_amount = (employer_share_amount !== 0) ? commaSeparateNumber(parseFloat(employer_share_amount).toFixed(2)) : '';
						total_premium_amount = (total_premium_amount !== 0) ? commaSeparateNumber(parseFloat(total_premium_amount).toFixed(2)) : '';
						arr += '<tr>';
						arr += '<td>'+(k+1)+'</td>';
						arr += '<td>'+fullname+'</td>';
						arr += '<td>'+bp_number+'</td>';
						arr += '<td class="text-right">'+basic_salary_amount+'</td>';
						arr += '<td class="text-left">'+position+'</td>';
						arr += '<td class="text-right">'+personal_share_amount+'</td>';
						arr += '<td class="text-right">'+employer_share_amount+'</td>';
						arr += '<td class="text-right">'+basic_salary_amount+'</td>';
						arr += '<td></td>';
						arr += '<td class="text-right">'+total_premium_amount+'</td>';
						arr += '<td colspan="2"></td>';

						arr += '</tr>';

					});

					net_premium_amount = (net_premium_amount !== 0) ? commaSeparateNumber(parseFloat(net_premium_amount).toFixed(2)) : '';
					net_employer_share_amount = (net_employer_share_amount !== 0) ? commaSeparateNumber(parseFloat(net_employer_share_amount).toFixed(2)) : '';
					net_personal_share_amount = (net_personal_share_amount !== 0) ? commaSeparateNumber(parseFloat(net_personal_share_amount).toFixed(2)) : '';

					arr += '<tr>';
					arr += '<td></td>'
					arr += '<td colspan="4" ><b>Total</b></td>'
					arr += '<td class="text-right"><b>'+net_personal_share_amount+'</b></td>'
					arr += '<td class="text-right"><b>'+net_employer_share_amount+'</b></td>'
					arr += '<td class="text-right"></td>';
					arr += '<td class="text-right"></td>';
					arr += '<td class="text-right"><b>'+net_premium_amount+'</b></td>'
					arr += '<td></td>'

					arr += '</tr>';


					$('#tbl_body').html(arr);

					$('#pay_period').text(months[_Month]+' '+_Year)

					$('#btnModal').trigger('click');
				}else{
					swal({
						  title: "No Records Found",
						  type: "warning",
						  showCancelButton: false,
						  confirmButtonClass: "btn-danger",
						  confirmButtonText: "Yes",
						  closeOnConfirm: false

					});
				}



			}
		})
	}


});
$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection