@extends('layouts.app-honorariareports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				@include('payrolls.reports.includes._signatory-one')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px; margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
								<table class="table table2" style="border: none !important;">
									<thead style="border:none !important;">
										<tr>
											<td style="border:none !important;" colspan="10">
												<div class="row">
							       			<div class="col-md-12 text-center">
							       				<h4>GENERAL PAYROLL</h4>
							       			</div>
							       		</div>
							       		<div class="row pl-3">
							       			<div class="col-md-12 text-left">
							       				<span style="font-weight: bold">OFFICE/DEPARTMENT:  REGULATORY OFFICE</span> <br>
							       				<span>Payment of BAC Honorarium under Contract Nos. RO-PG2018-002 and RO-PG2018-003</span>
							       			</div>
							       		</div>
											</td>
										</tr>
										<tr class="text-center">
											<td></td>
											<td>Name</td>
											<td>Committee <br> Designation</td>
											<td>No. of <br> SCPP</td>
											<td>Honorarium Rate </td>
											<td>Total Amount</td>
											<td>Tax Rate</td>
											<td>Less: Tax</td>
											<td>Net Amount</td>
											<td>Landbank <br> Acct. No.</td>
										</tr>
									</thead>
									<tfoot style="border:none;">
										<tr>
											<td style="border:none;" colspan="10">
												<div class="row">
							       			<div class="col-md-12 text-center">
							       				<span>
							       					I hereby certify that this payroll is correct and that	<br>
													the claims herein stated are for services actually	<br>
													and duly rendered as authorized by management.		 <br><br>
							       				</span>
							       				<span class=" font-weight-bold signatory_one"></span> <br>
												    <span class="position_one"></span>
							       				<br>
							       			</div>
							       		</div>
							       		<div class="row" style="margin-top: 50px;">
							       			<div class="col-md-4" style="padding-left: 30px;">
							       				<span>
								       				Certified:  (1) as to accuracy of computation (2) accounting <br>
													codes and journal entries are proper; (3) properly entered in 	<br>
													the accounting records.
							       				</span>
							       			</div>
							       			<div class="col-md-4 text-center" style="padding-left: 20px;">
							       				<span>
							       					Approved for appropriation and funds being
													available:
							       				</span>
							       			</div>
							       			<div class="col-md-4 text-center" style="padding-left: 20px;">
							       				<span>
							       					<b>APPROVED:</b>
							       				</span>
							       			</div>
							       		</div>
							       		<div class="row" style="margin-top: 30px;">
							       			<div class="col-md-4 text-center">
							       				<span class=" font-weight-bold signatory_two"></span> <br>
												    <span class="position_two"></span> <br>
												RO-18-______

							       			</div>
							       			<div class="col-md-4 text-center">
							       				<span class=" font-weight-bold signatory_three"></span> <br>
												    <span class="position_three"></span>

							       			</div>
							       			<div class="col-md-4 text-center">
							       				<span class=" font-weight-bold signatory_four"></span> <br>
												    <span class="position_four"></span>

							       			</div>
							       		</div>
											</td>
										</tr>
									</tfoot>
									 <tbody id="tbl_body">
									 </tbody>
								</table>
	       			</div>
	       		</div>
	       		
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _createdAt;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});

	$('#signatory_4').change(function(){
		signatory_four = $(this).find(':selected').text();
		position_four 	= $(this).find(':selected').data('position');

		$('.signatory_four').text(signatory_four);
		$('.position_four').text(position_four);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}




	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select Year and Month!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'year':_Year,
					'month':_Month,
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction.length !== 0){

						body = [];
						ctr = 1;

						subHAmount 	  		= 0;
						subHonorariaAmount 	= 0;
						subTotalAmount 		= 0;
						subTaxAmount 		= 0;
						subNetAmount 		= 0;
						$.each(data.transaction,function(k,v){

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							positions = (v.positions !== null) ? v.positions.Name : '';

							accountNumber = (v.employeeinfo) ? v.employeeinfo.account_number : '';

							noOfScpp = (v.no_of_scpp) ? v.no_of_scpp : 0;
							honorariaAmount = (v.amount) ? v.amount : 0;
							percentage = (v.percentage) ? v.percentage : 0;

							totalAmount = parseFloat(honorariaAmount) * noOfScpp;
							taxAmount = parseFloat(totalAmount) * percentage;
							netAmount = parseFloat(totalAmount) - parseFloat(taxAmount);

							subHAmount += parseFloat(honorariaAmount);
							subHonorariaAmount += parseFloat(honorariaAmount);
							subTotalAmount += parseFloat(totalAmount);
							subTaxAmount += parseFloat(taxAmount);
							subNetAmount += parseFloat(netAmount);


							honoraria_amount = (honorariaAmount !== 0) ? commaSeparateNumber(parseFloat(honorariaAmount).toFixed(2)) : '';
							total_amount = (totalAmount !== 0) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';
							tax_amount = (taxAmount !== 0) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
							net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';
							percentage = parseFloat(percentage) * 100 +' %';
							account_number = (accountNumber !== null) ? accountNumber : '';

							body += '<tr>';
							body += '<td>'+ctr+'</td>';
							body += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
							body += '<td class="text-left">'+positions+'</td>';
							body += '<td class="text-center">'+noOfScpp+'</td>'; // No of SCPP
							body += '<td class="text-right">'+honoraria_amount+'</td>';
							body += '<td class="text-right">'+total_amount+'</td>'; // total amount
							body += '<td class="text-right">'+percentage+'</td>'; // tax rate
							body += '<td class="text-right">'+tax_amount+'</td>'; // less tax
							body += '<td class="text-right">'+net_amount+'</td>'; // net amount
							body += '<td class="text-right">'+accountNumber+'</td>'; // land bank accnt number
							body += '</tr>';

							ctr++;
						});

						ctr = 1;

						sub_honoraria_amount = (subHAmount !== 0) ? commaSeparateNumber(parseFloat(subHAmount).toFixed(2)) : '';
						sub_total_amount = (subTotalAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalAmount).toFixed(2)) : '';
						sub_tax_amount = (subTaxAmount !== 0) ? commaSeparateNumber(parseFloat(subTaxAmount).toFixed(2)) : '';
						sub_net_amount = (subNetAmount !== 0) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '';


						body += '<tr>';
						body += '<td></td>';
						body += '<td class="text-left">TOTAL</td>';
						body += '<td class="text-left"></td>';
						body += '<td class="text-right"></td>'; // No of SCPP
						body += '<td class="text-right">'+sub_honoraria_amount+'</td>';
						body += '<td class="text-right">'+sub_total_amount+'</td>'; // total amount
						body += '<td class="text-right"></td>'; // tax rate
						body += '<td class="text-right">'+sub_tax_amount+'</td>'; // less tax
						body += '<td class="text-right">'+sub_net_amount+'</td>'; // net amount
						body += '<td class="text-right"></td>'; // land bank accnt number
						body += '</tr>';


						$('#tbl_body').html(body);

						$('#month_year').text(months[_Month]+' '+_Year);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection