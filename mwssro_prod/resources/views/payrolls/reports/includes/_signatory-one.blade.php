<div class="row pl-4 pr-4">
	<div class="col-md-6">
		<span>Signatory One</span>
		<div class="form-group">
			<select class="form-control font-style2 position"  id="signatory_1" name="signatory_one">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{{ @$employee->employeeinformation->positions->Name }}" data-selected="position_one">{!! $employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<span>Signatory Two</span>
		<div class="form-group">
			<select class="form-control font-style2 position"  id="signatory_2" name="signatory_two">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{{ @$employee->employeeinformation->positions->Name }}" data-selected="position_two">{!! $employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>

<div class="row pl-4 pr-4">
	<div class="col-md-6">
		<span>Signatory Three</span>
		<div class="form-group">
			<select class="form-control font-style2 position" id="signatory_3" name="signatory_three">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{{ @$employee->employeeinformation->positions->Name }}" data-selected="position_three">{!! $employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<span>Signatory Four</span>
		<div class="form-group">
			<select class="form-control font-style2 position" id="signatory_4" name="signatory_four">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{{ @$employee->employeeinformation->positions->Name }}" data-selected="position_four">{!! $employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>

<input type="hidden" name="position_one" id="position_one" class="form-control">
<input type="hidden" name="position_two" id="position_two" class="form-control">
<input type="hidden" name="position_three" id="position_three" class="form-control">
<input type="hidden" name="position_four" id="position_four" class="form-control">
<input type="hidden" name="position_five" id="position_five" class="form-control">