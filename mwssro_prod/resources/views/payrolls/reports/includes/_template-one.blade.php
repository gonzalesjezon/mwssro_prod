
<style type="text/css">
.tr td{
	border-bottom: none;
}
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
</style>
<div class="row m-1 p-2">
	<div class="col-md-12">
		<div class="form-group">
			<table class="table noborder border datatable" id="tblPosted">
				<thead>
					<tr>
						<th>Date Posted</th>
						<th>Transaction Date</th>
						@if($module == 'payslips')
						<th>Employee Name</th>
						@else
						<th>Pay Period</th>
						@endif

						@if($module == 'payrollworksheets')
						<th>Part</th>
						@endif
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($postedreports as $key => $value)
					<tr class="text-center">
						<td>{!! $value->created_at !!}</td>
						<td>{!! date("F", strtotime($value->year."-".$value->month."-01")) .' '. $value->year !!}</td>
						@if($module == 'payslips')
						<td>{!! @$value->employee->getFullName() !!}</td>
						@else
						<td>{!! @$value->pay_period !!}</td>
						@endif
						@if($module == 'payrollworksheets')
						<td>{!! @$value->part !!}</td>
						@endif
						<td>
							<a class="btn btn-success btn-xs view" data-empid="{{ @$value->employee_id }}" data-year="{{$value->year}}" data-month="{{$value->month}}" data-report_type="{{$value->report_type}}" data-signatory_one="{{ @$value->signatory->signatory_one}}" data-signatory_two="{{ @$value->signatory->signatory_two}}" data-signatory_three="{{ @$value->signatory->signatory_three}}" data-signatory_four="{{ @$value->signatory->signatory_four}}" data-position_one="{{@$value->signatory->position_one}}" data-position_two="{{ @$value->signatory->position_two}}" data-position_three="{{@$value->signatory->position_three}}" data-position_four="{{ @$value->signatory->position_four}}"  data-pay_period="{{$value->pay_period}}" data-part="{{$value->part}}"  data-monthno="{{$value->month}}">View</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var table = $('#tblPosted').DataTable({
		 	'paging':true,
			"oLanguage": {
				"oPaginate": {
				"sFirst": "<<", // This is the link to the first page
				"sPrevious": "<", // This is the link to the previous page
				"sNext": ">", // This is the link to the next page
				"sLast": ">>" // This is the link to the last page
				}
			}
		 });
	})
</script>

