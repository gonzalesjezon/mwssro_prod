<div class="row pl-3 pr-3 mt-0">
	<div class="col-md-6">
		<form action="{{ url($module_prefix.'/'.$module.'/generateCSV') }}" method="post" id="form">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="_year" id="_year">
			<input type="hidden" name="_month" id="_month">
			<input type="hidden" name="_payperiod" id="_payperiod">
			<button class="btn btn-success btn-xs btn-editbg" id="generate_csv" >
				Generate CSV
			</button>
		</form>
	</div>
</div>

