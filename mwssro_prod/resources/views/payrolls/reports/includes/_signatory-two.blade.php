<div class="row pl-4 pr-4">
	<div class="col-md-6">
		<span>Signatory One</span>
		<div class="form-group">
			<select class="form-control font-style2" id="signatory_1">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{{ @$employee->employeeinformation->positions->Name }}">{!! $employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<span>Signatory Two</span>
		<div class="form-group">
			<select class="form-control font-style2" id="signatory_2">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{{ @$employee->employeeinformation->positions->Name }}">{!! $employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>