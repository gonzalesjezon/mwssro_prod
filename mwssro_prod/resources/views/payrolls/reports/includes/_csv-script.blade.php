<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click','#generate_csv',function(){
			let selectedYear 	= $('#select_year').find(':selected').val();
			let selectedMonth = $('#select_month').find(':selected').val();
			let payPeriod 	  = $('#semi_pay_period').find(':selected').val();

			$('#_year').val(selectedYear);
			$('#_month').val(selectedMonth);
			$('#_payperiod').val(payPeriod);
			
		});
	});
</script>