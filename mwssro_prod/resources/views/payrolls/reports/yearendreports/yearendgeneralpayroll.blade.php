@extends('layouts.app-yearendreports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				@include('payrolls.reports.includes._signatory-one')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
								<table class="table table2" style="border: none !important;">
								<thead style="border:none !important;">
									<tr>
										<td colspan="12" style="border:none !important;">
											<div class="row">
						       			<div class="col-md-12 text-center">
						       				<h4>GENERAL PAYROLL</h4>
						       			</div>
						       		</div>
						       		<div class="row pl-3 pr-3">
						       			<div class="col-md-12 text-left">
						       				<span>Payment of Year-End Bonus & Cash Gift for CY 2017 per GCG CPCS Implementing Circular No. 2016-01 dated 11 May 2017</span>
						       			</div>
						       		</div>
										</td>
									</tr>
									<tr class="text-center">
										<td></td>
										<td>Name</td>
										<td>Designation</td>
										<td>SG</td>
										<td>Basic Salary</td>
										<td>%</td>
										<td>Year End Bonus</td>
										<td>Cash Gift</td>
										<td>Year End Bonus & <br> Cash Gift</td>
										<td>Less: ITW</td>
										<td>Net Amount</td>
										<td>Land Bank <br> Accnt. No</td>
									</tr>
								</thead>
								<tfoot style="border:none;">
									<tr>
										<td colspan="12" style="border:none;">
											<div class="row">
						       			<div class="col-md-12 text-center">
						       				<span>
						       					I hereby certify that this payroll is correct and that	<br>
												the claims herein stated are for services actually	<br>
												and duly rendered as authorized by management.		 <br><br>
						       				</span>
						       				<span class=" font-weight-bold signatory_one"></span> <br>
											    <span class="position_one"></span>
						       				<br>
						       			</div>
						       		</div>
						       		<div class="row" style="margin-top: 50px;">
						       			<div class="col-md-4" style="padding-left: 30px;">
						       				<span>
							       				Certified:  (1) as to accuracy of computation (2) accounting <br>
												codes and journal entries are proper; (3) properly entered in 	<br>
												the accounting records.
						       				</span>
						       			</div>
						       			<div class="col-md-4 text-center" style="padding-left: 20px;">
						       				<span>
						       					Approved for appropriation and funds being
												available:
						       				</span>
						       			</div>
						       			<div class="col-md-4 text-center" style="padding-left: 20px;">
						       				<span>
						       					<b>APPROVED:</b>
						       				</span>
						       			</div>
						       		</div>
						       		<div class="row" style="margin-top: 30px;">
						       			<div class="col-md-4 text-center">
						       				<span class=" font-weight-bold signatory_two"></span> <br>
											     <span class="position_two"></span>
											RO-18-______

						       			</div>
						       			<div class="col-md-4 text-center">
						       				<span class=" font-weight-bold signatory_three"></span> <br>
											     <span class="position_three"></span>

						       			</div>
						       			<div class="col-md-4 text-center">
						       				<span class=" font-weight-bold signatory_four"></span> <br>
											     <span class="position_four"></span>

						       			</div>
						       		</div>
										</td>
									</tr>
								</tfoot>
								 <tbody id="tbl_body">
								 </tbody>
								</table>
	       			</div>
	       		</div>
	       		
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});

	$('#signatory_4').change(function(){
		signatory_four = $(this).find(':selected').text();
		position_four 	= $(this).find(':selected').data('position');

		$('.signatory_four').text(signatory_four);
		$('.position_four').text(position_four);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}


	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction.lenght != 0){

						arr = [];

						sub_total_basicSalary = 0;
						sub_total_yearEndAmount = 0;
						sub_total_cashGift = 0;
						sub_yearEnd_and_cashGift = 0;

						$.each(data.transaction,function(k,v){

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							positions = (v.positions !== null) ? v.positions.Name : '';
							salarygrade = (v.salaryinfo) ? v.salaryinfo.salarygrade.Name : '';
							basic_salary = (v.transaction) ? v.transaction.total_basicpay_amount : 0;
							percentage = (v.percentage !== null) ? v.percentage : '';
							atmNo = (v.employeeinfo.atm_no) ? v.employeeinfo.atm_no : '';
							year_end_amount = (v.amount !== null) ? v.amount : 0;
							cash_gift_amount = (v.cash_gift_amount !== null) ? v.cash_gift_amount : 0;
							yearEnd_and_cashGift = (parseFloat(year_end_amount) + parseFloat(cash_gift_amount));

							sub_total_yearEndAmount += parseFloat(year_end_amount);
							sub_total_basicSalary += parseFloat(basic_salary);
							sub_total_cashGift += parseFloat(cash_gift_amount);
							sub_yearEnd_and_cashGift += parseFloat(yearEnd_and_cashGift);




							basic_salary = (basic_salary !== 0) ? commaSeparateNumber(parseFloat(basic_salary).toFixed(2)) : '';
							year_end_amount = (year_end_amount !== 0) ? commaSeparateNumber(parseFloat(year_end_amount).toFixed(2)) : '';
							cash_gift_amount = (cash_gift_amount !== 0) ? commaSeparateNumber(parseFloat(cash_gift_amount).toFixed(2)) : '';
							yearEnd_and_cashGift = (yearEnd_and_cashGift !== 0) ? commaSeparateNumber(parseFloat(yearEnd_and_cashGift).toFixed(2)) : '';


							arr += '<tr>';
							arr += '<td>'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
							arr += '<td class="text-left">'+positions+'</td>';
							arr += '<td class="text-left">'+salarygrade+'</td>';
							arr += '<td class="text-right">'+basic_salary+'</td>';
							arr += '<td class="text-right">'+percentage+'</td>';
							arr += '<td class="text-right">'+year_end_amount+'</td>';
							arr += '<td class="text-right">'+cash_gift_amount+'</td>';
							arr += '<td class="text-right">'+yearEnd_and_cashGift+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right">'+yearEnd_and_cashGift+'</td>';
							arr += '<td class="text-right">'+atmNo+'</td>';
							arr += '</tr>';
						});

						sub_total_basicSalary = (sub_total_basicSalary !== 0) ? commaSeparateNumber(parseFloat(sub_total_basicSalary).toFixed(2)) : '';
						sub_total_yearEndAmount = (sub_total_yearEndAmount !== 0) ? commaSeparateNumber(parseFloat(sub_total_yearEndAmount).toFixed(2)) : '';
						sub_total_cashGift = (sub_total_cashGift !== 0) ? commaSeparateNumber(parseFloat(sub_total_cashGift).toFixed(2)) : '';
						sub_yearEnd_and_cashGift = (sub_yearEnd_and_cashGift !== 0) ? commaSeparateNumber(parseFloat(sub_yearEnd_and_cashGift).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td></td>';
						arr += '<td class="text-left">Grand Total</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-right">'+sub_total_basicSalary+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+sub_total_yearEndAmount+'</td>';
						arr += '<td class="text-right">'+sub_total_cashGift+'</td>';
						arr += '<td class="text-right">'+sub_yearEnd_and_cashGift+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+sub_yearEnd_and_cashGift+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';






						$('#tbl_body').html(arr);

						// days = daysInMonth(_monthNumber,_Year);

						// if(_payPeriod == 'monthly'){
						// 	_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						// }else{
						// 	switch(_semiPayPeriod){
						// 		case 'firsthalf':
						// 			_coveredPeriod = _Month+' 1-15, '+_Year;
						// 		break;
						// 		default:
						// 			_coveredPeriod =_Month+' 16-'+days+', '+_Year;
						// 		break;
						// 	}
						// }

						// $('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection