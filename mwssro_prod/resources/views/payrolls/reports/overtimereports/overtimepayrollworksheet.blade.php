@extends('layouts.app-overtimereports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	.p-print{
		font-size: 12px !important;
	    color: #333 !important;
	    line-height: 0.6em !important;
	}
	.td-border-none{
		border:none !important;
	}

	@media print{
		.table>thead>tr>td, .table>tbody>tr>td{
			border: 1px solid #333 !important;
			padding: 2px !important;
		}
		.vertical-align-middle{
			vertical-align: middle !important;
		}
	}

	.table2>thead>tr>td, .table2>tbody>tr>td{
		border: 1px solid #333 !important;
		padding: 2px !important;
	}

</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg mb-2 btn_save" style="display: inline-block;width: 30%;">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px !important;margin: auto;">
	       		
						<table class="table table2" style="border:none !important;">
							<thead style="border:none !important;">
								<tr>
									<td colspan="15" style="border:none !important;">
										<div class="row">
						       			<div class="col-md-12 text-center">
						       				<h4 class="font-weight-bold">PAYROLL WORKSHEET</h4>
						       			</div>
						       		</div>
						       		<div class="row pl-2 pr-2">
						       			<div class="col-md-12 text-left">
						       				<span>Payment of overtime services for the month/s of <span id="month_year"></span></span>
						       			</div>
						       		</div>
									</td>
								</tr>
								<tr class="text-center">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>Basic</td>
											<td>Period</td>
											<td colspan="6">Hrs.</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr class="text-center">
											<td></td>
											<td>Name</td>
											<td>SG</td>
											<td>Designation</td>
											<td>Salary</td>
											<td>Covered</td>
											<td>Reg. OT <br> Rate</td>
											<td>OT Hrs.</td>
											<td>Amt.</td>
											<td>Sat/Sun</td>
											<td>OT Hr.</td>
											<td>Amt.</td>
											<td>Total</td>
											<td>ITW</td>
											<td>Net Amount</td>
										</tr>
							</thead>
						 <tbody id="tbl_body">
						 </tbody>
						</table>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})



	$(document).on('click','#preview',function(){

		if(!_Year && !_Month && !_payPeriod){
			swal({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					// 'pay_period':_semiPayPeriod
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction !== 0){

						arr = [];
						subBasicAmount 		  = 0;
						subRegularOTRate 		= 0;
						subRegularOTHrs 		= 0;
						subRegularOTAmount 	= 0;
						subWeekendOTRate 		= 0;
						subWeekendOTHrs 		= 0;
						subWeekendOTAmount 	= 0;
						subOtTotal 					= 0;
						subOtTax 						= 0;
						subOtNet 						= 0;
						$.each(data.transaction,function(k,v){

							firstname 			= (v.employees.firstname !== null) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname !== null) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees.middlename !== null) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							positions 			= (v.employeeinformation.positions !== null) ? v.employeeinformation.positions.Name : '';
							salarygrade 		= (v.salaryinfo.salarygrade !== null) ? v.salaryinfo.salarygrade.Name : '';
							basicAmount 		= (v.basic_amount) ? v.basic_amount : 0;
							otCovered 			= (v.ot_covered_period) ? v.ot_covered_period : '';
							regularOtRate 	= (v.regular_ot_rate) ? v.regular_ot_rate : '';
							regularOtHrs 		= (v.regular_ot_hrs) ? v.regular_ot_hrs : '';
							regularOtAmount = (v.regular_ot_amount) ? v.regular_ot_amount : '';
							weekendOtRate 	= (v.weekend_ot_rate) ? v.weekend_ot_rate : '';
							weekendOtHrs 		= (v.weekend_ot_hrs) ? v.weekend_ot_hrs : '';
							weekendOtAmount = (v.weekend_ot_amount) ? v.weekend_ot_amount : '';
							otTotal = (v.ot_total) ? v.ot_total : '';
							otTax 	= (v.ot_tax) ? v.ot_tax : '';
							otNet 	= (v.ot_net) ? v.ot_net : '';

							subBasicAmount += Number(basicAmount);
							subRegularOTRate += Number(regularOtRate);
							subRegularOTHrs += Number(regularOtHrs);
							subRegularOTAmount += Number(regularOtAmount);
							subWeekendOTRate += Number(weekendOtRate);
							subWeekendOTHrs += Number(weekendOtHrs);
							subWeekendOTAmount += Number(weekendOtAmount);
							subOtTotal += Number(otTotal);
							subOtTax += Number(otTax);
							subOtNet += Number(otNet);

							basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
							regular_ot_amount = (regularOtAmount !== 0) ? commaSeparateNumber(parseFloat(regularOtAmount).toFixed(2)) : '';
							weekend_ot_amount = (weekendOtAmount !== 0) ? commaSeparateNumber(parseFloat(weekendOtAmount).toFixed(2)) : '';
							ot_total = (otTotal !== 0) ? commaSeparateNumber(parseFloat(otTotal).toFixed(2)) : '';
							ot_tax = (otTax !== 0) ? commaSeparateNumber(parseFloat(otTax).toFixed(2)) : '';
							ot_net = (otNet !== 0) ? commaSeparateNumber(parseFloat(otNet).toFixed(2)) : '';


							arr += '<tr>';
							arr += '<td>'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left" nowrap>'+lastname+' '+firstname+' '+middlename+'</td>';
							arr += '<td class="text-center" nowrap>'+salarygrade+'</td>';
							arr += '<td class="text-left" nowrap>'+positions+'</td>';
							arr += '<td class="text-right">'+basic_amount+'</td>';
							arr += '<td class="text-right">'+otCovered+'</td>';
							arr += '<td class="text-right">'+regularOtRate+'</td>';
							arr += '<td class="text-right">'+regularOtHrs+'</td>';
							arr += '<td class="text-right">'+regular_ot_amount+'</td>';
							arr += '<td class="text-right">'+weekendOtRate+'</td>';
							arr += '<td class="text-right">'+weekendOtHrs+'</td>';
							arr += '<td class="text-right">'+weekend_ot_amount+'</td>';
							arr += '<td class="text-right">'+ot_total+'</td>';
							arr += '<td class="text-right">'+ot_tax+'</td>';
							arr += '<td class="text-right">'+ot_net+'</td>';
							arr += '</tr>';
						});

						sub_basic_amount = (subBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '0.00';
						sub_regular_ot_amount = (subRegularOTAmount !== 0) ? commaSeparateNumber(parseFloat(subRegularOTAmount).toFixed(2)) : '0.00';
						sub_weekend_ot_amount = (subWeekendOTAmount !== 0) ? commaSeparateNumber(parseFloat(subWeekendOTAmount).toFixed(2)) : '0.00';
						sub_ot_total = (subOtTotal !== 0) ? commaSeparateNumber(parseFloat(subOtTotal).toFixed(2)) : '0.00';
						sub_ot_tax = (subOtTax !== 0) ? commaSeparateNumber(parseFloat(subOtTax).toFixed(2)) : '0.00';
						sub_ot_net = (subOtNet !== 0) ? commaSeparateNumber(parseFloat(subOtNet).toFixed(2)) : '0.00';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td></td>';
						arr += '<td class="text-left">Total</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-right">'+sub_basic_amount+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+subRegularOTRate+'</td>';
						arr += '<td class="text-right">'+subRegularOTHrs+'</td>';
						arr += '<td class="text-right">'+sub_regular_ot_amount+'</td>';
						arr += '<td class="text-right">'+subWeekendOTRate+'</td>';
						arr += '<td class="text-right">'+subWeekendOTHrs+'</td>';
						arr += '<td class="text-right">'+sub_weekend_ot_amount+'</td>';
						arr += '<td class="text-right">'+sub_ot_total+'</td>';
						arr += '<td class="text-right">'+sub_ot_tax+'</td>';
						arr += '<td class="text-right">'+sub_ot_net+'</td>';
						arr += '</tr>';



						$('#tbl_body').html(arr);

						days = daysInMonth(_Month,_Year);

						
						from = (data.From) ? data.From : '';
						to = (data.To) ? data.To : '';
						_coveredPeriod = from+' to '+to+' '+_Year;

						$('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$('#print').on('click',function(){
	$('#reports').printThis();
});

	/*VIEW POSTED*/
$(document).on('click','.view',function(){
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)

		$('#semi_pay_period').val(_semiPayPeriod)

		$('#preview').trigger('click');
	});


/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

$('.position').change(function(){
	name = $(this).find(':selected').data('selected');
	value = $(this).find(':selected').data('position');
	
	$('#'+name).val(value)
});

})
</script>
@endsection