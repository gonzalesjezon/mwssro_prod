@extends('app-remittances')


@section('remittances-content')


<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 900px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid ">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{ url('images/mwssreportheader.png') }}" style="height: 80px;">
	       				<h5>Employees Compensation Insurance Premium (ECIP)</h5>
	       				<h6>Employer ID No.: <span id="employer_id_number"></span></h6>
	       				<h6>Period Covered: <span id="month_year"></span></h6>
	       			</div>
					<table class="table">
						<thead class="text-center">
							<tr >
								<td><b>#</b></td>
								<td><b>EMPLOYEE NAME</b></td>
								<td><b>PERSONAL SHARE (PS)</b></td>
								<td><b>GOVERNMENT SHARE (GS)</b></td>
								<td><b>TOTAL</b></td>
							</tr>
						</thead>
						<tfoot class="text-center">
						 	<tr style="border:1px solid #c0c0c0;">
						 		<td style="border:none;"></td>
						 		<td style="border:none;" class="text-left"><b>Total</b></td>
						 		<td style="border:none;font-weight: bold"><span id="net_total_philhealth_contribution"></span></td>
						 		<td style="border:none;font-weight: bold"><span id="net_er_philhealth_share"></span></td>
						 		<td style="border:none;font-weight: bold;border-right:1px solid #c0c0c0;"><span id="net_total_philhealth"></span></td>
						 	</tr>
						 	<tr style="border:1px solid #c0c0c0;" class="text-left" >
						 		<td style="border:none;"></td>
						 		<td style="border:none;" colspan="2">
						 		Certified Correct
						 		<br><br>
						 		<b>ANTONIA LYNNELY L. BAUTISTA</b>
						 		<br>
						 		<span>Chief Admin Officer, HRDD</span>
						 		</td>
						 		<td  style="border:none;border-right: 1px solid #c0c0c0;" colspan="2">
						 		Approved for Payment
						 		<br>
						 		<br>
						 		<b>GWEN GRECIA-DE VERA</b>
						 		<br>
						 		<span>Executive Director</span>
						 		</td>
						 	</tr>
					 	</tfoot>
					 <tbody id="tbl_body">
					 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);

					if(data.length !== 0){
						arr = [];
						ctr = 0;
						net_total_philhealth_contribution 	 = 0;
						net_er_philhealth_share 	 		 = 0;
						net_philhealth 			 	 		 = 0;
						net_total_philhealth 		 		 = 0;

						$.each(data,function(key,val){
							philhealth_contribution 			= 0;
							er_philhealth_share 				= 0;
							total_philhealth 					= 0;
							sub_total_philhealth 				= 0;
							sub_total_philhealth_contribution 	= 0;
							sub_total_er_philhealth_share 	 	= 0;

							arr += '<tr style="border:1px solid #c0c0c0;">';
							arr += '<td style="border-right:none;"></td>';
							arr += '<td style="border-left:none;" colspan="4"><b>'+key+'</b></td>';
							arr += '</tr>';

							$.each(val,function(k,v){

							arr += '<tr style="border:1px solid #c0c0c0;" class="text-center">';
								arr += '<td >'+(ctr+1)+'</td>';
								arr += '<td >&nbsp;'+v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename+'</td>';

								philhealth_contribution = (v.employeeinfo.philhealth_contribution) ? v.employeeinfo.philhealth_contribution : 0;
								sub_total_philhealth_contribution += parseFloat(philhealth_contribution);
								philhealth_contribution = (philhealth_contribution == 0) ? '-' : parseFloat(philhealth_contribution).toFixed(2);
								arr += '<td>'+philhealth_contribution+'</td>';

								er_philhealth_share = (v.employeeinfo.er_philhealth_share) ? v.employeeinfo.er_philhealth_share : 0;
								sub_total_er_philhealth_share += parseFloat(er_philhealth_share);
								er_philhealth_share = (er_philhealth_share == 0) ? '-' : parseFloat(er_philhealth_share).toFixed(2);
								arr += '<td>'+er_philhealth_share+'</td>';

								er_philhealth_share = (er_philhealth_share == '-') ? 0 :er_philhealth_share;
								philhealth_contribution = (philhealth_contribution == '-') ? 0 :philhealth_contribution;

								total_philhealth = (parseFloat(philhealth_contribution) + parseFloat(er_philhealth_share));
								sub_total_philhealth += parseFloat(total_philhealth);

								total_philhealth = (total_philhealth) ? parseFloat(total_philhealth).toFixed(2) : 0;
								arr += '<td>'+commaSeparateNumber(total_philhealth) +'</td>';

							arr += '</tr>';
							ctr++;
							});

							ctr = 0;
							arr += '<tr style="border:1px solid #c0c0c0;" class="text-center">';
							arr += '<td style="border:none"></td>';
							arr += '<td style="border:none;" class="text-left"><b>Sub Total</b></td>';


							net_total_philhealth_contribution += parseFloat(sub_total_philhealth_contribution);
							sub_total_philhealth_contribution = (sub_total_philhealth_contribution !== 0) ? parseFloat(sub_total_philhealth_contribution).toFixed(2) : '-';
							arr += '<td style="border:none;font-weight:bold">'+ commaSeparateNumber(sub_total_philhealth_contribution) +'</td>';

							net_er_philhealth_share += parseFloat(sub_total_er_philhealth_share);
							sub_total_er_philhealth_share = (sub_total_er_philhealth_share !== 0) ? parseFloat(sub_total_er_philhealth_share).toFixed(2) : '-';
							arr += '<td style="border:none;font-weight:bold">'+commaSeparateNumber(sub_total_er_philhealth_share) +'</td>';

							sub_total_er_philhealth_share = (sub_total_er_philhealth_share == '-') ? 0 :
							sub_total_er_philhealth_share;

							net_total_philhealth += parseFloat(sub_total_philhealth);
							sub_total_philhealth = (sub_total_philhealth !== 0) ? parseFloat(sub_total_philhealth).toFixed(2) : 0;
							arr += '<td style="border:none;font-weight:bold">'+commaSeparateNumber(sub_total_philhealth) +'</td>';

							arr += '</tr>';
						});

						net_total_philhealth_contribution = (net_total_philhealth_contribution == 0) ? '-' : parseFloat(net_total_philhealth_contribution).toFixed(2);
						$('#net_total_philhealth_contribution').text(commaSeparateNumber(net_total_philhealth_contribution) );

						net_er_philhealth_share = (net_er_philhealth_share == 0) ? '-' : parseFloat(net_er_philhealth_share).toFixed(2);
						$('#net_er_philhealth_share').text(commaSeparateNumber(net_er_philhealth_share) );

						net_total_philhealth = (net_total_philhealth == 0) ? '-' : parseFloat(net_total_philhealth).toFixed(2);
						$('#net_total_philhealth').text(commaSeparateNumber(net_total_philhealth) );

						$('#tbl_body').html(arr);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});
})
</script>
@endsection