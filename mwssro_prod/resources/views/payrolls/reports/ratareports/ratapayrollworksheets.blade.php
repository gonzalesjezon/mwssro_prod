@extends('layouts.app-ratareports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
	@media print{
		.table>thead>tr>td, .table>tbody>tr>td{
			padding: 2px !important;
		}
		.vertical-align-middle{
			vertical-align: middle !important;
		}
	}

	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 2px !important;
	}
	.vertical-align-middle{
		vertical-align: middle !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row pl-4 pr-4">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select class="form-control font-style2" id="semi_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg mb-2 btn_save" style="display: inline-block;width: 30%;">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%">
	    <div class="mypanel border0" style="height:90%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<h5> <b>GENERAL PAYROLL</b></h5>
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>PAYMENT OF RATA FOR THE PERIOD </b> <span id="month_year"></span></h6>
	       			</div>
					<table class="table" id="payroll_transfer" style="border: 2px solid #333;"></table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false



			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':_semiPayPeriod
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					if(data.transaction.length !== 0){

						arr = [];
						ctr = 1;
						thead 	= [];
						grand_total = [];
						arr_loan = [];
						arr_deduction = [];
						arr_subtotal_loan = [];
						arr_subtotal_deduction = [];
						cols = data.cols;
						net_ra_amount = 0;
						net_ta_amount = 0;
						net_total_amount = 0;
						net_tax_amount = 0;
						net_ra_deduction_amount = 0;
						net_ta_deduction_amount = 0;
						net_deduction_amount = 0;
						net_amount_total = 0;

						arr += '<thead>';
						arr	+= '<tr class="text-center" style="font-weight:bold" >';
						arr += '<td rowspan="2" class="vertical-align-middle"></td>';
						// arr += '<td rowspan="2" class="vertical-align-middle">Dept Code</td>';
						arr += '<td rowspan="2" class="vertical-align-middle">Name</td>';
						arr += '<td rowspan="2" class="vertical-align-middle">Designation</td>';
						arr += '<td rowspan="2" class="vertical-align-middle">SG</td>';
						arr += '<td rowspan="2" class="vertical-align-middle">RA</td>';
						arr += '<td rowspan="2" class="vertical-align-middle">TA</td>';
						arr += '<td rowspan="2" class="vertical-align-middle">TOTAL</td>';
						arr += '<td colspan="'+(Number(cols)+1)+'">Deductions</td>';
						arr += '<td rowspan="2" class="vertical-align-middle" >Net Amount</td>';
						arr += '<td rowspan="2" class="vertical-align-middle">Landbank Acct No.</td>';
						arr +=	'</tr>';
						arr	+= '<tr style="font-weight: bold;" class="text-center">';
						if(data.rata_deduction.length !== 0){
								$.each(data.rata_deduction,function(k1,v1){
									arr += '<td>'+v1.deductions.name+'</td>';
									arr_deduction[v1.deduction_id+'_id'] = v1.deduction_id;
									arr_subtotal_deduction[v1.deduction_id+'_id']  = 0;
								})
							}
						arr	+= '<td>Total</td>';
						arr += '</tr>';
						arr += '</thead>';

						$.each(data.transaction,function(k,v){
							ra_amount = 0;
							ta_amount = 0;
							total_amount = 0;

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees.middlename) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							fullname 				= lastname+' '+firstname+' '+middlename;
							account_number = (v.employeeinfo.account_number) ? v.employeeinfo.account_number : '';
							position = (v.positions) ? v.positions.Name : '';
							deptCode = (v.employeeinfo.dept_code) ? v.employeeinfo.dept_code : '';
							salarygrade = (v.salaryinfo.salarygrade_id) ? v.salaryinfo.salarygrade.Code : '';
							ra_amount = (v.representation_amount) ? v.representation_amount : 0;
							ta_amount = (v.transportation_amount) ? v.transportation_amount : 0;

							totalAmount = (parseFloat(ra_amount) + parseFloat(ta_amount));							
							net_ra_amount += parseFloat(ra_amount);
							net_ta_amount += parseFloat(ta_amount);
							net_total_amount += parseFloat(totalAmount);

							ra_amount = (ra_amount) ? commaSeparateNumber(parseFloat(ra_amount).toFixed(2)) : '';
							ta_amount = (ta_amount) ? commaSeparateNumber(parseFloat(ta_amount).toFixed(2)) : '';
							total_amount = (totalAmount) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';
							

							arr += '<tbody>';
							arr += '<tr>';
							arr += '<td>'+ctr+'</td>';
							// arr += '<td>'+deptCode+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td>'+position+'</td>';
							arr += '<td class="text-center">'+salarygrade+'</td>';
							arr += '<td class="text-right">'+ra_amount+'</td>';
							arr += '<td class="text-right">'+ta_amount+'</td>';
							arr += '<td class="text-right">'+total_amount+'</td>';

							totalDeduction = 0;
							if(data.rata_deduction.length !== 0){
									$.each(data.rata_deduction,function(k2,v2){
										deduction = [];
										deduction_dispay = [];
										deduction_id = v2.deduction_id;
										$.each(v.deductions,function(k3,v3){
											own_deduction_id = v3.deduction_id;
											deduction_dispay['deduction_amount_'+own_deduction_id] = v3.deduction_amount;
										});

										if(deduction_id){
											if (deduction_dispay['deduction_amount_'+deduction_id]) {
												arr_subtotal_deduction[deduction_id+'_id'] += parseFloat(deduction_dispay['deduction_amount_'+deduction_id]);
												totalDeduction += Number(deduction_dispay['deduction_amount_'+deduction_id]);
											}
											deduction_dispay['deduction_amount_'+deduction_id] = (deduction_dispay['deduction_amount_'+deduction_id]) ? commaSeparateNumber(parseFloat(deduction_dispay['deduction_amount_'+deduction_id]).toFixed(2)) : '0.00';
											arr += '<td class="text-right" >'+deduction_dispay['deduction_amount_'+deduction_id]+'</td>'
										}else{
											arr += '<td class="text-right"></td>';
										}

									});
								}

							net_amount = parseFloat(totalAmount) - parseFloat(totalDeduction);
							net_deduction_amount += parseFloat(totalDeduction);
							net_amount_total += parseFloat(net_amount);

							total_deduction_amount = (totalDeduction) ? commaSeparateNumber(parseFloat(totalDeduction).toFixed(2)) : '0.00';
							net_amount = (net_amount) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '0.00';

							arr += '<td class="text-right">'+total_deduction_amount+'</td>';
							arr += '<td class="text-right">'+net_amount+'</td>';
							arr += '<td class="text-right">'+account_number+'</td>';
							arr += '</tr>';

							ctr++;
						});

						net_ra_amount = (net_ra_amount !== 0) ? commaSeparateNumber(parseFloat(net_ra_amount).toFixed(2)) : '0.00';
						net_ta_amount = (net_ta_amount !== 0) ? commaSeparateNumber(parseFloat(net_ta_amount).toFixed(2)) : '0.00';
						net_total_amount = (net_total_amount !== 0) ? commaSeparateNumber(parseFloat(net_total_amount).toFixed(2)) : '0.00';
						net_tax_amount = (net_tax_amount !== 0) ? commaSeparateNumber(parseFloat(net_tax_amount).toFixed(2)) : '0.00';
						net_ra_deduction_amount = (net_ra_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_ra_deduction_amount).toFixed(2)) : '0.00';
						net_ta_deduction_amount = (net_ta_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_ta_deduction_amount).toFixed(2)) : '0.00';
						net_deduction_amount = (net_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_deduction_amount).toFixed(2)) : '0.00';
						net_amount_total = (net_amount_total !== 0) ? commaSeparateNumber(parseFloat(net_amount_total).toFixed(2)) : '0.00';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td></td>';
						arr += '<td>Total</td>';
						// arr += '<td></td>';
						arr += '<td class="text-center"></td>';
						arr += '<td class="text-center"></td>';
						arr += '<td class="text-right">'+net_ra_amount+'</td>';
						arr += '<td class="text-right">'+net_ta_amount+'</td>';
						arr += '<td class="text-right">'+net_total_amount+'</td>';
						if(data.rata_deduction.length !== 0){
								$.each(data.rata_deduction,function(k,v){
									arr += '<td class="text-right">'+commaSeparateNumber(parseFloat(v.net_amount).toFixed(2))+'</td>';
								});
							}
						arr += '<td class="text-right">'+net_deduction_amount+'</td>';
						arr += '<td class="text-right">'+net_amount_total+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';
						arr += '</tbody>';
						$('#payroll_transfer').html(arr);

						days = daysInMonth(_Month,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod =  months[_Month]+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}

	$('#print').on('click',function(){
		$('#reports').printThis();
	});

	/*VIEW POSTED*/
$(document).on('click','.view',function(){
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)

		$('#preview').trigger('click');
	});


/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

$('.position').change(function(){
	name = $(this).find(':selected').data('selected');
	value = $(this).find(':selected').data('position');
	
	$('#'+name).val(value)
});

})
</script>
@endsection