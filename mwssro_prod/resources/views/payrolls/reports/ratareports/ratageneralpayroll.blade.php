@extends('layouts.app-ratareports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	@media print{
		.table>thead>tr>td, .table>tbody>tr>td{
			padding: 2px !important;
		}
		.vertical-align-middle{
			vertical-align: middle !important;
		}
	}

	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 2px !important;
	}
	.vertical-align-middle{
		vertical-align: middle !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row pl-4 pr-4">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select class="form-control font-style2" id="semi_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._signatory-one')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg mb-2 btn_save" style="display: inline-block;width: 30%;">
				Post
			</a>
			@include('payrolls.reports.includes._csv-button')
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>
	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:90%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports" style="width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<span class="font-weight-bold">Republic of the Philippines</span><br>
								<span class="font-weight-bold">{!! $company_name !!}</span> <br>
								<span >{!! $company->Address !!}</span><br><br>
								<span class="font-weight-bold">GENERAL PAYROLL</span> <br> <br>
								PAYMENT OF RATA FOR THE PERIOD OF <span id="month_year"></span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table">
							<thead>
								<tr class="text-center" style="font-weight: bold">
									<td rowspan="2" class="">#</td>
									<!-- <td rowspan="2" class="vertical-align-middle">Code</td> -->
									<td rowspan="2" class="vertical-align-middle" style="width: 250px !important;">Name</td>
									<td rowspan="2" class="vertical-align-middle" style="width: 250px !important;">Dept. Designation</td>
									<td rowspan="2" class="vertical-align-middle">SG</td>
									<td colspan="3" class="vertical-align-middle" style="width: 130px !important;">GAA Rates</td>
									<td rowspan="2" class="vertical-align-middle" style="width: 115px !important;">Deductions</td>
									<td rowspan="2" class="vertical-align-middle" style="width: 115px !important;">
										1/2 Month <br> Net Amount
									</td>
									<td rowspan="2" colspan="2" class="vertical-align-middle" >Landbank Acct. No.</td>
								</tr>
								<tr class="text-center">
									<td>RA</td>
									<td>TA</td>
									<td>Gross</td>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="12" style="border:none">
										<div class="col-md-6">
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12 text-center">
													<b>C E R T I F I C A T I O N</b>
												</div>
											</div>
											<div class="row" >
												<div class="col-md-6" style="text-align: justify;">
													This is to certify that the herein named
													 officials have actually incurred
													representation expenses and transportation
													 expenses during the period.
													Further certify that officials receiving
													transportation allowance for the said
													period are not actually using RP/MWSS vehicle.

												</div>
												<div class="col-md-6" style="text-align: justify;padding-left: 36px;">
													This is to certify that the herein-named
													officials have been present for at least
													six (6) days for the period <span id="signatory_date"></span>.

												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="4" style="border:none">
										<div class="mt-4">
											Certified:  (1) as to accuracy of computation (2) accounting 	<br>
											codes and journal entries are proper; (3) properly entered in 	<br>
											the accounting records.
										</div>
									</td>
									<td colspan="2" style="border:none">
										<div class="mt-4">
											Approved for appropriation and funds <br>
											being available:
										</div>
									</td >
									<td class="text-center" colspan="3" style="border:none">
								      <div class="mt-4">
								      	<span class="signatory_one font-weight-bold"></span> <br>
								      	<span class="position_one"></span>
								      </div>
									</td>
									<td style="border: none !important;" colspan="3"></td>
								</tr>
								<tr>
									<td style="border:none"></td>
									<td style="border:none"></td>
									<td colspan="2" class="text-center" style="border:none"></td>
									<td colspan="4" class="text-center" style="border:none"></td>
									<td colspan="4" class="text-center" style="border:none;padding-right: 120px;">Approved:</td>
								</tr>
								<tr>
									<td colspan="2" class="text-center" style="border:none">
										<span class="signatory_two font-weight-bold"></span> <br>
								    <span class="position_two"></span>
										RO-18-______
									</td>
									<td colspan="5"  style="border:none">
										<div class="row">
											<div class="col-md-12 text-center">
												<span class="signatory_three font-weight-bold"></span> <br>
										     <span class="position_three"></span>
											</div>
										</div>
									</td>
									<td style="border:none"></td>
									<td colspan="4" class="text-center" style="border:none">
										<span class="signatory_four font-weight-bold"></span> <br>
								     <span class="position_four"></span>
									</td>
								</tr>
							</tfoot>
							 <tbody id="tbl_body">
							 </tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
@include('payrolls.reports.includes._csv-script')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});
	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});

	$('#signatory_4').change(function(){
		signatory_four = $(this).find(':selected').text();
		position_four 	= $(this).find(':selected').data('position');

		$('.signatory_four').text(signatory_four);
		$('.position_four').text(position_four);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month && !_semiPayPeriod){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':_semiPayPeriod
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					if(data.length !== 0){

						arr = [];


						total_representation_amount = 0;
						total_transportation_amount = 0;
						total_gross_amount = 0;
						net_deduction_amount = 0;
						net_amount = 0;
						$.each(data,function(k,v){

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees.middlename) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							fullname 				= lastname+' '+firstname+' '+middlename;
							account_number = (v.employeeinfo.account_number) ? v.employeeinfo.account_number : '';

							salary_grade = (v.salaryinfo.salarygrade_id) ? v.salaryinfo.salarygrade.Code : '';
							position = (v.positions) ? v.positions.Name : '';
							deptCode = (v.employeeinfo.dept_code) ? v.employeeinfo.dept_code : '';
							total_deduction = 0;
							if(v.deductions.length > 0)
							{
								$.each(v.deductions,function(k1,v1){
									amount = (v1.deduction_amount) ? v1.deduction_amount : 0;
									total_deduction += Number(amount);
								});
							}
							

							representation_amount = (v.representation_amount) ? v.representation_amount : 0;
							transportation_amount = (v.transportation_amount) ? v.transportation_amount : 0;
							gross_amount = parseFloat(representation_amount) + parseFloat(transportation_amount);

							total_amount = parseFloat(gross_amount) - parseFloat(total_deduction);

							total_representation_amount += parseFloat(representation_amount);
							total_transportation_amount += parseFloat(transportation_amount);
							net_deduction_amount += parseFloat(total_deduction);
							total_gross_amount += parseFloat(gross_amount);
							net_amount += parseFloat(total_amount);

							representation_amount = (representation_amount) ? commaSeparateNumber(parseFloat(representation_amount).toFixed(2)) : '';
							transportation_amount = (transportation_amount) ? commaSeparateNumber(parseFloat(transportation_amount).toFixed(2)) : '';
							gross_amount = (gross_amount) ? commaSeparateNumber(parseFloat(gross_amount).toFixed(2)) : '';
							total_deduction = (total_deduction) ? commaSeparateNumber(parseFloat(total_deduction).toFixed(2)) : '';
							total_amount = (total_amount) ? commaSeparateNumber(parseFloat(total_amount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td>'+(k+1)+'</td>';
							// arr += '<td>'+deptCode+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td>'+position+'</td>';
							arr += '<td class="text-center">'+salary_grade+'</td>';
							arr += '<td class="text-right">'+representation_amount+'</td>';
							arr += '<td class="text-right">'+transportation_amount+'</td>';
							arr += '<td class="text-right">'+gross_amount+'</td>';
							arr += '<td class="text-right">'+total_deduction+'</td>';
							arr += '<td class="text-right">'+total_amount+'</td>';
							arr += '<td colspan="2" class="text-center">'+account_number+'</td>';
							arr += '</tr>';

						});
						total_transportation_amount = (total_transportation_amount !== 0) ? commaSeparateNumber(parseFloat(total_transportation_amount).toFixed(2)) : '';
						total_representation_amount = (total_representation_amount !== 0) ? commaSeparateNumber(parseFloat(total_representation_amount).toFixed(2)) : '';
						total_gross_amount = (total_gross_amount !== 0) ? commaSeparateNumber(parseFloat(total_gross_amount).toFixed(2)) : '';
						net_deduction_amount = (net_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_deduction_amount).toFixed(2)) : '';
						net_amount = (net_amount !== 0) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';
						arr += '<tr>';
						arr += '<td></td>';
						// arr += '<td></td>';
						arr += '<td><b>Total</b></td>';
						arr += '<td></td>';
						arr += '<td></td>';
						arr += '<td class="text-right"><b>'+total_representation_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+total_transportation_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+total_gross_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+net_deduction_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+net_amount+'</b></td>';
						arr += '<td colspan="2"></td>';
						arr += '</tr>';


						$('#tbl_body').html(arr);

						days = daysInMonth(_Month,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod =  months[_Month]+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);
						$('#signatory_date').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$('#print').on('click',function(){
	$('#reports').printThis();
});

/*VIEW POSTED*/
$(document).on('click','.view',function(){
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)

		$('#preview').trigger('click');
	});


/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

$('.position').change(function(){
	name = $(this).find(':selected').data('selected');
	value = $(this).find(':selected').data('position');
	
	$('#'+name).val(value)
});

})
</script>
@endsection