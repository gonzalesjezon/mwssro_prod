@extends('layouts.app-cnapeireports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
	.p-print{
		font-size: 12px !important;
	    color: #333 !important;
	    line-height: 0.6em !important;
	}
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				@include('payrolls.reports.includes._signatory-one')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
							<table class="table table2" style="border: none !important;">
							<thead >
								<tr>
									<td style="border:none !important;" colspan="12">
										<div class="row">
					       			<div class="col-md-12 text-center">
					       				<h4>GENERAL PAYROLL</h4>
					       			</div>
					       		</div>
					       		<div class="row pl-3 pr-3">
					       			<div class="col-md-12 text-left">
					       				<span>Payment of Collective Negotiation Agreement Incentive (CNA) and Productivity Enhancement Incentive (PEI) for FY 2017 per DBM BC No. 2017-3</span> <br>
					       				<span>dated 16 November 2017 and GCG MC No. 2017-05 dated 05 December 2017 respectively.</span>
					       			</div>
					       		</div>
									</td>
								</tr>
								<tr class="text-center">
									<td></td>
									<td>Name</td>
									<td>Designation</td>
									<td>SG</td>
									<td>%</td>
									<td>P25000 <br> CNA</td>
									<td>%</td>
									<td>P5000 br PEI</td>
									<td>Total</td>
									<td>Less: Angecy Fee</td>
									<td>Net Amount</td>
									<td>Land Bank <br> Accnt. No</td>
								</tr>
							</thead>
							<tfoot style="border:none;">
								<tr>
									<td colspan="12" style="border:none;">
										<div class="row">
					       			<div class="col-md-12 text-center">
					       				<span>
					       					I hereby certify that this payroll is correct and that	<br>
											the claims herein stated are for services actually	<br>
											and duly rendered as authorized by management.
					       				</span>
					       				<br><br><br>
					       				<span class=" font-weight-bold signatory_one"></span> <br>
					       				<span class="position_one"></span>
					       				<br>
					       			</div>
					       		</div>
					       		<div class="row" style="margin-top: 50px;">
					       			<div class="col-md-4" style="padding-left: 30px;">
					       				<span>
						       				Certified:  (1) as to accuracy of computation (2) accounting <br>
											codes and journal entries are proper; (3) properly entered in 	<br>
											the accounting records.
					       				</span>
					       			</div>
					       			<div class="col-md-4 text-center" style="padding-left: 20px;">
					       				<span>
					       					Approved for appropriation and funds being
											available:
					       				</span>
					       			</div>
					       			<div class="col-md-4 text-center" style="padding-left: 20px;">
					       				<span>
					       					<b>APPROVED:</b>
					       				</span>
					       			</div>
					       		</div>
					       		<div class="row" style="margin-top: 30px;">
					       			<div class="col-md-4 text-center">
					       				<p class="p-print font-weight-bold signatory_two"></p>
									    <p class="p-print position_two"></p>
									    <p class="p-print" >RO-18-______</p>

					       			</div>
					       			<div class="col-md-4 text-center">
					       				<p class="p-print font-weight-bold signatory_three"></p>
				      					<p class="p-print position_three"></p>

					       			</div>
					       			<div class="col-md-4 text-center">
					       				<p class="p-print font-weight-bold signatory_four"></p>
				               	<p class="p-print position_four"></p>
					       			</div>
					       		</div>
									</td>
								</tr>
							</tfoot>
							 <tbody id="tbl_body">
							 </tbody>
							</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});

	$('#signatory_4').change(function(){
		signatory_four = $(this).find(':selected').text();
		position_four 	= $(this).find(':selected').data('position');

		$('.signatory_four').text(signatory_four);
		$('.position_four').text(position_four);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}



	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.lenght != 0){

						arr = [];
						sub_total_cna_amount = 0;
						sub_total_pei_amount = 0;
						sub_total_amount = 0;
						sub_total_agency_amount = 0;
						sub_total_net_amount = 0;
						$.each(data.transaction,function(k,v){

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							
							positions = (v.positions) ? v.positions.Name : '';
							salarygrade = (v.salaryinfo) ? v.salaryinfo.salarygrade.Name : '';
							percentage = (v.percentage) ? v.percentage : '';
							cna_amount = (v.special) ? v.special.amount : 0;
							pei_amount = (v.amount) ? v.amount : 0;
							atmNo = (v.employeeinfo.atm_no) ? v.employeeinfo.atm_no : '';

							total = (parseFloat(cna_amount) + parseFloat(pei_amount));
							agency_fee_amount = (parseFloat(cna_amount)*0.1);
							net_amount = (parseFloat(total) - parseFloat(agency_fee_amount));

							sub_total_cna_amount += parseFloat(cna_amount);
							sub_total_pei_amount += parseFloat(pei_amount);
							sub_total_amount += parseFloat(total);
							sub_total_agency_amount += parseFloat(agency_fee_amount);
							sub_total_net_amount += parseFloat(net_amount);


							cna_amount = (cna_amount !== 0) ? commaSeparateNumber(parseFloat(cna_amount).toFixed(2)) : '';
							pei_amount = (pei_amount !== 0) ? commaSeparateNumber(parseFloat(pei_amount).toFixed(2)) : '';
							total = (total !== 0) ? commaSeparateNumber(parseFloat(total).toFixed(2)) : '';
							agency_fee_amount = (agency_fee_amount !== 0) ? commaSeparateNumber(parseFloat(agency_fee_amount).toFixed(2)) : '';
							net_amount = (net_amount !== 0) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';


							arr += '<tr>';
							arr += '<td>'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
							arr += '<td class="text-left">'+positions+'</td>';
							arr += '<td class="text-left">'+salarygrade+'</td>';
							arr += '<td class="text-left">'+percentage+'</td>';
							arr += '<td class="text-right">'+cna_amount+'</td>';
							arr += '<td class="text-right">'+percentage+'</td>';
							arr += '<td class="text-right">'+pei_amount+'</td>';
							arr += '<td class="text-right">'+total+'</td>';
							arr += '<td class="text-right">'+agency_fee_amount+'</td>';
							arr += '<td class="text-right">'+net_amount+'</td>';
							arr += '<td class="text-right">'+atmNo+'</td>';
							arr += '</tr>';
						});

						sub_total_cna_amount = (sub_total_cna_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_cna_amount).toFixed(2)) : ''
						sub_total_pei_amount = (sub_total_pei_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_pei_amount).toFixed(2)) : ''
						sub_total_amount = (sub_total_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_amount).toFixed(2)) : ''
						sub_total_agency_amount = (sub_total_agency_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_agency_amount).toFixed(2)) : ''
						sub_total_net_amount = (sub_total_net_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_net_amount).toFixed(2)) : ''

						arr += '<tr style="font-weight:bold">';
						arr += '<td></td>';
						arr += '<td class="text-left">GRAND TOTAL</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-right">'+sub_total_cna_amount+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+sub_total_pei_amount+'</td>';
						arr += '<td class="text-right">'+sub_total_amount+'</td>';
						arr += '<td class="text-right">'+sub_total_agency_amount+'</td>';
						arr += '<td class="text-right">'+sub_total_net_amount+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';





						$('#tbl_body').html(arr);

						// days = daysInMonth(_monthNumber,_Year);

						// if(_payPeriod == 'monthly'){
						// 	_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						// }else{
						// 	switch(_semiPayPeriod){
						// 		case 'firsthalf':
						// 			_coveredPeriod = _Month+' 1-15, '+_Year;
						// 		break;
						// 		default:
						// 			_coveredPeriod =_Month+' 16-'+days+', '+_Year;
						// 		break;
						// 	}
						// }

						// $('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection