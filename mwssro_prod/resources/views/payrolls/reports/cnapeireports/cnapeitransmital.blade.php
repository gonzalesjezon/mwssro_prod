@extends('layouts.app-cnapeireports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row pl-4 pr-4">
					<div class="col-md-6">
						<span>Print Date</span>
						<div class="form-group">
							<input type="text" name="print_date" class="form-control font-style2 datepicker" id="print_date">
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._transmital-signatory')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="padding-bottom: 120px;">
	       		<table class="table" style="font-family: Book Antiqua !important;border: none !important;">
	       			@include('payrolls.reports.includes._header-transmital')
	       			<tbody style="border: none !important;">
	       				<tr>
	       					<td style="border: none !important;">
					       		<div class="row">
					       			<div class="col-md-12 text-center">
					       				<h4 style="font-weight: bold;"></h4>
					       			</div>
					       		</div>
					       		<div class="row" style="margin-top: 50px;">
					       			<div class="col-md-12 text-left">
					       				<span style="font-size: 12px;padding-left: 25px;">
					       					<!-- <i>Control No. RO-18-103 (07/18 P-56)</i> -->
					       					<i>Control No. RO-19</i>
					       				</span>
					       			</div>
					       		</div>
					       		<div class="row">
					       			<div class="col-md-12 text-right" style="padding-right: 50px">
					       				<span id="month_year"></span>
					       			</div>
					       		</div>
					       		<div class="row">
					       			<div class="col-md-12 text-left" style="padding-left: 30px;">
					       				<br>
					       				<span>
						       				<b>The Manager</b>	<br>
											Landbank 	<br>
											Katipunan Branch <br>
											Quezon City	 <br>
											<br>
											Madam:
					       				</span>
					       			</div>
					       		</div>
					       		<div class="row">
					       			<div class="col-md-12" style="padding-left: 30px;">
					       				<br>
					       				<span>
							 <span style="padding-left: 50px;"></span>Please  debit  from  our  Current  Account  No. 1462-1011-78  the  amount  of  <span id="amount_word"></span> AND <span id="decimal"></span>/100 ( <span id="amount"></span>)  and  credit  the  same  to the  attached  list of  officials   with  their  corresponding Landbank Account Nos. for payment of  Collective Negotiation Agreement Incentive for the period  <span id="for_month"></span>.		<br>

					       				</span>
					       			</div>
					       		</div>
					       		<br>
					       		<div class="row" style="padding-bottom: 30px;">
					       			<div class="col-md-12 text-center" style="padding-left: 250px;">
					       				<span>
					       					Very truly yours, <br><br><br><br>
					       					<span class="font-weight-bold signatory_one"></span> <br>
					       					<span class="position_one"></span>

					       				</span>
					       			</div>
					       		</div>
					       		<div class="row">
					       			<div class="col-md-12 text-center" style="padding-bottom: 50px;">
					       				<b>APPROVED</b>
					       			</div>
					       		</div>
					       		<div class="row">
					       			<div class="col-md-6 text-center">
					       				<span class="font-weight-bold signatory_two"></span> <br>
					       				<span class="position_two"></span>
					       			</div>
					       			<div class="col-md-6 text-center">
					       				<span class="font-weight-bold signatory_three"></span> <br>
					       					<span class="position_three"></span>
					       			</div>
					       		</div>
	       					</td>
	       				</tr>
	       			</tbody>
	       			@include('payrolls.reports.includes._footer-transmital')
	       		</table>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	});
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	$('.datepicker').datepicker({
		dateFormat:'MM d, yy'
	});



	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select Year, Month, Pay Period!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					if(data.transaction.length != 0){
						net_pay = data.transaction;
						sub_total = 0;
						var sub_total_word;

						// GET DECIMAL
						decimal = data.transaction.toString().split('.');

						sub_total_word = net_pay;

						// CONVERT THE NUMBER TO WORD
						convert_to_word = toWords(sub_total_word);
						net_pay = (net_pay !== 0) ? commaSeparateNumber(parseFloat(net_pay).toFixed(2)) : '';

						decimal = (decimal.length !== 1) ? decimal[1] : '00';

						$('#decimal').text(decimal);
						$('#amount').text(net_pay);
						$('#amount_word').text(convert_to_word.toUpperCase());

						days = daysInMonth(_Month,_Year)

						// if(_payPeriod == 'monthly'){
							_coveredPeriod =  months[_Month]+' 1-'+days+', '+_Year;
						// }else{
						// 	switch(_semiPayPeriod){
						// 		case 'firsthalf':
						// 			_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
						// 		break;
						// 		default:
						// 			_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
						// 		break;
						// 	}
						// }
						// PRINT DATE
						// date = new Date();
						// current_day = date.getDate();
						print_date = $('#print_date').val();

						$('#for_month').text(_coveredPeriod);
						$('#month_year').text(print_date);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});


function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection