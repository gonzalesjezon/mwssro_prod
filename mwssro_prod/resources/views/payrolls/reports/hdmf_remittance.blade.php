@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait2.css') }}">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 960px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table table2" id="tbl_body" style="border:none !important;">
								</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					if(data.length !== 0){

						body = [];

						body += '<thead style="border:none; !important;">';
						body += '<tr>';
						body += '<td colspan="11" style="border:none !important;">';
						body += `<div class="row mb-4">
						       			<div class="col-md-12 text-center" >
						       				@include('payrolls.reports.includes._header')
													For the month of <span id="pay_period"></span>		<br>	<br>
													<span class="font-weight-bold">HDMF</span> <br>
													Schedule of Remittance
						       			</div>
						       		</div>`;
						body += '</td>';
						body += '</tr>';
						body += '<tr class="text-center">';
						body += '<td>EYERID</td>';
						body += '<td>HDMFID</td>';
						body += '<td>TIN_NO</td>';
						body += '<td>BDATE</td>';
						body += '<td>LANME</td>';
						body += '<td>FNAME</td>';
						body += '<td>MID</td>';
						body += '<td>PERCOV</td>';
						body += '<td>EE</td>';
						body += '<td>ER</td>';
						body += '<td>REM</td>';
						body += '<tr>';
						body += '</thead>';

						body += '<tbody>';
						subERShare 		 = 0;
						subTotalPagibig  = 0;
						subTotalAmount   = 0;

						$.each(data,function(k,v){

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							tin 		= (v.employees.pis_employee.TIN) ? v.employees.pis_employee.TIN: '';
							pagibig 	= (v.employees.pis_employee.PAGIBIG) ? v.employees.pis_employee.PAGIBIG: '';
							bday 		= (v.employees.pis_employee.BirthDate) ? v.employees.pis_employee.BirthDate: '';

							pagibigShare = (v.pagibig_share) ? v.pagibig_share : 0;
							mp1Amount 	 = (v.mp1_amount) ? v.mp1_amount : 0;

							totalPagibig = Number(pagibigShare) + Number(mp1Amount);
							totalAmount  = Number(totalPagibig) + Number(pagibigShare);

							subERShare += Number(pagibigShare);
							subTotalPagibig += Number(totalPagibig);
							subTotalAmount += Number(totalAmount);

							pagibig_share = (totalPagibig) ? commaSeparateNumber(Number(totalPagibig).toFixed(2)) : '0.00';
							er_share = (pagibigShare) ? commaSeparateNumber(Number(pagibigShare).toFixed(2)) : '0.00';
							total_pagibig = (totalAmount) ? commaSeparateNumber(Number(totalAmount).toFixed(2)) : '0.00';

							body += '<tr>';
							body += '<td class="text-center">04B02160 000</td>'; // EYERID
							body += '<td class="text-center">'+pagibig+'</td>'; // HDMFID
							body += '<td class="text-center">'+tin+'</td>'; // TIN NO
							body += '<td class="text-center">'+bday+'</td>'; // BIRTHDAY
							body += '<td>'+lastname+'</td>';
							body += '<td>'+firstname+'</td>';
							body += '<td>'+middlename+'</td>';
							body += '<td class="text-center">'+months[_Month]+' '+_Year+'</td>'; // PERCOV
							body += '<td class="text-right">'+pagibig_share+'</td>';
							body += '<td class="text-right">'+er_share+'</td>';
							body += '<td class="text-right">'+total_pagibig+'</td>';
							body += '</tr>';

						});

						sub_pagibig_share = (subTotalPagibig) ? commaSeparateNumber(Number(subTotalPagibig).toFixed(2)) : '0.00';
						sub_er_share = (subERShare) ? commaSeparateNumber(Number(subERShare).toFixed(2)) : '0.00';
						sub_total_pagibig = (subTotalAmount) ? commaSeparateNumber(Number(subTotalAmount).toFixed(2)) : '0.00';

						body += '<tr>';
						body += '<td class="font-weight-bold">TOTAL</td>'; // EYERID
						body += '<td></td>'; // HDMFID
						body += '<td></td>'; // TIN NO
						body += '<td></td>'; // BIRTHDAY
						body += '<td></td>';
						body += '<td></td>';
						body += '<td></td>';
						body += '<td></td>'; // PERCOV
						body += '<td class="text-right font-weight-bold">'+sub_pagibig_share+'</td>';
						body += '<td class="text-right font-weight-bold">'+sub_er_share+'</td>';
						body += '<td class="text-right font-weight-bold">'+sub_total_pagibig+'</td>';
						body += '</tr>';

						body += '</tbody>';


						$('#tbl_body').html(body);
						$('#pay_period').text(months[_Month]+' '+_Year)

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection