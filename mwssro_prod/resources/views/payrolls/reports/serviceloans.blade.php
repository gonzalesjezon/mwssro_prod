@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<style type="text/css">
	.table>thead>tr>td, .table>tbody>tr>td{
		padding: 1px !important;
		border: 1px solid #333 !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				@include('payrolls.reports.includes._transmital-signatory')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<!-- <a class="btn btn-success btn-xs btn-editbg">
				Post
			</a> -->
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;width: 1300px;margin: auto;">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table table2" id="tbl_body" style="border:none !important;">
								</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatory_one;
	var position_one;
	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	var signatory_two;
	var position_two;
	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	var signatory_three;
	var position_three;
	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction.length !== 0){

						loanCount = (data.loancount) ? data.loancount : 0
						totalCol = Number(loanCount) + 5;

						body = [];
						arr_loan = [];
						arr_subtotal_loan = [];
						ctr = 1;

						payPeriod = months[_Month]+' '+_Year;

						body += '<thead style="border:none !important;">';
						body += `<tr>
									<td style="border:none !important;" colspan="${totalCol}">
										<div class="row">
					       			<div class="col-md-12 text-center" >
					       				@include('payrolls.reports.includes._header')
												For the month of <span id="pay_period">${payPeriod}</span>		<br>	<br>
												<span class="font-weight-bold">Remittance of Service Loan Accounts</span>
					       			</div>
					       		</div>
									</td>
									</tr>`;
						body += '<tr class="text-center" style="font-weight: bold;">'
						body += '<td rowspan="2" class="border-top" >No</td>';
						body += '<td rowspan="2" class="border-top">Name</td>';
						body += '<td rowspan="2" class="border-top">BP No</td>';
						body += '<td colspan="'+loanCount+'" class="border-top">Repayment on Service Loan Accounts</td>';
						body += '<td rowspan="2" class="border-top">Sub Total</td>';
						body += '<td rowspan="2" class="border-top">Remarks</td>';
						body += '</tr>';
						body += '<tr class="text-center" style="font-weight: bold">';
						if(data.loanlists.length !== 0){
							$.each(data.loanlists,function(k1,v1){
								body += '<td>'+v1.loans.name+'</td>';
								arr_loan[v1.loan_id+'_id'] = v1.loan_id;
								arr_subtotal_loan[v1.loan_id+'_id']  = 0;
							})
						}
						body += '</tr>'
						body += '</thead>';

						subTotalLoan = 0;
						$.each(data.transaction,function(k,v){

							firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
							lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
							middlename 			= (v.employees.middlename) ? v.employees.middlename : '';
							middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
							fullname = lastname+' '+firstname+' '+middlename;
							bp_no = (v.employeeinfo.bp_no) ? v.employeeinfo.bp_no : '';

							totalLoan = (v.total_loan) ? v.total_loan : 0;


							subTotalLoan += parseFloat(totalLoan);

							total_loan = (totalLoan) ? commaSeparateNumber(parseFloat(totalLoan).toFixed(2)) : '';

							body += '<tr>';
							body += '<td>'+ctr+'</td>';
							body += '<td>'+fullname+'</td>';
							body += '<td>'+bp_no+'</td>';
							if(data.loanlists.length !== 0){
								$.each(data.loanlists,function(k2,v2){
									loan = [];
									loan_dispay = [];
									loan_id = v2.loan_id;
									$.each(v.loaninfo_transaction,function(k3,v3){
										own_loan_id = v3.loan_id;
										loan_dispay['loan_amount_'+own_loan_id] = v3.amount;
									});

									if(loan_id){
										if (loan_dispay['loan_amount_'+loan_id]) {
											arr_subtotal_loan[loan_id+'_id'] += parseFloat(loan_dispay['loan_amount_'+loan_id]);
										}
										loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '0.00';
										body += '<td class="text-right" >'+loan_dispay['loan_amount_'+loan_id]+'</td>'
									}else{
										body += '<td class="text-right"></td>';
									}

								});


							}
							body += '<td class="text-right">'+total_loan+'</td>';
							body += '<td class="text-right"></td>'; // REMARKS
							body += '</tr>';

							ctr++;

						});

						sub_total_loan = (subTotalLoan) ? commaSeparateNumber(parseFloat(subTotalLoan).toFixed(2)) : '';

						body += '<tr>';
						body += '<td></td>';
						body += '<td>TOTAL</td>';
						body += '<td></td>';
						if(data.loanlists.length !== 0){
							for (var k in arr_loan){
							    if (typeof arr_loan[k] !== 'function') {
							    	arr_subtotal_loan[arr_loan[k]+'_id'] = (arr_subtotal_loan[arr_loan[k]+'_id'] !== 0) ? commaSeparateNumber(parseFloat(arr_subtotal_loan[arr_loan[k]+'_id']).toFixed(2)) : '0.00';
							         body += '<td  class="text-right">'+arr_subtotal_loan[arr_loan[k]+'_id']+'</td>';
							         arr_subtotal_loan[arr_loan[k]+'_id'] = 0;
							    }
							}
						}
						body += '<td class="text-right">'+sub_total_loan+'</td>';
						body += '<td class="text-right"></td>'; // REMARKS
						body += '</tr>';

						signOne = (signatory_one) ? signatory_one : '';
						signTwo = (signatory_two) ? signatory_two : '';
						signThree = (signatory_three) ? signatory_three : '';

						posOne = (position_one) ? position_one : '';
						posTwo = (position_two) ? position_two : '';
						posThree = (position_three) ? position_three : '';

						body += `
						<tfoot style="border:none;">
							<tr>
								<td colspan="${totalCol}" style="border:none;">
									<div class="row mb-8 text-center">
											<div class="col-md-4 font-weight-bold">
												Prepared  by:
											</div>
											<div class="col-md-4 font-weight-bold">
												Checked & Verified:
											</div>
											<div class="col-md-4 font-weight-bold">
												Approved:
											</div>
										</div>
										<div class="row mb-1 text-center">
											<div class="col-md-4">
												<span class="signatory_one font-weight-bold">${signOne}</span> <br>
								    		<span class="position_one">${posOne}</span>
											</div>
											<div class="col-md-4">
												<span class="signatory_two font-weight-bold">${signTwo}</span> <br>
								    		<span class="position_two">${posTwo}</span>
											</div>
											<div class="col-md-4">
												<span class="signatory_three font-weight-bold">${signThree}</span> <br>
								    		<span class="position_three">${posThree}</span>
											</div>
										</div>
								</td>
							</tr>
						</tfoot>`;

						$('#tbl_body').html(body);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection