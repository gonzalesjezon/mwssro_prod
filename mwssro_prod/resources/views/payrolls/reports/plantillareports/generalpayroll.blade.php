@extends('layouts.app-plantillareports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
	.p-print{
		font-size: 12px !important;
	    color: #333 !important;
	    line-height: 0.6em !important;
	}
	.td-border-none{
		border:none !important;
	}

	@media print{
		.table>thead>tr>th, .table>tbody>tr>td{
			padding: 2px !important;
		}
		.vertical-align-middle{
			vertical-align: middle !important;
		}
	}

	.table>thead>tr>th, .table>tbody>tr>td{
		padding: 2px !important;
	}

	.vertical-align-middle{
		vertical-align: middle !important;
	}

	.tr > td{
		font-size: 7pt !important;
		font-weight: bold !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row pl-4 pr-4">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select class="form-control font-style2" id="semi_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._signatory-one')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg mb-2 btn_save" style="display: inline-block;width: 30%;">
				Post
			</a>
			@include('payrolls.reports.includes._csv-button')
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:90%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports" style="width: 1300px !important;margin: auto;">
	       		<!-- <div class="row">
	       			<div class="col-md-12 text-left" style="font-weight: bold">
	       				<img src="{{url('/images/mwssreportheader.png')}}" style="height: 50px;margin-top: 20px;"> <br>
	       			</div>
	       		</div> -->
	       		<div class="row" style="margin-left:15px !important;">
	       			<div class="col-md-12">
								<table class="table" style="border:none !important;font-family: Book Antiqua;">
									<thead class="text-center" style="font-weight: bold;border-top: none;">
										<tr>
											<td colspan="10" style="border: none !important;border-bottom: 1px solid silver !important;">
												<div class="row" style="margin-left: -5px;">
													<div class="col-md-12 text-center">
														<h4 class="font-weight-bold">GENERAL PAYROLL</h4>
													</div>
												</div>
												<div class="row" style="margin-left: -5px;">
													<div class="col-md-12 text-left">
														<h5 class="font-weight-bold mb-1 pb-1">Office/Department: Regulatory Office</h5>
														<span>Payment of salaries and allowances for the period <span id="month_year"></span></span>

													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td ></td>
											<td style="width: 20em !important;"></td>
											<td style="width: 25em !important;"></td>
											<td></td>
											<td>Basic</td>
											<td>Gross</td>
											<td>Total</td>
											<td>Less/Add:</td>
											<td>1/2 Month</td>
											<td>Land Bank</td>
										</tr>
										<tr>
											<td></td>
											<td>Names in Full</td>
											<td>Designation</td>
											<td>SG</td>
											<td>Salary</td>
											<td>Income</td>
											<td>Deductions</td>
											<td>Other Deduction</td>
											<td>Net Amount</td>
											<td>Accnt. No</td>
										</tr>
									</thead>
								<tfoot style="border:none !important;" >
									<tr>
										<td colspan="10" class="td-border-none">
											<div class="row">
								       			<div class="col-md-12 text-center">
								       				<span>
								       					I hereby certify that this payroll is correct and that	<br>
														the claims herein stated are for services actually	<br>
														and duly rendered as authorized by management.
								       				</span>
								       				<br><br><br>
								       				<span class=" font-weight-bold signatory_one"></span> <br>
								       				<span class="position_one"></span>
								       				<br>
								       			</div>
								       		</div>
								       		<div class="row" style="margin-top: 50px;">
								       			<div class="col-md-4" style="padding-left: 30px;">
								       				<span>
									       				Certified:  (1) as to accuracy of computation (2) accounting <br>
														codes and journal entries are proper; (3) properly entered in 	<br>
														the accounting records.
								       				</span>
								       			</div>
								       			<div class="col-md-4 text-center" style="padding-left: 20px;">
								       				<span>
								       					Approved for appropriation and funds being
														available:
								       				</span>
								       			</div>
								       			<div class="col-md-4 text-center" style="padding-left: 20px;">
								       				<span>
								       					<b>APPROVED:</b>
								       				</span>
								       			</div>
								       		</div>
								       		<div class="row" style="margin-top: 30px;">
								       			<div class="col-md-4 text-center">
								       				<p class="p-print font-weight-bold signatory_two"></p>
												    <p class="p-print position_two"></p>
												    <p class="p-print" >RO-18-______</p>

								       			</div>
								       			<div class="col-md-4 text-center">
								       				<p class="p-print font-weight-bold signatory_three"></p>
							      					<p class="p-print position_three"></p>

								       			</div>
								       			<div class="col-md-4 text-center">
								       				<p class="p-print font-weight-bold signatory_four"></p>
							               	<p class="p-print position_four"></p>

								       			</div>
								       		</div>
										</td>
									</tr>
								</tfoot>
								 <tbody id="tbl_body">
								 </tbody>
								</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
@include('payrolls.reports.includes._csv-script')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$('#signatory_1').change(function(){
		signatory_one = $(this).find(':selected').text();
		position_one 	= $(this).find(':selected').data('position');

		$('.signatory_one').text(signatory_one);
		$('.position_one').text(position_one);
	});

	$('#signatory_2').change(function(){
		signatory_two = $(this).find(':selected').text();
		position_two 	= $(this).find(':selected').data('position');

		$('.signatory_two').text(signatory_two);
		$('.position_two').text(position_two);
	});

	$('#signatory_3').change(function(){
		signatory_three = $(this).find(':selected').text();
		position_three 	= $(this).find(':selected').data('position');

		$('.signatory_three').text(signatory_three);
		$('.position_three').text(position_three);
	});

	$('#signatory_4').change(function(){
		signatory_four = $(this).find(':selected').text();
		position_four 	= $(this).find(':selected').data('position');

		$('.signatory_four').text(signatory_four);
		$('.position_four').text(position_four);
	});


	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}



	$(document).on('click','#preview',function(){
		if(!_Year && !_Month && !_semiPayPeriod){
			Swal.fire({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':_semiPayPeriod
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data !== ""){
						arr = [];
						ctr = 1;

						netBasic 	 = 0;
						netGross 	 = 0;
						netDeduction = 0;
						netPay 		 = 0;
						netOtherDeduct = 0;

						$.each(data.transaction,function(k1,v1){

							arr += '<tr>';
							arr += '<td></td>';
							arr += '<td class="text-center font-weight-bold">'+k1+'</td>';
							arr += '<td colspan="18"></td>';
							arr += '</tr>';

							subBasic 	 = 0;
							subGross 	 = 0;
							subDeduction = 0;
							subNet 		 = 0;
							subOtherDeduct = 0;

							$.each(v1,function(k2,v2){

								subDeptBasic 	 = 0;
								subDeptGross 	 = 0;
								subDeptDeduction = 0;
								subDeptNet 		 = 0;
								subDeptOtherDeduct = 0;

								$.each(v2,function(k,v){
									firstname 			= (v.employees.firstname) ? v.employees.firstname : '';
									lastname 				= (v.employees.lastname) ? v.employees.lastname + ',' : '';
									middlename 			= (v.employees.middlename) ? v.employees.middlename : '';
									middlename 			= (middlename) ? middlename.substring(-1,1) + '.' : '';
									positions 			= (v.positions) ? v.positions.Name : '';
									salarygrade 		= (v.salaryinfo.salarygrade) ? 'SG '+v.salaryinfo.salarygrade.Code : '';
									account_number 		= (v.employeeinfo.account_number) ? v.employeeinfo.account_number : "";
									basicAmount 		= (v.total_basicpay_amount) ? v.total_basicpay_amount : 0;
									totalOtherDeduct 	= (v.total_otherdeduct) ? v.total_otherdeduct : 0;
									totalDeduction		= (v.net_deduction) ? v.net_deduction : 0;
									grossPay 			= (v.gross_pay) ? v.gross_pay : 0;
									totalPay 			= (v.net_pay) ? v.net_pay : 0;

									otherDeduction = 0;
									if(v.additional_deduction.lenght != 0)
									{
										$.each(v.additional_deduction,function(k1,v1){
											otherDeduction += Number(v1.amount);
										});
									}

									switch(_semiPayPeriod){
										case 'firsthalf':
											totalPay = (v.first_net) ? v.first_net : 0;
											break;
										case 'secondhalf':
											totalPay = (v.second_net) ? v.second_net : 0;
											break;
									}
									

									// totalDeduction = (parseFloat(totalDeduction) - parseFloat(totalOtherDeduct));

									subDeptBasic += Number(basicAmount);
									subDeptGross += Number(grossPay);
									subDeptDeduction += Number(totalDeduction);
									subDeptOtherDeduct += Number(otherDeduction);
									subDeptNet += Number(totalPay);

									basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '0.00';
									gross_income = (grossPay) ? commaSeparateNumber(parseFloat(grossPay).toFixed(2)) : '0.00';
									total_deduction = (totalDeduction) ? commaSeparateNumber(parseFloat(totalDeduction).toFixed(2)) : '0.00';
									otherdeduct_amount = (otherDeduction) ? commaSeparateNumber(parseFloat(otherDeduction).toFixed(2)) : '0.00';
									net_amount = (totalPay) ? commaSeparateNumber(parseFloat(totalPay).toFixed(2)) : '0.00';

									arr += '<tr>';
									arr += '<td>'+ctr+'</td>';
									arr += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
									arr += '<td class="text-left">'+positions+'</td>';
									arr += '<td class="text-center">'+salarygrade+'</td>';
									arr += '<td class="text-right">'+basic_amount+'</td>';
									arr += '<td class="text-right">'+gross_income+'</td>';
									arr += '<td class="text-right">'+total_deduction+'</td>';
									arr += '<td class="text-right">'+otherdeduct_amount+'</td>';
									arr += '<td class="text-right">'+net_amount+'</td>';
									arr += '<td class="text-right">'+account_number+'</td>';
									arr += '</tr>';

									ctr++;

								});

								subBasic += Number(subDeptBasic);
								subGross += Number(subDeptGross);
								subDeduction += Number(subDeptDeduction);
								subNet += Number(subDeptNet);
								subOtherDeduct += Number(subDeptOtherDeduct);

								dept_basic = (subDeptBasic) ? commaSeparateNumber(parseFloat(subDeptBasic).toFixed(2)) : '0.00';
								dept_gross = (subDeptGross) ? commaSeparateNumber(parseFloat(subDeptGross).toFixed(2)) : '0.00';
								dept_deduction = (subDeptDeduction) ? commaSeparateNumber(parseFloat(subDeptDeduction).toFixed(2)) : '0.00';
								dept_net = (subDeptNet) ? commaSeparateNumber(parseFloat(subDeptNet).toFixed(2)) : '0.00';
								dept_other_deduct = (subDeptOtherDeduct) ? commaSeparateNumber(parseFloat(subDeptOtherDeduct).toFixed(2)) : '0.00';

								arr += '<tr class="tr">';
								arr += '<td class="text-left"></td>';
								arr += '<td class="font-weight-bold">'+k2+' Dept. TOTAL</td>';
								arr += '<td class="text-left"></td>';
								arr += '<td class="text-center"></td>';
								arr += '<td class="text-right">'+dept_basic+'</td>';
								arr += '<td class="text-right">'+dept_gross+'</td>';
								arr += '<td class="text-right">'+dept_deduction+'</td>';
								arr += '<td class="text-right">'+dept_other_deduct+'</td>';
								arr += '<td class="text-right">'+dept_net+'</td>';
								arr += '<td class="text-right"></td>';
								arr += '</tr>';

							});

							netBasic += Number(subBasic);
							netGross += Number(subGross);
							netDeduction += Number(subDeduction);
							netPay += Number(subNet);
							netOtherDeduct += Number(subOtherDeduct);

							sub_basic = (subBasic) ? commaSeparateNumber(parseFloat(subBasic).toFixed(2)) : '0.00';
							sub_gross = (subGross) ? commaSeparateNumber(parseFloat(subGross).toFixed(2)) : '0.00';
							sub_deduction = (subDeduction) ? commaSeparateNumber(parseFloat(subDeduction).toFixed(2)) : '0.00';
							sub_net = (subNet) ? commaSeparateNumber(parseFloat(subNet).toFixed(2)) : '0.00';
							sub_other_deduct = (subOtherDeduct) ? commaSeparateNumber(parseFloat(subOtherDeduct).toFixed(2)) : '0.00';

							arr += '<tr>';
							arr += '<td class="text-left"></td>';
							arr += '<td class="font-weight-bold">AREA TOTAL</td>';
							arr += '<td class="text-left"></td>';
							arr += '<td class="text-center"></td>';
							arr += '<td class="text-right">'+sub_basic+'</td>';
							arr += '<td class="text-right">'+sub_gross+'</td>';
							arr += '<td class="text-right">'+sub_deduction+'</td>';
							arr += '<td class="text-right">'+sub_other_deduct+'</td>';
							arr += '<td class="text-right">'+sub_net+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '</tr>';

						});

						net_basic = (netBasic) ? commaSeparateNumber(parseFloat(netBasic).toFixed(2)) : '0.00';
						net_gross = (netGross) ? commaSeparateNumber(parseFloat(netGross).toFixed(2)) : '0.00';
						net_deduction = (netDeduction) ? commaSeparateNumber(parseFloat(netDeduction).toFixed(2)) : '0.00';
						net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '0.00';
						net_other = (netOtherDeduct) ? commaSeparateNumber(parseFloat(netOtherDeduct).toFixed(2)) : '0.00';

						arr += '<tr>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="font-weight-bold">GRAND TOTAL</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-center"></td>';
						arr += '<td class="text-right">'+net_basic+'</td>';
						arr += '<td class="text-right">'+net_gross+'</td>';
						arr += '<td class="text-right">'+net_deduction+'</td>';
						arr += '<td class="text-right">'+net_other+'</td>';
						arr += '<td class="text-right">'+net_pay+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';


						$('#tbl_body').html(arr);

						days = daysInMonth(_Month,_Year);

						if(_payPeriod == 'monthly'){
							_coveredPeriod =  months[_Month]+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});


function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}


$('#print').on('click',function(){
	$('#reports').printThis();
});

/*VIEW POSTED*/
$(document).on('click','.view',function(){
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)

		$('#preview').trigger('click');
	});


/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

$('.position').change(function(){
	name = $(this).find(':selected').data('selected');
	value = $(this).find(':selected').data('position');
	
	$('#'+name).val(value)
});

})
</script>
@endsection