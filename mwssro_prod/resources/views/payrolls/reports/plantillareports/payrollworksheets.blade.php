@extends('layouts.app-plantillareports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">
<style type="text/css">
	@media print{
		.table>thead>tr>td, .table>tbody>tr>td{
			padding: 2px !important;
			border: 1px solid #333 !important;
		}
		.vertical-align-middle{
			vertical-align: middle !important;
		}
	}

	.table2>thead>tr>td, .table2>tbody>tr>td{
		padding: 2px !important;
		border: 1px solid #333 !important;
	}

	.vertical-align-middle{
		vertical-align: middle !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes._template-one')
			</td>
		</tr>
		<tr>
			<td>
				@include('payrolls.reports.includes._months-year')
				<div class="row p-4">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select class="form-control font-style2" id="semi_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<span>Select part</span>
						<div class="form-group">
							<select class="form-control font-style2" id="select_part" name="part">
								<option value=""></option>
								<option value="part_1">Part 1</option>
								<option value="part_2">Part 2</option>
								<option value="part_3">Part 3</option>
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg btn_save" style="width: 30%;">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%">
	    <div class="mypanel border0" style="height:90%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="font-family: Book Antiqua !important;">
						<table class="table table2" id="payroll_transfer" style="width: 1300px !important;margin: auto;border: none !important;">
						</table>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){
		let part = $('#select_part :selected').val();

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getPayrollWorksheetReport',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':_semiPayPeriod
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				
					if(data.length !== 0){

						arr = [];

						days = daysInMonth(_Month,_Year)

						switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
								break;
							}


						arr += `<thead class="border-0">`;
						switch(part){
							case 'part_1':
								arr += `<tr>
										<td colspan="14" style="border:none !important;border-bottom:1px solid #333 !important;">
											<div class="row pl-2">
						       			<div class="col-md-6">
						       				<h5><b>Payroll Worksheet</b></h5>
						       				<h6><b>Payment of salaries and allowances for the period:</b> <span id="month_year">`+_coveredPeriod+`</span></h6>
						       			</div>
						       			<div class="col-md-6"></div>
						       		</div>
					       		</td>
				       		</tr>
				       		<tr>
										<td colspan="14">&nbsp;</td>
									</tr>
									<tr class="text-center" style="font-weight:bold" >
										<td rowspan="3" class="vertical-align-middle" >#</td>
										<td rowspan="3" class="vertical-align-middle" >Name</td>
										<td rowspan="3" class="vertical-align-middle" >Designation</td>
										<td rowspan="3" class="vertical-align-middle" >SG</td>
										<td rowspan="3" class="vertical-align-middle" >Basic Salary</td>
										<td rowspan="3" class="vertical-align-middle" >PERA</td>
										<td rowspan="3" class="vertical-align-middle" >GROSS</td>
										<td colspan="2">GSIS</td>
										<td colspan="1">GSIS</td>
										<td colspan="2">PAGIBIG</td>
										<td colspan="2">PHILHEALTH</td>
									<tr>
									<tr class="text-center" style="font-weight:bold">
										<td >PS</td>
										<td >GS</td>
										<td >EC</td>
										<td >PS</td>
										<td >GS</td>
										<td >PS</td>
										<td >GS</td>
									</tr>`;

								break;
							case 'part_2':
								arr += `<tr>
										<td colspan="17" style="border:none !important;border-bottom:1px solid #333 !important;">
											<div class="row pl-2">
						       			<div class="col-md-6">
						       				<h5><b>Payroll Worksheet</b></h5>
						       				<h6><b>Payment of salaries and allowances for the period:</b> <span id="month_year">`+_coveredPeriod+`</span></h6>
						       			</div>
						       			<div class="col-md-6"></div>
						       		</div>
					       		</td>
				       		</tr>
								<tr>
									<td colspan="17">&nbsp;</td>
									</tr>
									<tr class="text-center" style="font-weight:bold" >
										<td rowspan="3" class="vertical-align-middle" >#</td>
										<td rowspan="3" class="vertical-align-middle" >Name</td>
										<td colspan="7">GSIS</td>
										<td colspan="2">PAGIBIG</td>
										<td colspan="2">Welfare Fund</td>
										<td rowspan="3" class="vertical-align-middle" >Legal Fees</td>
										<td colspan="1">WASSSLAI</td>
										<td colspan="1">WASSSLAI</td>
										<td rowspan="3" class="vertical-align-middle" >Fortunecare</td>
									<tr>
									<tr class="text-center">
										<td>Opt. Ins.</td>
										<td>Opt Loan</td>
										<td>Consoloan</td>
										<td>Policy Loan</td>
										<td>Emerg Loan</td>
										<td>LCH-DCS</td>
										<td>Educ Loan</td>
										<td>MPL</td>
										<td>Pagibig II</td>
										<td>PS</td>
										<td>GS</td>
										<td>Dep</td>
										<td>Loan</td>
									</tr>`;

								break;
							case 'part_3':
							arr += `<tr>
										<td colspan="17" style="border:none !important;border-bottom:1px solid #333 !important;">
											<div class="row pl-2">
						       			<div class="col-md-6">
						       				<h5><b>Payroll Worksheet</b></h5>
						       				<h6><b>Payment of salaries and allowances for the period:</b> <span id="month_year">`+_coveredPeriod+`</span></h6>
						       			</div>
						       			<div class="col-md-6"></div>
						       		</div>
					       		</td>
				       		</tr>
				       		<tr>
												<td colspan="12">&nbsp;</td>
												<td colspan="2" class="text-center">Add/Less</td>
												<td colspan="3"></td>
											</tr>
											<tr class="text-center">
												<td rowspan="3" class="vertical-align-middle" >#</td>
												<td rowspan="3" class="vertical-align-middle" >Name</td>
												<td rowspan="2" >Tax</td>
												<td colspan="2">Multi-Purpose Coop</td>
												<td rowspan="2" class="vertical-align-middle" >Union Dues</td>
												<td rowspan="2" class="vertical-align-middle" >MPLP Ln</td>
												<td rowspan="2" class="vertical-align-middle" >SSS <br> Contribution</td>
												<td rowspan="2" class="vertical-align-middle" >Total <br> Deductions</td>
												<td rowspan="2" class="vertical-align-middle" >Net Amount</td>
												<td rowspan="2" class="vertical-align-middle" >1/2 mo. Pay</td>
												<td rowspan="2" class="vertical-align-middle" >Amount <br> GSIS-HL</td>
												<td rowspan="2" class="vertical-align-middle" >Travel <br> Allowance</td>
												<td rowspan="2" class="vertical-align-middle" >Telephone Bills</td>
												<td rowspan="2" class="vertical-align-middle" >Net Pay</td>
												<td rowspan="2" class="vertical-align-middle" nowrap >Landbank Acct. No.</td>
											</tr>
											<tr>
												<td>Contribution</td>
												<td>Loan</td>
											</tr>`;

								break;
						}

									`</thead>`
						arr += `<tbody>`;

						var net_salary 				 = 0;
						var net_pera 				 = 0;
						var net_gross_pay 			 = 0;
						var net_gsis_ps 			 = 0;
						var net_gsis_gs 			 = 0;
						var net_pagibig_ps 			 = 0;
						var net_pagibig_gs 			 = 0;
						var net_philhealth_ps 		 = 0;
						var net_philhealth_gs 		 = 0;
						var net_pagibig_two_amount 	 = 0;
						var net_tax_contribution 	 = 0;
						var net_consoloan_amount 	 = 0;
						var net_policy_amount 	 	 = 0;
						var net_optional_life_amount = 0;
						var net_education_loan_amount = 0;
						var net_calamity_amount 	 = 0;
						var net_deduction 	 	 	 = 0;
						var net_pay 		 	 	 = 0;
						var net_loans 		 	 	 = 0;
						var net_one_half_net_pay 	 = 0;
						var net_lch_loan 			 = 0;
						var net_wf_loan 			 = 0;
						var net_wasslai_loan 		 = 0;
						var net_mwss_loan 		 	 = 0;
						var net_multi_purpose_loan 	 = 0;
						var net_sss_cont_amount  	 = 0;
						var net_mpcc_amount  		 = 0;
						var net_ud_amount  			 = 0;
						var net_wf_amount  			 = 0;
						var net_wd_amount  			 = 0;
						var net_ecc_amount 			 = 0;
						var netFortuneCare 			 = 0;
						var netLegaFee 				 = 0;
						var netTelBill				 = 0;
						var  netNetPay = 0;
						var netMPLPLoan = 0;

						$.each(data,function(k,v){
							var multi_purpose_loan 	 	 = 0;
							var consoloan_amount 		 = 0;
							var policy_amount 	 		 = 0;
							var optional_life_amount 	 = 0;
							var education_loan_amount 	 = 0;
							var calamity_amount 	 	 = 0;
							var lch_loan  			 	 = 0;
							var totalDeduction 		 = 0;
							var totalNetPay			 = 0;
							var total_loans 			 = 0;
							var one_half_net_pay  		 = 0;
							var wf_loan  		 		 = 0;
							var wasslai_loan  		 	 = 0;
							var mwss_loan 				 = 0;
							var sss_cont_amount  		 = 0;
							var mpcc_amount  			 = 0;
							var ud_amount  				 = 0;
							var wf_amount  				 = 0;
							var wd_amount  				 = 0;
							var ecc_amount 				 = 0;
							var fortuneCare 			 = 0;
							var legalFee 				 = 0;
							var telBill = 0;
							var mplpLoan = 0;

							lastname 		= (v.employees.lastname) ? v.employees.lastname+',' : '';
							firstname 	= (v.employees.firstname) ? v.employees.firstname : '';
							middlename 	= (v.employees.middlename) ? v.employees.middlename : '';
							middlename 	= (middlename) ? middlename.substring(-1,1) + '.' : '';
							fullname = lastname+' '+firstname+' '+middlename
							salarygrade = (v.salaryinfo) ? 'SG '+v.salaryinfo.salarygrade.Code : '';
							position = (v.positions) ? v.positions.Name : '';

							account_number = (v.employeeinfo.account_number) ? v.employeeinfo.account_number : '';

							monthly_rate_amount = (v.total_basicpay_amount) ? v.total_basicpay_amount : 0;
							net_salary += parseFloat(monthly_rate_amount);

							pera_amount = (v.benefit_transactions) ? v.benefit_transactions.amount : 0;
							net_pera += parseFloat(pera_amount);

							gross_pay = (v.gross_pay) ? v.gross_pay : 0;
							net_gross_pay += parseFloat(gross_pay);

							gsis_ps = (v.gsis_ee_share) ? v.gsis_ee_share : 0;
							net_gsis_ps += parseFloat(gsis_ps);

							gsis_gs = (v.gsis_er_share) ? v.gsis_er_share : 0;
							net_gsis_gs += parseFloat(gsis_gs);

							mp1Amount  		= (v.mp1_amount) ? v.mp1_amount : 0;
							pagibigShare 	= (v.pagibig_share) ? v.pagibig_share : 0;
							pagibig_ps 		 = Number(mp1Amount) + Number(pagibigShare);

							net_pagibig_ps += Number(pagibig_ps);

							pagibig_gs = (v.employeeinfo.er_pagibig_share) ? v.employeeinfo.er_pagibig_share : 0;
							net_pagibig_gs += parseFloat(pagibig_gs);

							philhealth_ps = (v.phic_share) ? v.phic_share : 0;
							net_philhealth_ps += parseFloat(philhealth_ps);

							philhealth_gs = (v.employeeinfo.er_philhealth_share) ? v.employeeinfo.er_philhealth_share : 0;
							net_philhealth_gs += parseFloat(philhealth_gs);

							pagibig_two_amount = (v.mp2_amount) ? v.mp2_amount : 0;
							net_pagibig_two_amount += parseFloat(pagibig_two_amount);

							tax_contribution = (v.tax_amount) ? v.tax_amount : 0;
							net_tax_contribution += parseFloat(tax_contribution);


							if(v.loaninfo_transaction.length !== 0){
								$.each(v.loaninfo_transaction,function(key,val){
									if(val.loans.name){
										switch(val.loans.code){
											case 'CSL':
												consoloan_amount = (val.amount) ? val.amount : 0
											break;
											case 'PL':
												policy_amount = (val.amount) ? val.amount : 0 ;
											break;
											case 'OLI':
												optional_life_amount = (val.amount) ? val.amount : 0 ;
											break;
											case 'EA':
												education_loan_amount = (val.amount) ? val.amount : 0 ;
											break;
											case 'EL':
												calamity_amount = (val.amount) ? val.amount : 0 ;
											break;
											case 'MPL':
												multi_purpose_loan = (val.amount) ? val.amount : 0 ;
											break;
											case 'LCH-DCS':
												lch_loan = (val.amount) ? val.amount : 0 ;
											break;
											case 'WFLOAN':
												wf_loan = (val.amount) ? val.amount : 0 ;
											break;
											case 'WASSSLAILOAN':
												wasslai_loan = (val.amount) ? val.amount : 0 ;
											break;
											case 'MPCLOAN':
												mwss_loan = (val.amount) ? val.amount : 0 ;
											break;

											case 'MPLPLOAN':
												mplpLoan = (val.amount) ? val.amount : 0 ;
											break;
										}
									}
								})
								consoloan_amount = (consoloan_amount) ? consoloan_amount : 0;
								net_consoloan_amount += parseFloat(consoloan_amount);

								policy_amount = (policy_amount) ? policy_amount : 0;
								net_policy_amount += parseFloat(policy_amount);

								education_loan_amount = (education_loan_amount) ? education_loan_amount : 0;
								net_education_loan_amount += parseFloat(education_loan_amount);

								calamity_amount = (calamity_amount) ? calamity_amount : 0;
								net_calamity_amount += parseFloat(calamity_amount);

								net_lch_loan += parseFloat(lch_loan);
								net_wf_loan  += parseFloat(wf_loan);
								net_wasslai_loan  += parseFloat(wasslai_loan);
								net_mwss_loan  += parseFloat(mwss_loan);
								net_multi_purpose_loan  += parseFloat(multi_purpose_loan);
								net_optional_life_amount  += parseFloat(optional_life_amount);
								netMPLPLoan += Number(mplpLoan);

								// total_loans = (parseInt(consoloan_amount) + parseInt(policy_amount) + parseInt(education_loan_amount)  + parseInt(calamity_amount) + parseFloat(lch_loan) + parseFloat(wf_loan) + parseFloat(wasslai_loan) + parseFloat(mwss_loan) + parseFloat(multi_purpose_loan) + parseFloat(optional_life_amount)) ;

								total_loans = (v.total_loan) ? v.total_loan : 0;

								net_loans += parseFloat(total_loans);
							}

							payPeriod = "";
							if(v.deductioninfo_transaction.length !== 0){
								$.each(v.deductioninfo_transaction,function(k1,v1){
									if(v1.deductions.code){
										switch(v1.deductions.code){
											case 'SSS':
												sss_cont_amount = (v1.amount) ? v1.amount : 0
											break;
											case 'MPCC':
												mpcc_amount = (v1.amount) ? v1.amount : 0 ;
											break;
											case 'UD':
												ud_amount = (v1.amount) ? v1.amount : 0 ;
											break;
											case 'WF':
												wf_amount = (v1.amount) ? v1.amount : 0 ;
											break;
											case 'WD':
												wd_amount = (v1.amount) ? v1.amount : 0 ;
											break;
											case 'FC':
												fortuneCare = (v1.amount) ? v1.amount : 0 ;
											break;

											case 'LO':
												legalFee = (v1.amount) ? v1.amount : 0 ;
											break;

											case 'TELBILL':
												telBill = (v1.amount) ? v1.amount : 0 ;
												
											break;
										}

										payPeriod = (v1.pay_period) ? v1.pay_period : "";
									}
								})




								net_sss_cont_amount += parseFloat(sss_cont_amount);
								net_mpcc_amount += parseFloat(mpcc_amount);
								net_ud_amount += parseFloat(ud_amount);
								net_wf_amount += parseFloat(wf_amount);
								net_wd_amount += parseFloat(wd_amount);
								netFortuneCare += parseFloat(fortuneCare);
								netLegaFee += Number(legalFee);
								netTelBill += Number(telBill);

							}


							
							// if(v.additional_deduction.length !== 0){
							// 	$.each(v.additional_deduction,function(k1,v1){
							// 		if(v1.deductions.code){
							// 			switch(v1.deductions.code){
											
							// 			}
							// 		}
							// 	})

							// 	netTelBill += Number(telBill);

							// }



							ecc_amount = (v.ecc_amount) ? v.ecc_amount : 0;
							net_ecc_amount += parseFloat(ecc_amount);


							totalDeduction = (v.net_deduction) ? v.net_deduction : 0;
							net_deduction += Number(totalDeduction);

							totalNetPay = (v.net_pay) ? v.net_pay : 0;
							net_pay += parseFloat(totalNetPay);

							halfNet = 0;
							switch(_semiPayPeriod){
								case 'firsthalf':
									halfNet = (v.first_net) ? v.first_net : 0;
									break;
								case 'secondhalf':
									halfNet = (v.second_net) ? v.second_net : 0;
									break;
							}

							one_half_net_pay = Number(totalNetPay) / 2;
							net_one_half_net_pay += parseFloat(one_half_net_pay);

							if(payPeriod != _semiPayPeriod)
							{
								telBill = 0;
							}

							netNetPay += Number(halfNet);

							monthly_rate_amount = (monthly_rate_amount) ? commaSeparateNumber(parseFloat(monthly_rate_amount).toFixed(2)) : '0.00';
							pera_amount = (pera_amount) ? commaSeparateNumber(parseFloat(pera_amount).toFixed(2)) : '0.00';
							sss_cont_amount = (sss_cont_amount) ? commaSeparateNumber(parseFloat(sss_cont_amount).toFixed(2)) : '0.00';
							mpcc_amount = (mpcc_amount) ? commaSeparateNumber(parseFloat(mpcc_amount).toFixed(2)) : '0.00';
							ud_amount = (ud_amount) ? commaSeparateNumber(parseFloat(ud_amount).toFixed(2)) : '0.00';
							wf_amount = (wf_amount) ? commaSeparateNumber(parseFloat(wf_amount).toFixed(2)) : '0.00';
							wd_amount = (wd_amount) ? commaSeparateNumber(parseFloat(wd_amount).toFixed(2)) : '0.00';
							wf_loan = (wf_loan) ? commaSeparateNumber(parseFloat(wf_loan).toFixed(2)) : '0.00';
							gross_pay = (gross_pay) ? commaSeparateNumber(parseFloat(gross_pay).toFixed(2)) : '0.00';
							gsis_ps = (gsis_ps) ? commaSeparateNumber(parseFloat(gsis_ps).toFixed(2)) : '0.00';
							gsis_gs = (gsis_gs) ? commaSeparateNumber(parseFloat(gsis_gs).toFixed(2)) : '0.00';
							pagibig_ps = (pagibig_ps) ? commaSeparateNumber(parseFloat(pagibig_ps).toFixed(2)) : '0.00';
							pagibig_gs = (pagibig_gs) ? commaSeparateNumber(parseFloat(pagibig_gs).toFixed(2)) : '0.00';
							philhealth_ps = (philhealth_ps) ? commaSeparateNumber(parseFloat(philhealth_ps).toFixed(2)) : '0.00';
							philhealth_gs = (philhealth_gs) ? commaSeparateNumber(parseFloat(philhealth_gs).toFixed(2)) : '0.00';
							consoloan_amount = (consoloan_amount) ? commaSeparateNumber(parseFloat(consoloan_amount).toFixed(2)) : '0.00';
							policy_amount = (policy_amount) ? commaSeparateNumber(parseFloat(policy_amount).toFixed(2)) : '0.00';
							optional_life_amount = (optional_life_amount) ? commaSeparateNumber(parseFloat(optional_life_amount).toFixed(2)) : '0.00';
							education_loan_amount = (education_loan_amount) ? commaSeparateNumber(parseFloat(education_loan_amount).toFixed(2)) : '0.00';
							calamity_amount = (calamity_amount) ? commaSeparateNumber(parseFloat(calamity_amount).toFixed(2)) : '0.00';
							lch_loan = (lch_loan) ? commaSeparateNumber(parseFloat(lch_loan).toFixed(2)) : '0.00';
							wf_loan = (wf_loan) ? commaSeparateNumber(parseFloat(wf_loan).toFixed(2)) : '0.00';
							wasslai_loan = (wasslai_loan) ? commaSeparateNumber(parseFloat(wasslai_loan).toFixed(2)) : '0.00';
							mwss_loan = (mwss_loan) ? commaSeparateNumber(parseFloat(mwss_loan).toFixed(2)) : '0.00';
							multi_purpose_loan = (multi_purpose_loan) ? commaSeparateNumber(parseFloat(multi_purpose_loan).toFixed(2)) : '0.00';
							pagibig_two_amount = (pagibig_two_amount) ? commaSeparateNumber(parseFloat(pagibig_two_amount).toFixed(2)) : '0.00';
							tax_contribution= (tax_contribution) ? commaSeparateNumber(parseFloat(tax_contribution).toFixed(2)) : '0.00';
							total_deduction = (totalDeduction) ? commaSeparateNumber(parseFloat(totalDeduction).toFixed(2)) : '0.00';
							total_net_pay = (totalNetPay) ? commaSeparateNumber(parseFloat(totalNetPay).toFixed(2)) : '0.00';
							one_half_net_pay = (one_half_net_pay) ? commaSeparateNumber(parseFloat(one_half_net_pay).toFixed(2)) : '0.00';
							ecc_amount = (ecc_amount !== 0) ? ecc_amount : '0.00';

							fortune_care = (fortuneCare) ? commaSeparateNumber(parseFloat(fortuneCare).toFixed(2)) : '0.00';
							legal_fee = (legalFee) ? commaSeparateNumber(parseFloat(legalFee).toFixed(2)) : '0.00';
							tel_bill = (telBill) ? commaSeparateNumber(parseFloat(telBill).toFixed(2)) : '0.00';
							total_pay = (halfNet) ? commaSeparateNumber(parseFloat(halfNet).toFixed(2)) : '0.00';
							mplp_loan = (mplpLoan) ? commaSeparateNumber(parseFloat(mplpLoan).toFixed(2)) : '0.00';



							arr += '<tr class="text-right">';
							arr += '<td >'+(parseInt(k) + 1)+'</td>';
							arr += '<td class="text-left" nowrap>'+fullname+'</td>';
							switch(part){
								case 'part_1':
									arr += '<td class="text-left"  nowrap>'+position+'</td>';
									arr += '<td class="text-center" nowrap>'+salarygrade+'</td>';
									arr += '<td>'+monthly_rate_amount+'</td>';
									arr += '<td>'+pera_amount+'</td>';
									arr += '<td>'+gross_pay+'</td>';
									arr += '<td>'+gsis_ps+'</td>';
									arr += '<td>'+gsis_gs+'</td>';
									arr += '<td>'+ecc_amount+'</td>';
									arr += '<td>'+pagibig_ps+'</td>';
									arr += '<td>'+pagibig_gs+'</td>';
									arr += '<td>'+philhealth_ps+'</td>';
									arr += '<td>'+philhealth_gs+'</td>';
									break;

								case 'part_2':
									arr += '<td>0.00</td>';
									arr += '<td>'+optional_life_amount+'</td>';
									arr += '<td>'+consoloan_amount+'</td>';
									arr += '<td>'+policy_amount+'</td>';
									arr += '<td>'+calamity_amount+'</td>';
									arr += '<td>'+lch_loan+'</td>';
									arr += '<td>'+education_loan_amount+'</td>';
									arr += '<td>'+multi_purpose_loan+'</td>';
									arr += '<td>'+pagibig_two_amount+'</td>';
									arr += '<td>'+wf_amount+'</td>';
									arr += '<td>'+wf_loan+'</td>';
									arr += '<td>'+legal_fee+'</td>';
									arr += '<td>'+wd_amount+'</td>';
									arr += '<td>'+wasslai_loan+'</td>';
									arr += '<td>'+fortune_care+'</td>';
									break;

								case 'part_3':
									arr += '<td>'+tax_contribution+'</td>';
									arr += '<td>'+mpcc_amount+'</td>';
									arr += '<td>'+mwss_loan+'</td>';
									arr += '<td>'+ud_amount+'</td>';
									arr += '<td>'+mplp_loan+'</td>';
									arr += '<td>'+sss_cont_amount+'</td>';
									arr += '<td>'+total_deduction+'</td>';
									arr += '<td>'+total_net_pay+'</td>';
									arr += '<td>'+one_half_net_pay+'</td>';
									arr += '<td>0.00</td>';
									arr += '<td>0.00</td>';
									arr += '<td>'+tel_bill+'</td>';
									arr += '<td>'+total_pay+'</td>';
									arr += '<td>'+account_number+'</td>';
									break;
							}
							
							arr += '</tr>';
						});
						net_salary = (net_salary) ? commaSeparateNumber(parseFloat(net_salary).toFixed(2)) : '0.00';
						net_pera = (net_pera) ? commaSeparateNumber(parseFloat(net_pera).toFixed(2)) : '0.00';
						net_gross_pay = (net_gross_pay) ? commaSeparateNumber(parseFloat(net_gross_pay).toFixed(2)) : '0.00';
						net_gsis_ps = (net_gsis_ps) ? commaSeparateNumber(parseFloat(net_gsis_ps).toFixed(2)) : '0.00';
						net_gsis_gs = (net_gsis_gs) ? commaSeparateNumber(parseFloat(net_gsis_gs).toFixed(2)) : '0.00';
						net_pagibig_ps = (net_pagibig_ps) ? commaSeparateNumber(parseFloat(net_pagibig_ps).toFixed(2)) : '0.00';
						net_pagibig_gs = (net_pagibig_gs) ? commaSeparateNumber(parseFloat(net_pagibig_gs).toFixed(2)) : '0.00';
						net_philhealth_ps = (net_philhealth_ps) ? commaSeparateNumber(parseFloat(net_philhealth_ps).toFixed(2)) : '0.00';
						net_philhealth_gs = (net_philhealth_gs) ? commaSeparateNumber(parseFloat(net_philhealth_gs).toFixed(2)) : '0.00';
						net_consoloan_amount = (net_consoloan_amount) ? commaSeparateNumber(parseFloat(net_consoloan_amount).toFixed(2)) : '0.00';
						net_policy_amount = (net_policy_amount) ? commaSeparateNumber(parseFloat(net_policy_amount).toFixed(2)) : '0.00';
						net_calamity_amount = (net_calamity_amount) ? commaSeparateNumber(parseFloat(net_calamity_amount).toFixed(2)) : '0.00';
						net_lch_loan = (net_lch_loan) ? commaSeparateNumber(parseFloat(net_lch_loan).toFixed(2)) : '0.00';
						net_education_loan_amount = (net_education_loan_amount) ? commaSeparateNumber(parseFloat(net_education_loan_amount).toFixed(2)) : '0.00';
						net_wf_loan = (net_wf_loan) ? commaSeparateNumber(parseFloat(net_wf_loan).toFixed(2)) : '0.00';
						net_wasslai_loan = (net_wasslai_loan) ? commaSeparateNumber(parseFloat(net_wasslai_loan).toFixed(2)) : '0.00';
						net_mwss_loan = (net_mwss_loan) ? commaSeparateNumber(parseFloat(net_mwss_loan).toFixed(2)) : '0.00';
						net_pagibig_two_amount = (net_pagibig_two_amount) ? commaSeparateNumber(parseFloat(net_pagibig_two_amount).toFixed(2)) : '0.00';
						net_multi_purpose_loan = (net_multi_purpose_loan) ? commaSeparateNumber(parseFloat(net_multi_purpose_loan).toFixed(2)) : '0.00';
						net_tax_contribution = (net_tax_contribution) ? commaSeparateNumber(parseFloat(net_tax_contribution).toFixed(2)) : '0.00';
						net_optional_life_amount = (net_optional_life_amount) ? commaSeparateNumber(parseFloat(net_optional_life_amount).toFixed(2)) : '0.00';
						net_sss_cont_amount = (net_sss_cont_amount) ? commaSeparateNumber(parseFloat(net_sss_cont_amount).toFixed(2)) : '0.00';
						net_mpcc_amount = (net_mpcc_amount) ? commaSeparateNumber(parseFloat(net_mpcc_amount).toFixed(2)) : '0.00';
						net_wf_amount = (net_wf_amount) ? commaSeparateNumber(parseFloat(net_wf_amount).toFixed(2)) : '0.00';
						net_wd_amount = (net_wd_amount) ? commaSeparateNumber(parseFloat(net_wd_amount).toFixed(2)) : '0.00';
						net_ud_amount = (net_ud_amount) ? commaSeparateNumber(parseFloat(net_ud_amount).toFixed(2)) : '0.00';
						net_deduction = (net_deduction) ? commaSeparateNumber(parseFloat(net_deduction).toFixed(2)) : '0.00';
						net_pay = (net_pay) ? commaSeparateNumber(parseFloat(net_pay).toFixed(2)) : '0.00';
						net_one_half_net_pay = (net_one_half_net_pay) ? commaSeparateNumber(parseFloat(net_one_half_net_pay).toFixed(2)) : '0.00';
						net_ecc_amount = (net_ecc_amount) ? commaSeparateNumber(parseFloat(net_ecc_amount).toFixed(2)) : '0.00';
						net_fortune_care = (netFortuneCare) ? commaSeparateNumber(parseFloat(netFortuneCare).toFixed(2)) : '0.00';
						net_legal_fee = (netLegaFee) ? commaSeparateNumber(parseFloat(netLegaFee).toFixed(2)) : '0.00';
						net_tel_bill = (netTelBill) ? commaSeparateNumber(parseFloat(netTelBill).toFixed(2)) : '0.00';
						net_net_pay= (netNetPay) ? commaSeparateNumber(parseFloat(netNetPay).toFixed(2)) : '0.00';
						net_mplp_loan= (netMPLPLoan) ? commaSeparateNumber(parseFloat(netMPLPLoan).toFixed(2)) : '0.00';

						arr += '<tr class="text-right" style="font-weight:bold">';
						arr += '<td></td>';
						
						switch(part)
						{
							case 'part_1':
								arr += '<td colspan="3">Total</td>';
								arr += '<td><b>'+net_salary+'</b></td>';
								arr += '<td>'+net_pera+'</td>';
								arr += '<td>'+net_gross_pay+'</td>';
								arr += '<td>'+net_gsis_ps+'</td>';
								arr += '<td>'+net_gsis_gs+'</td>';
								arr += '<td>'+net_ecc_amount+'</td>';
								arr += '<td>'+net_pagibig_ps+'</td>';
								arr += '<td>'+net_pagibig_gs+'</td>';
								arr += '<td>'+net_philhealth_ps+'</td>';
								arr += '<td>'+net_philhealth_gs+'</td>';
								break;
							case 'part_2':
								arr += '<td>Total</td>';
								arr += '<td>0.00</td>';
								arr += '<td>'+net_optional_life_amount+'</td>';
								arr += '<td>'+net_consoloan_amount+'</td>';
								arr += '<td>'+net_policy_amount+'</td>';
								arr += '<td>'+net_calamity_amount+'</td>';
								arr += '<td>'+net_lch_loan+'</td>';
								arr += '<td>'+net_education_loan_amount+'</td>';
								arr += '<td>'+net_multi_purpose_loan+'</td>';
								arr += '<td>'+net_pagibig_two_amount+'</td>';
								arr += '<td>'+net_wf_amount+'</td>';
								arr += '<td>'+net_wf_loan+'</td>';
								arr += '<td>'+net_legal_fee+'</td>';
								arr += '<td>'+net_wd_amount+'</td>';
								arr += '<td>'+net_wasslai_loan+'</td>';
								arr += '<td>'+net_fortune_care+'</td>';
								break;
							case 'part_3':
								arr += '<td >Total</td>';
								arr += '<td>'+net_tax_contribution+'</td>';
								arr += '<td>'+net_mpcc_amount+'</td>';
								arr += '<td>'+net_mwss_loan+'</td>';
								arr += '<td>'+net_ud_amount+'</td>';
								arr += '<td>'+net_mplp_loan+'</td>';
								arr += '<td>'+net_sss_cont_amount+'</td>';
								arr += '<td>'+net_deduction+'</td>';
								arr += '<td>'+net_pay+'</td>';
								arr += '<td>'+net_one_half_net_pay+'</td>';
								arr += '<td>0.00</td>';
								arr += '<td>0.00</td>';
								arr += '<td>'+net_tel_bill+'</td>';
								arr += '<td>'+net_net_pay+'</td>';
								arr += '<td></td>';
								break;
						}

						arr += '</tr>';
						arr += '</tbody>';

						$('#payroll_transfer').html(arr);
						$('#days_in_month').text(days)

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}

	$('#print').on('click',function(){
		$('#reports').printThis();
	});

	/*VIEW POSTED*/
$(document).on('click','.view',function(){
		_Year = $(this).data('year');
		_Month = $(this).data('month')
		_semiPayPeriod = $(this).data('pay_period');
		signOne = $(this).data('signatory_one');
		signTwo = $(this).data('signatory_two');
		signThree = $(this).data('signatory_three');
		signFour = $(this).data('signatory_four');
		posOne = $(this).data('position_one');
		posTwo = $(this).data('position_two');
		posThree = $(this).data('position_three');
		posFour = $(this).data('position_four');
		part = $(this).data('part');

		$('.signatory_one').text(signOne)
		$('.signatory_two').text(signTwo)
		$('.signatory_three').text(signThree)
		$('.signatory_four').text(signFour)

		$('.position_one').text(posOne)
		$('.position_two').text(posTwo)
		$('.position_three').text(posThree)
		$('.position_four').text(posFour)
		$('#select_part').val(part);

		$('#preview').trigger('click');
	});


/* FOR POSTING */
$(document).on('click','.btn_save',function(){
	Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.value) {

    	arr = {};

    	$("[class*='form-control']").each(function(k,v){
    		name = $(this).attr('name');
    		value = $(this).val();

    		arr[name] = value;

    	});

      $.ajax({
      	url:base_url+module_prefix+module+'/postReport',
      	data:{
      		'_token':`{!! csrf_token() !!}`,
      		'data':arr
      	},
      	type:'POST',
      	dataType:'JSON',
      	beforeSend:function(){
        	$('.btn_save').html('<i class="fa fa-spinner fa-spin"></i> Posting...').prop('disabled',true);
        },
      	success:function(res){

      		Swal.fire({
						  title: 'Posted Successfully!',
						  type: "success",
						  showCancelButton: false,
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "Yes",
					}).then((result) => {
	      			if(result.value)
	      			{
	      				window.location.href = base_url+module_prefix+module;
	      			}
					});
      	}
      })
    }

  });
	
});

})
</script>
@endsection