
<?php

(strtolower($module) == 'generalpayrollhonoraria') ? $generalpayrollhonoraria = 'active' : $generalpayrollhonoraria = '';
(strtolower($module) == 'honorariatransmital') ? $honorariatransmital = 'active' : $honorariatransmital = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $generalpayrollhonoraria }}">
            <a href="{{ url('payrolls/reports/honorariareports/generalpayrollhonoraria') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $honorariatransmital }}">
            <a href="{{ url('payrolls/reports/honorariareports/honorariatransmital') }}"  >
                <span>TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




