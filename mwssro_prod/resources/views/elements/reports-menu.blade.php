<?php
(strtolower($module) == 'payslips') ? $payslips = 'active' : $payslips = '';
(strtolower($module) == 'multipurposeloans') ? $multipurposeloans = 'active' : $multipurposeloans = '';
(strtolower($module) == 'taxesremittance') ? $taxesremittance = 'active' : $taxesremittance = '';
(strtolower($module) == 'ssscontributions') ? $ssscontributions = 'active' : $ssscontributions = '';
(strtolower($module) == 'mwsscooperative') ? $mwsscooperative = 'active' : $mwsscooperative = '';
(strtolower($module) == 'welfarefund') ? $welfarefund = 'active' : $welfarefund = '';
(strtolower($module) == 'mwsstubig') ? $mwsstubig = 'active' : $mwsstubig = '';
(strtolower($module) == 'serviceloans') ? $serviceloans = 'active' : $serviceloans = '';
(strtolower($module) == 'premiumcontributions') ? $premiumcontributions = 'active' : $premiumcontributions = '';
(strtolower($module) == 'wasslai') ? $wasslai = 'active' : $wasslai = '';
(strtolower($module) == 'generalpayrolltravelallowance') ? $generalpayrolltravelallowance = 'active' : $generalpayrolltravelallowance = '';
(strtolower($module) == 'gippayrollworksheet') ? $gippayrollworksheet = 'active' : $gippayrollworksheet = '';
(strtolower($module) == 'deductedloanreports') ? $deductedloanreports = 'active' : $deductedloanreports = '';
(strtolower($module) == 'loandetailsreports') ? $loandetailsreports = 'active' : $loandetailsreports = '';
(strtolower($module) == 'gsisloanreports') ? $gsisloanreports = 'active' : $gsisloanreports = '';
(strtolower($module) == 'lwopadjustments') ? $lwopadjustments = 'active' : $lwopadjustments = '';
(strtolower($module) == 'stepincrementreports') ? $stepincrementreports = 'active' : $stepincrementreports = '';
(strtolower($module) == 'hdmf_remittance') ? $hdmf_remittance = 'active' : $hdmf_remittance = '';
(strtolower($module) == 'hdmf2_remittance') ? $hdmf2_remittance = 'active' : $hdmf2_remittance = '';
(strtolower($module) == 'mpl_remittance') ? $mpl_remittance = 'active' : $mpl_remittance = '';
?>
<nav class="nav-sidebar">
    <ul class="nav">
        <li >
        	<a href="{{ url('payrolls/reports/plantillareports/generalpayroll') }}"  >
				<span>REGULAR</span>
			</a>
        </li>
        <li >
            <a href="{{ url('payrolls/reports/nonplantillareports/cocgeneralpayroll') }}"  >
                <span>CONTRACT OF SERVICE</span>
            </a>
        </li>
         <li class="{{ $gippayrollworksheet }}">
            <a href="{{ url('payrolls/reports/gippayrollworksheet') }}"  >
                <span>GIP PAYROLL WORKSHEET</span>
            </a>
        </li>
        <li >
            <a href="{{ url('payrolls/reports/overtimereports/generalpayrollovertime') }}"  >
                <span>OVERTIME</span>
            </a>
        </li>
        <li >
            <a href="{{ url('payrolls/reports/ratareports/ratageneralpayroll') }}"  >
                <span>RATA</span>
            </a>
        </li>
        <li class="{{ $payslips }}">
            <a href="{{ url('payrolls/reports/payslips') }}"  >
                <span>PAYSLIP</span>
            </a>
        </li>
         <li class="{{ $multipurposeloans }}">
            <a href="{{ url('payrolls/reports/multipurposeloans') }}"  >
                <span>MULTI PURPOSE LOANS</span>
            </a>
        </li>
        <li class="{{ $taxesremittance }}">
            <a href="{{ url('payrolls/reports/taxesremittance') }}"  >
                <span>TAXES REMITTANCE</span>
            </a>
        </li>
        <li class="{{ $ssscontributions }}">
            <a href="{{ url('payrolls/reports/ssscontributions') }}"  >
                <span>SSS CONTRIBUTIONS</span>
            </a>
        </li>
        <li class="{{ $mwsscooperative }}">
            <a href="{{ url('payrolls/reports/mwsscooperative') }}"  >
                <span>MWSS-RO MPC</span>
            </a>
        </li>
        <li class="{{ $mwsstubig }}">
            <a href="{{ url('payrolls/reports/mwsstubig') }}"  >
                <span>MWSS-RO TUBIG</span>
            </a>
        </li>
        <li class="{{ $welfarefund }}">
            <a href="{{ url('payrolls/reports/welfarefund') }}"  >
                <span>WELFARE FUND</span>
            </a>
        </li>
         <li class="{{ $serviceloans }}">
            <a href="{{ url('payrolls/reports/serviceloans') }}"  >
                <span>SERVICE LOANS</span>
            </a>
        </li>
        <li class="{{ $premiumcontributions }}">
            <a href="{{ url('payrolls/reports/premiumcontributions') }}"  >
                <span>PREMIUM CONTRIBUTIONS</span>
            </a>
        </li>
        <li class="{{ $wasslai }}">
            <a href="{{ url('payrolls/reports/wasslai') }}"  >
                <span>WASSLAI CONTRIBUTION AND LOAN</span>
            </a>
        </li>
        <li class="{{ $generalpayrolltravelallowance }}">
            <a href="{{ url('payrolls/reports/generalpayrolltravelallowance') }}"  >
                <span>GENRAL PAYROLL TRAVEL ALLOWANCE</span>
            </a>
        </li>
        <li >
            <a href="{{ url('payrolls/reports/cnapeireports/cnapeigeneralpayroll') }}"  >
                <span>CNA & PEI</span>
            </a>
        </li>
        <li >
            <a href="{{ url('payrolls/reports/midyearreports/midyeargeneralpayroll') }}"  >
                <span>MID YEAR BONUS</span>
            </a>
        </li>
         <li >
            <a href="{{ url('payrolls/reports/monetizationreports/monetizationgeneralpayroll') }}"  >
                <span>LEAVE MONETIZATION</span>
            </a>
        </li>
        <li >
            <a href="{{ url('payrolls/reports/yearendreports/yearendgeneralpayroll') }}"  >
                <span>YEAR END AND CASH GIFT BONUS</span>
            </a>
        </li>
        <li class="{{ $deductedloanreports }}">
            <a href="{{ url('payrolls/reports/deductedloanreports') }}"  >
                <span>DEDUCTED LOAN REPORTS</span>
            </a>
        </li>
        <li class="{{ $loandetailsreports }}">
            <a href="{{ url('payrolls/reports/loandetailsreports') }}"  >
                <span>LOAN DETAIL REPORTS</span>
            </a>
        </li>
        <li class="{{ $gsisloanreports }}">
            <a href="{{ url('payrolls/reports/gsisloanreports') }}"  >
                <span>GSIS REMITTANCE REPORTS</span>
            </a>
        </li>
         <li class="{{ $lwopadjustments }}">
            <a href="{{ url('payrolls/reports/lwopadjustments') }}"  >
                <span>SALARY ADJUSTMENT REPORTS</span>
            </a>
        </li>
        <li class="{{ $stepincrementreports }}">
            <a href="{{ url('payrolls/reports/stepincrementreports') }}"  >
                <span>STEP INCREMENT REPORTS</span>
            </a>
        </li>
        <li>
            <a href="{{ url('payrolls/reports/honorariareports/generalpayrollhonoraria') }}"  >
                <span>HONORARIUM REPORTS</span>
            </a>
        </li>

        <li class="{{ $hdmf_remittance }}">
            <a href="{{ url('payrolls/reports/hdmf_remittance') }}"  >
                <span>HDMF REMITTANCE</span>
            </a>
        </li>

        <li class="{{ $hdmf2_remittance }}">
            <a href="{{ url('payrolls/reports/hdmf2_remittance') }}"  >
                <span>HDMF II REMITTANCE</span>
            </a>
        </li>

        <li class="{{ $mpl_remittance }}">
            <a href="{{ url('payrolls/reports/mpl_remittance') }}"  >
                <span>MPL REMITTANCE</span>
            </a>
        </li>

    </ul>
</nav>




