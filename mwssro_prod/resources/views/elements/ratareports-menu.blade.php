
<?php

(strtolower($module) == 'ratageneralpayroll') ? $ratageneralpayroll = 'active' : $ratageneralpayroll = '';
(strtolower($module) == 'ratatransmital') ? $ratatransmital = 'active' : $ratatransmital = '';
(strtolower($module) == 'ratapayrollworksheets') ? $ratapayrollworksheets = 'active' : $ratapayrollworksheets = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $ratageneralpayroll }}">
            <a href="{{ url('payrolls/reports/ratareports/ratageneralpayroll') }}"  >
                <span>RATA GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $ratapayrollworksheets }}">
            <a href="{{ url('payrolls/reports/ratareports/ratapayrollworksheets') }}"  >
                <span>RATA PAYROLL WORKSHEETS</span>
            </a>
        </li>
        <li class="{{ $ratatransmital }}">
            <a href="{{ url('payrolls/reports/ratareports/ratatransmital') }}"  >
                <span>RATA TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




