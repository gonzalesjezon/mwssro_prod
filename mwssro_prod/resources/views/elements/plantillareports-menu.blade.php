
<?php
(strtolower($module) == 'payrollworksheets') ? $payrollworksheets = 'active' : $payrollworksheets = '';
(strtolower($module) == 'generalpayroll') ? $generalpayroll = 'active' : $generalpayroll = '';
(strtolower($module) == 'regulartransmital') ? $regulartransmital = 'active' : $regulartransmital = ''
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $generalpayroll }}">
            <a href="{{ url('payrolls/reports/plantillareports/generalpayroll') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $payrollworksheets }}">
            <a href="{{ url('payrolls/reports/plantillareports/payrollworksheets') }}"  >
                <span>PAYROLL WORKSHEET</span>
            </a>
        </li>
        <li class="{{ $regulartransmital }}">
            <a href="{{ url('payrolls/reports/plantillareports/regulartransmital') }}"  >
                <span>REGULAR TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




