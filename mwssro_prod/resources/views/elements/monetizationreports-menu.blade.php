
<?php

(strtolower($module) == 'monetizationtransmital') ? $monetizationtransmital = 'active' : $monetizationtransmital = '';
(strtolower($module) == 'monetizationgeneralpayroll') ? $monetizationgeneralpayroll = 'active' : $monetizationgeneralpayroll = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $monetizationgeneralpayroll }}">
            <a href="{{ url('payrolls/reports/monetizationreports/monetizationgeneralpayroll') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $monetizationtransmital }}">
            <a href="{{ url('payrolls/reports/monetizationreports/monetizationtransmital') }}"  >
                <span>TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




