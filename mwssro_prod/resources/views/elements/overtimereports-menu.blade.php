
<?php

(strtolower($module) == 'overtimetransmital') ? $overtimetransmital = 'active' : $overtimetransmital = '';
(strtolower($module) == 'generalpayrollovertime') ? $generalpayrollovertime = 'active' : $generalpayrollovertime = '';
(strtolower($module) == 'overtimepayrollworksheet') ? $overtimepayrollworksheet = 'active' : $overtimepayrollworksheet = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $generalpayrollovertime }}">
            <a href="{{ url('payrolls/reports/overtimereports/generalpayrollovertime') }}"  >
                <span>GENERAL PAYROLL OVERTIME</span>
            </a>
        </li>
        <li class="{{ $overtimepayrollworksheet }}">
            <a href="{{ url('payrolls/reports/overtimereports/overtimepayrollworksheet') }}"  >
                <span>OVERTIME PAYROLL WORKSHEET</span>
            </a>
        </li>
        <li class="{{ $overtimetransmital }}">
            <a href="{{ url('payrolls/reports/overtimereports/overtimetransmital') }}"  >
                <span>OVERTIME TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




