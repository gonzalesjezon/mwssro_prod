<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeriesCode extends Model
{

    protected $primaryKey = 'id';
    protected $table  	  = 'pms_series_codes';

    protected $fillable = [

    	'code',
    	'description',
    	'created_by',
    	'updated_by'
    	
    ];
}
