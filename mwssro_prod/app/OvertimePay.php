<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OvertimePay extends Model
{
    protected $table = 'pms_overtime';
    protected $fillable = [
		'employee_id',
		'employee_number',

		# Regular
		'regular_ot_rate',
		'regular_no_hrs',
		'regular_ot_hrs',
		'regular_ot_amount',

		# Weekend
		'weekend_ot_rate',
		'weekend_no_hrs',
		'weekend_ot_hrs',
		'weekend_ot_amount',

		# Holiday
		'holiday_ot_rate',
		'holiday_no_hrs',
		'holiday_ot_hrs',
		'holiday_ot_amount',

		'ot_total',
		'ot_tax',
		'ot_net',
		'ot_covered_period',
		'basic_amount',

		'from_date',
		'to_date',

		'year',
		'month',
		'status',
		'created_by',
		'updated_by'
    ];

    public function employeeinformation(){
    	return $this->belongsTo('App\EmployeeInformation','employee_id','employee_id')->with('positions');
    }
    public function salaryinfo(){
    	return $this->belongsTo('App\SalaryInfo','employee_id','employee_id')->with('salarygrade');
    }
    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
    public function employeeinfo(){
    	return $this->belongsTo('App\EmployeeInfo','employee_id','employee_id');
    }

    public function transaction()
    {
    	return $this->belongsTo('App\Transaction','employee_id','employee_id');	
    }
}
