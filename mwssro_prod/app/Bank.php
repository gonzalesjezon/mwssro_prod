<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'pms_banks';
    protected $fillable = [
    	'code',
    	'name',
    	'branch_name',
    	'bank_accountno',
    	'remarks'
    ];
}
