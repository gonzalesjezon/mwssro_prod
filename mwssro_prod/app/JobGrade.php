<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobGrade extends Model
{
    protected $table = 'pms_jobgrade';
	protected $fillable = [
		'job_grade',
		'step1',
		'step2',
		'step3',
		'step4',
		'step5',
		'step6',
		'step7',
		'step8',
		'remarks',
	];
}
