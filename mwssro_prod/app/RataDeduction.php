<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RataDeduction extends Model
{
    protected $table = 'pms_rata_deductions';
    protected $fillable = [
    	'employee_id',
    	'rata_id',
    	'deduction_id',
    	'deduction_amount',
    	'year',
    	'month',
        'pay_period',
    	'created_by',
    	'updated_by'
    ];

    public function deductions()
    {
        return $this->belongsTo('App\Deduction','deduction_id');
    }
}
