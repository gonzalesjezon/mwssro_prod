<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{

	protected $primaryKey = 'RefId';
	protected $table = 'office';
    // protected $table = 'pms_offices';
    protected $fillable = [
    	'Code',
    	'Name',

    ];

}
