<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonPlantillaEmployeeInfo extends Model
{
    protected $table = 'pms_nonplantilla_employeeinfo';
    protected $fillable = [

        'employee_number',
        'employee_id',
        'bank_id',
        'taxpolicy_id',
        'taxpolicy_two_id',
        'responsibility_id',
        'atm_no',
        'daily_rate_amount',
        'monthly_rate_amount',
        'overtime_balance_amount',
        'inputted_amount',
        'threshhold_amount',
        'tax_id_number',
        'phic_amount'

    ];

    public function tax_policy(){
        return $this->belongsTo('App\TaxPolicy','tax_policy_id');
    }
    public function banks(){
        return $this->belongsTo('App\Bank','jo_bank_id');
    }
    public function taxPolicyOne(){
        return $this->belongsTo('App\TaxPolicy','taxpolicy_id');
    }
    public function taxPolicyTwo(){
        return $this->belongsTo('App\TaxPolicy','taxpolicy_two_id');
    }
    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','employee_id','employee_id');
    }
}
