<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeductionInfoTransaction extends Model
{
    protected $table 	= 'pms_deductioninfo_transactions';
    protected $fillable = [
		'employee_id',
		'deduction_id',
		'deduction_info_id',
        'certificate_no',
		'amount',
		'status',
        'year',
        'month',
        'pay_period',
		'created_by',
		'updated_by',
    ];

    public function deductionInfo(){
    	return $this->belongsTo('App\DeductionInfo','deduction_info_id')->with('deductions');
    }
    public function deductions(){
    	return $this->belongsTo('App\Deduction','deduction_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function employeeinfo(){
        return $this->belongsTo('App\EmployeeInfo','employee_id','employee_id');
    }
}
