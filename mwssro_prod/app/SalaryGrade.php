<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryGrade extends Model
{
	protected $primaryKey = 'RefId';
	protected $table = 'salarygrade';
	protected $fillable = [
		'Code',
		'Name',
	];

}
