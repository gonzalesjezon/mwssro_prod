<?php
   class SysForm {
      public function eform($attr) {
         $type  = $attr["type"];
         $label = $attr["label"];
         $id    = $attr["id"];
         $name  = $attr["name"];
         $style = $attr["style"];
         $other = $attr["other"];
         $class = $attr["class"];
         $row   = $attr["row"];
         $col   = $attr["col"];

         if ($row) echo '<div class="row">';
         if ($col != "") echo '<div class="col-xs-'.$col.'">';

         echo
         '<label for="'.$id.'">' . $label . ':</label>';
         if ($attr["br"]) {
            echo '<br/>';
         }
         echo
         '<input type="'.$type.'" name="'.$name.'" id="'.$id.'" class="form-input '.$class.'" ';
               if ($style != "") {
                  echo ' style="'.$style.'" ';
               }
               if ($other != "") {
                  echo $other;
               }
         echo
         '>';

         if ($row) echo '</div>';
         if ($col != "") echo '</div>';
      }

   }
?>