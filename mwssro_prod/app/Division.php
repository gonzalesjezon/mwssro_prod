<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{

	protected $primaryKey = 'RefId';
	protected $table = 'division';
    // protected $table = 'pms_divisions';
    protected $fillable = [
    	'Code',
    	'Name',

    ];

}
