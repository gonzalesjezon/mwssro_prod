<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialPayrollTransaction extends Model
{
    protected $table = 'pms_specialpayroll_transactions';
    protected $fillable = [
        'employee_number',
		'employee_id',
		'benefit_info_id',
		'position_item_id',
        'division_id',
		'office_id',
        'no_of_scpp',
		'year',
		'month',
        'percentege',
        'no_of_months_entitled',
        'cost_uniform_amount',
        'cash_gift_amount',
        'amount',
		'created_by',
		'updated_by',

    ];

    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
    public function benefitinfo(){
    	return $this->belongsTo('App\BenefitInfo','benefit_info_id');
    }
    public function offices(){
    	return $this->belongsTo('App\Office','office_id');
    }
    public function positions(){
        return $this->belongsTo('App\Position','position_id');
    }
    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','position_item_id');
    }
    public function divisions(){
        return $this->belongsTo('App\Division','division_id');
    }
    public function salaryinfo(){
        return $this->belongsTo('App\salaryinfo','employee_id')->with('salarygrade');
    }
    public function employeeinfo(){
        return $this->belongsTo('App\EmployeeInfo','employee_id','employee_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction','employee_id','employee_id');
    }

    public function special()
    {
        return $this->belongsTo('App\SpecialPayrollTransaction','employee_id','employee_id');   
    }

}
