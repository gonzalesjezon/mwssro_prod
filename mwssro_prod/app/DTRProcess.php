<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DTRProcess extends Model
{
    protected $primaryKey = 'RefId';
    protected $table = 'dtr_process';
    protected $fillable = [

    	'EmployeesRefId',
    	'Tardy_Deduction_EQ',
    	'Undertime_Deduction_EQ',
    	'Total_Absent_Count',
        'VL_Used',
        'VL_Earned',
    	'Month',
    	'Year'
    ];
}
