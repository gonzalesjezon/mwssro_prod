<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PISEmployee extends Model
{
    protected $primaryKey = 'AgencyId';

    protected $table = 'employees';

    protected $fillable = [
    	'PAGIBIG',
    	'GSIS',
    	'PHIC',
    	'TIN',
    	'SSS',
    	'BirthDate'
    ];

}
