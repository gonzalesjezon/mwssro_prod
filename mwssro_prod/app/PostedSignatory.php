<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostedSignatory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pms_signatory';
    protected $fillable = [
    	'signatory_one',
		  'signatory_two',
		  'signatory_three',
		  'signatory_four',
		  'signatory_five',
		  'position_one',
		  'position_two',
		  'position_three',
		  'position_four',
		  'position_five',
    ];
}
