<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $primaryKey = 'RefId';
    protected $table = 'company';
    protected $fillable = [
    	'Code',
    	'Name',
        'Address',

    ];

    public function company(){
    	return $this->hasOne('App\Company','RefId');
    }
}
