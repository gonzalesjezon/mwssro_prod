<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagibigTable extends Model
{
    protected $table = 'pagibig_table';
    protected $fillable = [
  
    	'below',
    	'above',
    	'employee_share',
    	'employer_share',
    	'max_ee_share',
    	'max_er_share',
    	'effectivity_date',
    	'remarks',
    ];

}
