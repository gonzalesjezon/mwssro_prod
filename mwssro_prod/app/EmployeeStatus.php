<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatus extends Model
{
	protected $primaryKey = 'RefId';
	protected $table = 'empstatus';
    // protected $table = 'pms_employee_status';
    protected $fillable = [
    	'Code',
    	'Name',
    	'category',
    	'created_by',
    	'updated_by'
    ];

}
