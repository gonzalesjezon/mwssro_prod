<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelRateTransaction extends Model
{
    protected $table = 'pms_travelrate_transactions';
    protected $fillable  = [
    	'employee_id',
    	'travel_rate_id',
        'position_id',
    	'rate_id',
    	'travel_rate_amount',
    	'number_of_days',
    	'net_amount',
        'remarks',
    	'created_by',
    	'updated_by'

    ];
    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function rates(){
        return $this->belongsTo('App\Rate','rate_id');
    }
    public function travelrates(){
        return $this->belongsTo('App\TravelRate','travel_rate_id');
    }
    public function positions(){
        return $this->belongsTo('App\Position','position_id');
    }

    public function payrollinfo(){
        return $this->belongsTo('App\EmployeeInfo','employee_id','employee_id');
    }

}
