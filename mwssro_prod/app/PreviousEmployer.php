<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreviousEmployer extends Model
{
    protected $table = 'pms_previous_employer';
    protected $fillable = [

		'employee_id',
		'allowances',
		'thirteen_month_pay',
		'deminimis_amount',
		'tax_witheld',
		'premium_amount',
		'taxable_basic_pay',
		'taxable_thirteen_month_pay',
		'taxable_allowances',
		'as_of_date',
		'created_by'

    ];
}
