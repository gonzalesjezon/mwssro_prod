<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeginningBalance extends Model
{
    protected $table = 'pms_beginning_balances';
    protected $fillable = [

		'employee_id',
		'premium_amount',
		'tax_witheld',
		'basic_pay',
		'overtime_pay',
		'thirteen_month_pay',
		'deminimis',
		'other_salaries',
		'taxable_basic_pay',
		'taxable_overtime_pay',
		'taxable_thirteen_month_pay',
		'taxable_other_salaries',
		'as_of_date',
		'created_by'
    ];
}
