<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostedReport extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pms_posted_reports';
    protected $fillable = [
	  'signatory_id',
	  'employee_id',
	  'year',
	  'month',
	  'pay_period',
	  'report_type',
	  'print_date',
      'part',
    ];

    public function signatory()
    {
    	return $this->belongsTo('App\PostedSignatory','signatory_id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee','employee_id');
    }
}
