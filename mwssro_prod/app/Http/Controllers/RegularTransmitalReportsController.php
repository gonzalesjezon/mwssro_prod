<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\EmployeeInfo;
use App\Employee;
use App\AttendanceInfo;
use App\Company;
class RegularTransmitalReportsController extends Controller
{
   function __construct(){

    	$this->title = 'REGULAR TRANSMITAL';
    	$this->module = 'regulartransmital';
        $this->module_prefix = 'payrolls/reports/plantillareports';
    	$this->controller = $this;

    }

    public function index(){

        $company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $employees = Employee::where('active',1)->orderBy('lastname','asc')->get();

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees' => $employees
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new Transaction;
    	$employeeinfo = new EmployeeInfo;
    	$attendanceinfo = new AttendanceInfo;

        $data = Input::all();

        $year      = $data['year'];
        $month     = $data['month'];
        $payPeriod = $data['pay_period'];
        $query = Transaction::where('year',$year)
						->where('month',$month);
                        if($payPeriod == 'firsthalf'){
                            $query = $query->sum('first_net');
                        }else{
                            $query = $query->sum('second_net');
                        }

       return json_encode([
        'transaction' => $query
       ]);
    }
}
