<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LoanInfo;
use App\Loan;
use App\Transaction;
use App\EmployeeInfo;
use App\LoanInfoTransaction;
use Input;
use App\Company;
use App\Employee;
class MultiPurposeLoanReportsController extends Controller
{
    function __construct(){
		$this->title = 'MULTI PURPOSE LOAN REPORT';
    	$this->module = 'multipurposeloans';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();

        $loan =        new Loan;
        $loaninfo    = new LoanInfoTransaction;

        $loan = $loan
        ->where('code','MPLPLOAN')
        ->select('id')->first();

        $query = LoanInfoTransaction::leftJoin('pms_employees as e','e.id','=','pms_loaninfo_transactions.employee_id')->with([
                'employees'
            ])
            ->where('loan_id',$loan->id)
            ->where('year',$data['year'])
            ->where('month',$data['month'])
            ->orderBy('e.lastname')
            ->get();


    	return json_encode($query);
    }
}
