<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\Employee;
class GSISLoanReportsController extends Controller
{
    function __construct(){
		$this->title = 'GSIS REMITTANCE REPORT';
    	$this->module = 'gsisloanreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

		$employee = new Employee;
		$employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;


		$status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $employee = $employee
        ->whereIn('id',$employee_id)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

    	$response = array(
    					'employee' 		=> $employee,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction         = new Transaction;
        $employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = Transaction::leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->with([
        		'loaninfoTransaction' => function($qry) use($year,$month){
                    $qry = $qry->where('year',$year)
                    ->where('month',$month);
        		},
        		'salaryinfo',
        		'employees',
                'employeeinfo'
            ])
        ->whereIn('employee_id',$employee_id)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('e.lastname')
		->get();


        return json_encode($query);
    }
}
