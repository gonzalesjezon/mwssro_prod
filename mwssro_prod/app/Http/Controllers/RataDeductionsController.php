<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\RataDeduction;
use App\Deduction;
use Input;
use Auth;
class RataDeductionsController extends Controller
{
    function __construct(){
        $this->title = 'RATA Deduction';
        $this->controller = $this;
        $this->module = 'rata_deductions';
        $this->module_prefix = 'payrolls/otherpayrolls';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deductions = Deduction::where('payroll_group','rata')->get();
        $response = array(
           'deductions'     => $deductions,
           'title'          => $this->title,
           'controller'     => $this->controller,
           'module'         => $this->module,
           'module_prefix'  => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $deduction_amount = (isset($request->deduction_amount)) ? str_replace(',','', $request->deduction_amount) : NULL;

        $deductions = RataDeduction::find($request->id);
        $message = 'Update Successfully.';
        if(empty($deductions))
        {
          $deductions = new RataDeduction;

          $this->validate($request,[
            'deduction_id' => 'required',
          ]);

          $message = 'Save Successfully.';

        }

        $deductions->employee_id       = $request->employee_id;
        $deductions->rata_id           = $request->rata_id;
        $deductions->deduction_id      = $request->deduction_id;
        $deductions->year              = $request->year;
        $deductions->month             = $request->month;
        $deductions->deduction_amount  = $deduction_amount;
        $deductions->created_by        = Auth::User()->id;

        $deductions->save();


        return json_encode(['status'=>true,'response'=> $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
