<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\AttendanceInfo;
use App\EmployeeStatus;
use App\Employee;
use App\PostedReport;
use App\PostedSignatory;
use Auth;
class COCPayrollWorksheetReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL WORKSHEETS';
    	$this->module = 'cocpayrollworksheets';
        $this->module_prefix = 'payrolls/reports/nonplantillareports';
    	$this->controller = $this;

    }

    public function index(){

        $employees = Employee::where('active',1)->orderBy('lastname','asc')->get();

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'          => $employees,
                       'postedreports'  => PostedReport::where('report_type','cospws')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function getPayrollWorksheetReport(){

    	$transaction = new NonPlantillaTransaction;
    	$employeeinfo = new NonPlantillaEmployeeInfo;
    	$attendanceinfo = new AttendanceInfo;
        $employeeStatus =  new EmployeeStatus;

        $data = Input::all();

        $year      = $data['year'];
        $month     = $data['month'];
        $payPeriod = $data['pay_period'];

        $employeeStatus = $employeeStatus->where('Code','COS')->first();

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_nonplantilla_transactions.employee_id')
        ->with(['employeeinfo'=> function($qry){
            $qry->with('taxPolicyOne','taxPolicyTwo');
        },
            'employees'])
        ->where('employee_status_id',$employeeStatus->RefId)
        ->where('year',$year)
        ->where('month',$month)
        ->where('sub_pay_period',$payPeriod)
        ->orderBy('e.lastname','asc')
        ->get();

        $query2 = $transaction->where('year',$year)
        ->where('month',$month)
        ->where('sub_pay_period',$payPeriod)
        ->first();

        $from = date('F d', strtotime(@$query2->from_date));
        $to   = date('F d', strtotime(@$query2->to_date));

       return json_encode([
            'transaction' => $query,
            'From' => $from,
            'To' => $to,
        ]);
       
    }

    public function postReport(Request $request){
        
        $postedreports = new PostedReport;
        $signatory     =  new PostedSignatory;

        $signatory->signatory_one   = @$request['data']['signatory_one'];
        $signatory->signatory_two   = @$request['data']['signatory_two'];
        $signatory->signatory_three = @$request['data']['signatory_three'];
        $signatory->signatory_four  = @$request['data']['signatory_four'];
        $signatory->position_one    = @$request['data']['position_one'];
        $signatory->position_two    = @$request['data']['position_two'];
        $signatory->position_three  = @$request['data']['position_three'];
        $signatory->position_four   = @$request['data']['position_four'];
        $signatory->created_by      = Auth::id();
        if($signatory->save()){
            $postedreports->pay_period   = @$request['data']['pay_period'];
            $postedreports->year         = @$request['data']['year'];
            $postedreports->month        = @$request['data']['month'];
            $postedreports->signatory_id = $signatory->id;
            $postedreports->created_by   = Auth::id();
            $postedreports->report_type  = 'cospws';
            $postedreports->save();
        }

        return json_encode([
            'status'=>true,
            'response'=> 'Report posted successfully.'
        ]);


    }
}
