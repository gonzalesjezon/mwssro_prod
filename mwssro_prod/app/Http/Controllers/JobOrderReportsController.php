<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Employee;
use App\AttendanceInfo;
class JobOrderReportsController extends Controller
{
    function __construct(){
		$this->title = 'JOB ORDER';
    	$this->module = 'joborder';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

		$nonplantilla = ['Job Order','Casual','Contract of Service','Contractual'];

		$employeestatus 	  =  new EmployeeStatus;
		$employee_information = new EmployeeInformation;

        $empstatus_id = $employeestatus->whereIn('name',$nonplantilla)->select('id')->get()->toArray();
        $employee_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

    	$employeeinfo = Employee::whereIn('id',$employee_id)->orderBy('lastname','asc')->get();

    	$response = array(
                        'employeeinfo'  => $employeeinfo,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getJobOrder(){

        $data = Input::all();

        $transaction    =  new NonPlantillaTransaction;
        $employeeinfo   = new NonPlantillaEmployeeInfo;
        $attendanceinfo = new AttendanceInfo;

        $query['transaction'] = $transaction->with(['employees','positionitems'=>function($qry){ $qry->with('positions'); },'offices'])
        									->where('year',$data['year'])
        									->where('month',$data['month'])
        									->where('employee_id',$data['id'])
        									->first();

        $query['employeeinfo'] = $employeeinfo->with('taxpolicyOne','taxpolicyTwo')->where('employee_id',$data['id'])
        									->first();

        $query['attendanceinfo'] =  $attendanceinfo->where('employee_id',$data['id'])
                                                ->where('month',$data['month'])
                                                ->where('year',$data['year'])
                                                ->first();

        return json_encode($query);
    }
}
