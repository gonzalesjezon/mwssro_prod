<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpecialPayrollTransaction;
use App\Employee;
use Input;
class MidYearGeneralPayrollReportsReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'midyeargeneralpayroll';
        $this->module_prefix = 'payrolls/reports/midyearreports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new SpecialPayrollTransaction;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query['transaction'] = SpecialPayrollTransaction::leftJoin('pms_employees as e','e.id','=','pms_specialpayroll_transactions.employee_id')
            ->with([
                'salaryinfo',
                'positions',
                'employees',
                'employeeinfo',
                'transaction'
            ])
            ->where('year',$year)
            ->where('month',$month)
            ->where('status','midyearbonus')
            ->orderBy('e.lastname')
            ->get();

       return json_encode($query);
    }
}
