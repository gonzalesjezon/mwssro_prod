<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\StepIncrement;
use App\Employee;
use Input;
class StepIncrementReportsController extends Controller
{
    function __construct(){
    	$this->title = 'STEP INCREMENT REPORTS';
    	$this->module = 'stepincrementreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employeeinfo = Employee::where('active',1)->orderBy('lastname','asc')->get();

        $response = array(
                        'employeeinfo'  => $employeeinfo,
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

        return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function getStepIncrements(){
        $data = Input::all();

        $employee_id = $data['id'];

        $stepincrement =  new StepIncrement;

        $query = $stepincrement
        ->where('employee_id',$employee_id)
        ->get();

        return $query;
    }

    public function show(){
        $data = Input::all();

        $employee_id = $data['id'];
        $month        = $data['month'];

        $stepincrement = new StepIncrement;

        $query = $stepincrement
        ->with('employees')
        ->where('employee_id',$employee_id)
        ->where('transaction_date',$month)
        ->first();

        return $query;
    }
}
