<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\OvertimePay;
use App\Employee;
use App\Transaction;
use App\EmployeeInfo;
use App\EmployeeInformation;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\EmployeeStatus;
use App\AttendanceInfo;
class OvertimePaysController extends Controller
{
    function __construct(){
        $this->title = 'OVERTIME PAY';
        $this->module = 'overtimepay';
        $this->module_prefix = 'payrolls';
        $this->controller = $this;

    }

    public function index(){

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'months'             => config('params.months'),
                        'latest_year'        => $this->latestYear(),
                        'earliest_year'      => $this->earliestYear(),
                        'current_month'      => (int)date('m')
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        $year           = Input::get('year');
        $month          = Input::get('month');
        $chkovertime    = Input::get('check_overtime');
        $employeeStatus = Input::get('status');


        

        if(isset($year) || isset($month) || isset($chkovertime) || isset($employeeStatus)){
            $data = $this->filter($year,$month,$chkovertime,$employeeStatus);
        }

        if($q){
            $data = $this->searchName($q,$year,$month,$chkovertime,$employeeStatus);
        }


        $response = array(
                        'data'          => @$data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    public function searchName($q,$year,$month,$chk_overtime,$employee_status){

        $cols = ['lastname','firstname'];

        $employeeinfo               = new EmployeeInformation;
        $transaction                = new OvertimePay;

        $empstatus_id = [];
        switch($employee_status){
            case 'plantilla':
                $empstatus_id = EmployeeStatus::where('category',1)
                ->select('RefId')
                ->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = EmployeeStatus::where('category',0)
                ->select('RefId')
                ->get()->toArray();
            break;
        }

        $employee_id  = EmployeeInformation::select('employee_id')
        ->whereIn('employee_status_id',$empstatus_id)
        ->get()->toArray();

        $arrHasOT = OvertimePay::select('employee_id')
        ->where('year',$year)
        ->where('month',$month)
        ->get()->toArray();

        $query = [];

        $employee = new Employee;
        switch ($chk_overtime) {
            case 'w/':
               $query = $employee->whereIn('id',$arrHasOT);
                break;

            default:
                $query = $employee->whereIn('id',$employee_id)
                ->whereNotIn('id',$arrHasOT);
                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();
        return $response;

    }

    public function filter($year,$month,$chkovertime,$employee_status){

        $empstatus_id = [];
        switch($employee_status){
            case 'plantilla':
                $empstatus_id = EmployeeStatus::where('category',1)
                ->select('RefId')
                ->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = EmployeeStatus::where('category',0)
                ->select('RefId')
                ->get()->toArray();
            break;
        }

        $employee_id  = EmployeeInformation::select('employee_id')
        ->whereIn('employee_status_id',$empstatus_id)
        ->get()->toArray();

        $arrHasOT = OvertimePay::select('employee_id')
        ->where('year',$year)
        ->where('month',$month)
        ->get()->toArray();

        $response = "";
        switch ($chkovertime) {
            case 'w/':
                    $response = Employee::whereIn('id',$arrHasOT)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();
                break;
            default:
                    $response = Employee::whereIn('id',$employee_id)
                    ->whereNotIn('id',$arrHasOT)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }

    public function getOvertimeInfo(){
        $data = Input::all();

        $year  = $data['year'];
        $month = $data['month'];

        $employeeinfo = new EmployeeInfo;
        $nonplantilla = new NonPlantillaEmployeeInfo;
        $overtimepay  = new OvertimePay;

        $query['plantilla'] = $employeeinfo
        ->where('employee_id',@$data['employee_id'])
        ->first();

        $query['nonplantilla'] = $nonplantilla
        ->where('employee_id',@$data['employee_id'])
        ->first();

        $query['overtimepay'] = $overtimepay
        ->where('employee_id',@$data['employee_id'])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('created_at','desc')
        ->first();

        return json_encode($query);
    }

    public function storeOvertimeInfo(Request $request){


        if(isset($request['employee_id'])){

            foreach ($request['employee_id'] as $key => $value) {

                if(isset($value)){
                    $overtimepay      = new OvertimePay;
                    if($request->status == 'plantilla' ){
                        $payrollInfo = EmployeeInfo::where('employee_id',$value)->first();
                    }else{
                        $payrollInfo = NonPlantillaEmployeeInfo::where('employee_id',$value)->first();
                    }
                    $overtimepay->fill($request->all());
                    $overtimepay->employee_id     = $value;
                    $overtimepay->employee_number = @$payrollInfo->employee_number;
                    $overtimepay->basic_amount    = @$payrollInfo->monthly_rate_amount;
                    $overtimepay->created_by      = Auth::id();
                    $overtimepay->save();
                }

            }
            $response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
        }else{
            $response = json_encode(['status'=>false,'response'=>'Save Failed!']);
        }

        return $response;

    }

    public function deleteOvertime(){
        $data = Input::all();

        $overtimepay = new OvertimePay;

        foreach ($data['empid'] as $key => $value) {

            $overtimepay->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

    public function store(Request $request){
        
        $this->validate($request,[
            'regular_no_hrs' => 'required'
        ]);

        $regularOtAmount = ($request->regular_ot_amount) ? str_replace(',', '', $request->regular_ot_amount) : 0;
        $weekendOtAmount = ($request->weekend_ot_amount) ? str_replace(',', '', $request->weekend_ot_amount) : 0;
        $holidayOtAmount = ($request->holiday_ot_amount) ? str_replace(',', '', $request->holiday_ot_amount) : 0;
        $otTotal = ($request->ot_total) ? str_replace(',', '', $request->ot_total) : 0;
        $otTax   = ($request->ot_tax) ? str_replace(',', '', $request->ot_tax) : 0;
        $otNet   = ($request->ot_net) ? str_replace(',', '', $request->ot_net) : 0;

        $overtime = OvertimePay::find($request->id);
        $overtime->fill($request->all());
        $overtime->regular_ot_amount = $regularOtAmount;
        $overtime->weekend_ot_amount = $weekendOtAmount;
        $overtime->holiday_ot_amount = $holidayOtAmount;
        $overtime->ot_total   = $otTotal;
        $overtime->ot_tax     = $otTax;
        $overtime->ot_net     = $otNet;
        $overtime->updated_by = Auth::id();
        $overtime->save();

        return json_encode(['status'=>true,'response'=> 'Save Successfully!']);
    }

}
