<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\DeductionInfo;
use App\Deduction;
use App\Loan;
use App\LoanInfo;
use App\EmployeeInfo;
use App\Company;
use App\Employee;
class WASSLAIReportsController extends Controller
{
    function __construct(){
		$this->title = 'WASSLAI CAPITAL CONTRIBUTION AND LOANS';
    	$this->module = 'wasslai';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

        $company = new Company;

    	$companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction      = new Transaction;
        $deduction        = new Deduction;
        $deductioninfo    = new DeductionInfo;
        $loan             = new Loan;
        $loaninfo         = new LoanInfo;
        $employeeinfo     = new EmployeeInfo;

        $deduction = $deduction
        ->where('code','WD')
        ->select('id')
        ->first();

        $loan = $loan
        ->where('code','WASSSLAILOAN')
        ->select('id')
        ->first();

        $l_employee = $loaninfo
        ->where('loan_id',$loan->id)
        ->pluck('employee_id')
        ->toArray();

        $d_employee = $deductioninfo
        ->where('deduction_id',$deduction->id)
        ->pluck('employee_id')
        ->toArray();

        $arrEmployee = Employee::orWhereIn('id',$l_employee)
        ->orWhereIn('id',$d_employee)
        ->where('active',1)
        ->pluck('id')->toArray();

        $query = Transaction::leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->with([
            'employees',
            'loan_transaction' => function($qry) use($year,$month,$loan){
                $qey = $qry->where('loan_id',$loan->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'deduction_transaction' => function($qry) use($year,$month,$deduction){
                $qry = $qry->where('deduction_id',$deduction->id)
                ->where('year',$year)
                ->where('month',$month);
            }
        ])
        ->whereIn('employee_id',$arrEmployee)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('e.lastname')
        ->get();

        return json_encode($query);
    }
}
