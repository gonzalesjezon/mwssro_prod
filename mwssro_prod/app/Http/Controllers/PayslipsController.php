<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\LoanInfoTransaction;
use App\Transaction;
use App\EmployeeInfo;
use App\BenefitInfoTransaction;
use App\DeductionInfoTransaction;
use App\SalaryInfo;
use App\Company;
use App\Office;
use App\Division;
use App\Department;
use App\Position;
use App\AttendanceInfo;
use App\OvertimePay;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\PostedSignatory;
use App\PostedReport;

use Input;
use Auth;
class PayslipsController extends Controller
{
    function __construct(){
    	$this->title = 'PAYSLIP';
    	$this->module = 'payslips';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employee = new Employee;

        $employeeinfo = $employee->orderBy('firstname','asc')->get();

    	$response = array(
                            'employeeinfo'  => $employeeinfo,
        					'module'        => $this->module,
        					'controller'    => $this->controller,
                            'module_prefix' => $this->module_prefix,
        					'title'		    => $this->title,
                            'months'        => config('params.months'),
                           'latest_year'    => $this->latestYear(),
                           'earliest_year'  => $this->earliestYear(),
                           'current_month'  => (int)date('m'),
                           'employees'      => Employee::where('active',1)->orderBy('firstname','asc')->get(),
                           'postedreports'  => PostedReport::where('report_type','payslips')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show()
    {

    }

    public function getPayslip(){
        $data = Input::all();

        $employee_id = $data['id'];
        $year        = $data['year'];
        $month       = $data['month'];

        $employeeinformation = new EmployeeInformation;

        $category = $employeeinformation->where('employee_id',$data['id'])->with('employeestatus')->first();

        if($category->employeestatus->category == 1){

            $transaction = new Transaction;

            $query['transaction'] = $transaction
            ->with([
                'employees',
                'offices',
                'positions',
                'employeeinfo',
                'salaryinfo',
                'benefit_transactions' => function($qry) use($year,$month){
                    $qry = $qry
                    ->where('year',$year)
                    ->where('month',$month);
                },

                'loaninfoTransaction' => function($qry) use($year,$month){
                    $qry = $qry
                    ->where('year',$year)
                    ->where('month',$month);
                },

                'deductioninfoTransaction' => function($qry) use($year,$month){
                    $qry = $qry
                    ->where('year',$year)
                    ->where('month',$month);
                },

                'special_payroll' => function($qry) use($year,$month){
                    $qry = $qry
                    ->where('year',$year)
                    ->where('month',$month);
                },
                'rata' => function($qry) use($year,$month){
                    $qry = $qry
                    ->where('year',$year)
                    ->where('month',$month);
                },
            ])
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->first();

            $query['plantilla'] = true;

        }else{

            $transaction2   = new NonPlantillaTransaction;
            $employeeinfo2  = new NonPlantillaEmployeeInfo;

            $query['nonplantilla'] = NonPlantillaTransaction::with('employees','offices','positions')
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->first();

            if(isset($query['nonplantilla'])){


                $query['overtime'] = OvertimePay::where('employee_id',$employee_id)
                ->where('year',$year)
                ->where('month',$month)
                ->orderBy('created_at','desc')
                ->first();

                $query['employeeinfo'] = NonPlantillaEmployeeInfo::with('employees')
                ->where('employee_id',$employee_id)
                ->first();

                $query['plantilla'] = false;
            }

        }

        return json_encode($query);
    }

    public function postReport(Request $request){
        $year  = $request['data']['year'];
        $month = $request['data']['month'];

        $transaction = Transaction::where('year',$year)
        ->where('month',$month)
        ->get();


        foreach ($transaction as $key => $value) {

            $posted = PostedReport::where('year',$value->year)
            ->where('employee_id',$value->employee_id)
            ->where('month',$value->month)
            ->where('report_type','payslips')
            ->first();

            if(!$posted){

                $signatory = new PostedSignatory;
                $signatory->signatory_one   = @$request['data']['signatory_one'];
                $signatory->position_one    = @$request['data']['position_one'];
                $signatory->created_by     = Auth::id();
                if($signatory->save())
                {
                    $posted = new PostedReport;
                    $posted->employee_id    = $value->employee_id;
                    $posted->signatory_id   = $signatory->id;
                    $posted->year           = @$request['data']['year'];
                    $posted->month          = @$request['data']['month'];
                    $posted->report_type    = 'payslips';
                    $posted->created_by     = Auth::id();
                    $posted->save();
                }

                // if($posted->save()){
                //     $transaction = Transaction::where('year',$year)
                //     ->where('month',$month)
                //     ->where('employee_id',$value->employee_id)
                //     ->update(['posted' => 1]);
                // }
            }
        }

        return json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
    }

}
