<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
use App\SalaryInfo;
use App\Rate;
use DateTime;

class CNATransactionsController extends Controller
{

    function __construct(){
        $this->title = 'COLLECTIVE NEGOTIATION AGREEMENT';
        $this->controller = $this;
        $this->module = 'cna_transactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = array(
           'title'              => $this->title,
           'controller'         => $this->controller,
           'module'             => $this->module,
           'module_prefix'      => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $q          = Input::get('q');
        // Search param
        $year       = Input::get('year');
        $month      = Input::get('month');
        $check      = Input::get('check');

        // Filter param
        $_check     = Input::get('_check');
        $_year      = Input::get('_year');
        $_month     = Input::get('_month');

        $data = $this->searchName($q,$check,$year,$month);

        if(isset($_year) || isset($_month) || isset($_check)){
            $data = $this->filter($_year,$_month,$_check);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function searchName($q,$check,$year,$month){

        $employeestatus            = new EmployeeStatus;
        $employeeinfo              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id = $employeeinfo
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        $hasTransact = $transaction
        ->whereIn('employee_id',$employee_id)
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','cna')
        ->select('employee_id')
        ->get()->toArray();

        $query = [];
        switch ($check) {
            case 'w/':
               $query = $employee->whereIn('id',$hasTransact);
                break;
            default:
                $query = $employee
                ->whereIn('id',$employee_id);
                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$check){


        $employeestatus        = new EmployeeStatus;
        $employeeinformation   = new EmployeeInformation;
        $transaction            = new SpecialPayrollTransaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;

        $empstatus_id = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employee_id  = $employeeinformation
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        $hasTransact = $transaction
        ->whereIn('employee_id',$employee_id)
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','cna')
        ->select('employee_id')
        ->get()->toArray();


        $query = [];
        $response = "";
        switch ($check) {
            case 'w/':

                    $response = $employee
                    ->whereIn('id',$hasTransact)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();
                break;

            default:
                    $response = $employee
                    ->whereIn('id',$employee_id)
                    ->whereNotIn('id',$hasTransact)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();

                break;
        }

        return $response;
    }


    public function processCNA(Request $request){
        $data = Input::all();
        $employeeinformation = new EmployeeInformation;

        foreach ($data['list_id'] as $key => $value) {

            if(isset($value)){

                $transaction     = new SpecialPayrollTransaction;
                $employeeinfo    = $employeeinformation->where('employee_id',$value)->first();

                $transaction->employee_id  = $value;
                $transaction->position_id  = @$employeeinfo->position_id;
                $transaction->amount       = $data['amount'];
                $transaction->year         = $data['year'];
                $transaction->month        = $data['month'];
                $transaction->status       = 'cna';
                $transaction->save();

            }
        }

        $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

        return $response;
    }

    public function showCNADatatable(){

        $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getCNAInfo(){
        $data = Input::all();

        $query = SpecialPayrollTransaction::with('positions')
                    ->where('year',$data['year'])
                    ->where('month',$data['month'])
                    ->where('status','cna')
                    ->where('employee_id',@$data['employee_id'])->get();

        return json_encode($query);
    }

    public function deleteCNA(){
        $data = Input::all();

        foreach ($data['empid'] as $key => $value) {

            SpecialPayrollTransaction::where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->where('status','cna')
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
