<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Deduction;

class DeductionsController extends Controller
{
    function __construct(){
        $this->controller = $this;
        $this->title = 'DEDUCTIONS';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'deductions';
        $this->table = 'deductions';

    }

    public function index(){
        $response = array(
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

     public function store(Request $request){

        $amount =  str_replace(',', '', $request->amount);

        $deduction = Deduction::find($request->for_update);
        $message = 'Update Successfully.';
        if(empty($deduction)){
            $deduction = new Deduction;
            $message = 'Save Successfully.';
            $this->validate($request,[
                'code'                  => 'required',
                'name'                  => 'required',
                // 'payroll_group'         => 'required',
                // 'tax_type'              => 'required',
                // 'remarks'               => 'required'
                // 'alphalist_classification' => 'required',
                // 'amount'                => 'required',
            ]);
        }

        $deduction->fill($request->all());
        $deduction->amount                   = $amount;
        $deduction->code                     = $request->code;
        $deduction->name                     = $request->name;
        $deduction->payroll_group            = $request->payroll_group;
        $deduction->tax_type                 = $request->tax_type;
        $deduction->remarks                  = $request->remarks;
        $deduction->save();


        return json_encode(['status'=>true,'response' => $message]);
    }


    public function show(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['code','name','tax_type','payroll_group'];


        $query = Deduction::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = Deduction::where('id',$id)->first();

        return json_encode($query);
    }
}
