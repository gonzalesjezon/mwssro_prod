<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Deduction;
use App\DeductionInfoTransaction;
use App\Company;
use App\Employee;
class SSSContributionReportsController extends Controller
{
    function __construct(){
		$this->title = 'SSS CONTRIBUTION REPORT';
    	$this->module = 'ssscontributions';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regular Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $deduction =        new Deduction;
        $deductioninfo    = new DeductionInfoTransaction;

        $deduction = $deduction
        ->where('code','SSS')
        ->select('id')
        ->first();

        $query = $deductioninfo
        ->with([
            'employeeinfo',
            'employees'=>function($qry){
                $qry->orderBy('lastname','asc');
            }
        ])
        ->where('deduction_id',$deduction->id)
        ->where('year',$year)
        ->where('month',$month)
        ->get();


    	return json_encode($query);
    }
}
