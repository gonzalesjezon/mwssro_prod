<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LoanInfoTransaction;
use App\Company;
use App\Loan;
use Input;
use Auth;

class MPLRemittanceController extends Controller
{
    function __construct(){
		$this->title = 'MPL Schedule of Remittance';
    	$this->module = 'mpl_remittance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

		$response = array(
					'module'        => $this->module,
					'controller'    => $this->controller,
                    'module_prefix' => $this->module_prefix,
					'title'		    => $this->title,
					'company' 		=> Company::first(),
                    'company_name'  => 'Metropolitan Waterworks and Sewerage System',
					'months'        => config('params.months'),
                    'latest_year'   => $this->latestYear(),
                    'earliest_year' => $this->earliestYear(),
                    'current_month' => (int)date('m')
					);

    	return view($this->module_prefix.'.'.$this->module, $response);
	}

	public function show(){

		$data 	= Input::all();
		$year 	= $data['year'];
		$month 	= $data['month'];

		$loan = Loan::where('code','MPL')->first();

		$query = LoanInfoTransaction::with([
			'employees' => function($qry){
				$qry = $qry->with('pis_employee');
			},
			'payrollinfo',
		])
		->leftJoin('pms_employees as e','e.id','=','pms_loaninfo_transactions.employee_id')
		->where('year',$year)
		->where('month',$month)
		->where('loan_id',$loan->id)
		->orderBy('e.lastname','asc')
		->get();

		return json_encode($query);
	}
}
