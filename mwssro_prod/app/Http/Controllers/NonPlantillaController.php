<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\NonPlantillaEmployeeInfo;
use App\NonPlantillaTransaction;
use Carbon\Carbon;
use App\AttendanceInfo;
use Session;

class NonPlantillaController extends Controller
{

    function __construct(){
        $this->title = 'NON PLANTILLA';
        $this->module = 'nonplantilla';
        $this->module_prefix = 'payrolls';
        $this->controller = $this;

    }

    public function index(){

        $benefit        = Benefit::orderBy('name','asc')->get();
        $loans          = Loan::orderBy('name','asc')->get();
        $deduction      = Deduction::orderBy('name','asc')->get();
        $empstatus      = EmployeeStatus::where('category',0)->get();


        $response = array(
                        'empstatus'     => $empstatus,
                        'benefit'       => $benefit,
                        'loans'         => $loans,
                        'deductions'     => $deduction,
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function showBenefitinfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitdatatable',$response);
    }

    public function showLoaninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.loandatatable',$response);
    }

     public function showDeductioninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.deductdatatable',$response);
    }

    public function show(){
        $q               = Input::get('q');
        $year            = Input::get('year');
        $month           = Input::get('month');
        $checkpayroll    = Input::get('checkpayroll');
        $employee_status = Input::get('employee_status');
        $sub_period      = Input::get('sub_period');

        if(isset($year) || isset($month) || isset($checkpayroll) || isset($employee_status) || isset($sub_period)){
            $data = $this->filter($year,$month,$checkpayroll,$employee_status,$sub_period);    
        }else{

        }

        if($q)
        {
            $data = $this->searchName($q,$checkpayroll,$employee_status,$year,$month,$sub_period);
        }

        $response = array(
                        'data'          => @$data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function searchName($q,$checkpayroll,$employee_status,$year,$month,$sub_period){

        $employeestatus              = new EmployeeStatus;
        $employee_information        = new EmployeeInformation;
        $employee                    = new Employee;
        $nonplantilla_employee_info  = new NonPlantillaEmployeeInfo;
        $transaction                 = new NonPlantillaTransaction;

        $cols = ['lastname','firstname','middlename'];

        $empstatus_id = $employeestatus
        ->where('category',0)
        ->where('RefId',$employee_status)
        ->select('RefId')
        ->get()
        ->toArray();

        $employee_id = $employee_information
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

       $hasTransact = $transaction
       ->select('employee_id')
       ->where('year',$year)
       ->where('month',$month)
       ->where('sub_pay_period',$sub_period)
       ->get()
       ->toArray();

        $query = [];
        switch ($checkpayroll) {
            case 'wpayroll':

               $query = $employee->whereIn('id',$hasTransact);

                break;

            // case 'wopayroll':

            //     $query = $employee
            //     ->whereNotIn('id',$hasTransact)
            //     ->whereIn('id',$employee_id);

            //     break;

            default:
                $query = $employee->whereIn('id',$employee_id);
                break;
        }


        $query = $query->where(function($qry) use($q, $cols){
            foreach ($cols as $key => $value) {
                $qry->orWhere($value,'like','%'.$q.'%');
            }
        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;
    }

    public function filter($year,$month,$checkpayroll,$employee_status,$sub_period){


        $employeestatus         = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $nonplantilla           = new NonPlantillaTransaction;
        $employee               = new Employee;
        $nonplantilla_employee_info  = new NonPlantillaEmployeeInfo;

        // --GET EMPLOYEE STATUS ID EX(JOB ORDER)
        $empstatus_id = $employeestatus
        ->where('category',0)
        ->where('RefId',$employee_status)
        ->select('RefId')
        ->get()
        ->toArray();

        $employee_id  = $employee_information
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')->get()->toArray();

        $nonplantilla_employee_id =  $nonplantilla_employee_info
        ->select('employee_id')
        ->whereNotNull('daily_rate_amount')
        ->get()->toArray();

        $query = [];
        $response = "";

        $query = $nonplantilla->select('employee_id')
            ->whereIn('employee_status_id',$empstatus_id)
            ->where('year',$year)
            ->where('month',$month)
            ->where('sub_pay_period',$sub_period)
            ->get()->toArray();

        switch ($checkpayroll) {
            case 'wpayroll':
                    $response = $employee
                    ->whereIn('id',$query)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();
                break;

            default:
                    $response = $employee
                    ->whereIn('id',$employee_id)
                    ->whereIn('id',$nonplantilla_employee_id)
                    ->whereNotIn('id',$query)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }

     public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){

        $data = Input::all();

        $transaction  = new NonPlantillaTransaction;
        $employeeinfo = new NonPlantillaEmployeeInfo;

        $query['transaction'] = $transaction
                            ->with('employees')
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            ->where('sub_pay_period',@$data['sub_period'])
                            ->where('employee_id',@$data['id'])->first();


        $query['employeeinfo'] = $employeeinfo
                                ->with('taxpolicyOne','taxpolicyTwo')
                                ->where('employee_id',@$data['id'])
                                ->first();


        // $query['benefitinfo'] = BenefitInfo::with(['benefits' => function($qry) {$qry->where('name','PERA');} ])->where('employeeinfo_id',$query['employeeinfo']['id'])
        //                                 ->get();

        // $query['deductioninfo'] = DeductionInfo::with('deductions')->where('employeeinfo_id',$query['employeeinfo']['id'])
        //                                 ->get();

        // $query['loaninfo'] = LoanInfo::with('loans')->where('employeeinfo_id',$query['employeeinfo']['id'])
                                        // ->get();

        return json_encode($query);
    }

    public function processPayroll() {
        $data = Input::all();

        $fromDate        = $data['from_date'];
        $toDate          = $data['to_date'];
        $year            = $data['year'];
        $month           = $data['month'];
        $semi_pay_period = $data['summary']['semi_pay_period'];
        $emp_code        = $data['emp_code'];
        $transactions =  new NonPlantillaTransaction;

        if(isset($data['transaction_id'])){

            //ATTENDACE

            // TRANSACTIONS
            $actual_basicpay     = ($data['summary']['actual_basicpay']) ? str_replace(',', '', $data['summary']['actual_basicpay']) : 0;
            $adjust_basicpay     = ($data['summary']['adjust_basicpay']) ? str_replace(',', '', $data['summary']['adjust_basicpay']) : 0;
            $total_basicpay      = ($data['summary']['total_basicpay']) ? str_replace(',', '', $data['summary']['total_basicpay']) : 0;

            $actual_absences     = ($data['summary']['actual_absences']) ? str_replace(',', '', $data['summary']['actual_absences']) : 0;
            $adjust_absences     = ($data['summary']['adjust_absences']) ? str_replace(',', '', $data['summary']['adjust_absences']) : 0;
            $total_absences      = ($data['summary']['total_absences']) ? str_replace(',', '', $data['summary']['total_absences']) : 0;

            $actual_tardines     = ($data['summary']['actual_tardines']) ? str_replace(',', '', $data['summary']['actual_tardines']) : 0;
            $adjust_tardines     = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['adjust_tardines']) : 0;
            $total_tardines      = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['total_tardines']) : 0;

            $actual_undertime     = ($data['summary']['actual_undertime']) ? str_replace(',', '', $data['summary']['actual_undertime']) : 0;
            $adjust_undertime     = ($data['summary']['adjust_undertime']) ? str_replace(',', '', $data['summary']['adjust_undertime']) : 0;
            $total_undertime      = ($data['summary']['total_undertime']) ? str_replace(',', '', $data['summary']['total_undertime']) : 0;
            $phicAmount           = ($data['summary']['phic_amount']) ? str_replace(',', '', $data['summary']['phic_amount']) : 0;
            $taxAmountTwo         = ($data['summary']['tax_amount_two']) ? str_replace(',', '', $data['summary']['tax_amount_two']) : 0;

            // $actual_contribution     = ($data['summary']['actual_contribution']) ? str_replace(',', '', $data['summary']['actual_contribution']) : 0;
            // $adjust_contribution     = ($data['summary']['adjust_contribution']) ? str_replace(',', '', $data['summary']['adjust_contribution']) : 0;
            // $total_contribution      = ($data['summary']['total_contribution']) ? str_replace(',', '', $data['summary']['total_contribution']) : 0;

            // $actual_loan     = ($data['summary']['actual_loan']) ? str_replace(',', '', $data['summary']['actual_loan']) : 0;
            // $adjust_loan     = ($data['summary']['adjust_loan']) ? str_replace(',', '', $data['summary']['adjust_loan']) : 0;
            // $total_loan      = ($data['summary']['total_loan']) ? str_replace(',', '', $data['summary']['total_loan']) : 0;

            // $actual_otherdeduct     = ($data['summary']['actual_otherdeduct']) ? str_replace(',', '', $data['summary']['actual_otherdeduct']) : 0;
            // $adjust_otherdeduct     = ($data['summary']['adjust_otherdeduct']) ? str_replace(',', '', $data['summary']['adjust_otherdeduct']) : 0;
            // $total_otherdeduct      = ($data['summary']['total_otherdeduct']) ? str_replace(',', '', $data['summary']['total_otherdeduct']) : 0;

            $basic_net_pay     = ($data['summary']['basic_net_pay']) ? str_replace(',', '', $data['summary']['basic_net_pay']) : 0;
            $net_deduction     = ($data['summary']['net_deduction']) ? str_replace(',', '', $data['summary']['net_deduction']) : 0;
            $grossPay         = ($data['summary']['gross_pay']) ? str_replace(',', '', $data['summary']['gross_pay']) : 0;
            $gross_taxable_pay = ($data['summary']['gross_taxable_pay']) ? $data['summary']['gross_taxable_pay'] : 0;
            $taxAmountOne      = ($data['summary']['tax_amount_one']) ? str_replace(',', '', $data['summary']['tax_amount_one']) : 0;

            $actual_workdays   = ($data['attendance']['actual_workdays']) ? $data['attendance']['actual_workdays'] : 0;
            $actual_absent     = ($data['attendance']['actual_absences']) ? $data['attendance']['actual_absences'] : 0;
            $actual_tard       = ($data['attendance']['actual_tardines']) ? $data['attendance']['actual_tardines'] : 0;
            $actual_under      = ($data['attendance']['actual_undertime']) ? $data['attendance']['actual_undertime'] : 0;
            $adjust_workdays   = ($data['attendance']['adjust_workdays']) ? $data['attendance']['adjust_workdays'] : 0;
            $adjust_absent     = ($data['attendance']['adjust_absences']) ? $data['attendance']['adjust_absences'] : 0;
            $adjust_tard       = ($data['attendance']['adjust_tardines']) ? $data['attendance']['adjust_tardines'] : 0;
            $adjust_under      = ($data['attendance']['adjust_undertime']) ? $data['attendance']['adjust_undertime'] : 0;

            $lwopAmount = $total_absences + $total_tardines + $total_undertime;


            $totalDeduction = $taxAmountOne + $taxAmountTwo + $phicAmount;
            $netPay  = $basic_net_pay - $totalDeduction;


            $transactions = NonPlantillaTransaction::find($data['transaction_id']);

            $transactions->actual_workdays  = $actual_workdays;
            $transactions->actual_absences  = $actual_absent;
            $transactions->actual_tardiness = $actual_tard;
            $transactions->actual_undertime = $actual_under;
            $transactions->adjust_workdays  = $adjust_workdays;
            $transactions->adjust_absences  = $adjust_absent;
            $transactions->adjust_tardiness = $adjust_tard;
            $transactions->adjust_undertime = $adjust_under;

            $transactions->actual_basicpay_amount     = $actual_basicpay;
            $transactions->adjust_basicpay_amount     = $adjust_basicpay;
            $transactions->total_basicpay_amount      = $total_basicpay;
            $transactions->actual_absences_amount     = $actual_absences;
            $transactions->adjust_absences_amount     = $adjust_absences;
            $transactions->total_absences_amount      = $total_absences;
            $transactions->actual_undertime_amount    = $actual_tardines;
            $transactions->adjust_tardiness_amount    = $adjust_tardines;
            $transactions->total_tardiness_amount     = $total_tardines;
            $transactions->actual_undertime_amount    = $actual_undertime;
            $transactions->adjust_undertime_amount    = $adjust_undertime;
            $transactions->total_undertime_amount     = $total_undertime;
            $transactions->phic_amount                = $phicAmount;
            $transactions->basic_net_pay              = $basic_net_pay;
            $transactions->pay_period                 = @$pay_period;
            $transactions->sub_pay_period             = @$semi_pay_period;
            $transactions->tax_rate_amount_one        = $taxAmountOne;
            $transactions->tax_rate_amount_two        = $taxAmountTwo;
            $transactions->net_deduction              = $net_deduction;
            $transactions->gross_pay                  = $grossPay;
            $transactions->gross_taxable_pay          = $gross_taxable_pay;
            $transactions->net_pay                    = $netPay;

            $transactions->save();


            $response = json_encode(['status'=>true,'response'=>'Payroll Updated Successfully!']);

        }else{

            $ctr = 0;
            $error = 0;
            $error_id = [];
            foreach ($data['empid'] as $key => $_id) {

                if(isset($_id)){
                    $check = NonPlantillaEmployeeInfo::where('employee_id',$_id)->whereNotNull('daily_rate_amount')->first();
                    if($check === null){
                        $error++;
                        $error_id[] = $_id;
                    }
                }
            }

            $y = date('Y', strtotime($fromDate));
            $m = date('m', strtotime($toDate));

            // $cosDays = cal_days_in_month(CAL_GREGORIAN,$m,$y); // gsis days
            $cosDays = 22;
            $totalCosDays = $cosDays;
            $joDays = 22;

            switch ($semi_pay_period) {
                case 'firsthalf':
                    $cosDays = $cosDays / 2;
                    $joDays = $joDays / 2;
                    break;

                default:
                    // $cosDays = round(($cosDays / 2) - 1);
                    $cosDays = $cosDays / 2;
                    $joDays = $joDays / 2;
                    break;
            }

            $workdays   = 0;
            $threshHold = 250000;
            if($error == 0){
                foreach ($data['empid'] as $key => $_id) {
                    if(isset($_id)){

                        $employeeinfo        = NonPlantillaEmployeeInfo::where('employee_id',$_id)->first();

                        $employeeinformation = EmployeeInformation::where('employee_id',$_id)->first();
                        $monthly             = $employeeinfo->monthly_rate_amount;
                        $daily               = $employeeinfo->daily_rate_amount;;

                        $annual_rate_amount  = $monthly * 12;

                        $monthly_rate_amount = 0;
                        switch ($emp_code) {
                            case 'COS':
                                $temp_daily = $monthly / $totalCosDays;
                                $monthly_rate_amount = $temp_daily * $cosDays;
                                $workdays = $cosDays;
                                break;
                            case 'JO':
                                $monthly_rate_amount = $daily * $joDays;
                                $workdays = $joDays;
                                break;
                            default:
                                $monthly_rate_amount = $daily * 22;
                                $workdays = 22;
                                break;
                        }


                        $rateOne = TaxPolicy::where('id',$employeeinfo->taxpolicy_id)->select('job_grade_rate')->first();
                        $rateTwo = TaxPolicy::where('id',$employeeinfo->taxpolicy_two_id)->select('job_grade_rate')->first();
                        $halfAmount     = $monthly / 2;
                        $taxAmountOne   = $halfAmount * @$rateOne->job_grade_rate;
                        # Tax Rate Two Computation
                        // $threshHoldAmount = $annual_rate_amount - $threshHold;
                        // $grossTaxRateTwo  = $threshHoldAmount * @$rateTwo->job_grade_rate;
                        // $taxAmountTwo     = ($grossTaxRateTwo / 12) / 2;
                        // $taxAmountTwo = $taxAmountTwo / 2;

                        # if threshold amount negavite
                        // if($threshHoldAmount < 0){
                        //     $taxAmountTwo = 0;
                        // }

                        // $netPay = $halfAmount - $taxAmountOne - round($taxAmountTwo,2);
                        $phicAmount = ($semi_pay_period == 'firsthalf') ? @$employeeinfo->phic_amount : 0;
                        $netPay = $halfAmount - $taxAmountOne - $phicAmount;

                        $transaction = new NonPlantillaTransaction;

                        $transaction->employee_id         = $_id;
                        $transaction->employeeinfo_id     = $employeeinfo->id;
                        $transaction->division_id         = $employeeinformation->division_id;
                        $transaction->company_id          = $employeeinformation->company_id;
                        $transaction->position_item_id    = $employeeinformation->position_item_id;
                        $transaction->office_id           = $employeeinformation->office_id;
                        $transaction->department_id       = $employeeinformation->department_id;
                        $transaction->employee_status_id  = $employeeinformation->employee_status_id;
                        $transaction->position_id         = $employeeinformation->position_id;
                        $transaction->employee_number     = @$employeeinformation->employee_number;
                        $transaction->phic_amount         = $phicAmount;
                        $transaction->actual_workdays     = $workdays;
                        $transaction->sub_pay_period      = $semi_pay_period;
                        $transaction->monthly_rate_amount = $monthly;
                        $transaction->actual_basicpay_amount = $halfAmount;
                        $transaction->gross_pay           = $halfAmount;
                        $transaction->annual_rate_amount  = $annual_rate_amount;
                        $transaction->tax_rate_amount_one = $taxAmountOne;
                        // $transaction->tax_rate_amount_two = $taxAmountTwo;
                        $transaction->gross_taxable_pay   = $netPay;
                        $transaction->net_pay             = $netPay;
                        $transaction->from_date           = $fromDate;
                        $transaction->to_date             = $toDate;
                        $transaction->year                = $year;
                        $transaction->month               = $month;

                        $transaction->save();
                        $ctr++;
                    }
                }
                $response = json_encode(['status'=>true,'response'=>'Processed Successfully! <br>'.$ctr.' Records Saved']);
            }else{
                $error_id = implode(',', $error_id);
                $response = json_encode(['status'=>false,'response'=>'Save Failed! <br>'.$error.' Records <br>No Employee Setup <br>Employee ID <br>['.$error_id.']']);
            }
        }

        return $response;
    }


    public function destroy(){
        $data = Input::all();

        $transactions = new NonPlantillaTransaction;

        foreach ($data['empid'] as $key => $value) {

            $transactions->where('employee_id',$data['empid'][$key])
                                ->where('year',$data['year'])
                                ->where('month',$data['month'])
                                ->where('sub_pay_period',$data['sub_period'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

}
