<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
use App\EmployeeInfo;
use App\AttendanceInfo;
use App\Company;
use App\Employee;
class RataTransmitalReportsController extends Controller
{
    function __construct(){
    	$this->title = 'RATA TRANSMITAL';
    	$this->module = 'ratatransmital';
        $this->module_prefix = 'payrolls/reports/ratareports';
    	$this->controller = $this;

    }

    public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];
        $pay_period = $data['pay_period'];

        $query['transaction'] = Rata::with([
            'deductions' => function($qry) use($year,$month,$pay_period){
                $qry = $qry->where('year',$year)
                ->where('month',$month)
                ->where('pay_period',$pay_period);
            },
        ])->where('year',$year)
        ->where('month',$month)
        ->where('pay_period',$pay_period)
        ->get();
        
       return json_encode($query);
    }
}
