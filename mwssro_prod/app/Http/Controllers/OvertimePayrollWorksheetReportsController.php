<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\OvertimePay;
use App\PostedSignatory;
use App\PostedReport;
class OvertimePayrollWorksheetReportsController extends Controller
{
    function __construct(){
    	$this->title = 'OVERTIME PAYROLL WORKSHEET';
    	$this->module = 'overtimepayrollworksheet';
        $this->module_prefix = 'payrolls/reports/overtimereports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'postedreports'  => PostedReport::where('report_type','otpws')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new OvertimePay;

        $data = Input::all();
        
        $year       = $data['year'];
        $month      = $data['month'];

        $query = OvertimePay::with([
            'employeeinfo',
            'employeeinformation',
            'salaryinfo',
            'employees',
            'transaction' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                        ->where('month',$month);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','plantilla')
        ->get();
        
       $query2 = OvertimePay::where('year',$year)
        ->where('month',$month)
        ->first();

        $from = date('F', strtotime(@$query2->from_date));
        $to   = date('F', strtotime(@$query2->to_date));

       return json_encode([
        'transaction' => $query,
        'From' => $from,
        'To' => $to,
       ]);
    }

    public function postReport(Request $request){
        
        $postedreports = new PostedReport;
        $postedreports->pay_period   = @$request['data']['pay_period'];
        $postedreports->year         = @$request['data']['year'];
        $postedreports->month        = @$request['data']['month'];
        $postedreports->created_by   = Auth::id();
        $postedreports->report_type  = 'otpws';
        $postedreports->save();

        return json_encode([
            'status'=>true,
            'response'=> 'Report posted successfully.'
        ]);


    }
}
