<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
use App\Company;
use App\Employee;
class YearEndTransmitalReportsReportsController extends Controller
{
    function __construct(){
    	$this->title = 'TRANSMITAL';
    	$this->module = 'yearendtransmital';
        $this->module_prefix = 'payrolls/reports/yearendreports';
    	$this->controller = $this;

    }

    public function index(){

    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new SpecialPayrollTransaction;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $year_end_amount = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','yearendbonus')
        ->sum('amount');

         $cash_gift_amount = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','yearendbonus')
        ->sum('cash_gift_amount');

        $net_amount = (float)$year_end_amount + (float)$cash_gift_amount;


        $query['transaction'] = $net_amount;

       	return json_encode($query);
    }
}
