<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfoTransaction;
use App\Benefit;
use App\Loan;
use App\Company;
use Input;
class ServiceLoanReportsController extends Controller
{
    function __construct(){
    	$this->title = 'SERVICE LOAN';
    	$this->module = 'serviceloans';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new Transaction;
        $loaninfo    = new LoanInfoTransaction;
        $loan        = Loan::where('loan_type','GSIS')
        ->pluck('id')
        ->toArray();


        $countLoanInfo = $loaninfo
        ->with('loans')
        ->whereIn('loan_id',$loan)
        ->where('year',$year)
        ->where('month',$month)
        ->groupBy('loan_id')
        ->get();

        $loanCount = count($countLoanInfo);

        $employee_id = $loaninfo
        ->whereIn('loan_id',$loan)
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()->toArray();

        $query = Transaction::leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->with([
            'employees' => function($qry){
                $qry->orderBy('lastname','asc');
                },
            'loaninfoTransaction',
            'positionitems',
            'positions',
            'divisions',
            'offices',
            'employeeinformation',
            'salaryinfo',
            'employeeinfo'
        ])
        ->whereIn('employee_id',$employee_id)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('e.lastname')
        ->get();


        return json_encode([
            'transaction'   => $query,
            'loanlists'     => $countLoanInfo,
            'loancount'     => $loanCount,
        ]);
    }
}
