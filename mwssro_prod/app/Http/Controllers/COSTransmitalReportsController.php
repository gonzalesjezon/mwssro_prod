<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\EmployeeStatus;
use App\Company;
use App\Employee;
class COSTransmitalReportsController extends Controller
{
    function __construct(){
    	$this->title = 'COS TRANSMITAL';
    	$this->module = 'costransmital';
        $this->module_prefix = 'payrolls/reports/nonplantillareports';
    	$this->controller = $this;

    }

    public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $employees = Employee::where('active',1)->orderBy('lastname','asc')->get();

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'          => $employees
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new NonPlantillaTransaction;
        $employeeStatus =  new EmployeeStatus;
        $data = Input::all();

        $year      = $data['year'];
        $month     = $data['month'];
        $payPeriod = $data['pay_period'];

        $employeeStatus = $employeeStatus->where('Code','COS')->first();

        $query = $transaction
        ->where('employee_status_id',$employeeStatus->RefId)
        ->where('year',$year)
        ->where('month',$month)
        ->where('sub_pay_period',$payPeriod)
        ->sum('net_pay');

        $query2 = $transaction->where('year',$year)
        ->whereMonth('month',$month)
        ->where('sub_pay_period',$payPeriod)
        ->first();

        $from = date('F d', strtotime(@$query2->from_date));
        $to   = date('F d', strtotime(@$query2->to_date));

       return json_encode([
            'transaction' => $query,
            'From' => $from,
            'To' => $to,
        ]);
    }

}
