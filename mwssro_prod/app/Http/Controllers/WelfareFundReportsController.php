<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Deduction;
use App\DeductionInfo;
use App\Transaction;
use App\Company;
use App\Employee;
class WelfareFundReportsController extends Controller
{
    function __construct(){
		$this->title = 'WELFARE FUND REPORT';
    	$this->module = 'welfarefund';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data   = Input::all();
        $year   = $data['year'];
        $month  = $data['month'];
    	$transaction       = new Transaction;
        $deduction         = new Deduction;
        $deductioninfo     = new DeductionInfo;

        $deductionId = $deduction->where('code','WF')->select('id')->first();

        $arrEmployee = DeductionInfo::where('deduction_id',$deductionId->id)
        ->where('terminated',0)
        ->pluck('employee_id')
        ->toArray();

        $query = Transaction::leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->with([
            'employees',
            'deduction_transaction' => function($qry) use($year,$month,$deductionId){
                $qry = $qry->where('year',$year)
                ->where('month',$month)
                ->where('deduction_id',$deductionId->id);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->whereIn('employee_id',$arrEmployee)
        ->orderBy('e.lastname')
        ->get()->toArray();


    	return json_encode($query);
    }
}
