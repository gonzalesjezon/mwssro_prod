<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\PostedReport;
use App\PostedSignatory;
use App\OvertimePay;
use App\Employee;
class GeneralPayrollOvertimeReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'generalpayrollovertime';
        $this->module_prefix = 'payrolls/reports/overtimereports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'        => config('params.months'),
                       'latest_year'    => $this->latestYear(),
                       'earliest_year'  => $this->earliestYear(),
                       'current_month'  => (int)date('m'),
                       'employees'      =>  Employee::where('active',1)->orderBy('firstname','asc')->get(),
                       'postedreports'  => PostedReport::where('report_type','otgp')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show()
    {

    	$transaction = new OvertimePay;

        $data = Input::all();

        $year       = $data['year'];
        $month      = $data['month'];
        // $pay_period = $data['pay_period'];

        $query = OvertimePay::with([
            'employeeinfo',
            'employeeinformation',
            'salaryinfo',
            'employees',
            'transaction' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                        ->where('month',$month);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','plantilla')
        ->get();

        $query2 = OvertimePay::where('year',$year)
        ->where('month',$month)
        ->first();

        $from = date('F', strtotime(@$query2->from_date));
        $to   = date('F', strtotime(@$query2->to_date));

       return json_encode([
        'transaction' => $query,
        'From' => $from,
        'To' => $to,
       ]);
    }

    public function postReport(Request $request){
        
        $postedreports = new PostedReport;
        $signatory     =  new PostedSignatory;

        $signatory->signatory_one   = @$request['data']['signatory_one'];
        $signatory->signatory_two   = @$request['data']['signatory_two'];
        $signatory->signatory_three = @$request['data']['signatory_three'];
        $signatory->signatory_four  = @$request['data']['signatory_four'];
        $signatory->position_one    = @$request['data']['position_one'];
        $signatory->position_two    = @$request['data']['position_two'];
        $signatory->position_three  = @$request['data']['position_three'];
        $signatory->position_four   = @$request['data']['position_four'];
        $signatory->created_by      = Auth::id();
        if($signatory->save()){
            $postedreports->pay_period   = @$request['data']['pay_period'];
            $postedreports->year         = @$request['data']['year'];
            $postedreports->month        = @$request['data']['month'];
            $postedreports->signatory_id = $signatory->id;
            $postedreports->created_by   = Auth::id();
            $postedreports->report_type  = 'otgp';
            $postedreports->save();
        }

        return json_encode([
            'status'=>true,
            'response'=> 'Report posted successfully.'
        ]);


    }
}
