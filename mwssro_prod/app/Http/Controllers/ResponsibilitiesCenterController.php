<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResponsibilityCenter;

use Input;
use Auth;
class ResponsibilitiesCenterController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'RESPONSIBILITY CENTER';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'responsibilitiescenter';
        $this->table = 'responsibilitycenter';
    }

    public function index(){
        $response = array(
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){


        $rcenter = ResponsibilityCenter::find($request->id);
        $message = 'Update Successfully.';
        if(empty($rcenter)){
            $this->validate($request,[
                'code'                  => 'required|unique:pms_responsibility_center',
                'name'                  => 'required',
            ]);
            $message = 'Save Successfully.';
            $rcenter = new ResponsibilityCenter;
        }
        $rcenter->fill($request->all());
        if($message == 'Update Successfully.'){
            $rcenter->updated_by = Auth::id();
        }else{
            $rcenter->created_by = Auth::id();
        }
        $rcenter->save();

        return json_encode(['status'=>true,'response' => $message]);
    }

    public function show(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['code','name'];


        $query = ResponsibilityCenter::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = ResponsibilityCenter::where('id',$id)->first();

        return json_encode($query);
    }
}