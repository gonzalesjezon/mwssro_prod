<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfo;
use Input;
class PayrollTransfersReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL TRANSFERS';
    	$this->module = 'payrolltransfers';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function getEmployeeinfo(){

        $q = Input::all();

        $query = Transaction::with(['employees','positionitems'=>function($qry){ $qry->with('positions'); },'offices','employeeinformation','salaryinfo'])->where('year',$q['year'])->where('month',$q['month']);

        $query = $query->with(['employeeinfo'=>function($qry){ $qry = $qry->with(['loaninfo' => function($qry){
                                $qry->with('loans:id,name')->get();
                            }]);
                        }])->orderBy('office_id','asc')->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                $data[$value->offices->name][$key] = $value;
            }

        }else{
            $data = [];
        }

        return json_encode(collect($data)->reverse()->toArray());
    }
}
