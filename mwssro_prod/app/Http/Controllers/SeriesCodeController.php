<?php

namespace App\Http\Controllers;

use App\SeriesCode;
use Illuminate\Http\Request;

use Auth;
use Input;

class SeriesCodeController extends Controller
{

    function __construct(){
        $this->title         = 'Series Code';
        $this->controller    = $this;
        $this->module        = 'series_codes';
        $this->module_prefix = 'payrolls/admin/filemanagers';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $response = array(
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'series'        => SeriesCode::orderBy('code','asc')->paginate(50),
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $response = array(
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'action'        => 'SeriesCodeController@store',
        );

        return view($this->module_prefix.'.'.$this->module.'.create',$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $series = SeriesCode::find(@$request->id);
        $message = 'Series Code Updated!';
        $status = true;
        if(empty($series)){

            $status = false;
            $this->validate($request,[
                'code' => 'required|unique:pms_series_codes',
                'description'
            ]);

            $message = 'Series Code Created!';
            $series = new SeriesCode;
            
        }

        
        $series->fill($request->all());

        if($status == true){
            $series->updated_by = Auth::id();
        }else{
            $series->created_by = Auth::id();
        }
        $series->save();

        return redirect()
            ->route('series_codes.edit',[
                'id' => $series->id
            ])->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SeriesCode  $seriesCode
     * @return \Illuminate\Http\Response
     */
    public function show(SeriesCode $seriesCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SeriesCode  $seriesCode
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $series = SeriesCode::find($id);

        $response = array(
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'series'        => $series,
        );

        return view($this->module_prefix.'.'.$this->module.'.edit',$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SeriesCode  $seriesCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SeriesCode  $seriesCode
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SeriesCode::destroy($id);
        return redirect($this->module_prefix.'/series_codes')->with('success', 'Series code successfully deleted!');
    }
}
