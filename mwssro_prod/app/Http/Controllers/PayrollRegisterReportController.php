<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
class PayrollRegisterReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL REGISTER REPORT';
    	$this->module = 'payrollregister';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function getEmployeeinfo(){

        $q = Input::all();

        $query = Transaction::with('employees','offices','employeeinformation','salaryinfo')->where('year',$q['year'])->where('month',$q['month']);

        $query = $query->with(['employeeinfo'=>function($qry){ $qry = $qry->with(['loaninfo' => function($qry){
                                $qry->with('loans:id,name')->get();
                            }]);
                        }])->orderBy('office_id','desc')->get();

        $data = [];
        if(isset($query)){

            foreach ($query as $key => $value) {

                $data[$value->offices->Name][$key] = $value;
            }

        }else{

            $data = [];
        }

        return json_encode($data);
    }
}
