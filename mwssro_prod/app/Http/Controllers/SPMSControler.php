<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SPMSControler extends Controller
{
   public function sysCaller ($file) {
      $param = ["file"=>$file];
      return view('pages.sysCaller',$param);
   }
   public function welcome ($sess) {
      $param = ["user_session"=>$sess];
      return view('pages.spmsWelcome',$param);
   }
}
