<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\Transaction;
use App\Company;
use App\Employee;
class TaxesRemittanceReportsController extends Controller
{
    function __construct(){
		$this->title = 'TAX REMITTANCE';
    	$this->module = 'taxesremittance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regular Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();

    	$transaction = new Transaction;

        $query = $transaction
        ->with([
            'employeeinfo',
            'employees'=>function($qry){
	        	$qry->orderBy('lastname','asc');
	        },
            'salaryinfo'
        ])
      ->where('year',$data['year'])
      ->where('month',$data['month'])
      ->get()->toArray();


    	return json_encode($query);
    }
}
