<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\LeaveMonetizationTransaction;
use App\Employee;
class MonetizationGeneralPayrollReportsReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'monetizationgeneralpayroll';
        $this->module_prefix = 'payrolls/reports/monetizationreports';
    	$this->controller = $this;

    }

    public function index(){

    	$transaction = new LeaveMonetizationTransaction;

    	$transaction = $transaction->groupBy('remarks')->get();

    	$response = array(
    					'transaction' 	=> $transaction,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new LeaveMonetizationTransaction;

        $data = Input::all();

        $created_at = $data['created_at'];

        $query['transaction'] = LeaveMonetizationTransaction::leftJoin('pms_employees as e','e.id','=','pms_leave_monetization_transactions.employee_id')
            ->with([
                'salarygrade',
                'salaryinfo',
                'positions',
                'employees',
                'transaction',
                'employeeinfo'
            ])
            ->where('pms_leave_monetization_transactions.created_at','like','%'.$created_at.'%')
            ->orderBy('e.lastname')
            ->get();

       return json_encode($query);
    }
}
