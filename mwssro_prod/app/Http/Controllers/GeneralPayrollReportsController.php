<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\EmployeeInfo;
use App\Employee;
use App\PostedSignatory;
use App\PostedReport;
use Carbon\Carbon;
use App\Exports\ExportRata;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
class GeneralPayrollReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'generalpayroll';
        $this->module_prefix = 'payrolls/reports/plantillareports';
    	$this->controller = $this;

    }

    public function index(){

        $employees = Employee::where('active',1)->orderBy('firstname','asc')->get();

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'        => config('params.months'),
                       'latest_year'    => $this->latestYear(),
                       'earliest_year'  => $this->earliestYear(),
                       'current_month'  => (int)date('m'),
                       'employees'      => $employees,
                       'postedreports'  => PostedReport::where('report_type','regulargp')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new Transaction;
    	$employeeinfo = new EmployeeInfo;

        $data = Input::all();

        $year      = $data['year'];
        $month     = $data['month'];
        $payPeriod = $data['pay_period'];

        $query = $transaction
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'salaryinfo',
            'positions',
            'employeeinfo',
            'employees',
            'offices',
            'divisions',
            'benefitTransactions'=>function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month)
                ->where('status','pera');
            },

            'additional_deduction' => function($qry) use($year,$month,$payPeriod){
                $qry = $qry
                ->where('pay_period',$payPeriod)
                ->where('year',$year)
                ->where('month',$month);
            },
        ])
		->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.Code','asc')
        ->orderBy('dept_code','desc')
        ->orderBy('item_number','asc')
        ->orderBy('salary_grade','desc')
        ->get();

        $data = [];

        if(isset($query)){
            foreach ($query as $key => $value) {
                if(isset($value->office_id)){
                    $name = explode('-', @$value->offices->Name);

                    $title = @$name[0];
                    if(strpos(@$value->offices->Name, '-'))
                    {
                        $title = @$name[1];
                    }

                    $data[@$title][@$value->dept_code][$key] = $value;
                }
            }
        }


       return json_encode([
        'transaction' => $data
       ]);
    }

    public function generateCSV(Request $request)
    {
        
        $year      = Input::get('_year');
        $month     = Input::get('_month');
        $payperiod = Input::get('_payperiod');

        $query = Transaction::where('year',$year)
        ->where('month',$month)
        ->get();

        $status = false;
        if(count($query) > 0)
        {   
            $string = [];
            foreach ($query as $key => $value) {

                $forRelease = $value->net_pay / 2;

                $string[$key] = array(
                    'account' =>  str_replace('-', '', $value->employeeinfo->atm_no),
                    'name'    => $value->employees->getNameForCSV(),
                    'amount'  => str_replace(array('.',','), '', number_format($forRelease,2))
                );
            }

            return Excel::download(new ExportRata($string), 'generalpayroll_'.date('YmdHis',strtotime(Carbon::now())).'.csv');

            $status = true;
        }


    }

    public function postReport(Request $request){
        
        $postedreports = new PostedReport;
        $signatory     =  new PostedSignatory;

        $signatory->signatory_one   = @$request['data']['signatory_one'];
        $signatory->signatory_two   = @$request['data']['signatory_two'];
        $signatory->signatory_three = @$request['data']['signatory_three'];
        $signatory->signatory_four  = @$request['data']['signatory_four'];
        $signatory->position_one    = @$request['data']['position_one'];
        $signatory->position_two    = @$request['data']['position_two'];
        $signatory->position_three  = @$request['data']['position_three'];
        $signatory->position_four   = @$request['data']['position_four'];
        $signatory->created_by      = Auth::id();
        if($signatory->save()){
            $postedreports->pay_period   = @$request['data']['pay_period'];
            $postedreports->year         = @$request['data']['year'];
            $postedreports->month        = @$request['data']['month'];
            $postedreports->signatory_id = $signatory->id;
            $postedreports->created_by   = Auth::id();
            $postedreports->report_type  = 'regulargp';
            $postedreports->save();
        }

        return json_encode([
            'status'=>true,
            'response'=> 'Report posted successfully.'
        ]);


    }
}
