<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\SalaryAdjustment;
use Input;
class LWOPAdjustmentReportsController extends Controller
{
    function __construct(){
    	$this->title = 'SCHEDULE SALARY ADJUSTMENT';
    	$this->module = 'lwopadjustments';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$employeeinfo = Employee::where('active',1)->orderBy('lastname','asc')->get();

        $response = array(
                        'employeeinfo'  => $employeeinfo,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function getAdjustments(){
        $data = Input::all();

        $employee_id = $data['id'];

        $adjustment =  new SalaryAdjustment;

        $query = $adjustment
        ->where('employee_id',$employee_id)
        ->get();

        return $query;
    }

    public function show(){
        $data = Input::all();

        $employee_id = $data['id'];
        $month        = $data['month'];

        $adjustment = new SalaryAdjustment;

        $query = $adjustment
        ->with('employees')
        ->where('employee_id',$employee_id)
        ->where('transaction_date',$month)
        ->first();

        return $query;
    }
}
