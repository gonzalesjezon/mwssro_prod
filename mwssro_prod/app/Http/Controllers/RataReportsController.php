<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Rata;
use App\Company;
use App\PostedReport;
use App\PostedSignatory;
use App\Employee;
use Carbon\Carbon;
use App\Exports\ExportRata;
use Maatwebsite\Excel\Facades\Excel;
class RataReportsController extends Controller
{
    function __construct(){
    	$this->title = 'REPRESENTATION AND TRANSPORTATION ALLOWANCE';
    	$this->module = 'ratageneralpayroll';
        $this->module_prefix = 'payrolls/reports/ratareports';
    	$this->controller = $this;

    }

    public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('firstname','asc')->get(),
                       'postedreports'  => PostedReport::where('report_type','ratagp')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year       = $q['year'];
        $month      = $q['month'];
        $pay_period = $q['pay_period'];

        $query = Rata::leftJoin('office as o','o.RefId','=','pms_rata.office_id')
        ->leftJoin('division as d','d.RefId','=','pms_rata.division_id')
        ->with([
            'salaryinfo',
            'employees',
            'offices',
            'positions',
            'employeeinfo',
            'deductions' => function($qry) use($year,$month,$pay_period){
                $qry = $qry->where('year',$year)
                ->where('month',$month)
                ->where('pay_period',$pay_period);
            },
            'leave'])
        ->where('year',$year)
        ->where('month',$month)
        ->where('pay_period',$pay_period);

        $query = $query
        ->orderBy('d.Code','asc')
        ->orderBy('o.Code','asc')
        ->orderBy('salary_grade','desc')
        ->get();

        return json_encode($query);

    }

    public function generateCSV(Request $request)
    {
        
        $year      = Input::get('_year');
        $month     = Input::get('_month');
        $payperiod = Input::get('_payperiod');

        $query = Rata::with([
            'deductions' => function($qry) use($year, $month, $payperiod){
                $qry = $qry
                ->where('year',$year)
                ->where('month',$month)
                ->where('pay_period',$payperiod);        
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->where('pay_period',$payperiod)
        ->get();

        $status = false;
        if(count($query) > 0)
        {   
            $string = [];
            foreach ($query as $key => $value) {

                $raAmount = ($value->representation_amount) ? $value->representation_amount : 0;
                $taAmount = ($value->transportation_amount) ? $value->transportation_amount : 0;

                $deductionAmount = 0;
                if(count($value->deductions) > 0){
                    foreach ($value->deuctions as $key2 => $value2) {
                        $deductionAmount += $value2->deduction_amount;
                    }
                }

                $forRelease = $raAmount + $taAmount - $deductionAmount;

                $string[$key] = array(
                    'account' =>  str_replace('-', '', $value->employeeinfo->atm_no),
                    'name'    => $value->employees->getNameForCSV(),
                    'amount'  => str_replace(array('.',','), '', number_format($forRelease,2))
                );
            }

            return Excel::download(new ExportRata($string), 'rata_'.date('YmdHis',strtotime(Carbon::now())).'.csv');

            $status = true;
        }


    }

    public function postReport(Request $request){
        
        $postedreports = new PostedReport;
        $signatory     =  new PostedSignatory;

        $signatory->signatory_one   = @$request['data']['signatory_one'];
        $signatory->signatory_two   = @$request['data']['signatory_two'];
        $signatory->signatory_three = @$request['data']['signatory_three'];
        $signatory->signatory_four  = @$request['data']['signatory_four'];
        $signatory->position_one    = @$request['data']['position_one'];
        $signatory->position_two    = @$request['data']['position_two'];
        $signatory->position_three  = @$request['data']['position_three'];
        $signatory->position_four   = @$request['data']['position_four'];
        $signatory->created_by      = Auth::id();
        if($signatory->save()){
            $postedreports->pay_period   = @$request['data']['pay_period'];
            $postedreports->year         = @$request['data']['year'];
            $postedreports->month        = @$request['data']['month'];
            $postedreports->signatory_id = $signatory->id;
            $postedreports->created_by   = Auth::id();
            $postedreports->report_type  = 'ratagp';
            $postedreports->save();
        }

        return json_encode([
            'status'=>true,
            'response'=> 'Report posted successfully.'
        ]);


    }
}
