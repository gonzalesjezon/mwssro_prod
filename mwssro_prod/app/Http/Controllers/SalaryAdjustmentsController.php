<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\SalaryInfo;
use App\PhilhealthPolicy;
use App\DeductionInfo;
use App\Deduction;
use App\SalaryAdjustment;
use Auth;
class SalaryAdjustmentsController extends Controller
{
    function __construct(){
    	$this->title = 'SALARY ADJUSTMENTS';
    	$this->module = 'salaryadjustments';
        $this->module_prefix = 'payrolls/otherpayrolls';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname'];

        $status              = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;

        $status = $status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $arr_employeeid = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();


        $query = $employee
                ->whereIn('id',$arr_employeeid)
                ->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function store(Request $request){

        $adjustment = new SalaryAdjustment;

        $transaction_date   = $request->transaction_date;
        $employee_id        = $request->employee_id;
        $transaction_id     = $request->transaction_id;

        $new_basic_pay = (isset($request->new_rate_amount)) ? str_replace(',', '', $request->new_rate_amount) : 0;
        $old_basic_pay = (isset($request->old_rate_amount)) ? str_replace(',', '', $request->old_rate_amount) : 0;
        $salary_adjustment = (isset($request->adjustment_amount)) ? str_replace(',', '', $request->adjustment_amount) : 0;
        $gsis_cont_amount = (isset($request->gsis_cont_amount)) ? str_replace(',', '', $request->gsis_cont_amount) : 0;
        $philhealth_cont_amount = (isset($request->philhealth_cont_amount)) ? str_replace(',', '', $request->philhealth_cont_amount) : 0;
        $provident_cont_amount = (isset($request->pf_cont_amount)) ? str_replace(',', '', $request->pf_cont_amount) : 0;
        $wtax_amount = (isset($request->tax_amount)) ? str_replace(',', '', $request->tax_amount) : 0;
        $first_amount = (isset($request->first_amount)) ? str_replace(',', '', $request->first_amount) : 0;
        $second_amount = (isset($request->second_amount)) ? str_replace(',', '', $request->second_amount) : 0;
        $third_amount = (isset($request->third_amount)) ? str_replace(',', '', $request->third_amount) : 0;
        $fourth_amount = (isset($request->fourth_amount)) ? str_replace(',', '', $request->fourth_amount) : 0;

        if(isset($transaction_id)){

            $adjustment = $adjustment->find($transaction_id);

            $adjustment->employee_id                = $employee_id;
            $adjustment->old_basic_pay_amount       = $old_basic_pay;
            $adjustment->new_basic_pay_amount       = $new_basic_pay;
            $adjustment->salary_adjustment_amount   = $salary_adjustment;
            $adjustment->gsis_cont_amount           = $gsis_cont_amount;
            $adjustment->philhealth_cont_amount     = $philhealth_cont_amount;
            $adjustment->provident_fund_amount      = $provident_cont_amount;
            $adjustment->wtax_amount                = $wtax_amount;
            $adjustment->first_deduction_amount     = $first_amount;
            $adjustment->second_deduction_amount    = $second_amount;
            $adjustment->third_deduction_amount     = $third_amount;
            $adjustment->fourth_deduction_amount    = $fourth_amount;
            $adjustment->date_from                  = $request->date_from;
            $adjustment->date_to                    = $request->date_to;
            $adjustment->transaction_date           = $transaction_date;
            $adjustment->updated_by                 = Auth::User()->id;

            $adjustment->save();

            $response = json_encode(['status'=>true,'response' => 'Update Successfully']);


        }else{

            // $this->validate($request,[
            // ]);

            $adjustment->employee_id                = $employee_id;
            $adjustment->old_basic_pay_amount       = $old_basic_pay;
            $adjustment->new_basic_pay_amount       = $new_basic_pay;
            $adjustment->salary_adjustment_amount   = $salary_adjustment;
            $adjustment->gsis_cont_amount           = $gsis_cont_amount;
            $adjustment->philhealth_cont_amount     = $philhealth_cont_amount;
            $adjustment->provident_fund_amount      = $provident_cont_amount;
            $adjustment->wtax_amount                = $wtax_amount;
            $adjustment->first_deduction_amount     = $first_amount;
            $adjustment->second_deduction_amount    = $second_amount;
            $adjustment->third_deduction_amount     = $third_amount;
            $adjustment->fourth_deduction_amount    = $fourth_amount;
            $adjustment->date_from                  = $request->date_from;
            $adjustment->date_to                    = $request->date_to;
            $adjustment->transaction_date           = $transaction_date;
            $adjustment->created_by                 = Auth::User()->id;

            $adjustment->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully']);

        }

        return $response;

    }

    public function showSalaryAdjustment(){

         $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);


    }

    public function getSalaryAdjustment(){

        $employee_id = Input::get('id');

        $salaryinfo            = new SalaryInfo;
        $philhealth_policy     = new PhilhealthPolicy;
        $deductioninfo         = new DeductionInfo;
        $deduction             = new Deduction;
        $adjustment            = new SalaryAdjustment;

        $deduction = $deduction->where('code','PF')->first();

        $deductioninfo = $deductioninfo
        ->where('deduction_id',@$deduction->id)
        ->where('employee_id',@$employee_id)
        ->orWhereNotNull('deduct_date_terminated')
        ->orderBy('created_at','desc')
        ->first();

        $salaryinfo = $salaryinfo
                    ->with('positions')
                    ->where('employee_id',@$employee_id)
                    ->orderBy('salary_effectivity_date','desc')
                    ->take(2)
                    ->get();

        $adjustment = $adjustment
        ->where('employee_id',$employee_id)
        ->get();

        $query['old_position']    = (isset($salaryinfo[1]->positions)) ? @$salaryinfo[1]->positions->Name : '';
        $query['new_position']    = (isset($salaryinfo[0]->positions)) ? @$salaryinfo[0]->positions->Name : '';
        $query['old_rate_amount'] = @$salaryinfo[0]->salary_old_rate;
        $query['new_rate_amount'] = @$salaryinfo[0]->salary_new_rate;
        $query['philhealth_policy'] = $philhealth_policy->orderBy('created_at','desc')->first();
        $query['pf_rate'] = @$deductioninfo->deduction_rate;
        $query['adjustments'] = @$adjustment;

        return $query;

    }

    public function getDaysInAMonth(){
        $data = Input::all();

        $transact_date = $data['transaction_date'];

        $year = date('Y', strtotime($transact_date));
        $month = date('m', strtotime($transact_date));

        $workdays = $this->countDays($year,$month,array(0,6));

        return json_encode(['workdays'=>$workdays]);

    }

    public function getCountedDays(){
        $data = Input::all();

        $from         = @$data['date_from'];
        $to           = @$data['date_to'];

        if(isset($from) && isset($to)){
            $start_date   = date('Y-m-d', strtotime($from));
            $end_date     = date('Y-m-d', strtotime($to));
            $counted_days = $this->getWorkingDays($start_date,$end_date);

            return json_encode(['counted_days'=>$counted_days]);
        }

    }

    public function deleteAdjustment(){
        $data = Input::all();

        $date           = $data['date'];
        $id             = $data['id'];
        $employee_id    = $data['employee_id'];

        $adjustments =  new SalaryAdjustment;

        $adjustments->destroy($id);

        $query['adjustments'] = $adjustments
        ->where('employee_id',$employee_id)
        ->get();

        return $query;

    }
}
