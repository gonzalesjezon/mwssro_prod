<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\LeaveMonetizationTransaction;
use App\Employee;
use App\Company;
class MonetizationTransmitalReportsReportsController extends Controller
{
    function __construct(){
    	$this->title = 'TRANSMITAL';
    	$this->module = 'monetizationtransmital';
        $this->module_prefix = 'payrolls/reports/monetizationreports';
    	$this->controller = $this;

    }

    public function index(){

    	$transaction = new LeaveMonetizationTransaction;

    	$transaction = $transaction->groupBy('remarks')->get();

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

    	$response = array(
    					'transaction' 	=> $transaction,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'company'       => Company::first(),
                        'company_name'  => $companyname,
                        'months'        => config('params.months'),
                        'latest_year'   => $this->latestYear(),
                       'earliest_year'  => $this->earliestYear(),
                       'current_month'  => (int)date('m'),    
                        'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new LeaveMonetizationTransaction;

        $data = Input::all();

        $created_at = $data['created_at'];

        $query['transaction'] = $transaction
        ->where('created_at','like','%'.$created_at.'%')
        ->sum('net_amount');
       return json_encode($query);
    }
}
