<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\Company;
use App\Employee;
class PremiumContributionReportsController extends Controller
{
    function __construct(){
    	$this->title  = 'PREMIUM CONTRIBUTION';
    	$this->module = 'premiumcontributions';
    	$this->controller = $this;
    	$this->module_prefix = 'payrolls/reports';
    }

    public function index(){

    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);

    }

    public function show(){

    	$data = Input::all();

    	$transaction = new Transaction;

        $query = Transaction::leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->with(
            'employees',
            'employeeinfo',
            'salaryinfo',
            'positions')
		 ->where('year',$data['year'])
         ->where('month',$data['month'])
         ->orderBy('e.lastname')
         ->get();

    	return json_encode($query);

    }
}
