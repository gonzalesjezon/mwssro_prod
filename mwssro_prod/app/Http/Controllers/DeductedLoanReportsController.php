<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\LoanInfoTransaction;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\LoanInfo;
use App\Transaction;
class DeductedLoanReportsController extends Controller
{
    function __construct(){
		$this->title = 'DEDUCTED LOANS REPORT';
    	$this->module = 'deductedloanreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction = new Transaction;
        $loantransation = new LoanInfoTransaction;

        $loantransation = $loantransation
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()->toArray();

        $query = Transaction::leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->with([
            'employees',
            'positions',
            'loaninfoTransaction' => function($qry) use($year,$month){
                $qry->where('year',$year)
                ->where('month',$month);
            }
        ])
        ->whereIn('employee_id',$loantransation)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('e.lastname')
        ->get();


        return json_encode($query);
    }
}
