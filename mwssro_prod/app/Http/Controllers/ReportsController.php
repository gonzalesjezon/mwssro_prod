<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportsController extends Controller
{
    
    function __construct(){
    	$this->title 	  	  = 'REPORTS';
    	$this->module_prefix = 'payrolls';
    	$this->module 	  	  = 'reports';
    	$this->controller 	  = $this;
    }

    public function index(){

    	$response = array(
    		'controller' 		 => $this->controller,
    		'module'  	 		 => $this->module,
    		'title' 	 		 => $this->title,
    		'module_prefix' 	 => $this->module_prefix,
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

 
}
