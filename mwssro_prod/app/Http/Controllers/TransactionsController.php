<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\EmployeeInfo;
use App\Transaction;
use Carbon\Carbon;
use App\AttendanceInfo;
use App\BenefitInfoTransaction;
use App\DeductionInfoTransaction;
use App\LoanInfoTransaction;
use Session;
use App\DTRProcess;
use App\EmployeeCreditBalance;
use App\PositionItem;
use Auth;

class TransactionsController extends Controller
{

    function __construct(){
    	$this->title = 'REGULAR PAYROLL';
    	$this->module = 'transactions';
        $this->module_prefix = 'payrolls';
    	$this->controller = $this;

    }

    public function index(){

        $benefit        = Benefit::where('remarks','allowance')->orderBy('name','asc')->get();
        $loans          = Loan::orderBy('name','asc')->get();
        $deduction      = Deduction::where('remarks','bill')->orderBy('name','asc')->get();


    	$response = array(
                        'benefit'       => $benefit,
                        'loans'         => $loans,
                        'deductions'     => $deduction,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                        'latest_year'        => $this->latestYear(),
                        'earliest_year'      => $this->earliestYear(),
                        'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        // for search param
        $year           = Input::get('year');
        $month          = Input::get('month');
        $checkpayroll   = Input::get('checkpayroll');

        // for filter param
        $_year           = Input::get('_year');
        $_month          = Input::get('_month');
        $_checkpayroll   = Input::get('_checkpayroll');

        $data = $this->searchName($q,$checkpayroll,$year,$month);

        if(isset($_year) || isset($_month) || isset($_checkpayroll)){
            $data = $this->filter($_year,$_month,$_checkpayroll);
        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }
    public function searchName($q,$checkpayroll,$year,$month){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new Transaction;

        $cols = ['lastname','firstname'];

        // Get id's permanent status
        $empstatus_id = $employee_status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $now = date('Y-m-d',strtotime($year.'-'.$month.'-'.'01'));
        $arrId = EmployeeInformation::where('resigned_date',null)
            ->whereIn('employee_status_id',$empstatus_id)
            ->orWhere('resigned_date','>=',$now)
            ->pluck('employee_id')
            ->toArray();

        $hasTransact = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()->toArray();
        $checkpayroll = 'wopayroll';
         if(isset($checkpayroll)){
            $checkpayroll = $checkpayroll;
         }

        $query = [];
        switch ($checkpayroll) {
            case 'wpayroll':
               $query = $employee->whereIn('id',$hasTransact);
                break;

            case 'wopayroll':
               $query = $employee->whereNotIn('id',$hasTransact);
                break;

        }

      $query = $employee->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query
        ->whereIn('id',$arrId)
        ->where('active',1)
        ->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$checkpayroll){

        $employee_status        = new EmployeeStatus;
        $employee_info          = new EmployeeInformation;
        $transaction            = new Transaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;

        $empstatus_id = $employee_status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        // Get all permanent employees

        $hasTransact = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpayroll) {
            case 'wpayroll':

                    $response = $employee->whereIn('id',$hasTransact)->orderBy('lastname','asc')->get();

                break;

            case 'wopayroll':

                    $now = Carbon::createFromFormat('Y-m-d',$year.'-'.$month.'-'.'01');

                    $arrId = EmployeeInformation::where('resigned_date',null)
                    ->whereIn('employee_status_id',$empstatus_id)
                    ->orWhere('resigned_date','>=',$now)
                    ->pluck('employee_id')
                    ->toArray();

                    $response = Employee::whereNotIn('id',$hasTransact)
                    ->whereIn('id',$arrId)
                    ->where('active',1)
                    ->orderBy('lastname','asc')
                    ->get();
                break;
        }
        return $response;
    }

    public function showBenefitinfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitdatatable',$response);
    }

    public function showLoaninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.loandatatable',$response);
    }

     public function showDeductioninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.deductdatatable',$response);
    }



     public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){

        $data = Input::all();

        $employee_id = @$data['id'];
        $employee_number = @$data['employee_number'];

        $transaction    = new Transaction;
        $employeeinfo   = new EmployeeInfo;
        $salaryinfo     = new SalaryInfo;
        $benefitinfo    = new BenefitInfoTransaction;
        $deductioninfo  = new DeductionInfoTransaction;
        $loaninfo       = new LoanInfoTransaction;
        $benefit        = new Benefit;

        $query['transaction'] = $transaction->with('employees')
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->first();

        $query['employeeinfo'] = $employeeinfo
                            ->where('employee_id',@$employee_id)
                            // ->where('employee_number',@$employee_number)
                            ->first();

        $effectivity_date = Carbon::now()->toDateTimeString();
        $query['salaryinfo'] = $salaryinfo
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->where('salary_effectivity_date','<=',$effectivity_date)
                            ->first();

        $query['benefitinfo'] = $benefitinfo->with('benefits','benefitinfo')
                                ->where('employee_id',@$employee_id)
                                // ->where('employee_number',@$employee_number)
                                ->where('year',@$data['year'])
                                ->where('month',@$data['month'])
                                ->get();

        $query['deductioninfo'] = $deductioninfo->with('deductions','deductioninfo')
                                ->where('employee_id',@$employee_id)
                                // ->where('employee_number',@$employee_number)
                                ->where('year',@$data['year'])
                                ->where('month',@$data['month'])
                                ->get();

        $query['loaninfo'] = $loaninfo
                            ->with('loans','loaninfo')
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            ->get();


        return json_encode($query);
    }

    public function updatePayroll(){
        $data = Input::all();

        $year        = @$data['year'];
        $month       = @$data['month'];
        $id          = @$data['transaction_id'];
        $employee_id = @$data['employee_id'];


        $this->computeTransaction($year,$month,$employee_id,$id,$data);

        return json_encode(['status'=>true,'response'=>'Payroll Updated Successfully!']);

    }

    public function processPayroll() {

        $data     = Input::all();
        $year     = @$data['year'];
        $month    = @$data['month'];
        $ctr      = 0;
        $error    = 0;
        $error_id = [];
        $id       = 0;
        foreach ($data['empid'] as $key => $_id) {

            if(isset($_id)){
                $check = DTRProcess::where('EmployeesRefId',$_id)->first();
                if($check === null){
                    $error++;
                    $error_id[$key] = $_id;
                }
            }
        }

        // $m = $m - 1;

        // $workdays = $this->countDays($y,$m,array(0,6));

        if($error == 0){
            foreach ($data['empid'] as $key => $_id) {
                if(isset($_id)){
                    $this->computeTransaction($year,$month,$_id,$id,$data);
                    $ctr++;
                }

            }

            $response = json_encode(['status'=>true,'response'=>'Processed Successfully! <br>'.$ctr.' Records Saved']);
        }else{
            $error_id = implode(',', $error_id);
            $response = json_encode(['status'=>false,'response'=>'Save Failed! <br>'.$error.' Records <br>No DTR Processed <br>Employee ID <br>['.$error_id.']']);
        }

        return $response;
    }

    public function computeTransaction($year,$month,$employee_id,$id,$data){

        $actualAbsent = @$data['attendance']['actual_absences'];

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));
        $daysInAMonth = cal_days_in_month(CAL_GREGORIAN,$month,$year); // gsis days

        $effectDate       = Carbon::now()->toDateTimeString();
        $employeeinfo     = EmployeeInformation::where('employee_id',$employee_id)->first(); // Employee Information
        $payrollinfo      = EmployeeInfo::where('employee_id',$employee_id)->first(); // Payroll Information
        $employee         = Employee::where('id',$employee_id)->first(); // Employee Information
        $salaryinfo       = SalaryInfo::where('salary_effectivity_date','<=',$effectDate)
                            ->where('employee_id',$employee_id)
                            ->orderBy('salary_effectivity_date','desc')
                            ->first();

        $item = PositionItem::where('RefId',$employeeinfo->position_item_id)->first();

        $monthlyRate = $salaryinfo->salary_new_rate;
        $dailyRate   = $salaryinfo->salary_new_rate / 22;


        $pagibigShare = 0;
        $phicShare    = 0;
        $mp1Amount    = 0;
        $mp2Amount    = 0;
        $gsisEEShare  = 0;
        $gsisERShare  = 0;

        $totalLoan       = 0;
        $totalDeduction  = 0;
        $peraAmount      = 0;
        $taxAmount       = 0;
        $lwopAmount      = 0;

        $transaction = Transaction::find($id);
        if(empty($transaction)){
            $transaction = new Transaction();

            $lwop = $this->computeLwop($employee_id,$m,$y,$dailyRate,$daysInAMonth);
            $lwopCounted = $lwop['lwop_counted'];
            $gsisEEShare = $lwop['ee_share'];
            $gsisERShare = $lwop['er_share'];

            $totalLoan      = $this->storeLoaninfoTransaction($employee_id,$year,$month);
            $totalDeduction = $this->storeDeduction($employee_id,$year,$month);
            $peraAmount     = $this->storePera($employee_id,$year,$month,$lwopCounted,$data);
            $lwopAmount     = $dailyRate * $lwopCounted;

            $transaction->actual_workdays       = 22;
            $transaction->employee_number       = $employee->employee_number;
            $transaction->employeeinfo_id       = $payrollinfo->id;
            $transaction->position_id           = $employeeinfo->position_id;
            $transaction->dept_code             = $employeeinfo->dept_code;
            $transaction->company_id            = $employeeinfo->company_id;
            $transaction->position_item_id      = $employeeinfo->position_item_id;
            $transaction->office_id             = $employeeinfo->office_id;
            $transaction->department_id         = $employeeinfo->department_id;
            $transaction->empstatus_id          = $employeeinfo->employee_status_id;
            $transaction->item_number           = @$employeeinfo->positionitems->Code;
            $transaction->dept_code             = @$payrollinfo->dept_code;
            $transaction->employee_id           = $employee_id;
            $transaction->salaryinfo_id         = $salaryinfo->id;
            $transaction->salary_grade          = $salaryinfo->salarygrade->Code;
            $transaction->ecc_amount            = 100;
            $transaction->year                  = $year;
            $transaction->month                 = $month;
            $transaction->actual_absences       = $lwopCounted;
            $transaction->total_absences_amount = $lwopAmount;

            if($lwopCounted == 0){
                $gsisEEShare = $payrollinfo->gsis_contribution;
                $gsisERShare = $payrollinfo->er_gsis_share;
            }

            $pagibigShare = $payrollinfo->pagibig_contribution;
            $phicShare    = $payrollinfo->philhealth_contribution;
            $mp1Amount    = $payrollinfo->pagibig_personal;
            $mp2Amount    = $payrollinfo->pagibig2;
            $taxAmount    = $payrollinfo->tax_contribution;
            $grossPay     = $monthlyRate + $peraAmount;

        }else{

            $grossPay       = $transaction->gross_pay;
            $gsisERShare    = $transaction->gsis_er_share;
            $gsisEEShare    = $transaction->gsis_ee_share;
            $pagibigShare   = $transaction->pagibig_share;
            $phicShare      = $transaction->phic_share;
            $mp2Amount      = $transaction->mp2_amount;
            $mp1Amount      = $transaction->mp1_amount;
            $taxAmount      = $transaction->tax_amount;
            
            if($data['wtax_amount'] != '0.00' && isset($data['wtax_amount']))
            {
                $taxAmount = str_replace(',', '', $data['wtax_amount']);
            }

            $totalLoan      = $transaction->total_loan;            
            $totalDeduction = $transaction->total_otherdeduct;
            $monthlyRate    = $transaction->actual_basicpay_amount;

            $gsis = [];
            if((int)$actualAbsent){
                $peraAmount     = $this->storePera($employee_id,$year,$month,$actualAbsent,$data);
                $gsis           = $this->computeLwopGsis($actualAbsent,$daysInAMonth,$dailyRate);
                $gsisEEShare    = $gsis['ee_share'];
                $gsisERShare    = $gsis['er_share'];
                $lwopAmount     = $dailyRate * $actualAbsent;

                $transaction->actual_absences       = $actualAbsent;
                $transaction->total_absences_amount = $lwopAmount;
            }

        }

        $totalContribution = $pagibigShare + $phicShare + $gsisEEShare + $mp2Amount + $mp1Amount;
        $netDeduction      = $totalLoan + $totalDeduction + $totalContribution + $taxAmount;
        $grossPay          = $grossPay - $lwopAmount;
        $netPay            = $grossPay - $netDeduction;
        $halfNet = $netPay / 2;

        $transaction->pagibig_share          = $pagibigShare;
        $transaction->phic_share             = $phicShare;
        $transaction->mp1_amount             = $mp1Amount;
        $transaction->mp2_amount             = $mp2Amount;
        $transaction->gsis_ee_share          = $gsisEEShare;
        $transaction->gsis_er_share          = $gsisERShare;
        $transaction->tax_amount             = $taxAmount;
        $transaction->total_loan             = $totalLoan;
        $transaction->total_otherdeduct      = $totalDeduction;
        $transaction->basic_net_pay          = $grossPay;
        $transaction->actual_basicpay_amount = $monthlyRate;
        $transaction->total_basicpay_amount  = $monthlyRate;
        $transaction->gross_pay              = $grossPay;
        $transaction->total_contribution     = $totalContribution;
        $transaction->total_otherdeduct      = $totalDeduction;
        $transaction->net_deduction          = $netDeduction;
        $transaction->net_pay                = $netPay;
        $transaction->first_net              = $halfNet;
        $transaction->second_net             = $halfNet;
                    
        $transaction->save();

    }

    public function computeLwop($employee_id,$m,$y,$daily_rate,$days_in_month){

        $creditbalance = EmployeeCreditBalance::where('EmployeesRefId',$employee_id)
        ->where('NameCredits','VL')
        ->first();

        // ================ LWOP =================== //

        $begBalAsOfDate = @$creditbalance->BegBalAsOfDate;
        $begbalMonth = date('m', strtotime($begBalAsOfDate));

        $dtrprocess = DTRProcess::where('EmployeesRefId',$employee_id)
        ->where('Year',$y)
        ->whereBetween('Month',array($begbalMonth,$m))
        ->get();

        $vl_balance = @$creditbalance->BeginningBalance;

        $undertime = 0;
        $tardiness = 0;
        $absent    = 0;
        $vl_used   = 0;
        $vl_earned = 0;
        $new_vl    = 0;
        $lwop_for_the_month = 0;

        foreach ($dtrprocess as $key2 => $value) {
            $deduction = 0;

            $tardiness  = ($value->Tardy_Deduction_EQ > 0) ? $value->Tardy_Deduction_EQ : 0 ;
            $undertime  = ($value->Undertime_Deduction_EQ > 0) ? $value->Undertime_Deduction_EQ : 0 ;
            $absent     = ($value->Total_Absent_Count > 0) ? $value->Total_Absent_Count : 0;
            $vl_used    = ($value->VL_Used > 0) ? $value->VL_Used : 0;
            $vl_earned  = ($value->VL_Earned > 0) ? $value->VL_Earned : 0;

            $deduction = $tardiness + $undertime + $absent;
            $new_vl = ($vl_earned + $vl_balance) - $deduction;

            if($new_vl < 0){
                $lwop_for_the_month = $new_vl;
                $vl_balance = 0;
            }else{
                $vl_balance = $new_vl;
            }
        }

           // ============ LWOP COMPUTATION ===========================
          // vl_used + tardiness + undertime + absent = total deduction
         // (vl_earned + vl_balance) - total deduction
        // ===========================================================
        $hourRateAmount  = $daily_rate / 8;
        $minutRateAmount = $hourRateAmount / 60;

        $eeShare = 0;
        $erShare = 0;

        $result      = [];
        $lwopDays    = 0;
        $lwopCounted = 0;
        if($lwop_for_the_month > 0){
            $lwopCounted = abs($lwop_for_the_month);
            $lwopDays = $days_in_month - $lwopCounted;
            $gsisAmount = $lwopDays * $daily_rate;
            $eeShare = $gsisAmount * 0.09;
            $erShare = $gsisAmount * 0.12;
        }

        $result['ee_share']     = $eeShare;
        $result['er_share']     = $erShare;
        $result['lwop_counted'] = $lwopCounted;

        return $result;
    }

    public function computeLwopGsis($lwop_for_the_month,$days_in_month,$daily_rate){

        $eeShare     = 0;
        $erShare     = 0;
        $result      = [];
        $lwopDays    = 0;
        $lwopCounted = 0;
        if($lwop_for_the_month > 0){
            $lwopCounted    = abs($lwop_for_the_month);
            $lwopDays       = $days_in_month - $lwopCounted;
            $gsisAmount     = $lwopDays * $daily_rate;
            $eeShare        = $gsisAmount * 0.09;
            $erShare        = $gsisAmount * 0.12;
        }

        $result['ee_share']     = $eeShare;
        $result['er_share']     = $erShare;

        return $result;
    }

    public function getBenefits($employee_id,$year,$month)
    {
        $query = BenefitInfoTransaction::where('year',$year)
        ->where('month',$month)
        ->where('employee_id',$employee_id)
        ->sum('amount');

        return $query;
    }

    public function storeLoaninfoTransaction($employee_id,$year,$month){
        $loaninfo =  new LoanInfo;

        $query = $loaninfo
        ->where('employee_id',$employee_id)
        ->where('start_date','<=',date('Y-m-d'))
        ->where('terminated',0)
        ->get();

        $amount = 0;

        foreach ($query as $key => $value) {

            $transaction =  new LoanInfoTransaction;

            $transaction->employee_id        = $employee_id;
            $transaction->loan_info_id       = $value->id;
            $transaction->loan_id            = $value->loan_id;
            $transaction->amount             = $value->loan_amount;
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();

            $amount += (float)$value->loan_amount;
        }


        return $amount;

    }

    public function storePera($employee_id,$year,$month,$lwop_for_the_month,$data){

        $peraId = Benefit::where('code','PERA')->first();
        $benefitInfo =  new BenefitInfo;

        $query = $benefitInfo::where('employee_id',$employee_id)
                            ->where('benefit_id',$peraId->id)
                            ->where('start_date','<=',date('Y-m-d'))
                            ->where('terminated',0)
                            ->first();

        $peraAmount = @$query->benefit_amount;

        $pera_amount = 0;

        if(isset($query)){

            if(isset($lwop_for_the_month)){

                $dailyPeraAmount = $peraAmount / 22;
                $lessPeraAmount = $dailyPeraAmount * $lwop_for_the_month;
                $newPeraAmount = $peraAmount - $lessPeraAmount;

                $pera_amount = $newPeraAmount;

            }else{
                $pera_amount = $peraAmount;
            }

            $transaction = BenefitInfoTransaction::find(@$data['pera_id']);
            if(empty($transaction)){
                $transaction = new BenefitInfoTransaction();
            }

            $transaction->employee_id        = $employee_id;
            $transaction->benefit_info_id    = $query->id;
            $transaction->benefit_id         = $query->benefit_id;
            $transaction->amount             = $pera_amount;
            $transaction->status             = 'pera';
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();
        }


        return $pera_amount;

    }

    public function storeDeduction($employee_id,$year,$month){

        $deductionInfo  =  new DeductionInfo;

        $query = $deductionInfo
        ->where('employee_id',$employee_id)
        ->where('start_date','<=',date('Y-m-d'))
        ->where('terminated',0)
        ->get();

        $amount = 0;
        foreach ($query as $key => $value) {
            $transaction    =  new DeductionInfoTransaction;
            $transaction->employee_id        = $employee_id;
            $transaction->deduction_info_id  = $value->id;
            $transaction->deduction_id       = $value->deduction_id;
            $transaction->amount             = $value->deduction_amount;
            $transaction->certificate_no     = $value->certificate_no;
            $transaction->status             = 'deductions';
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();

            $amount += $value->deduction_amount;
        }

        return $amount;

    }

    public function deletePayroll(){
        $data = Input::all();

        $transactions   = new Transaction;
        $benefitinfo    = new BenefitInfoTransaction;
        $deductioninfo  = new DeductionInfoTransaction;
        $loaninfo       = new LoanInfoTransaction;

        foreach ($data['empid'] as $key => $value) {

            $transactions->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();
            $deductioninfo->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();
            $benefitinfo->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();

            $loaninfo->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

    public function storeBenefitInfoTransaction(Request $request){

        $amount = ($request->bt_amount) ? str_replace(',', '', $request->bt_amount) :  0;

        $transaction = BenefitInfoTransaction::find($request->benefit_transaction_id);
        $message = 'Update Successfully.';
        if(empty($transaction)){
            $this->validate($request,[
                'bt_amount'     => 'required',
                'benefit_id'    => 'required'
            ]);
            $transaction = new BenefitInfoTransaction;
            $message = 'Save Successfully.';
        }

        $transaction->benefit_id     = $request->benefit_id;
        $transaction->transaction_id = $request->transaction_id;
        $transaction->employee_id    = $request->employee_id;
        $transaction->amount         = $amount;
        $transaction->status         = 'allowance';
        $transaction->year           = $request->year;
        $transaction->month          = $request->month;
        if($message == 'Update Successfully.'){
            $transaction->updated_by = Auth::User()->id;
        }else{
            $transaction->created_by = Auth::User()->id;

        }

        $transaction->save();

        $this->update_transaction($request->employee_id,$request->year,$request->month,$amount,'add','benefit');

        $transaction['benefitinfo'] = $transaction
            ->with('benefits','benefitinfo')
            ->where('employee_id',$request->employee_id)
            ->get();



        return json_encode(['status'=>true,'response'=>$message,'transaction'=>$transaction]);
    }

    public function storeDeductionInfoTransaction(Request $request){

        $amount = ($request->deduction_amount) ? str_replace(',', '', $request->deduction_amount) :  0;

        $transaction = Transaction::find($request->deduction_transaction_id);
        $message = 'Update Successfully.';
        if(empty($transaction)){

            $this->validate($request,[
                'deduction_amount'     => 'required',
                'deduction_id'          => 'required'
            ]);

            $transaction = new DeductionInfoTransaction;
            $message = 'Save Successfully.';
        }

        $transaction->fill($request->all());
        $transaction->deduction_id   = $request->deduction_id;
        $transaction->employee_id    = $request->employee_id;
        $transaction->amount         = $amount;
        $transaction->year           = $request->year;
        $transaction->month          = $request->month;
        $transaction->status         = 'deductions';

        if($message == 'Update Successfully.'){
            $transaction->updated_by = Auth::User()->id;
        }else{
            $transaction->created_by = Auth::User()->id;
        }

        $transaction->save();

        $this->update_transaction($request->employee_id,$request->year,$request->month,$amount,'add','deduction',$request->pay_period);

        $transaction['deductioninfo'] = $transaction
            ->where('employee_id',$request->employee_id)
            ->where('year',$request->year)
            ->where('month',$request->month)
            ->with('deductions','deductioninfo')
            ->get();

        return json_encode(['status'=>true,'response'=>$message,'transaction'=>$transaction]);
    }

    public function deleteLoan(){
        $data = Input::all();

        $employee_id = @$data['employee_id'];
        $year        = @$data['year'];
        $month       = @$data['month'];
        $id          = @$data['id'];
        $amount      = @$data['amount'];

        $transaction = new LoanInfoTransaction;

        $query = $transaction->destroy($id);

        $status = 'delete';
        $type   = 'loan';

        $this->update_transaction($employee_id,$year,$month,$amount,$status,$type);

        $data2 = $transaction
                ->with('loans','loaninfo')
                ->where('employee_id',@$employee_id)
                ->where('year',@$year)
                ->where('month',@$month)
                ->get();

        return json_encode(['status'=>'loans', 'data'=> $data2]);
    }

    public function deleteDeduction(){
        $data = Input::all();

        $employee_id = @$data['employee_id'];
        $year        = @$data['year'];
        $month       = @$data['month'];
        $id          = @$data['id'];
        $amount      = @$data['amount'];

        $transaction = new DeductionInfoTransaction;

        $query = $transaction->destroy($id);

        $status = 'delete';
        $type   = 'deduction';

        $this->update_transaction($employee_id,$year,$month,$amount,$status,$type);

        $data2 = $transaction
                ->with('deductions','deductioninfo')
                ->where('employee_id',@$employee_id)
                ->where('year',@$year)
                ->where('month',@$month)
                ->get();

        return json_encode(['status'=>'deductions', 'data'=> $data2]);
    }

    public function deleteBenefit(){
        $data = Input::all();

        $employee_id = @$data['employee_id'];
        $year        = @$data['year'];
        $month       = @$data['month'];
        $id          = @$data['id'];
        $amount      = @$data['amount'];

        $transaction = new BenefitInfoTransaction;
        $query = $transaction->destroy($id);

        $status = 'delete';
        $type   = 'benefit';

        $this->update_transaction($employee_id,$year,$month,$amount,$status,$type);

        $data2 = $transaction
                ->with('benefits','benefitinfo')
                ->where('employee_id',@$employee_id)
                ->where('year',@$year)
                ->where('month',@$month)
                ->get();

        return json_encode(['status'=>'benefits', 'data'=> $data2]);
    }


    public function update_transaction($employee_id,$year,$month,$amount,$status,$type,$pay_period)
    {
        // Update Transaction
        $query = Transaction::where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('month',$month)->first();

        $totalDeduction = 0;
        $additional     = 0;
        switch ($type) {
            case 'loan':
                $totalDeduction = $query->total_loan;
                break;
            
            case 'deduction':
                $totalDeduction = $query->total_otherdeduct;
                break;
            case 'benefit':
                $additional = $amount;
                break;
        }

        $netDeduction   = $query->net_deduction;
        $newDeduction   = 0;
        $newTotaLoan    = 0;

        $earnedForPeriod = $query->gross_pay;

        switch ($status) {
            case 'delete':
                $earnedForPeriod    = $earnedForPeriod - $additional;
                $newTotalDeduction  = $totalDeduction - $amount;
                $newDeduction       = $netDeduction - $amount;
                break;
            
            case 'add':
                $earnedForPeriod    = $earnedForPeriod + $additional;
                $newTotalDeduction  = $totalDeduction + $amount;
                $newDeduction       = $netDeduction + $amount;
                break;
        }

        $firstNet  = $query->first_net;
        $secondNet = $query->second_net;
        switch ($pay_period) {
            case 'firsthalf':
                $firstNet = $firstNet - $amount;
                break;
            
            case 'secondhalf':
                $secondNet = $secondNet - $amount;
                break;
        }

        // $netPay     =  $earnedForPeriod - $newDeduction;
        $transaction = Transaction::find($query->id);
        switch ($type) {
            case 'loan':
                # code...
                $transaction->total_loan = $newTotalDeduction;
                break;
            
            case 'deduction':
                # code...
                // $transaction->total_otherdeduct = $newTotalDeduction;
                break;
        }
        $transaction->first_net  = $firstNet;
        $transaction->second_net = $secondNet;
        // $transaction->net_deduction = $newDeduction;
        // $transaction->net_pay       = $netPay;
        $transaction->save();

    }

}
