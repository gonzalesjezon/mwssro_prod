<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaction;
use App\Company;
use Input;
use Auth;

class HDMF2RemittanceController extends Controller
{
    function __construct(){
		$this->title = 'HDMF II Schedule of Remittance';
    	$this->module = 'hdmf2_remittance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

		$response = array(
					'module'        => $this->module,
					'controller'    => $this->controller,
         	'module_prefix' => $this->module_prefix,
					'title'		    	=> $this->title,
					'company' 			=> Company::first(),
          'company_name'  => 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office',
					'months'        => config('params.months'),
          'latest_year'   => $this->latestYear(),
          'earliest_year' => $this->earliestYear(),
          'current_month' => (int)date('m')
					);

    	return view($this->module_prefix.'.'.$this->module, $response);
	}

	public function show(){

		$data 	= Input::all();
		$year 	= $data['year'];
		$month 	= $data['month'];

		$query = Transaction::with([
			'employees' => function($qry){
				$qry = $qry->with('pis_employee');
			},
			'employeeinfo'
		])
		->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
		->where('year',$year)
		->where('month',$month)
		->where('mp2_amount','!=',0)
		->orderBy('e.lastname','asc')
		->get();

		return json_encode($query);
	}
}
