<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfo;
use App\Benefit;
use App\PostedReport;
use App\PostedSignatory;
use Auth;
use Input;
class PayrollWorksheetReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL WORKSHEET';
    	$this->module = 'payrollworksheets';
        $this->module_prefix = 'payrolls/reports/plantillareports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'        => config('params.months'),
                       'latest_year'    => $this->latestYear(),
                       'earliest_year'  => $this->earliestYear(),
                       'current_month'  => (int)date('m'),
                       'postedreports'  => PostedReport::where('report_type','regularpws')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function getPayrollWorksheetReport(){

        $q = Input::all();

        $year      = $q['year'];
        $month     = $q['month'];
        $payPeriod = $q['pay_period'];

        $transaction = new Transaction;
        $benefit     = new Benefit;

        $benefit = $benefit
        ->where('code','PERA')
        ->first();

        $query = $transaction
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'employees',
            'positions',
            'offices',
            'employeeinformation',
            'employeeinfo',
            'divisions',
            'benefitTransactions'  => function($qry) use($benefit){
                $qry->where('benefit_id',$benefit->id);
            },
            'loaninfoTransaction' => function($qry) use($year,$month){
                $qry->where('year',$year)
                ->where('month',$month);
            },
            'deductioninfoTransaction' => function($qry) use($year,$month){
                $qry = $qry
                ->where('year',$year)
                ->where('month',$month);
            },

            // 'additional_deduction' => function($qry) use($year,$month,$payPeriod){
            //     $qry = $qry
            //     ->where('pay_period',$payPeriod)
            //     ->where('year',$year)
            //     ->where('month',$month);
            // },

            'salaryinfo'
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.Code','asc')
        ->orderBy('item_number','asc')
        ->orderBy('salary_grade','desc')
        ->get();

        // $data = [];

        // if(isset($query)){
        //     foreach ($query as $key => $value) {
        //         if(isset($value->office_id)){
        //             $data[@$value->offices->Code.'|'.@$value->offices->Name][@$value->divisions->Name][$key] = $value;
        //         }
        //     }
        // }else{
        //     $data = [];
        // }


        return json_encode(collect($query)->reverse()->toArray());
        // return json_encode([
        //     'transaction' => $data
        // ]);
    }

    public function postReport(Request $request){
        
        $postedreports = new PostedReport;
        $postedreports->pay_period   = @$request['data']['pay_period'];
        $postedreports->year         = @$request['data']['year'];
        $postedreports->month        = @$request['data']['month'];
        $postedreports->part         = @$request['data']['part'];
        $postedreports->created_by   = Auth::id();
        $postedreports->report_type  = 'regularpws';
        $postedreports->save();


        return json_encode([
            'status'=>true,
            'response'=> 'Report posted successfully.'
        ]);


    }
}
