<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SpecialPayrollTransaction;
use App\Employee;
use Input;
class GeneralPayrollHonorariaReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL HONORARIUM';
    	$this->module = 'generalpayrollhonoraria';
        $this->module_prefix = 'payrolls/reports/honorariareports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $data = Input::all();

    	$transaction = new SpecialPayrollTransaction;

        $year  = $data['year'];
        $month = $data['month'];

        $query['transaction'] = SpecialPayrollTransaction::leftJoin('pms_employees as e','e.id','=','pms_specialpayroll_transactions.employee_id')
            ->with([
            	'employeeinfo',
            	'positions',
            	'employees'
            ])
            ->where('year',$year)
            ->where('month',$month)
            ->where('status','honoraria')
            ->orderBy('e.lastname')
            ->get();

       return json_encode($query);
    }
}
