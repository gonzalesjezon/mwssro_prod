<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\DeductionInfo;
use App\Deduction;
use App\Loan;
use App\LoanInfo;
use App\LoanInfoTransaction;
use App\EmployeeInfo;
use App\Company;
use App\Employee;
class MultiPurposeCooperativeReportsController extends Controller
{
    function __construct(){
		$this->title = 'MWSS-RO Multi-Purpose Cooperative';
    	$this->module = 'mwsscooperative';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

    	$transaction 	  = new Transaction;
        $deduction 		  = new Deduction;
        $deductioninfo    = new DeductionInfo;
        $loan 			  = new Loan;
        $loaninfo 		  = new LoanInfo;
        $employeeinfo     = new EmployeeInfo;

        $deduction = $deduction
        ->where('code','MPCC')
        ->select('id')
        ->first();

        $loan = $loan
        ->where('code','MPCLOAN')
        ->select('id')
        ->first();

        $loaninfo = $loaninfo
        ->where('loan_id',$loan->id)
        ->select('employee_id')
        ->get()->toArray();

        $deductioninfo = $deductioninfo
        ->where('deduction_id',$deduction->id)
        ->select('employee_id')
        ->get()->toArray();

        $arrEmployee = Employee::orWhereIn('id',$loaninfo)
        ->orWhereIn('id',$deductioninfo)
        ->where('active',1)
        ->pluck('id')->toArray();

        $query = Transaction::leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->with([
            'employees',
            'loan_transaction' => function($qry) use($year,$month,$loan){
                $qry->where('loan_id',$loan->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'deduction_transaction' => function($qry) use($year,$month,$deduction){
                $qry->where('deduction_id',$deduction->id)
                ->where('year',$year)
                ->where('month',$month);
            }
        ])
        ->whereIn('employee_id',$arrEmployee)
        ->where('year',$data['year'])
        ->where('month',$data['month'])
        ->orderBy('e.lastname')
        ->get();

    	return json_encode($query);
    }
}
