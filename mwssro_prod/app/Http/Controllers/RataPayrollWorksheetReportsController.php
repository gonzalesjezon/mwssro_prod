<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\PostedReport;
use App\PostedSignatory;
use App\Rata;
use App\Deduction;
use App\RataDeduction;
use App\DeductionInfoTransaction;
class RataPayrollWorksheetReportsController extends Controller
{
    function __construct(){
    	$this->title = 'WORKSHEET';
    	$this->module = 'ratapayrollworksheets';
        $this->module_prefix = 'payrolls/reports/ratareports';
    	$this->controller = $this;

    }

    public function index(){

    	$deductions = new Deduction;
    	$deductions = $deductions->where('payroll_group','rata')->get();
    	$response = array(
    					'deductions' 	=> $deductions,
    					'cols' 			=> count($deductions),
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'postedreports'  => PostedReport::where('report_type','ratapws')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year       = $q['year'];
        $month      = $q['month'];
        $pay_period = $q['pay_period'];

        $transaction = new Rata;

        $rataDeduction = RataDeduction::with('deductions')
        ->where('year',$year)
        ->where('month',$month)
        ->where('pay_period',$pay_period)
        ->selectRaw('*,deduction_id,sum(deduction_amount) as net_amount')
        ->groupBy('deduction_id')
        ->get();

        $transaction = $transaction
        ->leftJoin('office as o','o.RefId','=','pms_rata.office_id')
        ->leftJoin('division as d','d.RefId','=','pms_rata.division_id')
        ->with([
        	'employees',
        	'positions',
        	'salaryinfo',
            'employeeinfo',
        	'deductions' => function($qry) use($year,$month,$pay_period){
        		$qry = $qry->where('year',$year)
        		->where('month',$month)
                ->where('pay_period',$pay_period);
        	}
    	])
    	->where('year',$year)
    	->where('month',$month)
        ->where('pay_period',$pay_period)
        ->orderBy('d.Code','asc')
        ->orderBy('o.Code','asc')
        ->orderBy('salary_grade','desc')
    	->get();

    	return json_encode([
            'transaction'    => $transaction,
            'rata_deduction' => $rataDeduction,
            'cols' => count($rataDeduction),
        ]);
    }

    public function postReport(Request $request){
        
        $postedreports = new PostedReport;
        $postedreports->pay_period   = @$request['data']['pay_period'];
        $postedreports->year         = @$request['data']['year'];
        $postedreports->month        = @$request['data']['month'];
        $postedreports->created_by   = Auth::id();
        $postedreports->report_type  = 'ratapws';
        $postedreports->save();

        return json_encode([
            'status'=>true,
            'response'=> 'Report posted successfully.'
        ]);


    }
}

