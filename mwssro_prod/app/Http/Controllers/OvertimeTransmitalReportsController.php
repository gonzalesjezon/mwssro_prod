<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\OvertimePay;
use App\Company;
use App\Employee;
class OvertimeTransmitalReportsController extends Controller
{
    function __construct(){
    	$this->title = 'OVERTIME TRANSMITAL';
    	$this->module = 'overtimetransmital';
        $this->module_prefix = 'payrolls/reports/overtimereports';
    	$this->controller = $this;

    }

    public function index(){


    	$company = new Company;

        $companyname = 'Metropolitan Waterworks and Sewerage System <br> Regulatory Office';

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'company'       => $company->first(),
                        'company_name'  => $companyname,
                        'months'        => config('params.months'),
                       'latest_year'    => $this->latestYear(),
                       'earliest_year'  => $this->earliestYear(),
                       'current_month'  => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
                        );

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new OvertimePay;

        $data = Input::all();

        $year       = $data['year'];
        $month      = $data['month'];

        $query = OvertimePay::where('year',$year)
				        ->where('month',$month)
				        ->where('status','plantilla')
				        ->sum('ot_net');

       $query2 = OvertimePay::where('year',$year)
        ->where('month',$month)
        ->first();

        $from = date('F', strtotime(@$query2->from_date));
        $to   = date('F', strtotime(@$query2->to_date));

       return json_encode([
        'transaction' => $query,
        'From' => $from,
        'To' => $to,
       ]);
    }
}
