<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
class EmeTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'EXTRAORDINARY AND MISCELLANEOUS TRANSACTIONS';
    	$this->controller = $this;
    	$this->module = 'specialpayrolls/emetransactions';
        $this->module_prefix = 'payrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        => $this->title,
           'controller'        => $this->controller,
           'module'	        => $this->module,
           'module_prefix'     => $this->module_prefix,

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $check_eme 	= Input::get('check_eme');
        $checkeme 	= Input::get('checkeme');
        $data = "";

        $data = $this->searchName($q,$check_eme);

        if(isset($year) || isset($month) || isset($checkeme)){
            $data = $this->filter($year,$month,$checkeme);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkeme){

        $employee 	 	  = new Employee;
        $benefit  	 	  = new Benefit;
        $benefitinfo 	  = new BenefitInfo;
        $transaction 	  = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname','id'];

        $arrBenefit  = $benefit->whereIn('code',['EE','ME'])->select('id')->get()->toArray();
        $arrEmployee = $benefitinfo->whereIn('benefit_id',$arrBenefit)->select('employee_id')->get()->toArray();
        $employee_id = $transaction->whereIn('employee_id',$arrEmployee)->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkeme) {
            case 'weme':
                $query = $employee->whereIn('id',$employee_id);
                break;

            default:
                $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$arrEmployee);
                break;
        }

      	$query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->orderBy('lastname','asc')->get();

        return $response;
    }

    public function filter($year,$month,$checkeme){


        $employee           = new Employee;
        $benefit  	 	  	= new Benefit;
        $benefitinfo 	  	= new BenefitInfo;
        $transaction        = new SpecialPayrollTransaction;

        $arrBenefit 	 = $benefit->whereIn('code',['EE','ME'])->select('id')->get()->toArray();
        $arrEmployee 	 = $benefitinfo->whereIn('benefit_id',$arrBenefit)->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";

        switch ($checkeme) {
            case 'weme':

                $query =  $transaction->select('employee_id');

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)
                                        ->orderBy('lastname','asc')->get();


                break;

            case 'woeme':

                 $query =  $transaction->select('employee_id');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$arrEmployee)
                                        ->whereNotIn('id',$query)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function processEme(Request $request){
    	$data = Input::all();

    	$benefitinfo 		 = new BenefitInfo;
    	$employeeinformation = new EmployeeInformation;
        $positionitem 		 = new PositionItem;
        $benefit 			 = new Benefit;

        $benefit_id =  $benefit->whereIn('code',['EE','ME'])->select('id')->get()->toArray();

    	foreach ($data['list_id'] as $key => $value) {

	    	if(isset($value)){
                $data_eme   = new SpecialPayrollTransaction;
                $data_eme = $data_eme->where('employee_id',$value)->where('year',$data['year'])->where('month',$data['month'])->first();

                if(!isset($data_eme)){
            		$benefit_info 	= $benefitinfo->where('employee_id',$value)
        											    		->whereIn('benefit_id',$benefit_id)
        											    		->select('id')
        											    		->orderBy('benefit_id','asc')
        											    		->get();

            		$employeeinfo 	= $employeeinformation->where('employee_id',$value)->select('office_id','position_item_id')->get();

                    foreach ($benefit_info as $key => $val) {

                        $eme = new SpecialPayrollTransaction;

                        $eme->employee_id       = $value;
                        $eme->office_id         = @$employeeinfo[0]->office_id;
                        $eme->position_item_id  = @$employeeinfo[1]->position_item_id;
                        $eme->benefit_info_id   = $val->id;
                        $eme->year              = $data['year'];
                        $eme->month             = $data['month'];
                        $eme->created_by        = Auth::User()->id;
                        $eme->save();

                    }

                }

	    	}
    	}

    	$response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

    	return $response;
    }

    public function showEmeDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getEmeInfo(){
    	$data = Input::all();

    	$eme =  new SpecialPayrollTransaction;

    	$query = $eme->with(['positionitems'=>function($qry){
		    		$qry->with('positions');
			    	},'offices','benefitinfo'=>function($qry){
			    		$qry->with('benefits');
			    	}])->where('year',$data['year'])
			    	->where('month',$data['month'])
			    	->where('employee_id',@$data['employee_id'])->get();

    	return json_encode($query);
    }

    public function deleteEme(){
        $data = Input::all();

        $rata = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $rata->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

}
