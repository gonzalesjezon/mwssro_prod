<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Input;
use App\Position;
class PositionsController extends Controller
{
    function __construct(){
    	$this->title 		 = 'POSITION SETUP';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module 		 = 'positions';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
						'module'        => $this->module,
						'controller'    => $this->controller,
		                'module_prefix' => $this->module_prefix,
						'title'		    => $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['code','name'];

        $query = Position::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->orderBy('name','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$positions = new Position;

    	if(isset($request->position_id)){

    		$positions = Position::find($request->position_id);

	    	$positions->code 	   = $request->code;
	    	$positions->name 	   = $request->name;
	    	$positions->updated_by = Auth::User()->id;

	    	$positions->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

    		$this->validate($request,[
	    		'code' 	=> 'required|unique:pms_positions',
	    		'name' 	=> 'required'
    		]);

    		$positions->code 	   = $request->code;
	    	$positions->name 	   = $request->name;
	    	$positions->created_by = Auth::User()->id;

	    	$positions->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }
}
