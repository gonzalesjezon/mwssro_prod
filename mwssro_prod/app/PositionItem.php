<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionItem extends Model
{

	protected $primaryKey = 'RefId';
    protected $table = 'positionitem';
    protected $fillable = [

		'Code',
		'Name',
		'PositionRefId',

    ];

    public function position(){
    	return $this->belongsTo('App\Position','PositionRefId');
    }

}
