-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids.pms_access_modules
DROP TABLE IF EXISTS `pms_access_modules`;
CREATE TABLE IF NOT EXISTS `pms_access_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table pids.pms_access_modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_access_modules` DISABLE KEYS */;
INSERT INTO `pms_access_modules` (`id`, `access_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(14, 'HR', 1, 5, '2019-04-27 10:13:23', '2019-04-27 10:56:08');
/*!40000 ALTER TABLE `pms_access_modules` ENABLE KEYS */;

-- Dumping structure for table pids.pms_access_rights
DROP TABLE IF EXISTS `pms_access_rights`;
CREATE TABLE IF NOT EXISTS `pms_access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_module_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `to_view` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table pids.pms_access_rights: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_access_rights` DISABLE KEYS */;
INSERT INTO `pms_access_rights` (`id`, `access_module_id`, `module_id`, `to_view`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(21, 14, 2, 1, 1, 5, '2019-04-27 10:13:23', '2019-04-27 10:56:08'),
	(22, 14, 3, 1, NULL, 5, '2019-04-27 10:13:23', '2019-04-27 10:56:08'),
	(23, 14, 1, 1, NULL, 5, '2019-04-27 10:13:24', '2019-04-27 10:56:08'),
	(24, 14, 9, 1, NULL, 5, '2019-04-27 10:56:08', '2019-04-27 10:56:08'),
	(25, 14, 7, 1, NULL, 5, '2019-04-27 11:01:02', '2019-04-27 11:01:02'),
	(26, 14, 8, 1, NULL, 5, '2019-04-27 11:02:48', '2019-04-27 11:02:48');
/*!40000 ALTER TABLE `pms_access_rights` ENABLE KEYS */;

-- Dumping structure for table pids.pms_modules
DROP TABLE IF EXISTS `pms_modules`;
CREATE TABLE IF NOT EXISTS `pms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table pids.pms_modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_modules` DISABLE KEYS */;
INSERT INTO `pms_modules` (`id`, `module_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 'users', 'User', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(2, 'access_modules', 'Access Type', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(3, 'filemanagers', 'File Manager', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(4, 'employees_payroll_informations', 'Payroll Employee Information', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(5, 'annualtaxsetup', 'Annualization', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(6, 'regular_payroll', 'Regular Payroll', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(7, 'special_payroll', 'Special Payroll', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(8, 'other_payroll', 'Other Payroll', NULL, NULL, '2019-04-27 13:47:27', NULL),
	(9, 'reports', 'Reports', NULL, NULL, '2019-04-27 13:47:27', NULL);
/*!40000 ALTER TABLE `pms_modules` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
