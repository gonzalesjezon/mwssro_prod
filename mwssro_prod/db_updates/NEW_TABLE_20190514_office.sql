-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mwss.office
DROP TABLE IF EXISTS `office`;
CREATE TABLE IF NOT EXISTS `office` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `IsInterim` int(1) DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  `MFO_PAP` varchar(50) COLLATE utf32_bin DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Dumping data for table mwss.office: ~9 rows (approximately)
/*!40000 ALTER TABLE `office` DISABLE KEYS */;
INSERT INTO `office` (`RefId`, `Code`, `Name`, `IsInterim`, `Remarks`, `LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`, `MFO_PAP`) VALUES
	(1, '001', 'Office of the Chief Regulator', NULL, 'auto', '2018-06-11', '10:56:36', 'PHP', 'M', NULL),
	(2, '002', 'Technical Regulation Area', NULL, 'auto', '2018-06-11', '10:56:36', 'PHP', 'M', NULL),
	(3, '005', 'Administration and Legal Affairs', NULL, 'auto', '2018-06-11', '10:56:36', 'PHP', 'M', NULL),
	(4, '003', 'Financial Regulation Area', NULL, 'auto', '2018-06-11', '10:56:37', 'PHP', 'M', NULL),
	(5, '004', 'Customer Service Regulation Area', NULL, 'auto', '2018-06-11', '10:56:37', 'PHP', 'M', NULL),
	(6, '005', 'Office of the Deputy Administrator - Administration & Legal Matters', NULL, NULL, '2019-05-14', '08:15:42', 'Admin', 'A', NULL),
	(7, '002', 'Office of the Deputy Administrator - Technical Regulation', NULL, NULL, '2019-05-14', '08:15:42', 'Admin', 'A', NULL),
	(8, '003', 'Office of the Deputy Administrator - Financial Regulation', NULL, NULL, '2019-05-14', '08:15:42', 'Admin', 'A', NULL),
	(9, '004', 'Office of the Deputy Administrator - Customer Regulation', NULL, NULL, '2019-05-14', '08:15:42', 'Admin', 'A', NULL);
/*!40000 ALTER TABLE `office` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
