ALTER TABLE `pms_deductioninfo`
	ADD COLUMN `certificate_no` VARCHAR(225) NULL DEFAULT NULL AFTER `deduction_id`;

ALTER TABLE `pms_deductioninfo_transactions`
	ADD COLUMN `certificate_no` VARCHAR(50) NULL DEFAULT NULL AFTER `status`;