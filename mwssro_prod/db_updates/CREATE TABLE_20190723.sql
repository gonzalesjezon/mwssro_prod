CREATE TABLE `pms_series_codes` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`code` VARCHAR(50) NULL DEFAULT NULL,
	`description` VARCHAR(225) NULL DEFAULT NULL,
	`created_by` INT NULL DEFAULT NULL,
	`updated_by` INT NULL DEFAULT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci';


ALTER TABLE `pms_payroll_information`
	ADD COLUMN `series_id` INT(11) NULL DEFAULT NULL AFTER `providentfund_id`;


INSERT INTO `pms_series_codes` (`id`, `code`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, '01', 'Chief Regulator', 1, NULL, '2019-07-23 07:04:31', '2019-07-23 07:04:31'),
	(2, '02', 'Head Technical Assistant', 1, NULL, '2019-07-23 07:04:46', '2019-07-23 07:04:46'),
	(3, '03', 'MIS Design Specialist A', 1, NULL, '2019-07-23 07:04:59', '2019-07-23 07:04:59'),
	(4, '04', 'Dept. Manager-PID', 1, NULL, '2019-07-23 07:05:13', '2019-07-23 07:05:13'),
	(5, '05', 'Secretary  B', 1, NULL, '2019-07-23 07:06:28', '2019-07-23 07:06:28'),
	(6, '06', 'Artist-Illustrator A', 1, NULL, '2019-07-23 07:06:52', '2019-07-23 07:06:52'),
	(7, '07', 'Data Analyst Controller', 1, NULL, '2019-07-23 07:07:42', '2019-07-23 07:07:42'),
	(8, '08', 'Driver-Mechnaic B', 1, NULL, '2019-07-23 07:19:54', '2019-07-23 07:19:54'),
	(9, '09', 'Driver-Mechanic A', 1, NULL, '2019-07-23 07:20:10', '2019-07-23 07:20:10'),
	(10, '10', 'Dep Adm - TRA', 1, NULL, '2019-07-23 07:20:25', '2019-07-23 07:20:25'),
	(11, '11', 'Technical Assistant A', 1, NULL, '2019-07-23 07:20:44', '2019-07-23 07:20:44'),
	(12, '12', 'Dept Manager-WQCD', 1, NULL, '2019-07-23 07:21:04', '2019-07-23 07:21:04'),
	(13, '13', 'Dept Manager-OMD', 1, NULL, '2019-07-23 07:21:28', '2019-07-23 07:21:28'),
	(14, '14', 'Supvg. Water Utilities Reg. Officer', 1, NULL, '2019-07-23 07:21:47', '2019-07-23 07:21:47'),
	(15, '15', 'Dept. Mgr.-TCMD', 1, NULL, '2019-07-23 07:22:32', '2019-07-23 07:22:32'),
	(16, '16', 'Dept Manager-FAAMD', 1, NULL, '2019-07-23 07:22:49', '2019-07-23 07:22:49'),
	(17, '17', 'Supvg. Financial Mgt. Specialist', 1, NULL, '2019-07-23 07:23:06', '2019-07-23 07:23:06'),
	(18, '18', 'Sr. Water Utilities Reg. Off.', 1, 1, '2019-07-23 07:23:22', '2019-07-23 07:28:21'),
	(19, '19', 'Finance Officer C', 1, 1, '2019-07-23 07:24:02', '2019-07-23 07:30:50'),
	(20, '20', 'Chief Economist', 1, 1, '2019-07-23 07:24:25', '2019-07-23 07:31:10'),
	(21, '21', 'Dep Adm - CSR', 1, 1, '2019-07-23 07:24:51', '2019-07-23 07:31:42'),
	(22, '22', 'Driver-Mechanic B', 1, 1, '2019-07-23 07:25:13', '2019-07-23 07:32:04'),
	(23, '23', 'Dept. Manager-Compliants Monitoring', 1, NULL, '2019-07-23 07:32:25', '2019-07-23 07:32:25'),
	(24, '24', 'Spvg. WURO', 1, NULL, '2019-07-23 07:32:43', '2019-07-23 07:32:43'),
	(25, '25', 'Dept. Mgr.- MED', 1, NULL, '2019-07-23 07:33:02', '2019-07-23 07:33:02'),
	(26, '26', 'Dep Adm - ALA', 1, NULL, '2019-07-23 07:33:27', '2019-07-23 07:33:27'),
	(27, '27', 'Finance Officer B', 1, NULL, '2019-07-23 07:34:32', '2019-07-23 07:34:32'),
	(28, '28', 'Sr. Prop. Officer', 1, NULL, '2019-07-23 07:34:49', '2019-07-23 07:34:49'),
	(29, '29', 'Chief Corporate Accountant', 1, NULL, '2019-07-23 07:35:11', '2019-07-23 07:35:11'),
	(30, '30', 'Admin. Officer III', 1, NULL, '2019-07-23 07:35:27', '2019-07-23 07:35:27'),
	(31, '31', 'Records Officer D', 1, NULL, '2019-07-23 07:35:46', '2019-07-23 07:35:46'),
	(32, '32', 'Sr. IRM Officer A', 1, NULL, '2019-07-23 07:36:13', '2019-07-23 07:36:13'),
	(33, '33', 'Dept. Mgr.- Adm.', 1, NULL, '2019-07-23 07:36:52', '2019-07-23 07:36:52'),
	(34, '34', 'Fiscal Examiner A', 1, NULL, '2019-07-23 07:37:12', '2019-07-23 07:37:12'),
	(35, '35', 'Senior Corporate Attorney', 1, NULL, '2019-07-23 07:37:48', '2019-07-23 07:37:48'),
	(36, '36', 'Dept Manager - Legal', 1, NULL, '2019-07-23 07:38:02', '2019-07-23 07:38:02');
/*!40000 ALTER TABLE `pms_series_codes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
