-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mwss.pms_deductioninfo
DROP TABLE IF EXISTS `pms_deductioninfo`;
CREATE TABLE IF NOT EXISTS `pms_deductioninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `deduct_pay_period` varchar(225) DEFAULT NULL,
  `deduct_amount` decimal(9,2) DEFAULT NULL,
  `deduct_date_start` date DEFAULT NULL,
  `deduct_date_end` date DEFAULT NULL,
  `deduct_date_terminated` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

-- Dumping data for table mwss.pms_deductioninfo: ~77 rows (approximately)
/*!40000 ALTER TABLE `pms_deductioninfo` DISABLE KEYS */;
INSERT INTO `pms_deductioninfo` (`id`, `employeeinfo_id`, `employee_number`, `employee_id`, `deduction_id`, `deduct_pay_period`, `deduct_amount`, `deduct_date_start`, `deduct_date_end`, `deduct_date_terminated`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, NULL, '128', 29, 1, NULL, 1760.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, NULL, '121', 25, 1, NULL, 495.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, NULL, '21', 3, 1, NULL, 1760.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, NULL, '11', 1, 1, NULL, 1760.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, NULL, '132', 31, 1, NULL, 330.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, NULL, '124', 74, 1, NULL, 1760.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, NULL, '50', 47, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, NULL, '38', 10, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, NULL, '69', 48, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, NULL, '37', 9, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, NULL, '23', 5, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, NULL, '36', 49, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, NULL, '96', 50, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, NULL, '59', 13, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, NULL, '61', 14, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, NULL, '29', 7, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, NULL, '72', 52, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, NULL, '30', 8, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, NULL, '40', 11, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, NULL, '24', 56, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, NULL, '21', 3, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, NULL, '28', 6, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, NULL, '70', 58, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, NULL, '97', 16, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, NULL, '81', 61, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, NULL, '15', 2, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, NULL, '22', 4, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, NULL, '80', 15, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, NULL, '73', 65, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, NULL, '12', 66, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, NULL, '33', 69, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, NULL, '11', 1, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, NULL, '71', 71, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, NULL, '85', 72, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, NULL, '18', 76, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, NULL, '51', 12, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, NULL, '35', 78, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, NULL, '50', 47, 5, NULL, 2253.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, NULL, '38', 10, 5, NULL, 10036.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, NULL, '120', 24, 5, NULL, 1568.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, NULL, '69', 48, 5, NULL, 1500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, NULL, '37', 9, 5, NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, NULL, '23', 5, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, NULL, '110', 19, 5, NULL, 1200.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, NULL, '117', 23, 5, NULL, 700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, NULL, '36', 49, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, NULL, '96', 50, 5, NULL, 2500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(48, NULL, '59', 13, 5, NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, NULL, '61', 14, 5, NULL, 1500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, NULL, '29', 7, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, NULL, '72', 52, 5, NULL, 1300.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, NULL, '30', 8, 5, NULL, 1656.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, NULL, '40', 11, 5, NULL, 834.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, NULL, '131', 53, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, NULL, '121', 25, 5, NULL, 695.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, NULL, '24', 56, 5, NULL, 1656.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, NULL, '21', 3, 5, NULL, 2204.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, NULL, '28', 6, 5, NULL, 850.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, NULL, '70', 58, 5, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, NULL, '97', 16, 5, NULL, 1110.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, NULL, '81', 61, 5, NULL, 4000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, NULL, '112', 63, 5, NULL, 3226.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, NULL, '15', 2, 5, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, NULL, '22', 4, 5, NULL, 1200.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, NULL, '80', 15, 5, NULL, 4600.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(66, NULL, '73', 65, 5, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, NULL, '12', 66, 5, NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, NULL, '114', 21, 5, NULL, 2137.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(69, NULL, '122', 26, 5, NULL, 6212.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(70, NULL, '33', 69, 5, NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(71, NULL, '11', 1, 5, NULL, 2500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, NULL, '71', 71, 5, NULL, 1500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, NULL, '85', 72, 5, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, NULL, '18', 76, 5, NULL, 2500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(75, NULL, '109', 18, 5, NULL, 700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(76, NULL, '51', 12, 5, NULL, 1700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(77, NULL, '35', 78, 5, NULL, 3500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(78, NULL, '126', 27, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, NULL, '130', 51, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(80, NULL, '122', 26, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, NULL, '71', 71, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(82, NULL, '109', 18, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(83, NULL, '117', 23, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(84, NULL, '127', 28, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(85, NULL, '33', 69, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(86, NULL, '36', 49, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(87, NULL, '121', 25, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(88, NULL, '35', 78, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, NULL, '115', 22, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(90, NULL, '120', 24, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(91, NULL, '18', 76, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(92, NULL, '28', 6, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(93, NULL, '29', 7, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(94, NULL, '11', 1, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(95, NULL, '132', 31, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(96, NULL, '37', 9, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(97, NULL, '110', 19, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(98, NULL, '51', 12, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(99, NULL, '69', 48, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(100, NULL, '96', 50, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(101, NULL, '61', 14, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(102, NULL, '113', 20, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(103, NULL, '72', 52, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(104, NULL, '135', 33, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(105, NULL, '21', 3, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(106, NULL, '70', 58, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(107, NULL, '97', 16, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(108, NULL, '81', 61, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(109, NULL, '112', 63, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(110, NULL, '15', 2, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(111, NULL, '118', 64, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(112, NULL, '131', 53, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_deductioninfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
