-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mwss.pms_payroll_information
DROP TABLE IF EXISTS `pms_payroll_information`;
CREATE TABLE IF NOT EXISTS `pms_payroll_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bp_no` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `gsispolicy_id` int(11) DEFAULT NULL,
  `pagibigpolicy_id` int(11) DEFAULT NULL,
  `philhealthpolicy_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bankbranch_id` int(11) DEFAULT NULL,
  `wagestatus_id` int(11) DEFAULT NULL,
  `providentfund_id` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `pagibig_contribution` decimal(9,2) DEFAULT NULL,
  `er_pagibig_share` decimal(9,2) DEFAULT NULL,
  `philhealth_contribution` decimal(9,2) DEFAULT NULL,
  `er_philhealth_share` decimal(9,2) DEFAULT NULL,
  `gsis_contribution` decimal(9,2) DEFAULT NULL,
  `er_gsis_share` decimal(9,2) DEFAULT NULL,
  `tax_contribution` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `account_number` varchar(225) DEFAULT NULL,
  `pagibig2` decimal(9,2) DEFAULT NULL,
  `pagibig_personal` decimal(9,2) DEFAULT NULL,
  `no_ofdays_inayear` int(11) DEFAULT NULL,
  `no_ofdays_inamonth` int(11) DEFAULT NULL,
  `total_hours_inaday` int(11) DEFAULT NULL,
  `tax_payperiod` varchar(255) DEFAULT NULL,
  `tax_bracket` varchar(255) DEFAULT NULL,
  `tax_bracket_amount` decimal(9,2) DEFAULT NULL,
  `tax_inexcess` decimal(9,2) DEFAULT NULL,
  `union_dues` decimal(9,2) DEFAULT NULL,
  `mid_year_bonus` tinyint(4) DEFAULT NULL,
  `year_end_bonus` tinyint(4) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Dumping data for table mwss.pms_payroll_information: ~59 rows (approximately)
/*!40000 ALTER TABLE `pms_payroll_information` DISABLE KEYS */;
INSERT INTO `pms_payroll_information` (`id`, `bp_no`, `employee_id`, `employee_number`, `gsispolicy_id`, `pagibigpolicy_id`, `philhealthpolicy_id`, `taxpolicy_id`, `bank_id`, `bankbranch_id`, `wagestatus_id`, `providentfund_id`, `daily_rate_amount`, `monthly_rate_amount`, `annual_rate_amount`, `pagibig_contribution`, `er_pagibig_share`, `philhealth_contribution`, `er_philhealth_share`, `gsis_contribution`, `er_gsis_share`, `tax_contribution`, `overtime_balance_amount`, `account_number`, `pagibig2`, `pagibig_personal`, `no_ofdays_inayear`, `no_ofdays_inamonth`, `total_hours_inaday`, `tax_payperiod`, `tax_bracket`, `tax_bracket_amount`, `tax_inexcess`, `union_dues`, `mid_year_bonus`, `year_end_bonus`, `tax_id_number`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, NULL, 17, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1942.27, 42730.00, 512760.00, 100.00, 100.00, 550.00, 550.00, 3845.70, 5127.60, 4849.25, 256380.00, '1907-0148-58', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 9397.00, NULL, NULL, NULL, NULL, '2018-11-23 04:02:25', '2018-11-23 04:02:25', NULL),
	(2, '2004642387', 22, '115', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2708.95, 59597.00, 715164.00, 100.00, 100.00, 550.00, 550.00, 5363.73, 7151.64, 9066.00, 357582.00, '1907-0148-74', 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-11-23 04:06:12', '2018-11-23 05:49:53', NULL),
	(3, '2001099416', 47, '50', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5226.41, 114981.00, 1379772.00, 100.00, 100.00, 550.00, 550.00, 10348.29, 13797.72, 25327.53, 689886.00, '1467-0597-90', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 48314.00, NULL, NULL, NULL, NULL, '2018-11-23 04:06:20', '2018-11-23 04:06:20', NULL),
	(4, '2001099347', 10, '38', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4577.64, 100708.00, 1208496.00, 100.00, 100.00, 550.00, 550.00, 9063.72, 12084.96, 21045.63, 604248.00, '1467-0598-03', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 34041.00, NULL, NULL, NULL, NULL, '2018-11-23 04:06:32', '2018-11-23 04:06:32', NULL),
	(5, '2000940339', 30, '129', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2982.00, 65604.00, 787248.00, 100.00, 100.00, 550.00, 550.00, 5904.36, 7872.48, 10567.75, 393624.00, '0157-1769-43', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 32271.00, NULL, NULL, NULL, NULL, '2018-11-23 04:06:44', '2018-11-23 04:06:44', NULL),
	(6, '2004798467', 24, '120', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, '1907-0152-42', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-11-23 04:06:53', '2018-11-23 04:06:53', NULL),
	(7, NULL, 35, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 8846.00, 352302.00, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 25384.00, NULL, NULL, NULL, NULL, '2018-11-23 04:07:01', '2018-11-23 04:07:01', NULL),
	(8, '2001099383', 48, '69', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 722.50, 15895.00, 190740.00, 100.00, 100.00, 218.56, 218.56, 1430.55, 1907.40, 0.00, 95370.00, '1907-0148-82', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15895.00, NULL, NULL, NULL, NULL, '2018-11-23 04:07:10', '2018-11-23 04:07:10', NULL),
	(9, '2001099172', 9, '37', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2875.09, 63252.00, 759024.00, 100.00, 100.00, 550.00, 550.00, 5692.68, 7590.24, 9979.75, 379512.00, '1467-0598-11', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 29919.00, NULL, NULL, NULL, NULL, '2018-11-23 04:10:37', '2018-11-23 04:10:37', NULL),
	(10, '2001099419', 5, '23', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2337.64, 51428.00, 617136.00, 100.00, 100.00, 550.00, 550.00, 4628.52, 6171.36, 7023.75, 308568.00, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 18095.00, NULL, NULL, NULL, NULL, '2018-11-23 04:10:45', '2018-11-23 04:10:45', NULL),
	(11, '2004616647', 19, '110', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 699.09, 15380.00, 184560.00, 100.00, 100.00, 211.47, 211.48, 1384.20, 1845.60, 0.00, 92280.00, '1907-0149-04', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15380.00, NULL, NULL, NULL, NULL, '2018-11-23 04:10:53', '2018-11-23 04:10:53', NULL),
	(12, '2004760785', 23, '117', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, '1907-0149-12', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-11-23 04:11:46', '2018-11-23 04:11:46', NULL),
	(13, '2005300399', 29, '128', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5226.41, 114981.00, 1379772.00, 100.00, 100.00, 550.00, 550.00, 10348.29, 13797.72, 25327.53, 689886.00, '1907-0076-65', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 48314.00, NULL, NULL, NULL, NULL, '2018-11-23 04:11:59', '2018-11-23 04:11:59', NULL),
	(14, '2005286179', 27, '126', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, '1907-007-630', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-11-23 04:12:07', '2018-11-23 04:12:07', NULL),
	(15, '2001099389', 49, '36', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1882.41, 41413.00, 496956.00, 100.00, 100.00, 569.43, 569.43, 3727.17, 4969.56, 4520.00, 248478.00, '1467-0598-46', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 8080.00, NULL, NULL, NULL, NULL, '2018-11-23 04:12:17', '2018-11-23 04:12:17', NULL),
	(16, '2001099101', 13, '59', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2235.64, 49184.00, 590208.00, 100.00, 100.00, 550.00, 550.00, 4426.56, 5902.08, 6462.75, 295104.00, '1907-0149-20', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 15851.00, NULL, NULL, NULL, NULL, '2018-11-23 04:12:34', '2018-11-23 04:12:34', NULL),
	(17, '2003991748', 50, '96', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2235.64, 49184.00, 590208.00, 100.00, 100.00, 550.00, 550.00, 4426.56, 5902.08, 6462.75, 295104.00, '1467-0598-54', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 15851.00, NULL, NULL, NULL, NULL, '2018-11-23 04:12:45', '2018-11-23 04:12:45', NULL),
	(18, '2001099343', 14, '61', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1551.36, 34130.00, 409560.00, 100.00, 100.00, 469.29, 469.29, 3071.70, 4095.60, 2699.25, 204780.00, '1467-0598-70', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 797.00, NULL, NULL, NULL, NULL, '2018-11-23 04:12:53', '2018-11-23 04:12:53', NULL),
	(19, '2005286351', 28, '127', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, '1907-0149-47', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-11-23 04:13:03', '2018-11-23 04:13:03', NULL),
	(20, '2001099267', 7, '29', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2918.18, 64200.00, 770400.00, 100.00, 100.00, 550.00, 550.00, 5778.00, 7704.00, 10216.75, 385200.00, '1907-0149-55', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 30867.00, NULL, NULL, NULL, NULL, '2018-11-23 04:13:13', '2018-11-23 04:13:13', NULL),
	(21, '2004642401', 20, '113', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2424.59, 53341.00, 640092.00, 100.00, 100.00, 550.00, 550.00, 4800.69, 6400.92, 7502.00, 320046.00, '1907-0149-63', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 20008.00, NULL, NULL, NULL, NULL, '2018-11-23 04:13:24', '2018-11-23 04:13:24', NULL),
	(22, '2001099786', 51, '130', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, '1907-0077-20', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-11-23 04:13:33', '2018-11-23 04:13:33', NULL),
	(23, '2001099341', 52, '72', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 722.50, 15895.00, 190740.00, 100.00, 100.00, 218.56, 218.56, 1430.55, 1907.40, 0.00, 95370.00, '1907-0149-80', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15895.00, NULL, NULL, NULL, NULL, '2018-11-23 04:13:42', '2018-11-23 04:13:42', NULL),
	(24, '2001099214', 8, '30', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, '1467-0599-43', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-11-23 04:13:52', '2018-11-23 04:13:52', NULL),
	(25, '2001099068', 11, '40', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, '1467-0599-51', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-11-23 04:14:02', '2018-11-23 04:14:02', NULL),
	(26, NULL, 34, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-11-23 04:14:10', '2018-11-23 04:14:10', NULL),
	(27, '2005324441', 53, '131', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2388.82, 52554.00, 630648.00, 100.00, 100.00, 550.00, 550.00, 4729.86, 6306.48, 7305.25, 315324.00, '1907-0076-90', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 19221.00, NULL, NULL, NULL, NULL, '2018-11-23 04:14:19', '2018-11-23 04:14:19', NULL),
	(28, NULL, 33, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, '1907-0078-27', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-11-23 04:14:28', '2018-11-23 04:14:28', NULL),
	(29, '2004875973', 25, '121', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, '1907-0152-77', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-11-23 04:14:36', '2018-11-23 04:14:36', NULL),
	(30, '2004985079', 55, '123', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5226.41, 114981.00, 1379772.00, 100.00, 100.00, 550.00, 550.00, 10348.29, 13797.72, 25327.53, 689886.00, '1467-0737-41', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 48314.00, NULL, NULL, NULL, NULL, '2018-11-23 04:14:46', '2018-11-23 04:14:46', NULL),
	(31, '2001099410', 56, '24', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1882.41, 41413.00, 496956.00, 100.00, 100.00, 569.43, 569.43, 3727.17, 4969.56, 4520.00, 248478.00, '1907-0149-98', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 8080.00, NULL, NULL, NULL, NULL, '2018-11-23 04:14:54', '2018-11-23 04:14:54', NULL),
	(32, '2001099304', 3, '21', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2832.64, 62318.00, 747816.00, 100.00, 100.00, 550.00, 550.00, 5608.62, 7478.16, 9746.25, 373908.00, '1467-0599-94', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 28985.00, NULL, NULL, NULL, NULL, '2018-11-23 04:15:03', '2018-11-23 04:15:03', NULL),
	(33, '2001099380', 6, '28', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2918.18, 64200.00, 770400.00, 100.00, 100.00, 550.00, 550.00, 5778.00, 7704.00, 10216.75, 385200.00, ' 1467-0600-20 ', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 30867.00, NULL, NULL, NULL, NULL, '2018-11-23 04:15:11', '2018-11-23 04:15:11', NULL),
	(34, '2001099109', 58, '70', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 722.50, 15895.00, 190740.00, 100.00, 100.00, 218.56, 218.56, 1430.55, 1907.40, 0.00, 95370.00, '1907-0150-21', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15895.00, NULL, NULL, NULL, NULL, '2018-11-23 04:15:20', '2018-11-23 04:15:20', NULL),
	(35, '2001099034', 16, '97', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1006.77, 22149.00, 265788.00, 100.00, 100.00, 304.55, 304.55, 1993.41, 2657.88, 263.20, 132894.00, ' 1467-0600-46 ', 0.00, 0.00, NULL, NULL, NULL, NULL, '20833', 0.00, 1316.00, NULL, NULL, NULL, NULL, '2018-11-23 04:15:28', '2018-11-23 04:15:28', NULL),
	(36, '2001043796', 59, '133', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, '1907-0115-90', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-11-23 04:15:36', '2018-11-23 04:15:36', NULL),
	(37, '2005259968', 60, '125', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2982.00, 65604.00, 787248.00, 100.00, 100.00, 550.00, 550.00, 5904.36, 7872.48, 10567.75, 393624.00, '0717-0718-08', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 32271.00, NULL, NULL, NULL, NULL, '2018-11-23 04:15:44', '2018-11-23 04:15:44', NULL),
	(38, '2001099520', 61, '81', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2269.14, 49921.00, 599052.00, 100.00, 100.00, 550.00, 550.00, 4492.89, 5990.52, 6647.00, 299526.00, '1467-0603-80', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 16588.00, NULL, NULL, NULL, NULL, '2018-11-23 04:15:52', '2018-11-23 04:15:52', NULL),
	(39, '2004642402', 63, '112', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1318.64, 29010.00, 348120.00, 100.00, 100.00, 398.89, 398.89, 2610.90, 3481.20, 1635.40, 174060.00, '1907-0150-30', 0.00, 0.00, NULL, NULL, NULL, NULL, '20833', 0.00, 8177.00, NULL, NULL, NULL, NULL, '2018-11-23 04:16:03', '2018-11-23 04:16:03', NULL),
	(40, '2001099100', 2, '15', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, '1907-0150-48', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-11-23 04:16:12', '2018-11-23 04:16:12', NULL),
	(41, '2001099472', 4, '22', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, '1907-0150-64', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-11-23 04:16:20', '2018-11-23 04:16:20', NULL),
	(42, '2001099215', 64, '118', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 699.09, 15380.00, 184560.00, 100.00, 100.00, 211.47, 211.48, 1384.20, 1845.60, 0.00, 92280.00, '1907-0150-56', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15380.00, NULL, NULL, NULL, NULL, '2018-11-23 04:16:30', '2018-11-23 04:16:30', NULL),
	(43, '2001099102', 15, '80', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, '1907-0150-72', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-11-23 04:16:39', '2018-11-23 04:16:39', NULL),
	(44, '2001099103', 65, '73', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 722.50, 15895.00, 190740.00, 100.00, 100.00, 218.56, 218.56, 1430.55, 1907.40, 0.00, 95370.00, '1907-0150-80', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15895.00, NULL, NULL, NULL, NULL, '2018-11-23 04:16:48', '2018-11-23 04:16:48', NULL),
	(45, '2001099572', 66, '12', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4646.23, 102217.00, 1226604.00, 100.00, 100.00, 550.00, 550.00, 9199.53, 12266.04, 21498.33, 613302.00, '1907-0150-99', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 35550.00, NULL, NULL, NULL, NULL, '2018-11-23 04:16:57', '2018-11-23 04:16:57', NULL),
	(46, '2004642404', 21, '114', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1942.27, 42730.00, 512760.00, 100.00, 100.00, 550.00, 550.00, 3845.70, 5127.60, 4849.25, 256380.00, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 9397.00, NULL, NULL, NULL, NULL, '2018-11-23 04:17:06', '2018-11-23 04:17:06', NULL),
	(47, '2004875920', 26, '122', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 678.20, 145344.00, '1907-0152-69', 0.00, 0.00, NULL, NULL, NULL, NULL, '20833', 0.00, 3391.00, NULL, NULL, NULL, NULL, '2018-11-23 04:17:14', '2018-11-23 04:17:14', NULL),
	(48, '2001099414', 69, '33', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2961.91, 65162.00, 781944.00, 100.00, 100.00, 550.00, 550.00, 5864.58, 7819.44, 10457.25, 390972.00, '1467-0602-24', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 31829.00, NULL, NULL, NULL, NULL, '2018-11-23 04:17:23', '2018-11-23 04:17:23', NULL),
	(49, '2001099386', 1, '11', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1751.95, 38543.00, 462516.00, 100.00, 100.00, 529.97, 529.97, 3468.87, 4625.16, 3802.50, 231258.00, '1467-0602-32', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 5210.00, NULL, NULL, NULL, NULL, '2018-11-23 04:17:32', '2018-11-23 04:17:32', NULL),
	(50, '2003119253', 31, '132', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, '1907-0154-55', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-11-23 04:17:40', '2018-11-23 04:17:40', NULL),
	(51, '2001099262', 71, '71', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 801.23, 17627.00, 211524.00, 100.00, 100.00, 242.37, 242.37, 1586.43, 2115.24, 0.00, 105762.00, '1907-0151-53', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 17627.00, NULL, NULL, NULL, NULL, '2018-11-23 04:17:49', '2018-11-23 04:17:49', NULL),
	(52, '2001099527', 72, '85', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, '1467-0603-05', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-11-23 04:17:57', '2018-11-23 04:17:57', NULL),
	(53, '2000860240', 74, '124', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5839.41, 128467.00, 1541604.00, 100.00, 100.00, 550.00, 550.00, 11562.03, 15416.04, 29373.33, 770802.00, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 61800.00, NULL, NULL, NULL, NULL, '2018-11-23 04:18:07', '2018-11-23 04:18:07', NULL),
	(54, '2001099570', 76, '18', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2832.64, 62318.00, 747816.00, 100.00, 100.00, 550.00, 550.00, 5608.62, 7478.16, 9746.25, 373908.00, '1907-0151-61', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 28985.00, NULL, NULL, NULL, NULL, '2018-11-23 04:18:17', '2018-11-23 04:18:17', NULL),
	(55, NULL, 77, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-11-23 04:18:26', '2018-11-23 04:18:26', NULL),
	(56, '2004616661', 18, '109', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 699.09, 15380.00, 184560.00, 100.00, 100.00, 211.47, 211.48, 1384.20, 1845.60, 0.00, 92280.00, '1907-0151-70', 0.00, 0.00, NULL, NULL, NULL, NULL, '0', 0.00, 15380.00, NULL, NULL, NULL, NULL, '2018-11-23 04:18:33', '2018-11-23 04:18:33', NULL),
	(57, '2001099061', 12, '51', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1882.41, 41413.00, 496956.00, 100.00, 100.00, 569.43, 569.43, 3727.17, 4969.56, 4520.00, 248478.00, '1467-0603-48', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 8080.00, NULL, NULL, NULL, NULL, '2018-11-23 04:18:41', '2018-11-23 04:18:41', NULL),
	(58, NULL, 32, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, '1907-0078-00', 0.00, 0.00, NULL, NULL, NULL, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-11-23 04:18:48', '2018-11-23 04:18:48', NULL),
	(59, '2001099035', 78, '35', 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2961.91, 65162.00, 781944.00, 100.00, 100.00, 550.00, 550.00, 5864.58, 7819.44, 10457.25, 390972.00, '1907-0151-88', 0.00, 0.00, NULL, NULL, NULL, NULL, '33333', 2500.00, 31829.00, NULL, NULL, NULL, NULL, '2018-11-23 04:18:57', '2018-11-23 04:18:57', NULL);
/*!40000 ALTER TABLE `pms_payroll_information` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
