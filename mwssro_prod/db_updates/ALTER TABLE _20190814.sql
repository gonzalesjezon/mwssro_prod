ALTER TABLE `pms_rata`
	ADD COLUMN `pay_period` VARCHAR(225) NULL DEFAULT NULL AFTER `month`;

DROP TABLE IF EXISTS `pms_rata_deductions`;
CREATE TABLE IF NOT EXISTS `pms_rata_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `rata_id` int(11) DEFAULT NULL,
  `deduction_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping structure for table mwss.pms_deductions
DROP TABLE IF EXISTS `pms_deductions`;
CREATE TABLE IF NOT EXISTS `pms_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table mwss.pms_deductions: ~10 rows (approximately)
/*!40000 ALTER TABLE `pms_deductions` DISABLE KEYS */;
INSERT INTO `pms_deductions` (`id`, `code`, `name`, `payroll_group`, `tax_type`, `itr_classification`, `alphalist_classification`, `amount`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'MPCC', 'Multipurpose Coop - Contri', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-17 03:16:30', '2018-08-17 03:17:27', NULL),
	(3, 'UD', 'Union Dues', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-17 03:18:18', '2018-08-17 03:18:18', NULL),
	(4, 'WF', 'Welfare Fund - PS', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-17 06:55:43', '2018-08-17 06:55:43', NULL),
	(5, 'WD', 'WASSSLAI - Deposit', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-17 19:08:59', '2018-08-17 19:08:59', NULL),
	(9, 'LO', 'Legal Fee', NULL, NULL, NULL, NULL, 0.00, NULL, NULL, '2019-07-27 21:13:39', NULL),
	(10, 'FC', 'Fortune Care', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'SSS', 'SSS - Contribution', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'TELBILL', 'Tel. Bill', 'rata', 'Non Taxable', NULL, NULL, 0.00, NULL, '2019-08-14 08:45:05', '2019-08-14 08:47:10', NULL),
	(13, 'RA', 'RA', 'rata', NULL, NULL, NULL, 0.00, NULL, '2019-08-14 10:54:20', '2019-08-14 10:54:20', NULL),
	(14, 'TA', 'TA', 'rata', NULL, NULL, NULL, 0.00, NULL, '2019-08-14 10:54:31', '2019-08-14 10:54:31', NULL);
/*!40000 ALTER TABLE `pms_deductions` ENABLE KEYS */;

