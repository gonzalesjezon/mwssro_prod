USE `mwss`;
ALTER TABLE `pms_employees`
	CHANGE COLUMN `philhealth` `philhealth` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `gsis`,
	CHANGE COLUMN `created_by` `created_by` INT(11) NULL DEFAULT NULL AFTER `remarks`;