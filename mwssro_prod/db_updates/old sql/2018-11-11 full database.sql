-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for e2e_db
DROP DATABASE IF EXISTS `mwss`;
CREATE DATABASE IF NOT EXISTS `mwss` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mwss`;

-- Dumping structure for table e2e_db.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e2e_db.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2018_07_13_041020_create_employees_table', 1),
	(2, '2018_07_13_042843_create_employee_information_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table e2e_db.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `SystemRefId` int(10) DEFAULT NULL,
  `ScrnId` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `UseFor` int(10) DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Ordinal` int(6) DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Icons` varchar(50) COLLATE utf32_bin DEFAULT NULL,
  `Filename` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `Description` varchar(300) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32 COLLATE=utf32_bin COMMENT='System Modules';

-- Dumping data for table e2e_db.modules: ~4 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`RefId`, `SystemRefId`, `ScrnId`, `UseFor`, `Code`, `Ordinal`, `Name`, `Icons`, `Filename`, `Description`, `Remarks`, `LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`) VALUES
	(4, 1, '1521515120', NULL, '201F', 2, '201 File', NULL, 'scrn201File', NULL, NULL, '2018-03-20', '11:05:22', 'admin', 'A'),
	(5, 1, '1521520378', NULL, 'MSTF', 1, 'Master File', NULL, 'scrnMasterFile', NULL, NULL, '2018-03-20', '12:33:00', 'admin', 'A'),
	(6, 1, '1521520405', NULL, '201U', 3, '201 UPDATE', NULL, 'scrn201Update', NULL, NULL, '2018-03-20', '12:34:09', 'admin', 'A'),
	(7, 1, '1521520405', NULL, 'EMPM', 4, 'Employees Movement', NULL, 'scrnEmpMovement', NULL, NULL, '2018-03-20', '12:37:27', 'admin', 'A');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_adjustments
DROP TABLE IF EXISTS `pms_adjustments`;
CREATE TABLE IF NOT EXISTS `pms_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_adjustments: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_adjustments` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_adjustments` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_annualtax_policy
DROP TABLE IF EXISTS `pms_annualtax_policy`;
CREATE TABLE IF NOT EXISTS `pms_annualtax_policy` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `from_year` varchar(225) DEFAULT NULL,
  `to_year` varchar(225) DEFAULT NULL,
  `below_amount` decimal(12,2) DEFAULT NULL,
  `above_amount` decimal(12,2) DEFAULT NULL,
  `rate_percentage` decimal(12,2) DEFAULT NULL,
  `rate_amount` decimal(12,2) DEFAULT NULL,
  `excess_amount` decimal(12,2) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_annualtax_policy: ~5 rows (approximately)
/*!40000 ALTER TABLE `pms_annualtax_policy` DISABLE KEYS */;
INSERT INTO `pms_annualtax_policy` (`id`, `from_year`, `to_year`, `below_amount`, `above_amount`, `rate_percentage`, `rate_amount`, `excess_amount`, `effectivity_date`, `remarks`, `created_at`, `updated_at`, `updated_by`, `created_by`) VALUES
	(1, '2018', '2022', 250000.00, 400000.00, 20.00, 0.00, 250000.00, '2018-10-19', NULL, '2018-10-19 12:19:26', '2018-10-19 12:19:26', NULL, 2),
	(2, '2018', '2022', 400000.00, 800000.00, 25.00, 30000.00, 400000.00, '2018-10-19', NULL, '2018-10-19 12:29:02', '2018-10-19 12:29:02', NULL, 2),
	(3, '2018', '2022', 800000.00, 2000000.00, 30.00, 130000.00, 800000.00, '2018-10-19', NULL, '2018-10-19 12:38:17', '2018-10-19 12:38:17', NULL, 2),
	(4, '2018', '2022', 2000000.00, 8000000.00, 32.00, 490000.00, 2000000.00, '2018-10-19', NULL, '2018-10-19 13:02:26', '2018-10-19 13:02:26', NULL, 2),
	(5, '2018', '2022', 8000000.00, 0.00, 35.00, 2041000.00, 8000000.00, '2018-10-19', NULL, '2018-10-19 13:07:05', '2018-10-19 13:07:05', NULL, 2);
/*!40000 ALTER TABLE `pms_annualtax_policy` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_annual_tax_rates
DROP TABLE IF EXISTS `pms_annual_tax_rates`;
CREATE TABLE IF NOT EXISTS `pms_annual_tax_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `basic_amount` decimal(10,0) DEFAULT NULL,
  `contribution_amount` decimal(10,0) DEFAULT NULL,
  `pera_amount` decimal(10,0) DEFAULT NULL,
  `rata_amount` decimal(10,0) DEFAULT NULL,
  `longevity_amount` decimal(10,0) DEFAULT NULL,
  `hp_amount` decimal(10,0) DEFAULT NULL,
  `tax_amount` decimal(10,0) DEFAULT NULL,
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=451 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_attendance_info
DROP TABLE IF EXISTS `pms_attendance_info`;
CREATE TABLE IF NOT EXISTS `pms_attendance_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `total_workdays` decimal(9,2) DEFAULT NULL,
  `actual_absence` int(11) DEFAULT NULL,
  `adjust_absence` int(11) DEFAULT NULL,
  `total_absence` decimal(9,2) DEFAULT NULL,
  `actual_tardines` int(11) DEFAULT NULL,
  `adjust_tardines` int(11) DEFAULT NULL,
  `total_tardines` decimal(9,2) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `total_undertime` decimal(9,2) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `employee_status` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_attendance_info: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_attendance_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_attendance_info` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_bankbranches
DROP TABLE IF EXISTS `pms_bankbranches`;
CREATE TABLE IF NOT EXISTS `pms_bankbranches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_bankbranches: ~6 rows (approximately)
/*!40000 ALTER TABLE `pms_bankbranches` DISABLE KEYS */;
INSERT INTO `pms_bankbranches` (`id`, `code`, `name`, `address`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'MANILA', 'Manila', NULL, NULL, '2018-06-09 04:45:58', '2018-06-09 04:46:24', NULL),
	(2, 'QC', 'Quezon City', NULL, NULL, '2018-06-09 04:46:14', '2018-06-09 04:46:14', NULL),
	(3, 'CALOOCAN', 'Caloocan', NULL, NULL, '2018-06-09 04:46:48', '2018-06-09 04:46:48', NULL),
	(4, 'PASAY', 'Pasay', NULL, NULL, '2018-06-09 04:47:16', '2018-06-09 04:47:16', NULL),
	(5, 'PC', 'PASIG KAPITOLYO', NULL, NULL, '2018-06-21 04:30:31', '2018-06-21 04:30:31', NULL),
	(6, 'MY', 'Mandaluyong', NULL, NULL, '2018-06-21 07:56:18', '2018-06-21 07:56:18', NULL);
/*!40000 ALTER TABLE `pms_bankbranches` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_banks
DROP TABLE IF EXISTS `pms_banks`;
CREATE TABLE IF NOT EXISTS `pms_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `branch_name` varchar(225) DEFAULT NULL,
  `bank_accountno` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_banks: ~5 rows (approximately)
/*!40000 ALTER TABLE `pms_banks` DISABLE KEYS */;
INSERT INTO `pms_banks` (`id`, `code`, `name`, `branch_name`, `bank_accountno`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'BDO', 'Banco De Oro', 'Manila', '234567', NULL, '2018-06-09 04:43:22', '2018-06-22 12:14:56', NULL),
	(2, 'BPI', 'Bank of Philippine Islands', 'Manila', '234567', NULL, '2018-06-09 04:44:42', '2018-06-22 12:14:47', NULL),
	(3, 'MB', 'Metro Bank', 'Manila', '345678', NULL, '2018-06-09 04:45:04', '2018-06-22 12:13:58', NULL),
	(4, 'BSP', 'Bangko Sentral ng Pilipinas', 'Manila', '456756789', NULL, '2018-06-09 04:45:43', '2018-06-22 12:14:35', NULL),
	(5, 'LBP', 'Land Bank of the Philippines', 'Manila', '35645678', NULL, '2018-06-21 04:30:06', '2018-06-22 12:14:19', NULL);
/*!40000 ALTER TABLE `pms_banks` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_beginning_balances
DROP TABLE IF EXISTS `pms_beginning_balances`;
CREATE TABLE IF NOT EXISTS `pms_beginning_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `premium_amount` decimal(9,2) DEFAULT NULL,
  `tax_witheld` decimal(9,2) DEFAULT NULL,
  `basic_pay` decimal(9,2) DEFAULT NULL,
  `overtime_pay` decimal(9,2) DEFAULT NULL,
  `thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `deminimis` decimal(9,2) DEFAULT NULL,
  `other_salaries` decimal(9,2) DEFAULT NULL,
  `taxable_basic_pay` decimal(9,2) DEFAULT NULL,
  `taxable_overtime_pay` decimal(9,2) DEFAULT NULL,
  `taxable_thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `taxable_other_salaries` decimal(9,2) DEFAULT NULL,
  `as_of_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_benefitinfo_transactions
DROP TABLE IF EXISTS `pms_benefitinfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_benefitinfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `benefit_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_benefits
DROP TABLE IF EXISTS `pms_benefits`;
CREATE TABLE IF NOT EXISTS `pms_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `de_minimis_type` varchar(225) DEFAULT NULL,
  `computation_type` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_benefits: ~12 rows (approximately)
/*!40000 ALTER TABLE `pms_benefits` DISABLE KEYS */;
INSERT INTO `pms_benefits` (`id`, `code`, `name`, `amount`, `de_minimis_type`, `computation_type`, `payroll_group`, `tax_type`, `itr_classification`, `alphalist_classification`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(4, 'PERA', 'PERA', NULL, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:00:00', '2018-06-21 04:00:00', NULL),
	(5, 'REP', 'REPRESENTATION ALLOWANCE', NULL, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:01:53', '2018-06-21 04:01:53', NULL),
	(6, 'TRANSPO', 'TRANSPORTATION ALLOWANCE', NULL, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:02:43', '2018-06-21 04:02:43', NULL),
	(9, 'PEI', 'PERFORMANCE ENHANCEMENT INCENTIVE', 5000.00, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-12 01:38:03', '2018-08-01 06:51:47', NULL),
	(10, 'CEA', 'COMMUNICATIONS EXPENSE ALLOTMENT', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-12 05:08:03', '2018-07-12 05:08:03', NULL),
	(11, 'EE', 'EXTRAORDINARY EXPENSE', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-13 08:20:04', '2018-07-13 08:20:04', NULL),
	(12, 'ME', 'MISCELLANEOUS EXPENSE', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-13 08:20:29', '2018-07-13 08:20:29', NULL),
	(15, 'CG', 'CASH GIFT', 5000.00, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-14 12:04:38', '2018-07-24 09:58:04', NULL),
	(16, 'YEB', 'Year End Bonus', 0.00, NULL, 'Based on Attendance', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-08-01 15:12:25', '2018-08-01 15:12:53', NULL),
	(17, 'MYB', 'Mid Year Bonus', NULL, NULL, 'Based on Attendance', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-08-01 15:13:13', '2018-08-01 15:13:13', NULL),
	(18, 'PBB', 'Performance Base Bonus', 5000.00, NULL, 'Based on Attendance', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-08-01 15:14:40', '2018-08-01 15:16:42', NULL),
	(19, 'TA', 'TRAVEL ALLOWANCE', 0.00, NULL, 'Fixed Monthly', NULL, 'Taxable', NULL, NULL, 'allowance', '2018-09-11 09:49:02', '2018-09-11 09:49:02', NULL);
/*!40000 ALTER TABLE `pms_benefits` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_benefitsinfo
DROP TABLE IF EXISTS `pms_benefitsinfo`;
CREATE TABLE IF NOT EXISTS `pms_benefitsinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `benefit_description` varchar(225) DEFAULT NULL,
  `benefit_amount` decimal(9,2) DEFAULT NULL,
  `benefit_pay_period` varchar(225) DEFAULT NULL,
  `benefit_pay_sub` varchar(225) DEFAULT NULL,
  `benefit_effectivity_date` date DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_benefitsinfo: ~87 rows (approximately)
/*!40000 ALTER TABLE `pms_benefitsinfo` DISABLE KEYS */;
INSERT INTO `pms_benefitsinfo` (`id`, `employee_id`, `employee_number`, `employeeinfo_id`, `benefit_id`, `benefit_description`, `benefit_amount`, `benefit_pay_period`, `benefit_pay_sub`, `benefit_effectivity_date`, `date_from`, `date_to`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 1, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 4, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 5, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 6, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 7, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 8, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 9, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 10, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 11, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 12, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 13, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 14, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 15, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 16, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 17, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 18, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 19, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 20, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 21, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 22, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 23, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 24, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 25, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 26, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 27, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 28, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, 29, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 30, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 31, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, 32, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, 33, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, 34, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, 35, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, 36, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, 37, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, 38, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, 39, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, 40, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, 41, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, 42, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, 43, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, 44, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, 45, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, 46, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, 47, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(48, 48, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, 49, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, 50, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, 51, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, 52, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, 53, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, 54, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, 55, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, 56, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, 57, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, 58, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, 59, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, 60, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, 61, NULL, NULL, 4, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, 1, NULL, NULL, 6, NULL, 10000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, 4, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, 10, NULL, NULL, 6, NULL, 9000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, 12, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(66, 16, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, 21, NULL, NULL, 6, NULL, 9000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, 27, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(69, 28, NULL, NULL, 6, NULL, 9000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(70, 31, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(71, 38, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, 40, NULL, NULL, 6, NULL, 900.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, 54, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, 58, NULL, NULL, 6, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(75, 1, NULL, NULL, 5, NULL, 10000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(76, 4, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(77, 10, NULL, NULL, 5, NULL, 9000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(78, 12, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, 16, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(80, 21, NULL, NULL, 5, NULL, 9000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, 27, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(82, 28, NULL, NULL, 5, NULL, 9000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(83, 31, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(84, 38, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(85, 40, NULL, NULL, 5, NULL, 9000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(86, 54, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(87, 58, NULL, NULL, 5, NULL, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, 3, NULL, 7, 4, NULL, 2000.00, NULL, NULL, '2018-11-09', NULL, NULL, '2018-11-10 11:05:07', '2018-11-10 11:05:07', NULL);
/*!40000 ALTER TABLE `pms_benefitsinfo` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_deductioninfo
DROP TABLE IF EXISTS `pms_deductioninfo`;
CREATE TABLE IF NOT EXISTS `pms_deductioninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `deduct_pay_period` varchar(225) DEFAULT NULL,
  `deduct_amount` decimal(9,2) DEFAULT NULL,
  `deduct_date_start` date DEFAULT NULL,
  `deduct_date_end` date DEFAULT NULL,
  `deduct_date_terminated` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_deductioninfo: ~158 rows (approximately)
/*!40000 ALTER TABLE `pms_deductioninfo` DISABLE KEYS */;
INSERT INTO `pms_deductioninfo` (`id`, `employeeinfo_id`, `employee_number`, `employee_id`, `deduction_id`, `deduct_pay_period`, `deduct_amount`, `deduct_date_start`, `deduct_date_end`, `deduct_date_terminated`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 24, NULL, 24, 1, NULL, 990.00, NULL, NULL, NULL, '2018-08-16 12:21:03', '2018-08-16 12:21:03', NULL),
	(2, 29, NULL, 28, 1, NULL, 1760.00, NULL, NULL, NULL, '2018-08-16 12:21:36', '2018-08-16 12:21:36', NULL),
	(3, 38, NULL, 22, 1, NULL, 495.00, NULL, NULL, NULL, '2018-08-16 12:21:56', '2018-08-16 12:21:56', NULL),
	(4, 15, NULL, 47, 1, NULL, 1760.00, NULL, NULL, NULL, '2018-08-16 12:22:17', '2018-08-16 12:22:17', NULL),
	(5, 53, NULL, 33, 1, NULL, 1760.00, NULL, NULL, NULL, '2018-08-16 12:22:32', '2018-08-16 12:22:32', NULL),
	(6, 54, NULL, 34, 1, NULL, 330.00, NULL, NULL, NULL, '2018-08-16 12:22:52', '2018-08-16 12:22:52', NULL),
	(7, 5, NULL, 1, 1, NULL, 1760.00, NULL, NULL, NULL, '2018-08-16 12:23:16', '2018-08-16 12:23:16', NULL),
	(8, NULL, NULL, 3, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, NULL, NULL, 10, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, NULL, NULL, 31, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, NULL, NULL, 41, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, NULL, NULL, 36, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, NULL, NULL, 13, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, NULL, NULL, 20, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, NULL, NULL, 21, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, NULL, NULL, 43, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, NULL, NULL, 32, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, NULL, NULL, 45, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, NULL, NULL, 12, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, NULL, NULL, 38, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, NULL, NULL, 11, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, NULL, NULL, 14, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, NULL, NULL, 47, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, NULL, NULL, 29, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, NULL, NULL, 48, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, NULL, NULL, 49, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, NULL, NULL, 50, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, NULL, NULL, 52, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, NULL, NULL, 7, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, NULL, NULL, 58, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, NULL, NULL, 30, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, NULL, NULL, 54, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, NULL, NULL, 19, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, NULL, NULL, 33, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, NULL, NULL, 9, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, NULL, NULL, 55, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, NULL, NULL, 35, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, NULL, NULL, 27, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, NULL, NULL, 39, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, NULL, NULL, 23, 2, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, NULL, NULL, 10, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, NULL, NULL, 31, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, NULL, NULL, 41, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, NULL, NULL, 36, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, NULL, NULL, 13, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, NULL, NULL, 20, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(48, NULL, NULL, 21, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, NULL, NULL, 43, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, NULL, NULL, 32, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, NULL, NULL, 45, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, NULL, NULL, 12, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, NULL, NULL, 38, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, NULL, NULL, 11, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, NULL, NULL, 14, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, NULL, NULL, 47, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, NULL, NULL, 29, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, NULL, NULL, 48, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, NULL, NULL, 49, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, NULL, NULL, 50, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, NULL, NULL, 52, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, NULL, NULL, 7, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, NULL, NULL, 58, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, NULL, NULL, 30, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, NULL, NULL, 54, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(66, NULL, NULL, 19, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, NULL, NULL, 33, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, NULL, NULL, 9, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(69, NULL, NULL, 55, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(70, NULL, NULL, 35, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(71, NULL, NULL, 27, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, NULL, NULL, 39, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, NULL, NULL, 23, 4, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, NULL, NULL, 24, 5, NULL, 2980.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(75, NULL, NULL, 10, 5, NULL, 2253.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(76, NULL, NULL, 31, 5, NULL, 10036.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(77, NULL, NULL, 25, 5, NULL, 1568.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(78, NULL, NULL, 41, 5, NULL, 1500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, NULL, NULL, 36, 5, NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(80, NULL, NULL, 13, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, NULL, NULL, 37, 5, NULL, 1200.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(82, NULL, NULL, 17, 5, NULL, 700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(83, NULL, NULL, 20, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(84, NULL, NULL, 21, 5, NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(85, NULL, NULL, 43, 5, NULL, 1500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(86, NULL, NULL, 32, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(87, NULL, NULL, 45, 5, NULL, 1300.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(88, NULL, NULL, 12, 5, NULL, 1656.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, NULL, NULL, 38, 5, NULL, 834.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(90, NULL, NULL, 57, 5, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(91, NULL, NULL, 22, 5, NULL, 695.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(92, NULL, NULL, 14, 5, NULL, 1656.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(93, NULL, NULL, 47, 5, NULL, 2204.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(94, NULL, NULL, 29, 5, NULL, 850.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(95, NULL, NULL, 48, 5, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(96, NULL, NULL, 49, 5, NULL, 1110.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(97, NULL, NULL, 50, 5, NULL, 4000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(98, NULL, NULL, 51, 5, NULL, 3226.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(99, NULL, NULL, 52, 5, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(100, NULL, NULL, 7, 5, NULL, 1200.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(101, NULL, NULL, 58, 5, NULL, 4600.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(102, NULL, NULL, 30, 5, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(103, NULL, NULL, 54, 5, NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(104, NULL, NULL, 26, 5, NULL, 2137.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(105, NULL, NULL, 8, 5, NULL, 6212.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(106, NULL, NULL, 19, 5, NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(107, NULL, NULL, 33, 5, NULL, 2500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(108, NULL, NULL, 9, 5, NULL, 1500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(109, NULL, NULL, 55, 5, NULL, 400.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(110, NULL, NULL, 35, 5, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(111, NULL, NULL, 27, 5, NULL, 2500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(112, NULL, NULL, 15, 5, NULL, 700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(113, NULL, NULL, 39, 5, NULL, 1700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(114, NULL, NULL, 23, 5, NULL, 3500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(115, NULL, NULL, 3, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(116, NULL, NULL, 24, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(117, NULL, NULL, 25, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(118, NULL, NULL, 41, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(119, NULL, NULL, 36, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(120, NULL, NULL, 13, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(121, NULL, NULL, 37, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(122, NULL, NULL, 17, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(123, NULL, NULL, 5, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(124, NULL, NULL, 20, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(125, NULL, NULL, 21, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(126, NULL, NULL, 18, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(127, NULL, NULL, 43, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(128, NULL, NULL, 32, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(129, NULL, NULL, 44, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(130, NULL, NULL, 6, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(131, NULL, NULL, 45, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(132, NULL, NULL, 38, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(133, NULL, NULL, 57, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(134, NULL, NULL, 46, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(135, NULL, NULL, 22, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(136, NULL, NULL, 14, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(137, NULL, NULL, 47, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(138, NULL, NULL, 29, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(139, NULL, NULL, 48, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(140, NULL, NULL, 49, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(141, NULL, NULL, 50, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(142, NULL, NULL, 51, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(143, NULL, NULL, 52, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(144, NULL, NULL, 53, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(145, NULL, NULL, 7, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(146, NULL, NULL, 30, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(147, NULL, NULL, 26, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(148, NULL, NULL, 8, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(149, NULL, NULL, 19, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(150, NULL, NULL, 33, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(151, NULL, NULL, 34, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(152, NULL, NULL, 9, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(153, NULL, NULL, 55, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(154, NULL, NULL, 35, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(155, NULL, NULL, 27, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(156, NULL, NULL, 15, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(157, NULL, NULL, 39, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(158, NULL, NULL, 23, 3, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(159, 7, NULL, 3, 4, NULL, 100.00, NULL, NULL, NULL, '2018-11-10 12:03:35', '2018-11-10 12:03:35', NULL);
/*!40000 ALTER TABLE `pms_deductioninfo` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_deductioninfo_transactions
DROP TABLE IF EXISTS `pms_deductioninfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_deductioninfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `deduction_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_deductions
DROP TABLE IF EXISTS `pms_deductions`;
CREATE TABLE IF NOT EXISTS `pms_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_deductions: ~8 rows (approximately)
/*!40000 ALTER TABLE `pms_deductions` DISABLE KEYS */;
INSERT INTO `pms_deductions` (`id`, `code`, `name`, `payroll_group`, `tax_type`, `itr_classification`, `alphalist_classification`, `amount`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'SSS CONT', 'SSS CONTRIBUTION', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-16 12:15:15', '2018-08-16 12:38:27', NULL),
	(2, 'MPCC', 'MULTI-PURPOSE COOPERATIVE CONTRIBUTION', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-16 12:16:30', '2018-08-16 12:17:27', NULL),
	(3, 'UD', 'UNION DUES', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-16 12:18:18', '2018-08-16 12:18:18', NULL),
	(4, 'WF', 'WELFARE FUND', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-16 15:55:43', '2018-08-16 15:55:43', NULL),
	(5, 'WD', 'WASSSLAI  Capital Contribution', NULL, 'Taxable', NULL, NULL, 0.00, NULL, '2018-08-17 04:08:59', '2018-08-17 04:08:59', NULL),
	(6, 'SB', 'SMART BILL', NULL, 'Taxable', NULL, NULL, 0.00, 'bill', '2018-09-11 14:00:39', '2018-09-11 14:00:39', NULL),
	(7, 'GB', 'GLOBE BILL', NULL, 'Taxable', NULL, NULL, 0.00, 'bill', '2018-09-11 14:00:58', '2018-09-11 14:00:58', NULL),
	(8, 'WASSLAI', 'WASSLAI', 'rata', 'Taxable', NULL, NULL, 0.00, NULL, '2018-10-06 08:19:17', '2018-10-06 08:19:17', NULL);
/*!40000 ALTER TABLE `pms_deductions` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_departments
DROP TABLE IF EXISTS `pms_departments`;
CREATE TABLE IF NOT EXISTS `pms_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_departments: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_departments` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_divisions
DROP TABLE IF EXISTS `pms_divisions`;
CREATE TABLE IF NOT EXISTS `pms_divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_divisions: ~35 rows (approximately)
/*!40000 ALTER TABLE `pms_divisions` DISABLE KEYS */;
INSERT INTO `pms_divisions` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, '', 'Office of Chair AMB', NULL, NULL, NULL, NULL),
	(2, '', 'Office of Comm JRB', NULL, NULL, NULL, NULL),
	(3, '', 'Office of Comm SAQ', NULL, NULL, NULL, NULL),
	(4, '', 'Office of the Director', NULL, NULL, NULL, NULL),
	(5, '', 'Agency-to-Agency', NULL, NULL, NULL, NULL),
	(6, '', 'Economic Investigation Division', NULL, NULL, NULL, NULL),
	(7, 'LID', 'Litigation Division', NULL, '2018-07-25 05:04:22', NULL, 2),
	(8, '', 'General Services Division', NULL, NULL, NULL, NULL),
	(9, '', 'Notification Division', NULL, NULL, NULL, NULL),
	(10, '', 'Monitoring and Investigation Division', NULL, NULL, NULL, NULL),
	(11, '', 'Corporate Planning and Management Division', NULL, NULL, NULL, NULL),
	(12, '', 'M & A Review Division', NULL, NULL, NULL, NULL),
	(13, 'IMD', 'Information Management Division', NULL, '2018-07-25 04:55:47', NULL, NULL),
	(14, '', 'Policy and Markets Division', NULL, NULL, NULL, NULL),
	(15, '', 'Human Resources Development Division', NULL, NULL, NULL, NULL),
	(16, '', 'Training and Advocacy Division', NULL, NULL, NULL, NULL),
	(17, '', 'CKMO', NULL, NULL, NULL, NULL),
	(18, '', 'Budget Division', NULL, NULL, NULL, NULL),
	(19, '', 'Internal Legal Services', NULL, NULL, NULL, NULL),
	(20, '', 'Office of Comm ERB', NULL, NULL, NULL, NULL),
	(21, '', 'Accounting Division', NULL, NULL, NULL, NULL),
	(22, '', 'Policy Research Division', NULL, NULL, NULL, NULL),
	(23, '', 'Information and Communications Technology Division', NULL, NULL, NULL, NULL),
	(24, '', 'Office of Comm ACA', NULL, NULL, NULL, NULL),
	(25, '', 'Office of the Executive Director', NULL, NULL, NULL, NULL),
	(26, '', 'ALO', NULL, NULL, NULL, NULL),
	(27, '', 'Office of the Chairman', NULL, NULL, NULL, NULL),
	(28, '', 'EO', NULL, NULL, NULL, NULL),
	(29, '', 'Training Division', NULL, NULL, NULL, NULL),
	(30, '', 'Policy Research and Development Division', NULL, NULL, NULL, NULL),
	(31, '', 'Adjudication Division', NULL, NULL, NULL, NULL),
	(32, '', 'Merger and Acquisition Division', NULL, NULL, NULL, NULL),
	(33, '', 'Knowledge Management Division', NULL, NULL, NULL, NULL),
	(34, '', 'Legal Division', NULL, NULL, NULL, NULL),
	(36, 'LD', 'Legal Division', '2018-07-25 05:06:09', '2018-07-25 05:06:09', 2, NULL);
/*!40000 ALTER TABLE `pms_divisions` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_employees
DROP TABLE IF EXISTS `pms_employees`;
CREATE TABLE IF NOT EXISTS `pms_employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `citizenship_country_id` int(11) DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` decimal(4,2) DEFAULT NULL,
  `weight` decimal(4,2) DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_date_issued` date DEFAULT NULL,
  `govt_issued_valid_until` date DEFAULT NULL,
  `agency_employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biometrics` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `with_setup` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e2e_db.pms_employees: ~77 rows (approximately)
/*!40000 ALTER TABLE `pms_employees` DISABLE KEYS */;
INSERT INTO `pms_employees` (`id`, `employee_number`, `company_id`, `branch_id`, `lastname`, `firstname`, `middlename`, `extension_name`, `nickname`, `contact_number`, `mobile_number`, `telephone_number`, `birthday`, `birth_place`, `email_address`, `gender`, `civil_status`, `citizenship_id`, `citizenship_country_id`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_issued_id_number`, `govt_issued_id_place`, `govt_issued_id_date_issued`, `govt_issued_valid_until`, `agency_employee_number`, `biometrics`, `house_number`, `street`, `subdivision`, `brgy`, `city_id`, `province_id`, `country_id`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_brgy`, `permanent_city_id`, `permanent_province_id`, `permanent_country_id`, `permanent_telephone_number`, `image_path`, `active`, `with_setup`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '124', NULL, NULL, 'Ty', 'Patrick Lester', 'Ng ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(2, '125', NULL, NULL, 'Magsambol', 'Darrell John', 'S.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(3, '105', NULL, NULL, 'Abaloyan', 'Leo James', 'B.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:42:56', NULL),
	(4, NULL, NULL, NULL, 'Zapanta  ', 'Karl Mark', 'N. ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(5, '126', NULL, NULL, 'Bueno', 'Ma. Karmela', 'B. ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(6, '130', NULL, NULL, 'Dans', 'Ana Maria', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(7, '22', NULL, NULL, 'Medina', 'Lorna', 'C.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(8, '122', NULL, NULL, 'Rodriguez', 'Renato', 'L.', ' Jr.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(9, '71', NULL, NULL, 'Talplacido', 'Lamberto', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(10, '50', NULL, NULL, 'Agustin', 'Evelyn', 'B.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(11, '31', NULL, NULL, 'Fernandez', 'Darren', 'DB.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(12, '30', NULL, NULL, 'Dominguez', 'Joel', 'A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(13, '23', NULL, NULL, 'Bagaporo', 'Isabel', 'V.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(14, '24', NULL, NULL, 'Jallorina', 'Clarissa', 'T.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(15, '109', NULL, NULL, 'Villanueva', 'Christine Agatha ', 'R.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(16, '133', NULL, NULL, 'Macatangay', 'Mario', 'G. ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(17, '117', NULL, NULL, 'Benito', 'Ma. Carla', 'N.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(18, '127', NULL, NULL, 'Co', 'Maria Eloisa', 'A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(19, '33', NULL, NULL, 'Romero', 'Emelita', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(20, '36', NULL, NULL, 'Castasus', 'Candelaria', 'P. ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(21, '59', NULL, NULL, 'Chuegan', 'Christopher', 'D.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-19 08:55:38', NULL),
	(22, '121', NULL, NULL, 'Ignacio', 'Justine Irish', 'C.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(23, '35', NULL, NULL, 'Zausa', 'Maria Sharlene', 'P.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(24, '115', NULL, NULL, 'Abia', 'Charmaine Shiela', 'R.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(25, '120', NULL, NULL, 'Antonio', 'Mark Billy', 'B.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(26, '114', NULL, NULL, 'Pena', 'Mary Ann Monic', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(27, '18', NULL, NULL, 'Valdez', 'Rosalinda', 'T.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(28, '128', NULL, NULL, 'Britanico', 'Lee Robert ', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(29, '28', NULL, NULL, 'Leido', 'Steve', 'P.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(30, '73', NULL, NULL, 'Nagtalon', 'Crisanto', 'G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(31, '38', NULL, NULL, 'Alegre', 'Rosendo', 'O.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(32, '29', NULL, NULL, 'Cordova', 'Melchor', 'S.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(33, '11', NULL, NULL, 'Ruelos', 'Vincent', 'A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(34, '132', NULL, NULL, 'Soliven', 'Kimberly', 'O.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(35, '85', NULL, NULL, 'Tolentino', 'Olivia', 'I.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(36, '37', NULL, NULL, 'Ayapana', 'Francis Eduardo', 'P.', 'Jr.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(37, '110', NULL, NULL, 'Balingit', 'Lorna', 'C.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(38, '40', NULL, NULL, 'Espallardo', 'Carlito', 'E.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(39, '51', NULL, NULL, 'Villarba', 'Ma. Victoria', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(40, '123', NULL, NULL, 'Isorena', 'Claudine', 'O.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(41, '69', NULL, NULL, 'Avila', 'Vicente', 'T.', 'III', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(43, '61', NULL, NULL, 'Coloso', 'Roberto ', 'U.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(44, '113', NULL, NULL, 'Dagsa', 'Joriel ', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(45, '72', NULL, NULL, 'Diala', 'Roberto', 'A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(46, NULL, NULL, NULL, 'Garcia', 'Ranjev', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(47, '21', NULL, NULL, 'Javier', 'Ramon ', 'A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(48, '70', NULL, NULL, 'Lumbres', 'Edgar', 'G. ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(49, '97', NULL, NULL, 'Luz', 'Klea Rejoice', 'D.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(50, '81', NULL, NULL, 'Makiling', 'Maria Theresa', 'V.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(51, '112', NULL, NULL, 'Maravilla', 'Sir Gil', 'P.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(52, '15', NULL, NULL, 'Marcelino', 'Christian Bernard', 'D.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(53, '118', NULL, NULL, 'Medina', 'John Oliver', 'S.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(54, '12', NULL, NULL, 'Octa', 'Virginia', 'V.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(55, '14', NULL, NULL, 'Tenorio', 'Marie Claudeline', 'M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(56, '129', NULL, NULL, 'Along', 'Angela Sigrid', 'J.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(57, '131', NULL, NULL, 'Gacula', 'Manuel', 'D.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(58, '80', NULL, NULL, 'Minas', 'Crescenciano', 'B.', 'Jr.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(59, '108', NULL, NULL, 'Rubio', 'Rozzan Rae', 'O.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(61, NULL, NULL, NULL, 'Abaoag', 'Ronnie', 'V.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:41:44', NULL),
	(62, NULL, NULL, NULL, 'Quinajon', 'Aljohn Deo', 'E.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(63, 'COS 03', NULL, NULL, 'Pura', 'Blessa Marie', 'T.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(64, 'COS 04', NULL, NULL, 'Aquino', 'Al', 'P.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:49:40', NULL),
	(65, 'COS 05', NULL, NULL, 'Aquino', 'Joshua Patrick', 'V.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:50:04', NULL),
	(66, 'COS 06', NULL, NULL, 'Guinaling', 'Sheena', 'L.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:51:38', NULL),
	(67, 'COS 07', NULL, NULL, 'Sausa', 'Sheryl', 'D.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(68, NULL, NULL, NULL, 'Cheng', 'Elena', 'T.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:50:34', NULL),
	(69, NULL, NULL, NULL, 'Dacanay', 'Crisanto', 'B.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:50:51', NULL),
	(70, NULL, NULL, NULL, 'Gravador', 'Daniel', 'C.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 2, NULL, NULL, '2018-08-16 08:51:17', NULL),
	(71, NULL, NULL, NULL, 'Tante', 'Verline Gay', 'B.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(72, NULL, NULL, NULL, 'Tejero', 'Sheryl', 'E.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(73, NULL, NULL, NULL, 'Torres ', 'Joshua Rene', 'B.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, NULL, NULL),
	(74, NULL, NULL, NULL, 'Gala', 'Charis Abigail', 'F', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 2, NULL, '2018-09-12 13:03:36', '2018-09-12 13:03:36', NULL),
	(75, NULL, NULL, NULL, 'Gala', 'Elisha Faith', 'F', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 2, NULL, '2018-09-12 13:04:08', '2018-09-12 13:04:08', NULL),
	(76, NULL, NULL, NULL, 'Gonzalo', 'Sheryl', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 2, NULL, '2018-09-12 13:04:44', '2018-09-12 13:04:44', NULL),
	(77, NULL, NULL, NULL, 'Napuran', 'Ella Marie', 'H', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 2, NULL, '2018-09-12 13:05:07', '2018-09-12 13:09:11', NULL),
	(78, NULL, NULL, NULL, 'Pamatmat', 'Rachelle', 'V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 2, NULL, '2018-09-12 13:05:36', '2018-09-12 13:05:36', NULL),
	(79, NULL, NULL, NULL, 'Valdez', 'Jerome', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 2, NULL, '2018-09-12 13:06:01', '2018-09-12 13:06:01', NULL);
/*!40000 ALTER TABLE `pms_employees` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_employee_information
DROP TABLE IF EXISTS `pms_employee_information`;
CREATE TABLE IF NOT EXISTS `pms_employee_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `hired_date` date DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `resigned_date` date DEFAULT NULL,
  `rehired_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `pay_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_rate_id` int(11) DEFAULT NULL,
  `work_schedule_id` int(11) DEFAULT NULL,
  `appointment_status_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e2e_db.pms_employee_information: ~77 rows (approximately)
/*!40000 ALTER TABLE `pms_employee_information` DISABLE KEYS */;
INSERT INTO `pms_employee_information` (`id`, `employee_id`, `employee_number`, `company_id`, `division_id`, `department_id`, `office_id`, `employee_status_id`, `position_id`, `hired_date`, `assumption_date`, `resigned_date`, `rehired_date`, `start_date`, `end_date`, `pay_period`, `pay_rate_id`, `work_schedule_id`, `appointment_status_id`, `designation_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 46, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 64, NULL, NULL, NULL, NULL, NULL, 10, 2, NULL, '2018-05-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:49:40', NULL),
	(3, 67, NULL, NULL, NULL, NULL, NULL, 10, 2, NULL, '2018-05-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 61, NULL, NULL, NULL, NULL, NULL, 10, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:42:10', NULL),
	(5, 7, NULL, NULL, NULL, NULL, NULL, 1, 3, NULL, '2015-07-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 44, NULL, NULL, NULL, NULL, NULL, 1, 4, NULL, '2014-09-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 56, NULL, NULL, NULL, NULL, NULL, 1, 5, NULL, '2017-09-22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 27, NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, '2004-02-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 1, NULL, NULL, NULL, NULL, NULL, 7, 7, NULL, '2017-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 65, NULL, NULL, NULL, NULL, NULL, 10, 8, NULL, '2018-05-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:50:04', NULL),
	(11, 8, NULL, NULL, NULL, NULL, NULL, 1, 9, NULL, '2017-11-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 40, NULL, NULL, NULL, NULL, NULL, 7, 10, NULL, '2016-03-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 28, NULL, NULL, NULL, NULL, NULL, 7, 11, NULL, '2017-09-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 10, NULL, NULL, NULL, NULL, NULL, 7, 12, NULL, '2017-08-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 58, NULL, NULL, NULL, NULL, NULL, 1, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 16, NULL, NULL, NULL, NULL, NULL, 1, 14, NULL, '2018-01-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 12, NULL, NULL, NULL, NULL, NULL, 1, 15, NULL, '2017-12-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 31, NULL, NULL, NULL, NULL, NULL, 1, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 4, NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, '2018-05-22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 54, NULL, NULL, NULL, NULL, NULL, 1, 18, NULL, '1997-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 38, NULL, NULL, NULL, NULL, NULL, 1, 19, NULL, '1997-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 21, NULL, NULL, NULL, NULL, NULL, 1, 20, NULL, '2013-02-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-19 08:55:38', NULL),
	(23, 9, NULL, NULL, NULL, NULL, NULL, 1, 21, NULL, '2015-07-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 30, NULL, NULL, NULL, NULL, NULL, 1, 22, NULL, '1998-11-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 41, NULL, NULL, NULL, NULL, NULL, 1, 22, NULL, '1998-01-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 45, NULL, NULL, NULL, NULL, NULL, 1, 22, NULL, '1999-03-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 48, NULL, NULL, NULL, NULL, NULL, 1, 22, NULL, '1998-02-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 53, NULL, NULL, NULL, NULL, NULL, 1, 22, NULL, '2015-07-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 50, NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, '2003-12-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 26, NULL, NULL, NULL, NULL, NULL, 1, 24, NULL, '2014-09-24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, 51, NULL, NULL, NULL, NULL, NULL, 1, 25, NULL, '2014-09-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, 2, NULL, NULL, NULL, NULL, NULL, 6, 26, NULL, '2017-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, 55, NULL, NULL, NULL, NULL, NULL, 1, 27, NULL, '1997-09-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, 3, NULL, NULL, NULL, NULL, NULL, 1, 28, NULL, '2014-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:42:57', NULL),
	(36, 13, NULL, NULL, NULL, NULL, NULL, 1, 29, NULL, '1997-09-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, 49, NULL, NULL, NULL, NULL, NULL, 1, 30, NULL, '2005-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, 66, NULL, NULL, NULL, NULL, NULL, 10, 31, NULL, '2018-05-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:51:38', NULL),
	(39, 5, NULL, NULL, NULL, NULL, NULL, 1, 32, NULL, '2017-08-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, 15, NULL, NULL, NULL, NULL, NULL, 1, 32, NULL, '2014-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, 17, NULL, NULL, NULL, NULL, NULL, 1, 32, NULL, '2015-07-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, 22, NULL, NULL, NULL, NULL, NULL, 1, 32, NULL, '2016-01-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, 34, NULL, NULL, NULL, NULL, NULL, 1, 32, NULL, '2017-12-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, 37, NULL, NULL, NULL, NULL, NULL, 1, 32, NULL, '2014-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, 59, NULL, NULL, NULL, NULL, NULL, 1, 32, NULL, '2014-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, 57, NULL, NULL, NULL, NULL, NULL, 1, 33, NULL, '2017-11-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, 62, NULL, NULL, NULL, NULL, NULL, 10, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, 36, NULL, NULL, NULL, NULL, NULL, 1, 35, NULL, '2003-02-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, 6, NULL, NULL, NULL, NULL, NULL, 1, 36, NULL, '2017-11-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, 52, NULL, NULL, NULL, NULL, NULL, 1, 37, NULL, '2016-01-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, 43, NULL, NULL, NULL, NULL, NULL, 1, 38, NULL, '1999-07-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, 14, NULL, NULL, NULL, NULL, NULL, 1, 39, NULL, '1997-09-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, 18, NULL, NULL, NULL, NULL, NULL, 1, 39, NULL, '2017-08-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, 20, NULL, NULL, NULL, NULL, NULL, 1, 39, NULL, '1997-01-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, 25, NULL, NULL, NULL, NULL, NULL, 1, 39, NULL, '2015-10-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, 33, NULL, NULL, NULL, NULL, NULL, 1, 39, NULL, '2014-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, 35, NULL, NULL, NULL, NULL, NULL, 1, 39, NULL, '2016-03-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, 39, NULL, NULL, NULL, NULL, NULL, 1, 39, NULL, '1998-01-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, 23, NULL, NULL, NULL, NULL, NULL, 1, 40, NULL, '1997-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, 24, NULL, NULL, NULL, NULL, NULL, 1, 41, NULL, '2014-09-24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, 32, NULL, NULL, NULL, NULL, NULL, 1, 41, NULL, '1997-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, 47, NULL, NULL, NULL, NULL, NULL, 1, 42, NULL, '2004-02-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, 19, NULL, NULL, NULL, NULL, NULL, 1, 43, NULL, '1997-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, 29, NULL, NULL, NULL, NULL, NULL, 1, 43, NULL, '1997-01-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(66, 11, NULL, NULL, NULL, NULL, NULL, 1, 43, NULL, '1997-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, 63, NULL, NULL, NULL, NULL, NULL, 10, 44, NULL, '2018-05-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, 68, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:50:34', NULL),
	(69, 69, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:50:51', NULL),
	(70, 70, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2018-08-16 08:51:18', NULL),
	(71, 71, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, 72, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, 73, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, 74, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2018-09-12 13:03:36', '2018-09-12 13:03:36', NULL),
	(75, 75, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2018-09-12 13:04:08', '2018-09-12 13:04:08', NULL),
	(76, 76, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2018-09-12 13:04:44', '2018-09-12 13:04:44', NULL),
	(77, 77, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2018-09-12 13:05:07', '2018-09-12 13:05:07', NULL),
	(78, 78, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2018-09-12 13:05:36', '2018-09-12 13:05:36', NULL),
	(79, 79, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2018-09-12 13:06:01', '2018-09-12 13:06:01', NULL);
/*!40000 ALTER TABLE `pms_employee_information` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_employee_status
DROP TABLE IF EXISTS `pms_employee_status`;
CREATE TABLE IF NOT EXISTS `pms_employee_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_employee_status: ~11 rows (approximately)
/*!40000 ALTER TABLE `pms_employee_status` DISABLE KEYS */;
INSERT INTO `pms_employee_status` (`id`, `code`, `name`, `category`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 'P', 'Permanent', 1, NULL, '2018-08-16 08:59:50', NULL, 2),
	(2, 'T', 'Temporary', 1, NULL, '2018-08-16 09:00:22', NULL, 2),
	(3, 'J.O', 'Job Order', 0, NULL, '2018-08-16 09:01:16', NULL, 2),
	(4, 'CON', 'Contractual', NULL, NULL, NULL, NULL, NULL),
	(5, 'CAS', 'Casual', 1, NULL, '2018-08-16 08:59:32', NULL, 2),
	(6, 'COT', 'Coterminous', 1, NULL, '2018-08-16 09:00:36', NULL, 2),
	(7, 'FT', 'Termer', NULL, NULL, '2018-08-10 02:27:44', NULL, 2),
	(8, 'PROB', 'Probationary', 1, NULL, '2018-08-16 09:00:49', NULL, 2),
	(9, 'R', 'Regular', 1, NULL, '2018-08-16 09:02:12', NULL, 2),
	(10, 'COS', 'Contract of Service', 0, NULL, '2018-08-16 09:01:36', NULL, 2),
	(11, 'GIP', 'GIP', 0, '2018-09-12 13:02:17', '2018-09-12 13:02:17', 2, NULL);
/*!40000 ALTER TABLE `pms_employee_status` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_gsispolicy
DROP TABLE IF EXISTS `pms_gsispolicy`;
CREATE TABLE IF NOT EXISTS `pms_gsispolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `computation` varchar(225) DEFAULT NULL,
  `ee_percentage` decimal(9,2) DEFAULT NULL,
  `er_percentage` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_gsispolicy: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_gsispolicy` DISABLE KEYS */;
INSERT INTO `pms_gsispolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `computation`, `ee_percentage`, `er_percentage`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'STANDARD POLICY', 'Monthly', 'Both', 'System Generated', 'Monthly Salary', '{\'EE\':\'.09\',\'ER\':\'.12\',\'EC\':\'.01\'}', 0.09, 0.12, NULL, '2018-06-09 10:25:03', '2018-06-21 03:14:34', NULL);
/*!40000 ALTER TABLE `pms_gsispolicy` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_jobgrade
DROP TABLE IF EXISTS `pms_jobgrade`;
CREATE TABLE IF NOT EXISTS `pms_jobgrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_grade` varchar(225) DEFAULT NULL,
  `step1` decimal(9,2) DEFAULT NULL,
  `step2` decimal(9,2) DEFAULT NULL,
  `step3` decimal(9,2) DEFAULT NULL,
  `step4` decimal(9,2) DEFAULT NULL,
  `step5` decimal(9,2) DEFAULT NULL,
  `step6` decimal(9,2) DEFAULT NULL,
  `step7` decimal(9,2) DEFAULT NULL,
  `step8` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_jobgrade: ~19 rows (approximately)
/*!40000 ALTER TABLE `pms_jobgrade` DISABLE KEYS */;
INSERT INTO `pms_jobgrade` (`id`, `job_grade`, `step1`, `step2`, `step3`, `step4`, `step5`, `step6`, `step7`, `step8`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, '1', 14087.00, 14242.00, 14399.00, 14543.00, 14688.00, 14835.00, 14897.00, 14930.00, NULL, NULL, NULL, NULL),
	(2, '2', 14936.00, 15160.00, 15387.00, 15618.00, 15853.00, 16090.00, 16332.00, 16577.00, NULL, NULL, NULL, NULL),
	(3, '3', 17975.00, 18245.00, 18518.00, 18796.00, 19078.00, 19364.00, 19665.00, 19949.00, NULL, NULL, NULL, NULL),
	(4, '4', 22532.00, 22870.00, 23213.00, 23561.00, 23915.00, 24273.00, 24637.00, 25007.00, NULL, NULL, NULL, NULL),
	(5, '5', 27414.00, 27825.00, 28243.00, 28666.00, 29096.00, 29504.00, 29917.00, 30335.00, NULL, NULL, NULL, NULL),
	(6, '6', 30351.00, 30806.00, 31268.00, 31737.00, 32213.00, 32697.00, 33187.00, 33685.00, NULL, NULL, NULL, NULL),
	(7, '7', 40169.00, 40772.00, 41383.00, 42004.00, 42634.00, 43273.00, 43923.00, 44581.00, NULL, NULL, NULL, NULL),
	(8, '8', 45269.00, 45948.00, 46637.00, 47337.00, 48047.00, 48768.00, 49499.00, 50242.00, NULL, NULL, NULL, NULL),
	(9, '9', 55600.00, 56434.00, 57224.00, 57968.00, 58664.00, 58694.00, 58723.00, 58752.00, NULL, NULL, NULL, NULL),
	(10, '10', 59100.00, 59987.00, 60886.00, 61800.00, 62727.00, 63667.00, 64622.00, 65592.00, NULL, NULL, NULL, NULL),
	(11, '11', 69499.00, 70194.00, 70896.00, 71605.00, 72321.00, 73044.00, 73775.00, 73811.00, NULL, NULL, NULL, NULL),
	(12, '12', 73900.00, 75009.00, 76134.00, 77276.00, 78435.00, 79611.00, 80805.00, 82018.00, NULL, NULL, NULL, NULL),
	(13, '13', 86899.00, 88202.00, 89526.00, 90868.00, 92231.00, 93615.00, 95019.00, 96444.00, NULL, NULL, NULL, NULL),
	(14, '14', 112500.00, 114188.00, 115900.00, 117523.00, 119168.00, 120717.00, 12216.00, 122227.00, NULL, NULL, NULL, NULL),
	(15, '15', 122500.00, 124338.00, 126203.00, 128096.00, 130017.00, 131967.00, 133947.00, 135956.00, NULL, NULL, NULL, NULL),
	(16, '16', 169300.00, 171840.00, 174417.00, 177033.00, 179689.00, 182384.00, 185120.00, 187897.00, NULL, NULL, NULL, NULL),
	(17, '17', 220200.00, 223503.00, 226856.00, 230258.00, 233712.00, 237218.00, 240776.00, 244388.00, NULL, NULL, NULL, NULL),
	(18, '18', 354312.00, 359627.00, 365021.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, '19', 531468.00, 539440.00, 547532.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_jobgrade` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_leave
DROP TABLE IF EXISTS `pms_leave`;
CREATE TABLE IF NOT EXISTS `pms_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `rata_id` int(11) DEFAULT NULL,
  `leave_type` varchar(225) DEFAULT NULL,
  `number_of_leave_field` int(11) DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_leave: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_leave` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_leave` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_leave_monetization_transactions
DROP TABLE IF EXISTS `pms_leave_monetization_transactions`;
CREATE TABLE IF NOT EXISTS `pms_leave_monetization_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `salary_grade_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `salary_amount` decimal(9,2) DEFAULT NULL,
  `net_amount` decimal(9,2) DEFAULT NULL,
  `factor_rate` decimal(9,2) DEFAULT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_loaninfo_transactions
DROP TABLE IF EXISTS `pms_loaninfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_loaninfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL,
  `loan_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;

-- Dumping structure for table e2e_db.pms_loans
DROP TABLE IF EXISTS `pms_loans`;
CREATE TABLE IF NOT EXISTS `pms_loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `loan_type` varchar(225) DEFAULT NULL,
  `category` varchar(225) DEFAULT NULL,
  `gsis_excel_col` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_loans: ~11 rows (approximately)
/*!40000 ALTER TABLE `pms_loans` DISABLE KEYS */;
INSERT INTO `pms_loans` (`id`, `code`, `name`, `loan_type`, `category`, `gsis_excel_col`, `amount`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'CS', 'CONSOLOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:32:01', '2018-06-19 17:32:01', NULL),
	(2, 'PL', 'POLICY LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:32:37', '2018-06-19 17:35:22', NULL),
	(3, 'EL', 'EMERGENCY/CALAMITY LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:33:50', '2018-06-19 17:33:50', NULL),
	(4, 'OL', 'OPTIONAL LIFE', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:34:11', '2018-06-19 17:34:11', NULL),
	(5, 'MPL', 'MULTI PURPOSE LOAN', 'Pagibig Loan', NULL, NULL, NULL, NULL, '2018-06-19 17:34:41', '2018-06-19 17:34:41', NULL),
	(6, 'CL', 'CALAMITY LOAN', 'Pagibig Loan', NULL, NULL, NULL, NULL, '2018-06-19 17:34:58', '2018-06-19 17:34:58', NULL),
	(7, 'MPLP', 'MULTI-PURPOSE COOPERATIVE LOAN', 'NULL', NULL, NULL, NULL, NULL, '2018-08-16 12:17:15', '2018-08-16 12:17:15', NULL),
	(8, 'LCH-DCS', 'LCH-DCS', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 03:50:45', '2018-08-17 03:50:45', NULL),
	(9, 'EA', 'EDUCATION LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 03:53:49', '2018-08-17 03:53:49', NULL),
	(10, 'WASSSLAI Loan', 'WASSSLAI Loan', NULL, NULL, NULL, NULL, NULL, '2018-08-17 04:09:54', '2018-08-17 04:09:54', NULL),
	(11, 'WFLOAN', 'WELFARE FUND LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 06:51:16', '2018-08-17 06:51:16', NULL);
/*!40000 ALTER TABLE `pms_loans` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_loansinfo
DROP TABLE IF EXISTS `pms_loansinfo`;
CREATE TABLE IF NOT EXISTS `pms_loansinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL,
  `loan_totalamount` decimal(9,2) DEFAULT NULL,
  `loan_totalbalance` decimal(9,2) DEFAULT NULL,
  `loan_amortization` decimal(9,2) DEFAULT NULL,
  `loan_pay_period` varchar(225) DEFAULT NULL,
  `loan_date_granted` date DEFAULT NULL,
  `loan_date_started` date DEFAULT NULL,
  `loan_date_end` date DEFAULT NULL,
  `loan_date_terminated` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_loansinfo: ~128 rows (approximately)
/*!40000 ALTER TABLE `pms_loansinfo` DISABLE KEYS */;
INSERT INTO `pms_loansinfo` (`id`, `employeeinfo_id`, `employee_number`, `employee_id`, `loan_id`, `loan_totalamount`, `loan_totalbalance`, `loan_amortization`, `loan_pay_period`, `loan_date_granted`, `loan_date_started`, `loan_date_end`, `loan_date_terminated`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, NULL, NULL, 10, 5, NULL, NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, NULL, NULL, 41, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, NULL, NULL, 36, 5, NULL, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, NULL, NULL, 13, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, NULL, NULL, 20, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, NULL, NULL, 21, 5, NULL, NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, NULL, NULL, 32, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, NULL, NULL, 45, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, NULL, NULL, 38, 5, NULL, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, NULL, NULL, 41, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, NULL, NULL, 47, 5, NULL, NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, NULL, NULL, 50, 5, NULL, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, NULL, NULL, 7, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, NULL, NULL, 54, 5, NULL, NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, NULL, NULL, 19, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, NULL, NULL, 9, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, NULL, NULL, 27, 5, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, NULL, NULL, 23, 5, NULL, NULL, 7000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, NULL, NULL, 31, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, NULL, NULL, 41, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, NULL, NULL, 36, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, NULL, NULL, 13, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, NULL, NULL, 20, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, NULL, NULL, 21, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, NULL, NULL, 32, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, NULL, NULL, 45, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, NULL, NULL, 38, 7, NULL, NULL, 2400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, NULL, NULL, 47, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, NULL, NULL, 49, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, NULL, NULL, 50, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, NULL, NULL, 7, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, NULL, NULL, 58, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, NULL, NULL, 30, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, NULL, NULL, 54, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, NULL, NULL, 19, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, NULL, NULL, 33, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, NULL, NULL, 27, 7, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, NULL, NULL, 39, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, NULL, NULL, 23, 7, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, NULL, NULL, 3, 1, NULL, NULL, 6176.50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, NULL, NULL, 24, 1, NULL, NULL, 3324.21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, NULL, NULL, 10, 1, NULL, NULL, 8985.68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, NULL, NULL, 41, 1, NULL, NULL, 2507.15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, NULL, NULL, 36, 1, NULL, NULL, 8887.88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, NULL, NULL, 13, 1, NULL, NULL, 8920.52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(48, NULL, NULL, 17, 1, NULL, NULL, 1462.65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, NULL, NULL, 20, 1, NULL, NULL, 8280.81, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, NULL, NULL, 43, 1, NULL, NULL, 5920.07, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, NULL, NULL, 32, 1, NULL, NULL, 11135.91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, NULL, NULL, 44, 1, NULL, NULL, 6980.84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, NULL, NULL, 45, 1, NULL, NULL, 2757.09, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, NULL, NULL, 12, 1, NULL, NULL, 10118.30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, NULL, NULL, 38, 1, NULL, NULL, 6688.71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, NULL, NULL, 14, 1, NULL, NULL, 6688.71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, NULL, NULL, 47, 1, NULL, NULL, 7950.09, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, NULL, NULL, 48, 1, NULL, NULL, 2757.09, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, NULL, NULL, 49, 1, NULL, NULL, 3201.57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, NULL, NULL, 16, 1, NULL, NULL, 18639.53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, NULL, NULL, 50, 1, NULL, NULL, 9240.43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, NULL, NULL, 52, 1, NULL, NULL, 6606.09, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, NULL, NULL, 53, 1, NULL, NULL, 1906.94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, NULL, NULL, 7, 1, NULL, NULL, 3500.18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, NULL, NULL, 30, 1, NULL, NULL, 2757.09, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(66, NULL, NULL, 54, 1, NULL, NULL, 12403.21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, NULL, NULL, 19, 1, NULL, NULL, 12991.90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, NULL, NULL, 33, 1, NULL, NULL, 3613.68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(69, NULL, NULL, 34, 1, NULL, NULL, 3030.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(70, NULL, NULL, 9, 1, NULL, NULL, 5031.98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(71, NULL, NULL, 55, 1, NULL, NULL, 6606.09, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, NULL, NULL, 1, 1, NULL, NULL, 12611.04, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, NULL, NULL, 27, 1, NULL, NULL, 1170.15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, NULL, NULL, 13, 2, NULL, NULL, 300.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(75, NULL, NULL, 20, 2, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(76, NULL, NULL, 43, 2, NULL, NULL, 200.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(77, NULL, NULL, 32, 2, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(78, NULL, NULL, 45, 2, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, NULL, NULL, 38, 2, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(80, NULL, NULL, 29, 2, NULL, NULL, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, NULL, NULL, 48, 2, NULL, NULL, 300.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(82, NULL, NULL, 49, 2, NULL, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(83, NULL, NULL, 50, 2, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(84, NULL, NULL, 52, 2, NULL, NULL, 300.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(85, NULL, NULL, 53, 2, NULL, NULL, 100.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(86, NULL, NULL, 58, 2, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(87, NULL, NULL, 30, 2, NULL, NULL, 600.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(88, NULL, NULL, 19, 2, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, NULL, NULL, 9, 2, NULL, NULL, 200.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(90, NULL, NULL, 55, 2, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(91, NULL, NULL, 35, 2, NULL, NULL, 150.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(92, NULL, NULL, 27, 2, NULL, NULL, 400.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(93, 20, NULL, 27, 3, 0.00, 0.00, 1311.12, NULL, NULL, NULL, NULL, NULL, '2018-08-17 03:47:54', '2018-08-17 03:47:54', NULL),
	(94, 20, NULL, 27, 8, 0.00, 0.00, 5480.98, NULL, NULL, NULL, NULL, NULL, '2018-08-17 03:51:24', '2018-08-17 03:51:24', NULL),
	(95, NULL, NULL, 13, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(96, NULL, NULL, 45, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(97, NULL, NULL, 48, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(98, NULL, NULL, 52, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(99, NULL, NULL, 53, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(100, NULL, NULL, 7, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(101, NULL, NULL, 30, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(102, NULL, NULL, 19, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(103, NULL, NULL, 9, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(104, NULL, NULL, 35, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(105, NULL, NULL, 27, 9, NULL, NULL, 216.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(107, NULL, NULL, 24, 10, NULL, NULL, 17539.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(108, NULL, NULL, 41, 10, NULL, NULL, 1797.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(109, NULL, NULL, 13, 10, NULL, NULL, 13296.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(110, NULL, NULL, 20, 10, NULL, NULL, 9972.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(111, NULL, NULL, 43, 10, NULL, NULL, 6919.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(112, NULL, NULL, 32, 10, NULL, NULL, 4970.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(113, NULL, NULL, 45, 10, NULL, NULL, 3291.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(114, NULL, NULL, 12, 10, NULL, NULL, 22212.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(115, NULL, NULL, 14, 10, NULL, NULL, 7232.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(116, NULL, NULL, 29, 10, NULL, NULL, 200.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(117, NULL, NULL, 48, 10, NULL, NULL, 3858.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(118, NULL, NULL, 49, 10, NULL, NULL, 3858.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(119, NULL, NULL, 50, 10, NULL, NULL, 2968.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(120, NULL, NULL, 52, 10, NULL, NULL, 6397.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(121, NULL, NULL, 58, 10, NULL, NULL, 6515.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(122, NULL, NULL, 30, 10, NULL, NULL, 1930.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(123, NULL, NULL, 54, 10, NULL, NULL, 35781.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(124, NULL, NULL, 19, 10, NULL, NULL, 3641.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(125, NULL, NULL, 9, 10, NULL, NULL, 2212.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(126, NULL, NULL, 55, 10, NULL, NULL, 200.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(127, NULL, NULL, 35, 10, NULL, NULL, 12637.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(128, NULL, NULL, 27, 10, NULL, NULL, 10287.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(129, NULL, NULL, 15, 10, NULL, NULL, 6014.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(130, NULL, NULL, 39, 10, NULL, NULL, 200.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(140, 7, NULL, 3, 7, 0.00, 0.00, 500.00, NULL, NULL, NULL, NULL, NULL, '2018-11-10 12:03:19', '2018-11-10 12:03:19', NULL);
/*!40000 ALTER TABLE `pms_loansinfo` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_longevitypay
DROP TABLE IF EXISTS `pms_longevitypay`;
CREATE TABLE IF NOT EXISTS `pms_longevitypay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `lp` int(11) DEFAULT NULL,
  `basic_pay_amount` decimal(9,2) DEFAULT NULL,
  `longevity_amount` decimal(9,2) DEFAULT NULL,
  `longevity_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_longevitypay: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_longevitypay` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_longevitypay` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_nonplantilla_employeeinfo
DROP TABLE IF EXISTS `pms_nonplantilla_employeeinfo`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_employeeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `tax_amount_one` decimal(9,2) DEFAULT NULL,
  `tax_amount_two` decimal(9,2) DEFAULT NULL,
  `taxpolicy_two_id` int(11) DEFAULT NULL,
  `atm_no` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_nonplantilla_employeeinfo: ~19 rows (approximately)
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` DISABLE KEYS */;
INSERT INTO `pms_nonplantilla_employeeinfo` (`id`, `employee_id`, `employee_number`, `bank_id`, `taxpolicy_id`, `tax_amount_one`, `tax_amount_two`, `taxpolicy_two_id`, `atm_no`, `daily_rate_amount`, `monthly_rate_amount`, `annual_rate_amount`, `overtime_balance_amount`, `tax_id_number`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 61, NULL, NULL, 10, 748.20, 205.35, 9, NULL, 1133.64, 24940.08, 299280.96, 149640.48, NULL, '2018-10-07 03:30:45', '2018-10-07 03:30:45', NULL, NULL),
	(2, 64, NULL, NULL, 10, 548.46, 0.00, NULL, NULL, 831.00, 18282.00, 219384.00, 109692.00, NULL, '2018-10-07 03:33:37', '2018-10-07 03:43:57', NULL, NULL),
	(3, 65, NULL, NULL, 10, 517.62, 0.00, NULL, NULL, 784.27, 17253.94, 207047.28, 103523.64, NULL, '2018-10-07 03:34:00', '2018-10-07 03:44:19', NULL, NULL),
	(4, 68, NULL, NULL, 10, 1202.55, 962.61, 9, NULL, 1822.05, 40085.10, 481021.20, 240510.60, NULL, '2018-10-07 03:34:30', '2018-10-07 03:44:40', NULL, NULL),
	(5, 69, NULL, NULL, 10, 517.62, 0.00, NULL, NULL, 784.27, 17253.94, 207047.28, 103523.64, NULL, '2018-10-07 03:35:01', '2018-10-07 03:35:01', NULL, NULL),
	(6, 74, NULL, NULL, 10, 198.00, 0.00, NULL, NULL, 300.00, 6600.00, 79200.00, 39600.00, NULL, '2018-10-07 03:35:27', '2018-10-07 03:35:27', NULL, NULL),
	(7, 75, NULL, NULL, 10, 198.00, 0.00, NULL, NULL, 300.00, 6600.00, 79200.00, 39600.00, NULL, '2018-10-07 03:35:39', '2018-10-07 03:35:39', NULL, NULL),
	(8, 76, NULL, NULL, 10, 198.00, 0.00, NULL, NULL, 300.00, 6600.00, 79200.00, 39600.00, NULL, '2018-10-07 03:36:04', '2018-10-07 03:36:04', NULL, NULL),
	(9, 77, NULL, NULL, 10, 198.00, 0.00, NULL, NULL, 300.00, 6600.00, 79200.00, 39600.00, NULL, '2018-10-07 03:36:26', '2018-10-07 03:36:26', NULL, NULL),
	(10, 70, NULL, NULL, 10, 930.30, 0.00, NULL, NULL, 1409.55, 31010.10, 372121.20, 186060.60, NULL, '2018-10-07 03:36:45', '2018-10-07 03:36:45', NULL, NULL),
	(11, 66, NULL, NULL, 10, 517.62, 0.00, NULL, NULL, 784.27, 17253.94, 207047.28, 103523.64, NULL, '2018-10-07 03:37:02', '2018-10-07 03:37:02', NULL, NULL),
	(12, 78, NULL, NULL, 10, 198.00, 0.00, NULL, NULL, 300.00, 6600.00, 79200.00, 39600.00, NULL, '2018-10-07 03:37:24', '2018-10-07 03:37:24', NULL, NULL),
	(13, 63, NULL, NULL, 10, 665.37, 67.30, 9, NULL, 1008.14, 22179.08, 266148.96, 133074.48, NULL, '2018-10-07 03:38:04', '2018-10-07 03:38:04', NULL, NULL),
	(14, 79, NULL, NULL, 10, 198.00, 0.00, NULL, NULL, 300.00, 6600.00, 79200.00, 39600.00, NULL, '2018-10-07 03:45:56', '2018-10-07 03:45:56', NULL, NULL),
	(15, 73, NULL, NULL, 10, 548.46, 0.00, NULL, NULL, 831.00, 18282.00, 219384.00, 109692.00, NULL, '2018-10-07 03:46:16', '2018-10-07 03:46:16', NULL, NULL),
	(16, 72, NULL, NULL, 10, 786.72, 269.55, 9, NULL, 1192.00, 26224.00, 314688.00, 157344.00, NULL, '2018-10-07 03:46:35', '2018-10-07 03:46:35', NULL, NULL),
	(17, 71, NULL, NULL, 10, 548.46, 0.00, NULL, NULL, 831.00, 18282.00, 219384.00, 109692.00, NULL, '2018-10-07 03:47:00', '2018-10-07 03:47:00', NULL, NULL),
	(18, 62, NULL, NULL, 10, 1202.55, 0.00, NULL, NULL, 1822.05, 40085.10, 481021.20, 240510.60, NULL, '2018-10-07 03:47:47', '2018-10-07 03:47:47', NULL, NULL),
	(19, 67, NULL, NULL, 10, 548.46, 0.00, NULL, NULL, 831.00, 18282.00, 219384.00, 109692.00, NULL, '2018-10-07 03:48:23', '2018-10-07 03:48:23', NULL, NULL);
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_nonplantilla_transactions
DROP TABLE IF EXISTS `pms_nonplantilla_transactions`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `salaryinfo_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `actual_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `total_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `actual_absences_amount` decimal(9,2) DEFAULT NULL,
  `adjust_absences_amount` decimal(9,2) DEFAULT NULL,
  `total_absences_amount` decimal(9,2) DEFAULT NULL,
  `actual_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `adjust_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `total_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `actual_undertime_amount` decimal(9,2) DEFAULT NULL,
  `adjust_undertime_amount` decimal(9,2) DEFAULT NULL,
  `total_undertime_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `basic_net_pay` decimal(9,2) DEFAULT NULL,
  `actual_contribution` decimal(9,2) DEFAULT NULL,
  `adjust_contribution` decimal(9,2) DEFAULT NULL,
  `total_contribution` decimal(9,2) DEFAULT NULL,
  `actual_loan` decimal(9,2) DEFAULT NULL,
  `adjust_loan` decimal(9,2) DEFAULT NULL,
  `total_loan` decimal(9,2) DEFAULT NULL,
  `actual_otherdeduct` decimal(9,2) DEFAULT NULL,
  `adjust_otherdeduct` decimal(9,2) DEFAULT NULL,
  `total_otherdeduct` decimal(9,2) DEFAULT NULL,
  `net_deduction` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_one` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_two` decimal(9,2) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `gross_pay` decimal(9,2) DEFAULT NULL,
  `gross_taxable_pay` decimal(9,2) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `actual_absences` int(11) DEFAULT NULL,
  `adjust_absences` int(11) DEFAULT NULL,
  `actual_tardiness` int(11) DEFAULT NULL,
  `adjust_tardiness` int(11) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `net_pay` decimal(9,2) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `sub_pay_period` varchar(225) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_offices
DROP TABLE IF EXISTS `pms_offices`;
CREATE TABLE IF NOT EXISTS `pms_offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_offices: ~19 rows (approximately)
/*!40000 ALTER TABLE `pms_offices` DISABLE KEYS */;
INSERT INTO `pms_offices` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, NULL, 'Office of the Chairman', NULL, NULL, NULL, NULL),
	(2, NULL, 'Administrative Office', NULL, NULL, NULL, NULL),
	(3, NULL, 'Finance and Planning Management Office', NULL, NULL, NULL, NULL),
	(4, NULL, 'Competition Enforcement Office', NULL, NULL, NULL, NULL),
	(5, NULL, 'Mergers and Acquisitions Office', NULL, NULL, NULL, NULL),
	(6, 'EO', 'Economics Office', NULL, '2018-07-25 05:15:56', NULL, 2),
	(7, NULL, 'Communications and Knowledge Management Office', NULL, NULL, NULL, NULL),
	(8, NULL, 'Legal Office', NULL, NULL, NULL, NULL),
	(9, NULL, 'Office of the Executive Director', NULL, NULL, NULL, NULL),
	(10, NULL, 'Policy Research and Knowledge Management Office', NULL, NULL, NULL, NULL),
	(11, NULL, 'Administrative and Legal Offfice', NULL, NULL, NULL, NULL),
	(12, NULL, 'Financial&#44; Planning and Management Office', NULL, NULL, NULL, NULL),
	(13, NULL, 'Corporate Planning and Management Division', NULL, NULL, NULL, NULL),
	(14, NULL, 'Office of Comm JRB', NULL, NULL, NULL, NULL),
	(15, NULL, 'Office of Comm SAQ', NULL, NULL, NULL, NULL),
	(16, NULL, 'Office of Comm ERB', NULL, NULL, NULL, NULL),
	(17, NULL, 'Office of Comm ACA', NULL, NULL, NULL, NULL),
	(18, NULL, 'Office of Commissioner Stella Luz A. Quimbo', NULL, NULL, NULL, NULL),
	(19, NULL, 'Office of Commissioner Amabelle C. Asuncion', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_offices` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_overtime
DROP TABLE IF EXISTS `pms_overtime`;
CREATE TABLE IF NOT EXISTS `pms_overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `previous_balance` decimal(9,2) DEFAULT NULL,
  `used_amount` decimal(9,2) DEFAULT NULL,
  `available_balance` decimal(9,2) DEFAULT NULL,
  `actual_regular_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_regular_overtime` decimal(9,2) DEFAULT NULL,
  `total_regular_amount` decimal(9,2) DEFAULT NULL,
  `actual_special_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_special_overtime` decimal(9,2) DEFAULT NULL,
  `total_special_amount` decimal(9,2) DEFAULT NULL,
  `actual_regular_holiday_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_regular_holiday_overtime` decimal(9,2) DEFAULT NULL,
  `total_regular_holiday_amount` decimal(9,2) DEFAULT NULL,
  `total_overtime_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `pay_period` varchar(50) DEFAULT NULL,
  `sub_pay_period` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_pagibigpolicy
DROP TABLE IF EXISTS `pms_pagibigpolicy`;
CREATE TABLE IF NOT EXISTS `pms_pagibigpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `value` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_pagibigpolicy: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_pagibigpolicy` DISABLE KEYS */;
INSERT INTO `pms_pagibigpolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `value`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'STANDARD POLICY', NULL, 'Both', 'System Generated', 'Monthly Salary', NULL, NULL, '2018-06-21 03:12:28', '2018-06-21 03:12:28', NULL);
/*!40000 ALTER TABLE `pms_pagibigpolicy` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_payroll_information
DROP TABLE IF EXISTS `pms_payroll_information`;
CREATE TABLE IF NOT EXISTS `pms_payroll_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bp_no` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `gsispolicy_id` int(11) DEFAULT NULL,
  `pagibigpolicy_id` int(11) DEFAULT NULL,
  `philhealthpolicy_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bankbranch_id` int(11) DEFAULT NULL,
  `wagestatus_id` int(11) DEFAULT NULL,
  `providentfund_id` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `pagibig_contribution` decimal(9,2) DEFAULT NULL,
  `er_pagibig_share` decimal(9,2) DEFAULT NULL,
  `philhealth_contribution` decimal(9,2) DEFAULT NULL,
  `er_philhealth_share` decimal(9,2) DEFAULT NULL,
  `gsis_contribution` decimal(9,2) DEFAULT NULL,
  `er_gsis_share` decimal(9,2) DEFAULT NULL,
  `tax_contribution` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `atm_no` varchar(225) DEFAULT NULL,
  `pagibig2` decimal(9,2) DEFAULT NULL,
  `pagibig_personal` decimal(9,2) DEFAULT NULL,
  `no_ofdays_inayear` int(11) DEFAULT NULL,
  `no_ofdays_inamonth` int(11) DEFAULT NULL,
  `total_hours_inaday` int(11) DEFAULT NULL,
  `tax_payperiod` varchar(255) DEFAULT NULL,
  `tax_bracket` varchar(255) DEFAULT NULL,
  `tax_bracket_amount` decimal(9,2) DEFAULT NULL,
  `tax_inexcess` decimal(9,2) DEFAULT NULL,
  `union_dues` decimal(9,2) DEFAULT NULL,
  `mid_year_bonus` tinyint(4) DEFAULT NULL,
  `year_end_bonus` tinyint(4) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_payroll_information: ~58 rows (approximately)
/*!40000 ALTER TABLE `pms_payroll_information` DISABLE KEYS */;
INSERT INTO `pms_payroll_information` (`id`, `bp_no`, `employee_id`, `employee_number`, `gsispolicy_id`, `pagibigpolicy_id`, `philhealthpolicy_id`, `taxpolicy_id`, `bank_id`, `bankbranch_id`, `wagestatus_id`, `providentfund_id`, `daily_rate_amount`, `monthly_rate_amount`, `annual_rate_amount`, `pagibig_contribution`, `er_pagibig_share`, `philhealth_contribution`, `er_philhealth_share`, `gsis_contribution`, `er_gsis_share`, `tax_contribution`, `overtime_balance_amount`, `atm_no`, `pagibig2`, `pagibig_personal`, `no_ofdays_inayear`, `no_ofdays_inamonth`, `total_hours_inaday`, `tax_payperiod`, `tax_bracket`, `tax_bracket_amount`, `tax_inexcess`, `union_dues`, `mid_year_bonus`, `year_end_bonus`, `tax_id_number`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, NULL, 5, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-08-17 05:55:42', '2018-08-19 08:52:09', NULL),
	(2, NULL, 43, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1551.36, 34130.00, 409560.00, 100.00, 100.00, 469.29, 469.29, 3071.70, 4095.60, 2699.25, 204780.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 797.00, NULL, NULL, NULL, NULL, '2018-08-17 05:57:31', '2018-08-17 05:57:31', NULL),
	(3, NULL, 2, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2982.00, 65604.00, 787248.00, 100.00, 100.00, 550.00, 550.00, 5904.36, 7872.48, 10567.75, 393624.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 32271.00, NULL, NULL, NULL, NULL, '2018-08-17 05:57:48', '2018-08-17 05:57:48', NULL),
	(4, NULL, 19, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2918.18, 64200.00, 770400.00, 100.00, 100.00, 550.00, 550.00, 5778.00, 7704.00, 10216.75, 385200.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 30867.00, NULL, NULL, NULL, NULL, '2018-08-17 05:58:04', '2018-08-17 05:58:04', NULL),
	(5, NULL, 55, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1318.64, 29010.00, 348120.00, 100.00, 100.00, 398.89, 398.89, 2610.90, 3481.20, 1635.40, 174060.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 8177.00, NULL, NULL, NULL, NULL, '2018-08-17 05:58:20', '2018-08-17 05:58:20', NULL),
	(6, NULL, 1, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5839.41, 128467.00, 1541604.00, 100.00, 100.00, 550.00, 550.00, 11562.03, 15416.04, 29373.33, 770802.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 61800.00, NULL, NULL, NULL, NULL, '2018-08-17 05:58:39', '2018-08-17 05:58:39', NULL),
	(7, NULL, 3, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1942.27, 42730.00, 512760.00, 100.00, 100.00, 550.00, 550.00, 3845.70, 5127.60, 4849.25, 256380.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 9397.00, NULL, NULL, NULL, NULL, '2018-08-17 05:58:55', '2018-08-17 05:58:55', NULL),
	(8, NULL, 24, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2708.95, 59597.00, 715164.00, 100.00, 100.00, 550.00, 550.00, 5363.73, 7151.64, 9066.00, 357582.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 26264.00, NULL, NULL, NULL, NULL, '2018-08-17 05:59:09', '2018-08-17 05:59:09', NULL),
	(9, NULL, 10, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5226.41, 114981.00, 1379772.00, 100.00, 100.00, 550.00, 550.00, 10348.29, 13797.72, 25327.53, 689886.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 48314.00, NULL, NULL, NULL, NULL, '2018-08-17 05:59:25', '2018-08-17 05:59:25', NULL),
	(10, NULL, 31, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4577.64, 100708.00, 1208496.00, 100.00, 100.00, 550.00, 550.00, 9063.72, 12084.96, 21045.63, 604248.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 34041.00, NULL, NULL, NULL, NULL, '2018-08-17 05:59:42', '2018-08-17 05:59:42', NULL),
	(11, NULL, 56, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2982.00, 65604.00, 787248.00, 100.00, 100.00, 550.00, 550.00, 5904.36, 7872.48, 10567.75, 393624.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 32271.00, NULL, NULL, NULL, NULL, '2018-08-17 05:59:58', '2018-08-17 05:59:58', NULL),
	(12, NULL, 25, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-08-17 06:00:17', '2018-08-17 06:00:17', NULL),
	(13, NULL, 41, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 722.50, 15895.00, 190740.00, 100.00, 100.00, 218.56, 218.56, 1430.55, 1907.40, 0.00, 95370.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15895.00, NULL, NULL, NULL, NULL, '2018-08-17 06:00:38', '2018-08-17 06:00:38', NULL),
	(14, NULL, 36, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2875.09, 63252.00, 759024.00, 100.00, 100.00, 550.00, 550.00, 5692.68, 7590.24, 9979.75, 379512.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 29919.00, NULL, NULL, NULL, NULL, '2018-08-17 06:00:56', '2018-08-17 06:00:56', NULL),
	(15, NULL, 13, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2388.36, 52544.00, 630528.00, 100.00, 100.00, 550.00, 550.00, 4728.96, 6305.28, 7302.75, 315264.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 19211.00, NULL, NULL, NULL, NULL, '2018-08-17 06:01:21', '2018-08-17 06:01:21', NULL),
	(16, NULL, 37, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 699.09, 15380.00, 184560.00, 100.00, 100.00, 211.47, 211.48, 1384.20, 1845.60, 0.00, 92280.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15380.00, NULL, NULL, NULL, NULL, '2018-08-17 06:01:37', '2018-08-17 06:01:37', NULL),
	(17, NULL, 17, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-08-17 06:01:51', '2018-08-17 06:01:51', NULL),
	(18, NULL, 28, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5226.41, 114981.00, 1379772.00, 100.00, 100.00, 550.00, 550.00, 10348.29, 13797.72, 25327.53, 689886.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 48314.00, NULL, NULL, NULL, NULL, '2018-08-17 06:02:10', '2018-08-17 06:02:10', NULL),
	(19, NULL, 20, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1860.00, 40920.00, 491040.00, 100.00, 100.00, 562.65, 562.65, 3682.80, 4910.40, 4396.75, 245520.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 7587.00, NULL, NULL, NULL, NULL, '2018-08-17 06:02:26', '2018-08-17 06:02:26', NULL),
	(20, NULL, 21, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4249.45, 93488.00, 1121856.00, 100.00, 100.00, 550.00, 550.00, 8413.92, 11218.56, 18879.63, 560928.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 26821.00, NULL, NULL, NULL, NULL, '2018-08-17 06:02:45', '2018-08-17 06:02:45', NULL),
	(21, NULL, 18, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-08-17 06:03:10', '2018-08-17 06:03:10', NULL),
	(22, NULL, 32, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2918.18, 64200.00, 770400.00, 100.00, 100.00, 550.00, 550.00, 5778.00, 7704.00, 10216.75, 385200.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 30867.00, NULL, NULL, NULL, NULL, '2018-08-17 06:03:30', '2018-08-17 06:03:30', NULL),
	(23, NULL, 44, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2424.59, 53341.00, 640092.00, 100.00, 100.00, 550.00, 550.00, 4800.69, 6400.92, 7502.00, 320046.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 20008.00, NULL, NULL, NULL, NULL, '2018-08-17 06:03:49', '2018-08-17 06:03:49', NULL),
	(24, NULL, 6, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-08-17 06:04:09', '2018-08-17 06:04:09', NULL),
	(25, NULL, 45, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 722.50, 15895.00, 190740.00, 100.00, 100.00, 218.56, 218.56, 1430.55, 1907.40, 0.00, 95370.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15895.00, NULL, NULL, NULL, NULL, '2018-08-17 06:04:33', '2018-08-17 06:04:33', NULL),
	(26, NULL, 12, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-08-17 06:04:53', '2018-08-17 06:04:53', NULL),
	(27, NULL, 38, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-08-17 06:05:36', '2018-08-17 06:05:36', NULL),
	(28, NULL, 11, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 8846.00, 352302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 25384.00, NULL, NULL, NULL, NULL, '2018-08-17 06:05:57', '2018-08-17 06:05:57', NULL),
	(29, NULL, 57, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2388.36, 52544.00, 630528.00, 100.00, 100.00, 550.00, 550.00, 4728.96, 6305.28, 7302.75, 315264.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 19211.00, NULL, NULL, NULL, NULL, '2018-08-17 06:06:13', '2018-08-17 06:06:13', NULL),
	(30, NULL, 46, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-08-17 06:06:33', '2018-08-17 06:06:33', NULL),
	(31, NULL, 22, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-08-17 06:07:03', '2018-08-17 06:07:03', NULL),
	(32, NULL, 40, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 5226.41, 114981.00, 1379772.00, 100.00, 100.00, 550.00, 550.00, 10348.29, 13797.72, 25327.53, 689886.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 48314.00, NULL, NULL, NULL, NULL, '2018-08-17 06:07:23', '2018-08-17 06:07:23', NULL),
	(33, NULL, 14, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1860.00, 40920.00, 491040.00, 100.00, 100.00, 562.65, 562.65, 3682.80, 4910.40, 4396.75, 245520.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 7587.00, NULL, NULL, NULL, NULL, '2018-08-17 06:07:41', '2018-08-17 06:07:41', NULL),
	(34, NULL, 47, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2832.64, 62318.00, 747816.00, 100.00, 100.00, 550.00, 550.00, 5608.62, 7478.16, 9746.25, 373908.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 28985.00, NULL, NULL, NULL, NULL, '2018-08-17 06:08:06', '2018-08-17 06:08:06', NULL),
	(35, NULL, 29, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2918.18, 64200.00, 770400.00, 100.00, 100.00, 550.00, 550.00, 5778.00, 7704.00, 10216.75, 385200.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 30867.00, NULL, NULL, NULL, NULL, '2018-08-17 06:08:23', '2018-08-17 06:08:23', NULL),
	(36, NULL, 48, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 722.50, 15895.00, 190740.00, 100.00, 100.00, 218.56, 218.56, 1430.55, 1907.40, 0.00, 95370.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15895.00, NULL, NULL, NULL, NULL, '2018-08-17 06:08:38', '2018-08-17 06:08:38', NULL),
	(37, NULL, 49, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1006.77, 22149.00, 265788.00, 100.00, 100.00, 304.55, 304.55, 1993.41, 2657.88, 263.20, 132894.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 1316.00, NULL, NULL, NULL, NULL, '2018-08-17 06:09:28', '2018-08-17 06:09:28', NULL),
	(38, NULL, 16, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-08-17 06:09:48', '2018-08-17 06:09:48', NULL),
	(39, NULL, 50, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2269.14, 49921.00, 599052.00, 100.00, 100.00, 550.00, 550.00, 4492.89, 5990.52, 6647.00, 299526.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 16588.00, NULL, NULL, NULL, NULL, '2018-08-17 06:10:07', '2018-08-17 06:10:07', NULL),
	(40, NULL, 51, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1318.64, 29010.00, 348120.00, 100.00, 100.00, 398.89, 398.89, 2610.90, 3481.20, 1635.40, 174060.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 8177.00, NULL, NULL, NULL, NULL, '2018-08-17 06:10:30', '2018-08-17 06:10:30', NULL),
	(41, NULL, 52, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-08-17 06:10:53', '2018-08-17 06:10:53', NULL),
	(42, NULL, 7, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 917.23, 20179.00, 242148.00, 100.00, 100.00, 277.46, 277.46, 1816.11, 2421.48, 0.00, 121074.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 20179.00, NULL, NULL, NULL, NULL, '2018-08-17 06:11:23', '2018-08-17 06:11:23', NULL),
	(43, NULL, 53, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-08-17 06:11:46', '2018-08-17 06:11:46', NULL),
	(44, NULL, 58, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 18465.63, 552648.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 25441.00, NULL, NULL, NULL, NULL, '2018-08-17 06:12:06', '2018-08-17 06:12:06', NULL),
	(45, NULL, 30, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-08-17 06:12:40', '2018-08-17 06:12:40', NULL),
	(46, NULL, 54, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4577.64, 100708.00, 1208496.00, 100.00, 100.00, 550.00, 550.00, 9063.72, 12084.96, 21045.63, 604248.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 34041.00, NULL, NULL, NULL, NULL, '2018-08-17 06:13:06', '2018-08-17 06:13:06', NULL),
	(47, NULL, 26, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1942.27, 42730.00, 512760.00, 100.00, 100.00, 550.00, 550.00, 3845.70, 5127.60, 4849.25, 256380.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 9397.00, NULL, NULL, NULL, NULL, '2018-08-17 06:13:29', '2018-08-17 06:13:29', NULL),
	(48, NULL, 8, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 678.20, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 3391.00, NULL, NULL, NULL, NULL, '2018-08-17 06:13:53', '2018-08-17 06:13:53', NULL),
	(49, NULL, 59, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-08-17 06:14:18', '2018-08-17 06:14:18', NULL),
	(50, NULL, 33, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1751.95, 38543.00, 462516.00, 100.00, 100.00, 529.97, 529.97, 3468.87, 4625.16, 3802.50, 231258.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 5210.00, NULL, NULL, NULL, NULL, '2018-08-17 06:14:39', '2018-08-17 06:14:39', NULL),
	(51, NULL, 34, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 693.36, 15254.00, 183048.00, 100.00, 100.00, 209.74, 209.74, 1372.86, 1830.48, 0.00, 91524.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15254.00, NULL, NULL, NULL, NULL, '2018-08-17 06:15:02', '2018-08-17 06:15:02', NULL),
	(52, NULL, 9, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 794.23, 17473.00, 209676.00, 100.00, 100.00, 240.25, 240.25, 1572.57, 2096.76, 0.00, 104838.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 17473.00, NULL, NULL, NULL, NULL, '2018-08-17 06:15:25', '2018-08-17 06:15:25', NULL),
	(53, NULL, 35, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 3688.00, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 4752.00, NULL, NULL, NULL, NULL, '2018-08-17 06:15:46', '2018-08-17 06:15:46', NULL),
	(54, NULL, 27, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2832.64, 62318.00, 747816.00, 100.00, 100.00, 550.00, 550.00, 5608.62, 7478.16, 9746.25, 373908.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 28985.00, NULL, NULL, NULL, NULL, '2018-08-17 06:16:02', '2018-08-17 06:16:02', NULL),
	(55, NULL, 15, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 699.09, 15380.00, 184560.00, 100.00, 100.00, 211.47, 211.48, 1384.20, 1845.60, 0.00, 92280.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 15380.00, NULL, NULL, NULL, NULL, '2018-08-17 06:16:19', '2018-08-17 06:16:19', NULL),
	(56, NULL, 39, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 1882.41, 41413.00, 496956.00, 100.00, 100.00, 569.43, 569.43, 3727.17, 4969.56, 4520.00, 248478.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 8080.00, NULL, NULL, NULL, NULL, '2018-08-17 06:16:37', '2018-08-17 06:16:37', NULL),
	(57, NULL, 4, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 4249.45, 93488.00, 1121856.00, 100.00, 100.00, 550.00, 550.00, 8413.92, 11218.56, 18879.63, 560928.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 26821.00, NULL, NULL, NULL, NULL, '2018-08-17 06:16:53', '2018-08-17 06:16:53', NULL),
	(58, NULL, 23, NULL, 1, 2, 2, 2, NULL, NULL, NULL, NULL, 2918.18, 64200.00, 770400.00, 100.00, 100.00, 550.00, 550.00, 5778.00, 7704.00, 10216.75, 385200.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 30867.00, NULL, NULL, NULL, NULL, '2018-08-17 06:17:09', '2018-08-17 06:17:09', NULL);
/*!40000 ALTER TABLE `pms_payroll_information` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_philhealthpolicy
DROP TABLE IF EXISTS `pms_philhealthpolicy`;
CREATE TABLE IF NOT EXISTS `pms_philhealthpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `below` decimal(9,4) DEFAULT NULL,
  `above` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_philhealthpolicy: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_philhealthpolicy` DISABLE KEYS */;
INSERT INTO `pms_philhealthpolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `below`, `above`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'STANDARD POLICY', 'Monthly', 'Both', 'System Generated', 'Monthly Salary', 0.0275, 1100.00, NULL, '2018-06-14 05:47:40', '2018-06-21 03:12:52', NULL);
/*!40000 ALTER TABLE `pms_philhealthpolicy` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_positionitem_setup
DROP TABLE IF EXISTS `pms_positionitem_setup`;
CREATE TABLE IF NOT EXISTS `pms_positionitem_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `positionitem_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `salarygrade_id` int(11) DEFAULT NULL,
  `jobgrade_id` int(11) DEFAULT NULL,
  `step_inc` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_positionitem_setup: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_positionitem_setup` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_positionitem_setup` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_positions
DROP TABLE IF EXISTS `pms_positions`;
CREATE TABLE IF NOT EXISTS `pms_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_positions: ~44 rows (approximately)
/*!40000 ALTER TABLE `pms_positions` DISABLE KEYS */;
INSERT INTO `pms_positions` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, NULL, 'Admin. Officer III', NULL, NULL, NULL, NULL),
	(2, NULL, 'Administration Services Assistant', NULL, NULL, NULL, NULL),
	(3, NULL, 'Artist-Illustrator A', NULL, NULL, NULL, NULL),
	(4, NULL, 'Chief Corporate Accountant', NULL, NULL, NULL, NULL),
	(5, NULL, 'Chief Corporate Attorney', NULL, NULL, NULL, NULL),
	(6, NULL, 'Chief Economist', NULL, NULL, NULL, NULL),
	(7, NULL, 'Chief Regulator', NULL, NULL, NULL, NULL),
	(8, NULL, 'Computer Operator I', NULL, NULL, NULL, NULL),
	(9, NULL, 'Data Analyst Controller', NULL, NULL, NULL, NULL),
	(10, NULL, 'Dep Adm - ALA', NULL, NULL, NULL, NULL),
	(11, NULL, 'Dep Adm - CSR', NULL, NULL, NULL, NULL),
	(12, NULL, 'Dep Adm - TRA', NULL, NULL, NULL, NULL),
	(13, NULL, 'Dept Manager - Legal', NULL, NULL, NULL, NULL),
	(14, NULL, 'Dept Manager-OMD', NULL, NULL, NULL, NULL),
	(15, NULL, 'Dept Manager-WQCD', NULL, NULL, NULL, NULL),
	(16, NULL, 'Dept. Manager-Compliants Monitoring', NULL, NULL, NULL, NULL),
	(17, NULL, 'Dept. Manager-PID', NULL, NULL, NULL, NULL),
	(18, NULL, 'Dept. Mgr.- Adm.', NULL, NULL, NULL, NULL),
	(19, NULL, 'Dept. Mgr.- MED', NULL, NULL, NULL, NULL),
	(20, NULL, 'Dept. Mgr.-TCMD', NULL, NULL, NULL, NULL),
	(21, NULL, 'Driver-Mechanic A', NULL, NULL, NULL, NULL),
	(22, NULL, 'Driver-Mechanic B', NULL, NULL, NULL, NULL),
	(23, NULL, 'Finance Officer B', NULL, NULL, NULL, NULL),
	(24, NULL, 'Finance Officer C', NULL, NULL, NULL, NULL),
	(25, NULL, 'Fiscal Examiner A', NULL, NULL, NULL, NULL),
	(26, NULL, 'Head Technical Assistant', NULL, NULL, NULL, NULL),
	(27, NULL, 'IRM Officer A', NULL, NULL, NULL, NULL),
	(28, NULL, 'MIS Design Specialist A', NULL, NULL, NULL, NULL),
	(29, NULL, 'Principal Chemist', NULL, NULL, NULL, NULL),
	(30, NULL, 'Records Officer D', NULL, NULL, NULL, NULL),
	(31, NULL, 'Secretary', NULL, NULL, NULL, NULL),
	(32, NULL, 'Secretary  B', NULL, NULL, NULL, NULL),
	(33, NULL, 'Senior Corporate Attorney', NULL, NULL, NULL, NULL),
	(34, NULL, 'Senior Water Utilities Regulation Officer', NULL, NULL, NULL, NULL),
	(35, NULL, 'Spvg. WURO', NULL, NULL, NULL, NULL),
	(36, NULL, 'Sr. Information Officer', NULL, NULL, NULL, NULL),
	(37, NULL, 'Sr. IRM Officer A', NULL, NULL, NULL, NULL),
	(38, NULL, 'Sr. Prop. Officer', NULL, NULL, NULL, NULL),
	(39, NULL, 'Sr. Water Utilities Reg. Off.', NULL, NULL, NULL, NULL),
	(40, NULL, 'Supvg. Financial Mgt. Specialist', NULL, NULL, NULL, NULL),
	(41, NULL, 'Supvg. Water Utilities Reg. Officer', NULL, NULL, NULL, NULL),
	(42, NULL, 'Technical Assistant', NULL, NULL, NULL, NULL),
	(43, NULL, 'Technical Assistant A', NULL, NULL, NULL, NULL),
	(44, NULL, 'Water Utilities Regulation Officer', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_positions` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_position_items
DROP TABLE IF EXISTS `pms_position_items`;
CREATE TABLE IF NOT EXISTS `pms_position_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `position_level_id` int(11) DEFAULT NULL,
  `position_classification_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_position_items: ~149 rows (approximately)
/*!40000 ALTER TABLE `pms_position_items` DISABLE KEYS */;
INSERT INTO `pms_position_items` (`id`, `code`, `name`, `position_id`, `position_level_id`, `position_classification_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 'PHCC-COCH-1-2016', 'PHCC-COCH-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'PHCC-COM-1-2016', 'PHCC-COM-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'PHCC-COM-2-2016', 'PHCC-COM-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'PHCC-COM-3-2016', 'PHCC-COM-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'PHCC-DRV2-2-2016', 'PHCC-DRV2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'PHCC-EXA4-1-2016', 'PHCC-EXA4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'PHCC-TRNSP4-1-2016', 'PHCC-TRNSP4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'PHCC-CFER1-1-2016', 'PHCC-CFER1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'PHCC-DIR4-1-2016', 'PHCC-DIR4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'PHCC-HEA-1-2016', 'PHCC-HEA-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'PHCC-DIR4-5-2016', 'PHCC-DIR4-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'PHCC-DIR4-2-2016', 'PHCC-DIR4-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'PHCC-EXA4-3-2016', 'PHCC-EXA4-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 'PHCC-PSEC2-3-2016', 'PHCC-PSEC2-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 'PHCC-DIR3-1-2016', 'PHCC-DIR3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 'PHCC-DIR4-4-2016', 'PHCC-DIR4-4-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 'PHCC-EXA3-1-2016', 'PHCC-EXA3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 'PHCC-CMPRO5-1-2016', 'PHCC-CMPRO5-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 'PHCC-PSEC2-1-2016', 'PHCC-PSEC2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 'PHCC-ATY3-4-2016', 'PHCC-ATY3-4-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 'PHCC-DRV2-9-2016', 'PHCC-DRV2-9-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 'PHCC-CADOF-1-2016', 'PHCC-CADOF-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 'PHCC-RO3-1-2016', 'PHCC-RO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 'PHCC-ATY2-9-2016', 'PHCC-ATY2-9-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 'PHCC-INVA3-1-2016', 'PHCC-INVA3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 'PHCC-LEA2-9-2016', 'PHCC-LEA2-9-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 'PHCC-ADO3-2-2016', 'PHCC-ADO3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 'PHCC-ATY3-7-2016', 'PHCC-ATY3-7-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, 'PHCC-INVA3-2-2016', 'PHCC-INVA3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 'PHCC-EXA3-3-2016', 'PHCC-EXA3-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 'PHCC-PLO3-1-2016', 'PHCC-PLO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, 'PHCC-INFO4-1-2016', 'PHCC-INFO4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, 'PHCC-CMPRO4-1-2016', 'PHCC-CMPRO4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, 'PHCC-CADOF-2-2016', 'PHCC-CADOF-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, 'PHCC-CMPRO3-5-2016', 'PHCC-CMPRO3-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, 'PHCC-DIR4-3-2016', 'PHCC-DIR4-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, 'PHCC-LEA2-5-2016', 'PHCC-LEA2-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, 'PHCC-ATY4-4-2016', 'PHCC-ATY4-4-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, 'PHCC-TRNSP3-2-2016', 'PHCC-TRNSP3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, 'PHCC-INFO5-1-2016', 'PHCC-INFO5-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, 'PHCC-ATY3-9-2016', 'PHCC-ATY3-9-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, 'PHCC-SADOF-3-2016', 'PHCC-SADOF-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, 'PHCC-ATY4-1-2016', 'PHCC-ATY4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, 'PHCC-INFO3-2-2016', 'PHCC-INFO3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, 'PHCC-DRV2-10-2016', 'PHCC-DRV2-10-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, 'PHCC-ATY5-4-2016', 'PHCC-ATY5-4-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, 'PHCC-DRV2-7-2016', 'PHCC-DRV2-7-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(48, 'PHCC-INVA5-1-2016', 'PHCC-INVA5-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, 'PHCC-HRMO3-1-2016', 'PHCC-HRMO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, 'PHCC-INFO2-2-2016', 'PHCC-INFO2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, 'PHCC-HRMO2-1-2016', 'PHCC-HRMO2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, 'PHCC-DRV2-8-2016', 'PHCC-DRV2-8-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, 'PHCC-ATY5-1-2016', 'PHCC-ATY5-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, 'PHCC-CMPRO3-3-2016', 'PHCC-CMPRO3-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, 'PHCC-INFO1-1-2016', 'PHCC-INFO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, 'PHCC-ECO2-1-2016', 'PHCC-ECO2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, 'PHCC-ATY5-3-2016', 'PHCC-ATY5-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, 'PHCC-ECO3-1-2016', 'PHCC-ECO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, 'PHCC-ATY4-3-2016', 'PHCC-ATY4-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, 'PHCC-SEC2-9-2016', 'PHCC-SEC2-9-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, 'PHCC-TRNSP2-2-2016', 'PHCC-TRNSP2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, 'PHCC-SADOF-2-2016', 'PHCC-SADOF-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, 'PHCC-HRMO2-2-2016', 'PHCC-HRMO2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, 'PHCC-A2-2-2016', 'PHCC-A2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, 'PHCC-INFO3-1-2016', 'PHCC-INFO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(66, 'PHCC-TRNSP2-1-2016', 'PHCC-TRNSP2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, 'PHCC-PSEC2-2-2016', 'PHCC-PSEC2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, 'PHCC-A3-1-2016', 'PHCC-A3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(69, 'PHCC-BUDO3-2-2016', 'PHCC-BUDO3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(70, 'PHCC-CASH3-1-2016', 'PHCC-CASH3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(71, 'PHCC-ATY5-2-2016', 'PHCC-ATY5-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, 'PHCC-CMPRO1-1-2016', 'PHCC-CMPRO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, 'PHCC-INVA2-2-2016', 'PHCC-INVA2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, 'PHCC-SU01-1-2016', 'PHCC-SU01-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(75, 'PHCC-SEC2-5-2016', 'PHCC-SEC2-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(76, 'PHCC-CASH2-1-2016', 'PHCC-CASH2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(77, 'PHCC-INVA2-1-2016', 'PHCC-INVA2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(78, 'PHCC-ADO2-1-2016', 'PHCC-ADO2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, 'PHCC-HRMO3-2-2016', 'PHCC-HRMO3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(80, 'PHCC-ATY3-10-2016', 'PHCC-ATY3-10-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, 'PHCC-ECO3-2-2016', 'PHCC-ECO3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(82, 'PHCC-A2-1-2016', 'PHCC-A2-1-2016', 35, NULL, NULL, NULL, '2018-07-25 09:34:03', NULL, 2),
	(83, 'PHCC-ITO1-1-2016', 'PHCC-ITO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(84, 'PHCC-COM-4-2016', 'PHCC-COM-4-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(85, 'PHCC-EXED-1-2016', 'PHCC-EXED-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(86, 'PHCC-LEA2-11-2016', 'PHCC-LEA2-11-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(87, 'PHCC-ATY2-10-2016', 'PHCC-ATY2-10-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(88, 'PHCC-INVA4-1-2016', 'PHCC-INVA4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, 'PHCC-ITO3-1-2016', 'PHCC-ITO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(90, 'PHCC-PLO5-1-2016', 'PHCC-PLO5-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(91, 'PHCC-BUDO2-1-2016', 'PHCC-BUDO2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(92, 'PHCC-PSEC2-4-2016', 'PHCC-PSEC2-4-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(93, 'PHCC-A3-2-2016', 'PHCC-A3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(94, 'PHCC-PLO4-1-2016', 'PHCC-PLO4-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(95, 'PHCC-TRNSP1-1-2016', 'PHCC-TRNSP1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(96, 'PHCC-PSEC2-5-2016', 'PHCC-PSEC2-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(97, 'PHCC-PLO2-1-2016', 'PHCC-PLO2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(98, 'PHCC-CMPRO2-2-2016', 'PHCC-CMPRO2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(99, 'PHCC-ATY2-1-2016', 'PHCC-ATY2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(100, 'PHCC-SEC2-1-2016', 'PHCC-SEC2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(101, 'PHCC-ATY3-1-2016', 'PHCC-ATY3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(102, 'PHCC-CMPRO3-1-2016', 'PHCC-CMPRO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(103, 'PHCC-ATY2-11-2016', 'PHCC-ATY2-11-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(104, 'PHCC-ATY3-5-2016', 'PHCC-ATY3-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(105, 'PHCC-ITO1-2-2016', 'PHCC-ITO1-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(106, 'PHCC-ATY2-2-2016', 'PHCC-ATY2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(107, 'PHCC-ITO2-1-2016', 'PHCC-ITO2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(108, 'PHCC-CMPRO3-2-2016', 'PHCC-CMPRO3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(109, 'PHCC-BUDO1-1-2016', 'PHCC-BUDO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(110, 'PHCC-EXA4-2-2016', 'PHCC-EXA4-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(111, 'PHCC-SADOF-1-2016', 'PHCC-SADOF-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(112, 'PHCC-CMPRO2-3-2016', 'PHCC-CMPRO2-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(113, 'PHCC-INFOSA2-1-2016', 'PHCC-INFOSA2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(114, 'PHCC-ADO3-1-2016', 'PHCC-ADO3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(115, 'PHCC-DRV2-6-2016', 'PHCC-DRV2-6-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(116, 'PHCC-DRV2-1-2016', 'PHCC-DRV2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(117, 'PHCC-INFOSA1-1-2016', 'PHCC-INFOSA1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(118, 'PHCC-HRMO1-1-2016', 'PHCC-HRMO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(119, 'PHCC-EXA3-5-2016', 'PHCC-EXA3-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(120, 'PHCC-CASH1-1-2016', 'PHCC-CASH1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(121, 'PHCC-EXA3-6-2016', 'PHCC-EXA3-6-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(122, 'PHCC-ATY3-3-2016', 'PHCC-ATY3-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(123, 'PHCC-PLO1-1-2016', 'PHCC-PLO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(124, 'PHCC-SU03-1-2016', 'PHCC-SU03-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(125, 'PHCC-ATY1-1-2016', 'PHCC-ATY1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(126, 'PHCC-PLO2-2-2016', 'PHCC-PLO2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(127, 'PHCC-LEA2-1-2016', 'PHCC-LEA2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(128, 'PHCC-PLO3-2-2016', 'PHCC-PLO3-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(129, 'PHCC-EXA2-1-2016', 'PHCC-EXA2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(130, 'PHCC-INVA1-1-2016', 'PHCC-INVA1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(131, 'PHCC-ATY2-5-2016', 'PHCC-ATY2-5-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(132, 'PHCC-ATY2-12-2016', 'PHCC-ATY2-12-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(133, 'PHCC-SEC2-6-2016', 'PHCC-SEC2-6-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(134, 'PHCC-DIR3-3-2016', 'PHCC-DIR3-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(135, 'PHCC-RO1-1-2016', 'PHCC-RO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(136, 'PHCC-LEA2-2-2016', 'PHCC-LEA2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(137, 'PHCC-SEC2-3-2016', 'PHCC-SEC2-3-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(138, 'PHCC-BS2-1-2016', 'PHCC-BS2-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(139, 'PHCC-ECO2-2-2016', 'PHCC-ECO2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(140, 'PHCC-ADO1-1-2016', 'PHCC-ADO1-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(141, 'PHCC-ATY1-2-2016', 'PHCC-ATY1-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(142, 'PHCC-TRNSP3-1-2016', 'PHCC-TRNSP3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(143, 'PHCC-BS3-1-2016', 'PHCC-BS3-1-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(144, 'PHCC-LEA2-10-2016', 'PHCC-LEA2-10-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(145, 'PHCC-LEA2-12-2016', 'PHCC-LEA2-12-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(146, 'PHCC-ADO2-4-2016', 'PHCC-ADO2-4-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(147, 'PHCC-ADO2-2-2016', 'PHCC-ADO2-2-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(148, 'PHCC-LEA2-13-2016', 'PHCC-LEA2-13-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(149, 'SAMPLE', 'SAMPLE', 35, NULL, NULL, '2018-07-25 09:34:21', '2018-07-25 09:34:21', 2, NULL);
/*!40000 ALTER TABLE `pms_position_items` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_previous_employer
DROP TABLE IF EXISTS `pms_previous_employer`;
CREATE TABLE IF NOT EXISTS `pms_previous_employer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `deminimis_amount` decimal(9,2) DEFAULT NULL,
  `tax_witheld` decimal(9,2) DEFAULT NULL,
  `premium_amount` decimal(9,2) DEFAULT NULL,
  `taxable_basic_pay` decimal(9,2) DEFAULT NULL,
  `taxable_thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `taxable_allowances` decimal(9,2) DEFAULT NULL,
  `as_of_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_providentfund
DROP TABLE IF EXISTS `pms_providentfund`;
CREATE TABLE IF NOT EXISTS `pms_providentfund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `ee_share` decimal(9,2) DEFAULT NULL,
  `er_share` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_providentfund: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_providentfund` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_providentfund` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_rata
DROP TABLE IF EXISTS `pms_rata`;
CREATE TABLE IF NOT EXISTS `pms_rata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `number_of_actual_work` int(11) DEFAULT NULL,
  `number_of_leave_field` int(11) DEFAULT NULL,
  `number_of_work_days` int(11) DEFAULT NULL,
  `number_of_used_vehicles` int(11) DEFAULT NULL,
  `percentage_of_rata` varchar(225) DEFAULT NULL,
  `percentage_of_rata_value` decimal(9,2) DEFAULT NULL,
  `transportation_amount` decimal(9,2) DEFAULT NULL,
  `representation_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_rata_deductions
DROP TABLE IF EXISTS `pms_rata_deductions`;
CREATE TABLE IF NOT EXISTS `pms_rata_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `rata_id` int(11) DEFAULT NULL,
  `tax_deduction` decimal(9,2) DEFAULT NULL,
  `ra_deduction` decimal(9,2) DEFAULT NULL,
  `ta_deduction` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_rata_deductions: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_rata_deductions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_rata_deductions` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_rates
DROP TABLE IF EXISTS `pms_rates`;
CREATE TABLE IF NOT EXISTS `pms_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_rates: ~7 rows (approximately)
/*!40000 ALTER TABLE `pms_rates` DISABLE KEYS */;
INSERT INTO `pms_rates` (`id`, `rate`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 0.15, 'hp', 2, 2, '2018-09-06 11:25:40', '2018-09-06 16:23:15'),
	(2, 0.50, 'hp', 2, NULL, '2018-09-06 11:25:56', '2018-09-06 11:25:56'),
	(3, 0.23, 'hp', 2, NULL, '2018-09-06 16:23:23', '2018-09-06 16:23:23'),
	(4, 0.30, 'hp', 2, NULL, '2018-09-06 16:23:34', '2018-09-06 16:23:34'),
	(15, 1.00, 'travelrate', 2, NULL, '2018-09-10 10:52:22', '2018-09-10 10:52:22'),
	(16, 0.75, 'travelrate', 2, NULL, '2018-09-10 10:53:41', '2018-09-10 10:53:41'),
	(17, 0.50, 'travelrate', 2, NULL, '2018-09-10 10:53:56', '2018-09-10 10:53:56');
/*!40000 ALTER TABLE `pms_rates` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_salarygrade
DROP TABLE IF EXISTS `pms_salarygrade`;
CREATE TABLE IF NOT EXISTS `pms_salarygrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salary_grade` varchar(225) DEFAULT NULL,
  `step1` decimal(9,2) DEFAULT NULL,
  `step2` decimal(9,2) DEFAULT NULL,
  `step3` decimal(9,2) DEFAULT NULL,
  `step4` decimal(9,2) DEFAULT NULL,
  `step5` decimal(9,2) DEFAULT NULL,
  `step6` decimal(9,2) DEFAULT NULL,
  `step7` decimal(9,2) DEFAULT NULL,
  `step8` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_salarygrade: ~32 rows (approximately)
/*!40000 ALTER TABLE `pms_salarygrade` DISABLE KEYS */;
INSERT INTO `pms_salarygrade` (`id`, `salary_grade`, `step1`, `step2`, `step3`, `step4`, `step5`, `step6`, `step7`, `step8`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, '1', 10510.00, 10602.00, 10695.00, 10789.00, 10884.00, 10982.00, 11076.00, 11173.00, NULL, NULL, NULL, NULL),
	(2, '2', 11200.00, 11293.00, 11386.00, 11480.00, 11575.00, 11671.00, 11767.00, 11864.00, NULL, NULL, NULL, NULL),
	(3, '3', 11914.00, 12013.00, 12112.00, 11212.00, 12313.00, 12414.00, 12517.00, 12620.00, NULL, NULL, NULL, NULL),
	(4, '4', 12674.00, 12778.00, 12884.00, 12990.00, 13097.00, 13206.00, 13315.00, 13424.00, NULL, NULL, NULL, NULL),
	(5, '5', 13481.00, 13606.00, 13705.00, 13818.00, 13932.00, 14047.00, 14163.00, 14280.00, NULL, NULL, NULL, NULL),
	(6, '6', 14340.00, 14459.00, 14578.00, 14699.00, 14820.00, 14942.00, 15066.00, 15190.00, NULL, NULL, NULL, NULL),
	(7, '7', 15254.00, 15380.00, 15507.00, 15635.00, 15765.00, 15895.00, 16026.00, 16158.00, NULL, NULL, NULL, NULL),
	(8, '9', 17473.00, 17627.00, 17781.00, 17937.00, 18095.00, 18253.00, 18413.00, 18575.00, NULL, NULL, NULL, NULL),
	(9, '10', 18718.00, 18883.00, 19048.00, 19215.00, 19384.00, 19567.00, 19725.00, 19898.00, NULL, NULL, NULL, NULL),
	(10, '11', 20179.00, 20437.00, 20698.00, 20963.00, 21231.00, 21502.00, 21777.00, 22055.00, NULL, NULL, NULL, NULL),
	(11, '12', 22149.00, 22410.00, 22674.00, 22942.00, 23212.00, 23486.00, 23763.00, 24043.00, NULL, NULL, NULL, NULL),
	(12, '13', 24224.00, 24510.00, 24799.00, 25091.00, 25387.00, 25686.00, 25989.00, 26296.00, NULL, NULL, NULL, NULL),
	(13, '14', 26494.00, 26806.00, 27122.00, 27442.00, 27766.00, 28093.00, 28424.00, 28759.00, NULL, NULL, NULL, NULL),
	(14, '15', 29010.00, 29359.00, 29713.00, 30071.00, 30432.00, 30799.00, 31170.00, 31545.00, NULL, NULL, NULL, NULL),
	(15, '16', 31765.00, 32147.00, 32535.00, 32926.00, 33323.00, 33724.00, 34130.00, 34541.00, NULL, NULL, NULL, NULL),
	(16, '17', 34781.00, 35201.00, 35624.00, 36053.00, 36487.00, 36927.00, 37371.00, 37821.00, NULL, NULL, NULL, NULL),
	(17, '18', 38085.00, 38543.00, 39007.00, 39477.00, 39952.00, 40433.00, 40920.00, 41413.00, NULL, NULL, NULL, NULL),
	(18, '19', 42099.00, 42730.00, 43371.00, 44020.00, 44680.00, 45350.00, 46030.00, 46720.00, NULL, NULL, NULL, NULL),
	(19, '20', 47037.00, 47742.00, 48457.00, 49184.00, 49921.00, 50669.00, 57460.00, 52199.00, NULL, NULL, NULL, NULL),
	(20, '21', 52544.00, 53341.00, 54141.00, 54952.00, 55776.00, 56612.00, 57460.00, 58322.00, NULL, NULL, NULL, NULL),
	(21, '22', 58717.00, 59597.00, 60491.00, 61397.00, 62318.00, 63252.00, 64200.00, 65162.00, NULL, NULL, NULL, NULL),
	(22, '23', 65604.00, 66587.00, 67585.00, 68598.00, 69627.00, 70670.00, 71730.00, 72805.00, NULL, NULL, NULL, NULL),
	(23, '24', 73229.00, 74397.00, 75512.00, 76644.00, 77793.00, 78959.00, 80143.00, 81344.00, NULL, NULL, NULL, NULL),
	(24, '25', 82439.00, 83674.00, 84928.00, 86201.00, 87493.00, 88805.00, 90136.00, 91487.00, NULL, NULL, NULL, NULL),
	(25, '26', 92108.00, 93488.00, 94889.00, 96312.00, 97775.00, 99221.00, 100708.00, 102217.00, NULL, NULL, NULL, NULL),
	(26, '27', 102910.00, 104453.00, 106019.00, 107608.00, 109221.00, 110858.00, 112519.00, 114210.00, NULL, NULL, NULL, NULL),
	(27, '28', 114981.00, 116704.00, 118453.00, 120229.00, 122031.00, 123860.00, 125716.00, 127601.00, NULL, NULL, NULL, NULL),
	(28, '29', 128467.00, 130392.00, 132346.00, 134330.00, 136343.00, 138387.00, 140461.00, 142566.00, NULL, NULL, NULL, NULL),
	(29, '30', 143534.00, 145685.00, 147869.00, 150085.00, 152335.00, 154618.00, 156935.00, 159228.00, NULL, NULL, NULL, NULL),
	(30, '31', 198168.00, 201615.00, 205121.00, 208689.00, 212318.00, 216011.00, 219768.00, 223590.00, NULL, NULL, NULL, NULL),
	(31, '32', 233857.00, 238035.00, 242288.00, 246618.00, 251024.00, 255509.00, 260074.00, 264721.00, NULL, NULL, NULL, NULL),
	(32, '33', 289401.00, 298083.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_salarygrade` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_salaryinfo
DROP TABLE IF EXISTS `pms_salaryinfo`;
CREATE TABLE IF NOT EXISTS `pms_salaryinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `salarygrade_id` int(11) DEFAULT NULL,
  `jobgrade_id` int(11) DEFAULT NULL,
  `positionitem_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `step_inc` varchar(225) DEFAULT NULL,
  `salary_description` varchar(225) DEFAULT NULL,
  `salary_old_rate` decimal(9,2) DEFAULT NULL,
  `salary_adjustment` decimal(9,2) DEFAULT NULL,
  `salary_new_rate` decimal(9,2) DEFAULT NULL,
  `salary_effectivity_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_salaryinfo: ~58 rows (approximately)
/*!40000 ALTER TABLE `pms_salaryinfo` DISABLE KEYS */;
INSERT INTO `pms_salaryinfo` (`id`, `employee_id`, `employee_number`, `salarygrade_id`, `jobgrade_id`, `positionitem_id`, `position_id`, `step_inc`, `salary_description`, `salary_old_rate`, `salary_adjustment`, `salary_new_rate`, `salary_effectivity_date`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 1, NULL, 28, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 128467.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 04:49:20', NULL),
	(2, 2, NULL, 22, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 65604.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:05:02', NULL),
	(3, 3, NULL, 18, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 42730.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:06:53', NULL),
	(4, 4, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 93488.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:07:13', NULL),
	(5, 5, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15254.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:30:33', NULL),
	(6, 6, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 38085.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:07:43', NULL),
	(7, 7, NULL, 10, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 20179.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:08:02', NULL),
	(8, 8, NULL, 12, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 24224.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:08:15', NULL),
	(9, 9, NULL, 8, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 17473.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:08:33', NULL),
	(10, 10, NULL, 27, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 114981.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:08:56', NULL),
	(11, 11, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 58717.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:09:09', NULL),
	(12, 12, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 92108.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:09:21', NULL),
	(13, 13, NULL, 20, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 52544.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:35:21', NULL),
	(14, 14, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 40920.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:10:55', NULL),
	(15, 15, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15380.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:11:15', NULL),
	(16, 16, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 92108.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:11:35', NULL),
	(17, 17, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15254.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:35:53', NULL),
	(18, 18, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 38085.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:12:01', NULL),
	(19, 19, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 64200.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:05:53', NULL),
	(20, 20, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 40920.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:12:35', NULL),
	(21, 21, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 93488.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:12:55', NULL),
	(22, 22, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15254.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:38:06', NULL),
	(23, 23, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 64200.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:13:27', NULL),
	(24, 24, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 59597.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:13:48', NULL),
	(25, 25, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 38085.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:14:02', NULL),
	(26, 26, NULL, 18, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 42730.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:14:22', NULL),
	(27, 27, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 62318.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:14:45', NULL),
	(28, 28, NULL, 27, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 114981.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:15:00', NULL),
	(29, 29, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 64200.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:15:27', NULL),
	(30, 30, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15254.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:40:23', NULL),
	(31, 31, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 100708.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:15:47', NULL),
	(32, 32, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 64200.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:16:03', NULL),
	(33, 33, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 38543.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:16:34', NULL),
	(34, 34, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15254.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:41:34', NULL),
	(35, 35, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 38085.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:16:55', NULL),
	(36, 36, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 63252.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:17:26', NULL),
	(37, 37, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15380.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:17:44', NULL),
	(38, 38, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 92108.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:17:58', NULL),
	(39, 39, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 41413.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:18:20', NULL),
	(40, 40, NULL, 27, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 114981.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:18:32', NULL),
	(41, 41, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15895.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:19:03', NULL),
	(43, 43, NULL, 15, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 34130.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:02:14', NULL),
	(44, 44, NULL, 20, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 53341.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:25:30', NULL),
	(45, 45, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15895.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:25:50', NULL),
	(46, 46, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 38085.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:26:03', NULL),
	(47, 47, NULL, 21, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 62318.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:26:31', NULL),
	(48, 48, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15895.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:26:45', NULL),
	(49, 49, NULL, 11, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 22149.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:27:00', NULL),
	(50, 50, NULL, 19, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 49921.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:27:28', NULL),
	(51, 51, NULL, 14, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 29010.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:28:34', NULL),
	(52, 52, NULL, 17, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 38085.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:28:51', NULL),
	(53, 53, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15254.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:40:06', NULL),
	(54, 54, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 100708.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:29:28', NULL),
	(55, 55, NULL, 14, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 29010.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:29:44', NULL),
	(56, 56, NULL, 22, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 65604.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:30:03', NULL),
	(57, 57, NULL, 20, NULL, NULL, NULL, NULL, 'Salary Adjustment', 0.00, 0.00, 52544.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:30:18', NULL),
	(58, 58, NULL, 25, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 92108.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-17 05:30:34', NULL),
	(59, 59, NULL, 7, NULL, NULL, NULL, NULL, 'Initial Salary', 0.00, 0.00, 15254.00, '2018-08-07', '2018-08-08 00:00:00', '2018-08-09 05:41:02', NULL);
/*!40000 ALTER TABLE `pms_salaryinfo` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_salary_adjustments
DROP TABLE IF EXISTS `pms_salary_adjustments`;
CREATE TABLE IF NOT EXISTS `pms_salary_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `old_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `new_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `salary_adjustment_amount` decimal(13,2) DEFAULT NULL,
  `gsis_cont_amount` decimal(13,2) DEFAULT NULL,
  `philhealth_cont_amount` decimal(13,2) DEFAULT NULL,
  `provident_fund_amount` decimal(13,2) DEFAULT NULL,
  `wtax_amount` decimal(13,2) DEFAULT NULL,
  `first_deduction_amount` decimal(13,2) DEFAULT NULL,
  `second_deduction_amount` decimal(13,2) DEFAULT NULL,
  `third_deduction_amount` decimal(13,2) DEFAULT NULL,
  `fourth_deduction_amount` decimal(13,2) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_specialpayroll_transactions
DROP TABLE IF EXISTS `pms_specialpayroll_transactions`;
CREATE TABLE IF NOT EXISTS `pms_specialpayroll_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `benefit_info_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `percentage` decimal(9,2) DEFAULT NULL,
  `cost_uniform_amount` decimal(9,2) DEFAULT NULL,
  `no_of_months_entitled` decimal(9,2) DEFAULT NULL,
  `cash_gift_amount` decimal(9,2) DEFAULT NULL,
  `cna_amount` decimal(9,2) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_special_annual_tax
DROP TABLE IF EXISTS `pms_special_annual_tax`;
CREATE TABLE IF NOT EXISTS `pms_special_annual_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `clothing_allowance_amount` decimal(13,2) DEFAULT NULL,
  `cash_gift_amount` decimal(13,2) DEFAULT NULL,
  `loyalty_amount` decimal(13,2) DEFAULT NULL,
  `performance_base_amount` decimal(13,2) DEFAULT NULL,
  `collective_negotiation_amount` decimal(13,2) DEFAULT NULL,
  `pei_amount` decimal(13,2) DEFAULT NULL,
  `mid_year_amount` decimal(13,2) DEFAULT NULL,
  `year_end_amount` decimal(13,2) DEFAULT NULL,
  `honoraria_amount` decimal(13,2) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_step_increments
DROP TABLE IF EXISTS `pms_step_increments`;
CREATE TABLE IF NOT EXISTS `pms_step_increments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `old_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `new_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `salary_adjustment_amount` decimal(13,2) DEFAULT NULL,
  `gross_pay_amount` decimal(13,2) DEFAULT NULL,
  `gsis_cont_amount` decimal(13,2) DEFAULT NULL,
  `philhealth_cont_amount` decimal(13,2) DEFAULT NULL,
  `provident_fund_amount` decimal(13,2) DEFAULT NULL,
  `wtax_amount` decimal(13,2) DEFAULT NULL,
  `first_deduction_amount` decimal(13,2) DEFAULT NULL,
  `second_deduction_amount` decimal(13,2) DEFAULT NULL,
  `third_deduction_amount` decimal(13,2) DEFAULT NULL,
  `fourth_deduction_amount` decimal(13,2) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_taxannualtable
DROP TABLE IF EXISTS `pms_taxannualtable`;
CREATE TABLE IF NOT EXISTS `pms_taxannualtable` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `below` decimal(6,2) DEFAULT NULL,
  `above` decimal(6,2) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_taxannualtable: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_taxannualtable` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_taxannualtable` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_taxespolicy
DROP TABLE IF EXISTS `pms_taxespolicy`;
CREATE TABLE IF NOT EXISTS `pms_taxespolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `computation` varchar(225) DEFAULT NULL,
  `is_withholding` varchar(225) DEFAULT NULL,
  `job_grade_rate` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_taxespolicy: ~6 rows (approximately)
/*!40000 ALTER TABLE `pms_taxespolicy` DISABLE KEYS */;
INSERT INTO `pms_taxespolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `computation`, `is_withholding`, `job_grade_rate`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'STANDARD POLICY', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', 'Monthly', NULL, NULL, '2018-06-19 17:24:26', '2018-06-21 04:31:17', NULL),
	(6, 'EWT 2 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', 'Annual', 0.02, NULL, '2018-08-07 06:01:11', '2018-08-09 09:45:31', NULL),
	(7, 'EWT 5 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', NULL, 0.05, NULL, '2018-08-07 06:01:49', '2018-08-07 06:01:49', NULL),
	(8, 'PWT 5 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', NULL, 0.05, NULL, '2018-08-07 06:02:10', '2018-08-07 06:02:10', NULL),
	(9, 'ITW 10 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', NULL, 0.10, NULL, '2018-08-09 09:46:08', '2018-08-09 09:46:08', NULL),
	(10, 'ITW 3 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', 'Annual', 0.03, NULL, '2018-08-09 09:46:49', '2018-08-09 09:48:15', NULL);
/*!40000 ALTER TABLE `pms_taxespolicy` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_taxtable
DROP TABLE IF EXISTS `pms_taxtable`;
CREATE TABLE IF NOT EXISTS `pms_taxtable` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `payperiod` varchar(225) DEFAULT NULL,
  `salary_bracket_level1` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level2` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level3` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level4` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level5` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level6` decimal(9,2) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_taxtable: ~4 rows (approximately)
/*!40000 ALTER TABLE `pms_taxtable` DISABLE KEYS */;
INSERT INTO `pms_taxtable` (`id`, `payperiod`, `salary_bracket_level1`, `salary_bracket_level2`, `salary_bracket_level3`, `salary_bracket_level4`, `salary_bracket_level5`, `salary_bracket_level6`, `status`, `effectivity_date`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'Daily', 0.00, 0.00, 82.19, 356.16, 1342.47, 6602.74, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL),
	(2, 'Monthly', 0.00, 0.00, 2500.00, 10833.33, 40833.33, 200833.33, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL),
	(3, 'Semi Monthly', 0.00, 0.00, 1250.00, 5416.67, 20416.67, 100416.67, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL),
	(4, 'Weekly', 0.00, 0.00, 576.92, 2500.00, 9423.08, 46346.15, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL);
/*!40000 ALTER TABLE `pms_taxtable` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_transactions
DROP TABLE IF EXISTS `pms_transactions`;
CREATE TABLE IF NOT EXISTS `pms_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `salaryinfo_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `empstatus_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `actual_absences` int(11) DEFAULT NULL,
  `adjust_absences` int(11) DEFAULT NULL,
  `actual_tardiness` int(11) DEFAULT NULL,
  `adjust_tardiness` int(11) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `actual_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `total_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `actual_absences_amount` decimal(9,2) DEFAULT NULL,
  `adjust_absences_amount` decimal(9,2) DEFAULT NULL,
  `total_absences_amount` decimal(9,2) DEFAULT NULL,
  `actual_tardines_amount` decimal(9,2) DEFAULT NULL,
  `adjust_tardines_amount` decimal(9,2) DEFAULT NULL,
  `total_tardines_amount` decimal(9,2) DEFAULT NULL,
  `actual_undertime_amount` decimal(9,2) DEFAULT NULL,
  `adjust_undertime_amount` decimal(9,2) DEFAULT NULL,
  `total_undertime_amount` decimal(9,2) DEFAULT NULL,
  `basic_net_pay` decimal(9,2) DEFAULT NULL,
  `actual_contribution` decimal(9,2) DEFAULT NULL,
  `adjust_contribution` decimal(9,2) DEFAULT NULL,
  `total_contribution` decimal(9,2) DEFAULT NULL,
  `actual_loan` decimal(9,2) DEFAULT NULL,
  `adjust_loan` decimal(9,2) DEFAULT NULL,
  `total_loan` decimal(9,2) DEFAULT NULL,
  `actual_otherdeduct` decimal(9,2) DEFAULT NULL,
  `adjust_otherdeduct` decimal(9,2) DEFAULT NULL,
  `total_otherdeduct` decimal(9,2) DEFAULT NULL,
  `net_deduction` decimal(9,2) DEFAULT NULL,
  `ecc_amount` decimal(9,2) DEFAULT NULL,
  `tax_amount` decimal(9,2) DEFAULT NULL,
  `gross_pay` decimal(9,2) DEFAULT NULL,
  `gross_taxable_pay` decimal(9,2) DEFAULT NULL,
  `net_pay` decimal(9,2) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Dumping structure for table e2e_db.pms_travelrates
DROP TABLE IF EXISTS `pms_travelrates`;
CREATE TABLE IF NOT EXISTS `pms_travelrates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate_amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_travelrates: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_travelrates` DISABLE KEYS */;
INSERT INTO `pms_travelrates` (`id`, `rate_amount`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(2, 800.00, 'travelrate', 2, NULL, '2018-09-10 10:52:44', '2018-09-10 10:52:44');
/*!40000 ALTER TABLE `pms_travelrates` ENABLE KEYS */;

-- Dumping structure for table e2e_db.pms_travelrate_transactions
DROP TABLE IF EXISTS `pms_travelrate_transactions`;
CREATE TABLE IF NOT EXISTS `pms_travelrate_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `travel_rate_id` int(11) DEFAULT NULL,
  `rate_id` int(11) DEFAULT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `travel_rate_amount` decimal(9,2) DEFAULT NULL,
  `net_amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


-- Dumping structure for table e2e_db.pms_wagerates
DROP TABLE IF EXISTS `pms_wagerates`;
CREATE TABLE IF NOT EXISTS `pms_wagerates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wage_region` varchar(225) DEFAULT NULL,
  `wage_rate` decimal(9,2) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table e2e_db.pms_wagerates: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_wagerates` DISABLE KEYS */;
INSERT INTO `pms_wagerates` (`id`, `wage_region`, `wage_rate`, `effectivity_date`, `based_on`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'National Capital Region', 200.00, '2018-06-19', NULL, NULL, '2018-06-12 03:34:42', '2018-06-12 03:34:42', NULL);
/*!40000 ALTER TABLE `pms_wagerates` ENABLE KEYS */;

-- Dumping structure for table e2e_db.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e2e_db.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Jezon Gonzales', 'gonzales_jezon@yahoo.com', '$2y$10$o9TEoA6PVskFUo0cZ.0c/eHzcjnkBKYnN5FViptfAYPGbaX0rob8S', '6ugWfG2iGS1MyxKSpyo7GFnYUogTUJVOmCIANAne2mDVtNVqXPDhzE3jtb3z', '2018-07-13 08:52:26', '2018-07-13 08:52:26'),
	(2, 'Tester', 'admintester@gmail.com', '$2y$10$V5JxbFFe/k1xa2L0xh6.Cubom4q2l2A/9DEPC/jJVcVWORKFr.ycq', 'zyDlIr0twOvh9Cs3OesX2Z5CuZjI5o7x0QEjvnL4yZRR3R7kMaSJM43Snqop', '2018-07-15 02:12:16', '2018-07-15 02:12:16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
