-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mwss.pms_loans
DROP TABLE IF EXISTS `pms_loans`;
CREATE TABLE IF NOT EXISTS `pms_loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `loan_type` varchar(225) DEFAULT NULL,
  `category` varchar(225) DEFAULT NULL,
  `gsis_excel_col` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table mwss.pms_loans: ~11 rows (approximately)
/*!40000 ALTER TABLE `pms_loans` DISABLE KEYS */;
INSERT INTO `pms_loans` (`id`, `code`, `name`, `loan_type`, `category`, `gsis_excel_col`, `amount`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'CS', 'CONSOLOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:32:01', '2018-06-19 17:32:01', NULL),
	(2, 'PL', 'POLICY LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:32:37', '2018-06-19 17:35:22', NULL),
	(3, 'EL', 'EMERGENCY/CALAMITY LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:33:50', '2018-06-19 17:33:50', NULL),
	(4, 'OL', 'OPTIONAL LIFE', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:34:11', '2018-06-19 17:34:11', NULL),
	(5, 'MPL', 'MULTI PURPOSE LOAN', 'Pagibig Loan', NULL, NULL, NULL, NULL, '2018-06-19 17:34:41', '2018-06-19 17:34:41', NULL),
	(6, 'CL', 'CALAMITY LOAN', 'Pagibig Loan', NULL, NULL, NULL, NULL, '2018-06-19 17:34:58', '2018-06-19 17:34:58', NULL),
	(7, 'MPLP', 'MULTI-PURPOSE COOPERATIVE LOAN', 'NULL', NULL, NULL, NULL, NULL, '2018-08-16 12:17:15', '2018-08-16 12:17:15', NULL),
	(8, 'LCH-DCS', 'LCH-DCS', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 03:50:45', '2018-08-17 03:50:45', NULL),
	(9, 'EA', 'EDUCATION LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 03:53:49', '2018-08-17 03:53:49', NULL),
	(10, 'WASSSLAI Loan', 'WASSSLAI Loan', NULL, NULL, NULL, NULL, NULL, '2018-08-17 04:09:54', '2018-08-17 04:09:54', NULL),
	(11, 'WFLOAN', 'WELFARE FUND LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 06:51:16', '2018-08-17 06:51:16', NULL),
	(12, 'PFL', 'PROVIDENT FUND LOAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'ML', 'MEMBAI LOAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 'HHL', 'HDMF HOUSING LOAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_loans` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
