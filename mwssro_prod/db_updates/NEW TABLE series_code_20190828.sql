-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mwss.pms_series_codes
DROP TABLE IF EXISTS `pms_series_codes`;
CREATE TABLE IF NOT EXISTS `pms_series_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Dumping data for table mwss.pms_series_codes: ~32 rows (approximately)
/*!40000 ALTER TABLE `pms_series_codes` DISABLE KEYS */;
INSERT INTO `pms_series_codes` (`id`, `code`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, '01', 'Chief Regulator', 1, NULL, '2019-07-23 14:04:31', '2019-07-23 14:04:31'),
	(2, '02', 'Head Technical Assistant', 1, NULL, '2019-07-23 14:04:46', '2019-07-23 14:04:46'),
	(3, '03', 'MIS Design Specialist A', 1, NULL, '2019-07-23 14:04:59', '2019-07-23 14:04:59'),
	(4, '04', 'Dept. Manager-PID', 1, NULL, '2019-07-23 14:05:13', '2019-07-23 14:05:13'),
	(5, '05', 'Secretary  B', 1, NULL, '2019-07-23 14:06:28', '2019-07-23 14:06:28'),
	(6, '06', 'Artist-Illustrator A', 1, NULL, '2019-07-23 14:06:52', '2019-07-23 14:06:52'),
	(7, '07', 'Data Analyst Controller', 1, NULL, '2019-07-23 14:07:42', '2019-07-23 14:07:42'),
	(8, '08', 'Driver-Mechnaic B', 1, NULL, '2019-07-23 14:19:54', '2019-07-23 14:19:54'),
	(9, '09', 'Driver-Mechanic A', 1, NULL, '2019-07-23 14:20:10', '2019-07-23 14:20:10'),
	(10, '10', 'Dep Adm - TRA', 1, NULL, '2019-07-23 14:20:25', '2019-07-23 14:20:25'),
	(11, '11', 'Technical Assistant A', 1, NULL, '2019-07-23 14:20:44', '2019-07-23 14:20:44'),
	(12, '12', 'Dept Manager-WQCD', 1, NULL, '2019-07-23 14:21:04', '2019-07-23 14:21:04'),
	(13, '13', 'Dept Manager-OMD', 1, NULL, '2019-07-23 14:21:28', '2019-07-23 14:21:28'),
	(14, '14', 'Supvg. Water Utilities Reg. Officer', 1, NULL, '2019-07-23 14:21:47', '2019-07-23 14:21:47'),
	(15, '18', 'Dept. Mgr.-TCMD', 1, 1, '2019-07-23 14:22:32', '2019-07-26 17:08:11'),
	(16, '16', 'Dept Manager-FAAMD', 1, NULL, '2019-07-23 14:22:49', '2019-07-23 14:22:49'),
	(17, '17', 'Supvg. Financial Mgt. Specialist', 1, NULL, '2019-07-23 14:23:06', '2019-07-23 14:23:06'),
	(18, '15', 'Sr. Water Utilities Reg. Off.', 1, 1, '2019-07-23 14:23:22', '2019-07-26 17:07:53'),
	(19, '19', 'Finance Officer C', 1, 1, '2019-07-23 14:24:02', '2019-07-23 14:30:50'),
	(20, '20', 'Chief Economist', 1, 1, '2019-07-23 14:24:25', '2019-07-23 14:31:10'),
	(21, '21', 'Dep Adm - CSR', 1, 1, '2019-07-23 14:24:51', '2019-07-23 14:31:42'),
	(22, '22', 'Driver-Mechanic B', 1, 1, '2019-07-23 14:25:13', '2019-07-23 14:32:04'),
	(23, '23', 'Dept. Manager-Compliants Monitoring', 1, NULL, '2019-07-23 14:32:25', '2019-07-23 14:32:25'),
	(24, '24', 'Spvg. WURO', 1, NULL, '2019-07-23 14:32:43', '2019-07-23 14:32:43'),
	(25, '25', 'Dept. Mgr.- MED', 1, NULL, '2019-07-23 14:33:02', '2019-07-23 14:33:02'),
	(26, '26', 'Dep Adm - ALA', 1, NULL, '2019-07-23 14:33:27', '2019-07-23 14:33:27'),
	(27, '27', 'Finance Officer B', 1, NULL, '2019-07-23 14:34:32', '2019-07-23 14:34:32'),
	(28, '28', 'Sr. Prop. Officer', 1, NULL, '2019-07-23 14:34:49', '2019-07-23 14:34:49'),
	(29, '29', 'Chief Corporate Accountant', 1, NULL, '2019-07-23 14:35:11', '2019-07-23 14:35:11'),
	(30, '30', 'Admin. Officer III', 1, NULL, '2019-07-23 14:35:27', '2019-07-23 14:35:27'),
	(31, '31', 'Records Officer D', 1, NULL, '2019-07-23 14:35:46', '2019-07-23 14:35:46'),
	(32, '32', 'Sr. IRM Officer A', 1, NULL, '2019-07-23 14:36:13', '2019-07-23 14:36:13'),
	(33, '33', 'Dept. Mgr.- Adm.', 1, NULL, '2019-07-23 14:36:52', '2019-07-23 14:36:52'),
	(34, '34', 'Fiscal Examiner A', 1, NULL, '2019-07-23 14:37:12', '2019-07-23 14:37:12'),
	(35, '35', 'Senior Corporate Attorney', 1, NULL, '2019-07-23 14:37:48', '2019-07-23 14:37:48'),
	(36, '36', 'Dept Manager - Legal', 1, NULL, '2019-07-23 14:38:02', '2019-07-23 14:38:02');
/*!40000 ALTER TABLE `pms_series_codes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
