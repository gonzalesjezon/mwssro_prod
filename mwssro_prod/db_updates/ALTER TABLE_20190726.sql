ALTER TABLE `pms_payroll_information`
	ADD COLUMN `dept_code` VARCHAR(225) NULL DEFAULT NULL AFTER `atm_no`;


ALTER TABLE `pms_transactions`
	ADD COLUMN `dept_code` VARCHAR(225) NULL DEFAULT NULL AFTER `item_number`;