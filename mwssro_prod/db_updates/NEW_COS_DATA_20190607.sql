-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mwss.pms_nonplantilla_employeeinfo
DROP TABLE IF EXISTS `pms_nonplantilla_employeeinfo`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_employeeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `tax_amount_one` decimal(9,2) DEFAULT NULL,
  `tax_amount_two` decimal(9,2) DEFAULT NULL,
  `taxpolicy_two_id` int(11) DEFAULT NULL,
  `atm_no` varchar(225) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table mwss.pms_nonplantilla_employeeinfo: ~4 rows (approximately)
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` DISABLE KEYS */;
INSERT INTO `pms_nonplantilla_employeeinfo` (`id`, `employee_id`, `employee_number`, `bank_id`, `taxpolicy_id`, `tax_amount_one`, `tax_amount_two`, `taxpolicy_two_id`, `atm_no`, `daily_rate_amount`, `monthly_rate_amount`, `annual_rate_amount`, `overtime_balance_amount`, `tax_id_number`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 46, 'COS1000', NULL, 10, 0.72, -1040.45, 9, '1907-0148-66', 1198.36, 26364.00, 316368.00, 158184.00, NULL, '2018-11-23 03:24:46', '2018-11-23 03:24:46', NULL, NULL),
	(2, 37, 'COS1003', NULL, 10, 0.54, -1040.75, 9, '1907-0077-46', 905.00, 19910.00, 238920.00, 119460.00, NULL, '2018-11-23 03:25:01', '2018-11-23 03:25:01', NULL, NULL),
	(3, 38, 'COS1004', NULL, 10, 0.51, -1040.80, 9, '1906-0153-97', 853.59, 18779.00, 225348.00, 112674.00, NULL, '2018-11-23 03:25:14', '2018-11-23 03:25:14', NULL, NULL),
	(4, 45, 'COS1015', NULL, 10, 0.54, -1040.75, 9, '1907-0079-08', 905.00, 19910.00, 238920.00, 119460.00, NULL, '2018-11-23 03:25:33', '2018-11-23 03:25:33', NULL, NULL),
	(5, 43, 'COS1011', NULL, 10, 1.20, -1039.65, 9, '1907-0078-35', 1995.18, 43894.00, 526728.00, 263364.00, NULL, '2018-11-23 03:26:02', '2018-11-23 03:26:02', NULL, NULL),
	(6, 54, 'COS1009', NULL, 10, 0.93, -1040.10, 9, '1907-0077-70', 1995.18, 43894.00, 526728.00, 263364.00, NULL, '2018-11-23 03:27:02', '2018-11-23 03:27:02', NULL, NULL),
	(7, 39, 'COS1005', NULL, 10, 0.51, -1040.80, 9, '1907-0077-62', 853.59, 18779.00, 225348.00, 112674.00, NULL, '2018-11-23 03:27:16', '2018-11-23 03:27:16', NULL, NULL),
	(8, 57, 'COS1013', NULL, 10, 0.51, -1040.80, 9, '1907-0078-43', 853.59, 18779.00, 225348.00, 112674.00, NULL, '2018-11-23 03:27:29', '2018-11-23 03:27:29', NULL, NULL),
	(9, 62, 'COS1012', NULL, 10, 0.51, -1040.80, 9, '1907-0078-51', 853.59, 18779.00, 225348.00, 112674.00, NULL, '2018-11-23 03:27:41', '2018-11-23 03:27:41', NULL, NULL),
	(10, 67, 'COS1016', NULL, 10, 0.51, -1040.80, 9, '1907-0079-40', 853.59, 18779.00, 225348.00, 112674.00, NULL, '2018-11-23 03:27:55', '2018-11-23 03:27:55', NULL, NULL),
	(11, 44, 'COS1014', NULL, 10, 0.54, -1040.75, 9, '1907-0079-16', 905.00, 19910.00, 238920.00, 119460.00, NULL, '2018-11-23 03:28:09', '2018-11-23 03:28:09', NULL, NULL),
	(12, 68, 'COS1002', NULL, 10, 0.66, -1040.55, 9, '1906-0154-19', 1099.86, 24197.00, 290364.00, 145182.00, NULL, '2018-11-23 03:28:23', '2018-11-23 03:28:23', NULL, NULL),
	(13, 36, 'COS1001', NULL, 10, 1.20, -1039.65, 9, '1907-0076-81', 1995.18, 43894.00, 526728.00, 263364.00, NULL, '2018-11-23 03:28:57', '2018-11-23 03:28:57', NULL, NULL),
	(14, 40, 'COS1006', NULL, 10, 0.54, -1040.75, 9, '1907-0077-54', 905.00, 19910.00, 238920.00, 119460.00, NULL, '2018-11-23 03:29:11', '2018-11-23 03:29:11', NULL, NULL),
	(15, 70, 'COS1017', NULL, 10, 0.51, -1040.80, 9, NULL, 784.27, 17254.00, 207048.00, 103524.00, NULL, '2018-11-23 03:29:24', '2018-11-23 03:29:24', NULL, NULL),
	(16, 41, 'COS1007', NULL, 10, 0.54, -1040.75, 9, '1907-0077-97', 905.00, 19910.00, 238920.00, 119460.00, NULL, '2018-11-23 03:29:38', '2018-11-23 03:29:38', NULL, NULL),
	(17, 42, 'COS1008', NULL, 10, 0.78, -1040.35, 9, '1907-0777-89', 1302.09, 28646.00, 343752.00, 171876.00, NULL, '2018-11-23 03:29:51', '2018-11-23 03:29:51', NULL, NULL),
	(18, 73, 'COS1010', NULL, 10, 0.54, -1040.75, 9, '1907-0078-19', 905.00, 19910.00, 238920.00, 119460.00, NULL, '2018-11-23 03:30:09', '2018-11-23 03:30:09', NULL, NULL),
	(19, 75, 'COS1018', NULL, 10, 0.51, -1040.80, 9, '1907-0079-59', 853.59, 18779.00, 225348.00, 112674.00, NULL, '2018-11-23 03:30:22', '2018-11-23 03:30:22', NULL, NULL);
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
