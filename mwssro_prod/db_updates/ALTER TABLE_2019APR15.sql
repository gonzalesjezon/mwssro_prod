ALTER TABLE `pms_benefitsinfo`
	DROP COLUMN `employee_number`,
	DROP COLUMN `employeeinfo_id`,
	DROP COLUMN `benefit_description`,
	DROP COLUMN `benefit_pay_period`,
	DROP COLUMN `benefit_pay_sub`,
	DROP COLUMN `benefit_effectivity_date`,
	DROP COLUMN `date_from`,
	DROP COLUMN `date_to`;

ALTER TABLE `pms_benefitsinfo`
	ADD COLUMN `start_date` DATE NULL DEFAULT NULL AFTER `benefit_amount`,
	ADD COLUMN `end_date` DATE NULL DEFAULT NULL AFTER `start_date`,
	ADD COLUMN `date_terminated` DATE NULL DEFAULT NULL AFTER `end_date`,
	ADD COLUMN `terminated` INT NULL DEFAULT NULL AFTER `date_terminated`,
	ADD COLUMN `created_by` INT(11) NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `pms_loansinfo`
	CHANGE COLUMN `loan_totalamount` `loan_total_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `loan_id`,
	CHANGE COLUMN `loan_totalbalance` `loan_total_balance` DECIMAL(9,2) NULL DEFAULT NULL AFTER `loan_total_amount`,
	CHANGE COLUMN `loan_amortization` `loan_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `loan_total_balance`,
	CHANGE COLUMN `loan_date_started` `start_date` DATE NULL DEFAULT NULL AFTER `loan_date_granted`,
	CHANGE COLUMN `loan_date_end` `end_date` DATE NULL DEFAULT NULL AFTER `start_date`,
	CHANGE COLUMN `loan_date_terminated` `date_terminated` DATE NULL DEFAULT NULL AFTER `end_date`,
	ADD COLUMN `terminated` INT NULL DEFAULT '0' AFTER `date_terminated`,
	DROP COLUMN `employeeinfo_id`,
	DROP COLUMN `employee_number`,
	DROP COLUMN `loan_pay_period`;

ALTER TABLE `pms_loansinfo`
	ADD COLUMN `created_by` INT(11) NULL DEFAULT NULL AFTER `updated_by`;


ALTER TABLE `pms_deductioninfo`
	CHANGE COLUMN `deduct_amount` `deduction_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `deduction_id`,
	CHANGE COLUMN `deduct_date_start` `start_date` DATE NULL DEFAULT NULL AFTER `deduction_amount`,
	CHANGE COLUMN `deduct_date_end` `end_date` DATE NULL DEFAULT NULL AFTER `start_date`,
	CHANGE COLUMN `deduct_date_terminated` `date_terminated` DATE NULL DEFAULT NULL AFTER `end_date`,
	ADD COLUMN `terminated` INT NULL DEFAULT '0' AFTER `date_terminated`,
	ADD COLUMN `created_by` INT(11) NULL DEFAULT NULL AFTER `updated_at`,
	DROP COLUMN `employeeinfo_id`,
	DROP COLUMN `employee_number`,
	DROP COLUMN `deduct_pay_period`;

ALTER TABLE `pms_payroll_information`
	DROP COLUMN `employee_number`,
	DROP COLUMN `no_ofdays_inayear`,
	DROP COLUMN `no_ofdays_inamonth`,
	DROP COLUMN `total_hours_inaday`;

ALTER TABLE `pms_deductioninfo_transactions`
	DROP COLUMN `employee_number`,
	DROP COLUMN `transaction_id`,
	DROP COLUMN `deduction_info_id`;

ALTER TABLE `pms_deductioninfo_transactions`
	ADD COLUMN `deduction_info_id` INT(11) NULL DEFAULT NULL AFTER `deduction_id`;

ALTER TABLE `pms_loaninfo_transactions`
	DROP COLUMN `employee_number`,
	DROP COLUMN `transaction_id`;

ALTER TABLE `pms_benefitinfo_transactions`
	DROP COLUMN `employee_number`,
	DROP COLUMN `transaction_id`;
