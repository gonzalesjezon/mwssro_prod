ALTER TABLE `pms_nonplantilla_transactions`
	CHANGE COLUMN `year` `from_date` DATE NULL DEFAULT NULL AFTER `hold`,
	CHANGE COLUMN `month` `to_date` DATE NULL DEFAULT NULL AFTER `from_date`;