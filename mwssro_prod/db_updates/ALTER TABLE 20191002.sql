-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mwss_20191002.pms_overtime
DROP TABLE IF EXISTS `pms_overtime`;
CREATE TABLE IF NOT EXISTS `pms_overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `regular_ot_rate` decimal(9,3) DEFAULT NULL,
  `regular_no_hrs` decimal(9,3) DEFAULT NULL,
  `regular_ot_hrs` decimal(9,3) DEFAULT NULL,
  `regular_ot_amount` decimal(9,2) DEFAULT NULL,
  `weekend_ot_rate` decimal(9,3) DEFAULT NULL,
  `weekend_no_hrs` decimal(9,3) DEFAULT NULL,
  `weekend_ot_hrs` decimal(9,3) DEFAULT NULL,
  `weekend_ot_amount` decimal(9,2) DEFAULT NULL,
  `holiday_ot_rate` decimal(9,3) DEFAULT NULL,
  `holiday_no_hrs` decimal(9,3) DEFAULT NULL,
  `holiday_ot_hrs` decimal(9,3) DEFAULT NULL,
  `holiday_ot_amount` decimal(9,2) DEFAULT NULL,
  `ot_total` decimal(9,2) DEFAULT NULL,
  `ot_tax` decimal(9,2) DEFAULT NULL,
  `ot_net` decimal(9,2) DEFAULT NULL,
  `ot_covered_period` varchar(225) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
