<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Parameters
    |--------------------------------------------------------------------------
    |
    | Accessible via config('param.key');
    | eg: config('params._SUPER_ADMIN_ID_')
    | any other location as required by the application or its packages.
    */

    '_SUPER_ADMIN_ID_' => 1,
    'months' => [
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    ],

    'dept_code' => [
        '661' => '661',
        '662' => '662',
        '663' => '663',
        '771' => '771',
        '772' => '772',
        '773' => '773',
        '881' => '881',
        '882' => '882',
        '883' => '883',
        '991' => '991',
        '992' => '992',
        '993' => '993',
    ]


];
