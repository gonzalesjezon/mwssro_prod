$(document).ready(function() {
   $("#spSAVECANCEL").hide();
   $(".form-input").attr("disabled",true);
   $("[name='compType']").attr("disabled",true);
   $("#btnADD").click(function() {
      objBehaviour("ADD");
   });
   $("#btnEDIT").click(function() {
      objBehaviour("EDIT");
   });
   $("#btnSAVE").click(function() {
      objBehaviour("SAVE");
   });
   $("#btnCANCEL").click(function() {
      objBehaviour("CANCEL");
   });
   $("#btnEXIT").click(function() {
      gotoscrn("scrnDashboard","&paramTitle=Dashboard");
   });

   $("#rad_Govt").click(function() {
      $("[name='char_CompanyType']").val(1);
   });
   $("#rad_Private").click(function() {
      $("[name='char_CompanyType']").val(0);
   });

   $("#spSAVECANCEL").hide();

   $("[name='sint_ProvinceRefId']").change(function () {
      loadCity("sint_CityRefId",$(this).val());
   });

});

function closeSCRN(scrntype) {
   gotoscrn("scrnDashboard","&paramTitle=DASHBOARD");
}

function afterEditSave(refid) {
   $("#hmode").val("VIEW");
   $("#spADDEDITPRINT").show();
   $(".form-input, [name='compType']").attr("disabled",true);
   loadRecord(refid);
   alert("Record "+ refid +" Sucessfully Updated");
}

function afterNewSave(newRefId) {
   $("#hmode").val("VIEW");
   $("#spADDEDITPRINT").show();
   $(".form-input, [name='compType']").attr("disabled",true);
   loadRecord(newRefId);
   alert("New Record Inserted\nRef. ID: " + newRefId);
}

function objBehaviour(btnValue) {
   $("#spADDEDITPRINT, #spSAVECANCEL").hide();
   $(".form-input, [name='compType']").attr("disabled",true);
   switch (btnValue) {
      case "ADD":
         $("[name='hmode']").val(btnValue)
         $("#spSAVECANCEL").show();
         $(".form-input, [name='compType']").attr("disabled",false);
         $('[name="hRefId"]').val(0);
      break;
      case "EDIT":
         $("[name='hmode']").val(btnValue)
         if ($('[name="hRefId"]').val()) {
            $("#spSAVECANCEL").show();
            $(".form-input, [name='compType']").not("[name='char_Code']").attr("disabled",false);
         } else {
            alert("Err : No RefId Assigned !!!");
         }
      break;
      case "SAVE":
         if ($("#hTable").val() == "") {
            alert("No Table Assigned");
            return false;
         } else {
            if (saveProceed() == 0) {
               var objValue = getElement_ObjnValue();
               gSaveRecord(objValue,$("#hTable").val());
            }
         }
      break;
      case "CANCEL":
         $("#hmode").val("VIEW");
         $("#spADDEDITPRINT").show();
         $(".form-input, [name='compType']").attr("disabled",true);
      break;
   }
}

function loadRecord(refid) {
   $.post("ctrl_CompanyProfile.e2e.php",
   {
      t:"viewRecord",
      u:$("#hUser").val(),
      refid:refid
   },
   function(data,status) {
      if (status == "success") {
         var data = JSON.parse(data);
         var lgMain = false;
         var lgCompType = false;

         if (data.isMain == 1)
            lgMain = true;
         else
            lgMain = false;

         if (data.CompanyType == 1)
            lgCompType = true;
         else
            lgCompType = false;
         try {
            $('[name="hRefId"]').val(data["RefId"]);
            $('[name="char_Code"]').val(data["Code"]);
            $('[name="char_Address"]').val(data["Address"]);
            $('[name="char_Name"]').val(data["Name"]);
            $('[name="char_ZipCode"]').val(data["ZipCode"]);
            $('[name="char_TelNo"]').val(data["TelNo"]);
            $('[name="char_EmailAdd"]').val(data["EmailAdd"]);
            $('[name="char_Website"]').val(data["Website"]);
            $('[name="char_Label1"]').val(data["Label1"]);
            $('[name="char_Name1"]').val(data["Name1"]);
            $('[name="char_Position1"]').val(data["Position1"]);
            $('[name="char_Label2"]').val(data["Label2"]);
            $('[name="char_Name2"]').val(data["Name2"]);
            $('[name="char_Position2"]').val(data["Position2"]);
            $('[name="char_Label3"]').val(data["Label3"]);
            $('[name="char_Name3"]').val(data["Name3"]);
            $('[name="char_Position3"]').val(data["Position3"]);
            $('[name="char_Label4"]').val(data["Label4"]);
            $('[name="char_Name4"]').val(data["Name4"]);
            $('[name="char_Position4"]').val(data["Position4"]);
            $('[name="char_Remarks"]').val(data["Remarks"]);
            $('[name="char_CompanyType"]').val(data["CompanyType"]);
            $('[name="sint_isMain"]').prop("checked",lgMain);
            $('#rad_Govt').prop("checked",lgCompType);
            $('[name="char_SmallLogo"]').val(data["SmallLogo"]);
            $('[name="char_BigLogo"]').val(data["BigLogo"]);
            $('[name="char_PatternLogo"]').val(data["PatternLogo"]);
            $('[name="char_color1"]').val(data["Color1"]);
            $('[name="char_color2"]').val(data["Color2"]);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message + " Err in loadRecord");
            }
         }
      } else {
         alert(status + " in loadRecord");
      }
   });
}