$(document).ready(function(){
   $("#btnTransfer").click(function() {
      $("[name='sint_TransferToRefId'], #TransferedEmpName").val("");
      $("[name='sint_TransferToRefId'], [name='char_Remarks']").prop("disabled",false);
      $(".HideinAdd--").show();
      $("#sacaba").hide();
      $("[name='hmode']").val("EDIT");
   });
   $("#btnTransferNow").click(function() {
      if ($("#hTable").val() != "") {
         var fld = "";
         fld += "text|char_Remarks|" + "Transfer:" + $("[name='char_Remarks']").val() + "!";
         fld += "text|sint_TransferToRefId|" + $("[name='sint_TransferToRefId']").val() + "!";
         fld += "text|char_TransferedBy|" + $("#hUser").val() + "!";
         fld += "text|sint_EmployeesRefId|" + $("[name='sint_TransferToRefId']").val() + "!";
         fld += "text|sint_TransferFromRefId|" + $("[name='sint_EmployeesRefId']").val() + "!";
         if ($("[name='sint_TransferToRefId']").val() == ""){
            alert("Field Required!!!");
            $("[name='sint_TransferToRefId']").focus();
            return false;
         }else {
            /*alert(fld);
            return false;*/
            saveTransfer(fld,$("#hTable").val());
         }

      } else {
         alert("Table not assigned");
      }
      return false;
   });
   $("#btnCancelNow").click(function() {
      $("[name='sint_TransferToRefId'], [name='char_Remarks']").prop("disabled",true);
      $(".HideinAdd--").hide();
      $("[name='hmode']").val("VIEW");
      return false;
   });
});

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   gotoscrn ($("#hProg").val(), setAddURL() + "&txtRefId=" + $("[name='txtRefId']").val());
   return false;
}
function afterEditSave(RefId){
   alert("Record " + RefId + " Succesfully Updated");
   gotoscrn ($("#hProg").val(), setAddURL() + "&txtRefId=" + $("[name='txtRefId']").val());
   return false;
}
function afterDelete() {
   alert("Record Succesfully Deleted");
   gotoscrn ($("#hProg").val(), setAddURL() + "&txtRefId=" + $("[name='txtRefId']").val());
   return false;
}

function saveTransfer(Object_Value,dbtable) {
   if (!$("[name='hmode']").length) {
      console.log("Sys Error !!! No hmode created");
      alert("Sys Error !!! No hmode created");
      return false;
   }
   if (!$("[name='hRefId']").length) {
      console.log("Sys Error !!! No hmode hRefId created");
      alert("Sys Error !!! No hmode hRefId created");
      return false;
   }
   var saveSuccess = false;
   var TrnMode = $("[name='hmode']").val();
   if (Object_Value != "") {
      setCookie("cookies_Object_Value",Object_Value,1);
      if (TrnMode == "ADD") {
         var refid = 0;
      } else {
         var refid = $("[name='hRefId']").val();
         if (refid == 0) {
            alert("Warning!!! No RefId Assigned");
            return false;
         }
         if (TrnMode != "EDIT") {
            alert("Warning!!! Transaction Mode not in EDIT");
            return false;
         }
      }
      $.post("SystemAjax.e2e.php",
      {
         task:"ajxSaveRecord",
         refid:refid,
         hTrnMode:TrnMode,
         obj_n_value:Object_Value,
         hUser:$("[name='hUser']").val(),
         table:dbtable
      },
      function(data,status) {
         if (status == "success") {
            code = data;
            try {
               eval(code);
               // create

            } catch (e) {
                if (e instanceof SyntaxError) {
                    alert(e.message);
                }
            }
         }
      });

   }
}