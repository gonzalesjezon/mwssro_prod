$(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
   /*===============================================================================*/
   if ($("[id*='gridTable']").length) {
      $("[id*='gridTable']").DataTable();
      /*$("[id*='gridTable']").each(function () {
         var table = $(this).DataTable();
         // Sort by columns 1
         table
            .order([ 0, 'desc' ])
            .draw();
      });*/
   }
   /*alert(sessionStorage.getItem("AUTH_PAGE") + "==" + sessionStorage.getItem("auth"));
   if (sessionStorage.getItem("AUTH_PAGE") !== sessionStorage.getItem("auth")) {
         alert("Page Not Auth");
         sessionStorage.removeItem("AUTH_PAGE");
         sessionStorage.removeItem("auth");
         self.location = "idx.e2e.php?pageAuth=no";
   }*/

   $(function() {
      /*if ($("#tabs").length) {
         $("#tabs").tabs();
      }
      $(document).tooltip();
      */
      $(".overlay, #divView, #submenuADMINISTRATION, .imgMenu, .alert--").hide();
      $(".subLabel").slideUp(100);
      $("#tableDtl").show();
      $("[name='date_FiledDate']").val($("#hToday").val());
      $("[class*='mandatory--'], [class*='mandatory']").not("#inputUser, #inputPW").attr("placeholder","MANDATORY");
      //chkMultipleObjID();
      timedate();
      indicateActiveModules();
      if ($("#hUserGroup").val() == "COMPEMP") {
         //remIconDL();
      }
      $(".date--").attr("readonly",true);
   });
   var modules = "amsReqOvertime,amsReqForceLeave,amsAvailLeave,amsAvailCTO,amsAvailAuthority,amsAvailLeaveMonitization";
   sessionStorage.setItem("modules", modules);
});

