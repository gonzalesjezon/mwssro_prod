<?php
   require_once '../PHPExcel/Classes/PHPExcel.php';
   /*RUNNING*/
   $inputFileName = "../pds/TEST_PDS.xlt";
   $objReader = PHPExcel_IOFactory::createReaderForFile($inputFileName);
   $objPHPExcel = $objReader->load($inputFileName);
   //$workSheet = $objPHPExcel->getActiveSheet();
   //$lastRow = $workSheet->getHighestRow();
   $workSheet = $objPHPExcel->setActiveSheetIndex(0); // Personal & Educ BG
   function convDate($cellCoor) {
      $cell = $GLOBALS['objPHPExcel']->getActiveSheet()->getCell($cellCoor);
      $excelDate = $cell->getValue(); // gives you a number like 44444, which is days since 1900
      $stringDate = PHPExcel_Style_NumberFormat::toFormattedString($excelDate, 'MM-DD-YYYY');
      return $stringDate;
   }
/*
   echo "<br><b>SURNAME :</b> ".$workSheet->getCell('C7');
   echo "<br><b>FIRST NAME :</b> ".$workSheet->getCell('C8');
   echo "<br><b>MIDDLE NAME :</b> ".$workSheet->getCell('C9');
   echo "<br><b>EXT. NAME :</b> ".$workSheet->getCell('N8');
   echo "<br><b>DOB :</b> ".convDate('E10');
   echo "<br><b>BIRTH PLACE :</b> ".$workSheet->getCell('C11');
   echo "<br><b>SEX :</b> ".$workSheet->getCell('C12');
   echo "<br><b>CIVIL STATUS :</b> ".$workSheet->getCell('C13');
   echo "<br><b>CITIZENSHIP :</b> ".$workSheet->getCell('J10');
   echo "<br><b>If holder of  dual citizenship :</b> ".$workSheet->getCell('J11');
   echo "<br><b>Citizenship Country :</b> ".$workSheet->getCell('J12');
*/
?>

<!DOCTYPE html>
<html>
   <head>
      <title>PDS</title>
      <style type="text/css">
         body
         {
            font-family:Arial Narrow;
            font-size:8pt;
         }
         td {border:1px solid black;padding-bottom:3px;padding-top:3px;}
         table {width:100%;border-collapse:collapse;}
         .tabTitle {font-family:Arial Narrow;font-size:11pt;font-weight:bold;}
         .tabTitle_td {border-top:2px solid #000;border-bottom:2px solid #000;}
         .bgGray {background:#bfbfbf;color:white;}
         .bgGrayLabel {background:#bfbfbf;color:black;vertical-align:top;}
         .answer{font-size:10pt;font-weight:600;}
         .b-left{border-left:1px solid #000}
         .b-right{border-right:1px solid #000}
         .b-top{border-top:1px solid #000}
         .b-bottom{border-bottom:1px solid #000}
         .bgBlueLabel{background:#b3d1ff;color:black;vertical-align:top;}
         .page-- {height:14in;width:8.5in;border:1px solid #ddd;}
         #PersonalInformation, #Eligibility {display:none;}
         @media print {
            .ruler, .noPrint, .noPrint *{display: none !important;}
            .nextpage  {page-break-after:always;}
            .lastpage  {page-break-after:avoid;}
            .page-- {height:14in;width:8.5in;border:0px solid #ddd;}
         }
      </style>
   </head>
   <body>
      <input type="button" value="print" class="noPrint" onclick="self.print();">
      <div id="PersonalInformation" class="page-- nextpage">
         <table border=1 cellspacing=0 cellpadding=1>
            <tr align="center" class="ruler">
               <td width="17px"></td>
               <td width="135px"></td>
               <td width="13px"></td>
               <td width="80px"></td>
               <td width="100px"></td>
               <td width="20px"></td>
               <td width="80px"></td>
               <td width="55px"></td>
               <td width="41px"></td>
               <td width="56px"></td>
               <td width="58px"></td>
               <td width="75px"></td>
               <td width="75px"></td>
               <td width="*px"></td>
            </tr>
            <tr>
               <td colspan="14" align="center" style="padding-top:10px;padding-bottom:10px;position:relative;">
                  <span style="position:absolute;top:1px;left:3px;font-family:calibri;font-size:8pt;text-align:left;"><b><i>CS Form No. 212<br>Revised 2017</i></b></span>
                  <span style="font-family:Arial Black;font-size:20pt">PERSONAL DATA SHEET</span>
               </td>
            </tr>
            <tr>
               <td colspan="14">
                  <span style="font-family:Arial;font-size:8pt;">
                     <b><i>
                        WARNING: Any misinterpretation made in the Personal Data Sheet and the Work Experience Sheet shall cause the filing of administrative/criminal case/s against the person concerned.
                     </i></b>
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="14">
                  <span style="font-family:Arial;font-size:8pt">
                     <b><i>
                        READ THE ATTACHED GUIDE TO FILLING OUT THE PERSONAL DATA SHEET (PDS) BEFORE ACCOMPLISHING THE PDS FORM.
                     </i></b>
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="10">
                  <span>
                     Print legibly. Tick appropriate boxes ( <span style="font-family:wingdings">o</span> ) and use separate sheet if necessary. Indicate N/A if not applicable.  DO NOT ABBREVIATE.
                  </span>
               </td>
               <td class="bgGray">
                  <span>
                     1. CS ID No.
                  </span>
               </td>
               <td colspan="3" align="right">
                  <span>
                     (Do not fill up. For CSC use only)
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="14" class="bgGray" style="border-top:2px solid #000;border-bottom:2px solid #000;">
                  <span style="font-family:Arial Narrow;font-size:11pt;font-weight:bold;">
                     <i>I. PERSONAL INFORMATION</i>
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span >2.</span>
               </td>
               <td class="bgGrayLabel">
                  <span >SURNAME</span>
               </td>
               <td colspan="12">
                  <span class="answer">
                    surname
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel">
                  <span >&nbsp;</span>
               </td>
               <td class="bgGrayLabel">
                  <span >FIRST NAME</span>
               </td>
               <td colspan="9">
                  <span class="answer">
                    first name
                  </span>
               </td>
               <td colspan="2" class="bgGrayLabel">
                  <span >
                    NAME EXTENSION (JR., SR)
                  </span>
               </td>
               <td>
                  <span class="answer">
                    JR/SR
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel">
                  <span >&nbsp;</span>
               </td>
               <td class="bgGrayLabel">
                  <span style="font-family:Arial Narrow;font-size:8pt;width:140px;">MIDDLE NAME</span>
               </td>
               <td colspan="12">
                  <span class="answer">
                    middle name
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span >3.</span>
               </td>
               <td class="bgGrayLabel" colspan="3">
                  <span >DATE OF BIRTH (mm/dd/yyyy)</span>
               </td>
               <td colspan="2">
                  <span class="answer">
                    birthday
                  </span>
               </td>
               <td colspan="3" class="bgGrayLabel">
                  <span >16. CITIZENSHIP</span>
               </td>
               <td colspan="2" align="center">
                  <span style="font-family:'Wingdings 2'">R</span> Filipino
               </td>
               <td colspan="3">
                  <span style="font-family:'Wingdings';">o</span> Dual Citizenship
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span >4.</span>
               </td>
               <td class="bgGrayLabel">
                  <span >PLACE OF BIRTH</span>
               </td>
               <td colspan="4">
                  <span class="answer">Manila</span>
               </td>
               <td colspan="3" class="bgGrayLabel" align="center">
                  <span >If holder of dual citizenship,</span>
               </td>
               <td colspan="2">
               </td>
               <td colspan="1">
                  <span style="font-family:'Wingdings';">o</span> By Birth
               </td>
               <td colspan="2">
                  <span style="font-family:'Wingdings';">o</span> By Naturalization
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span >5.</span>
               </td>
               <td class="bgGrayLabel">
                  <span >SEX</span>
               </td>
               <td colspan="2" align="center">
                  <span style="font-family:'Wingdings';">o</span> Male
               </td>
               <td colspan="2" align="center">
                  <span style="font-family:'Wingdings';">o</span> Female
               </td>
               <td colspan="3" class="bgGrayLabel" align="center">
                  please indicate the details
               </td>
               <td colspan="5" align="center">
                  Pls. indicate country
                  <br>
                  Japan
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="4">
                  <span >6.</span>
               </td>
               <td class="bgGrayLabel" rowspan="4" align="top">
                  <span >CIVIL STATUS</span>
               </td>
               <td colspan="2">
                  <span style="font-family:'Wingdings';">o</span> Single
               </td>
               <td colspan="2">
                  <span style="font-family:'Wingdings';">o</span> Married
               </td>
               <td class="bgGrayLabel" rowspan="6" colspan="2" align="top">
                  <span>17. RESIDENTIAL ADDRESS</span>
               </td>
               <td colspan="4" align="center">
                  <span class="answer">617</span>
               </td>
               <td colspan="2" align="center">
                  <span class="answer">ANTIPOLO</span>
               </td>
            </tr>
            <tr>
               <td colspan="2">
                  <span style="font-family:'Wingdings';">o</span> Widowed
               </td>
               <td colspan="2">
                  <span style="font-family:'Wingdings';">o</span> Separated
               </td>
               <td colspan="4" align="center">
                  House/Block/Lot No.
               </td>
               <td colspan="2" align="center">
                  Street
               </td>
            </tr>
            <tr>
               <td colspan="2">
                  <span style="font-family:'Wingdings';">o</span> Other/s:
               </td>
               <td colspan="2"></td>
               <td colspan="4" align="center">
                 <span class="answer">GAGALANGIN</span>
               </td>
               <td colspan="2" align="center">
                  <span class="answer">167</span>
               </td>
            </tr>
            <tr>
               <td colspan="4"></td>
               <td colspan="4" align="center">
                  Subdivision/Village
               </td>
               <td colspan="2" align="center">
                  Barangay
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2" align="top">
                  <span>7.</span>
               </td>
               <td class="bgGrayLabel" rowspan="2" align="top">
                  <span>HEIGHT (m)</span>
               </td>
               <td colspan="4" rowspan="2" align="top">
                  <span class="answer">163</span>
               </td>
               <td colspan="4" align="center">
                  <span class="answer">MANILA</span>
               </td>
               <td colspan="2" align="center">
                  <span class="answer">METRO MANILA</span>
               </td>
            </tr>
            <tr>
               <td colspan="4" align="center">
                  City/Municipality
               </td>
               <td colspan="2" align="center">
                  Province
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span>8.</span>
               </td>
               <td class="bgGrayLabel">
                  <span>WEIGHT (kg)</span>
               </td>
               <td colspan="4">
                  <span class="answer">65</span>
               </td>
               <td class="bgGrayLabel" align="center" colspan="2" align="center">
                  <span>ZIP CODE</span>
               </td>
               <td colspan="6">
                  <span class="answer">1013</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2" align="top">
                  <span>9.</span>
               </td>
               <td class="bgGrayLabel" rowspan="2" align="top">
                  <span>BLOOD TYPE</span>
               </td>
               <td colspan="4" rowspan="2" align="top">
                  <span class="answer">B</span>
               </td>
               <td class="bgGrayLabel" align="center" colspan="2" rowspan="6" align="top">
                  <span>18. PERMANENT ADDRESS</span>
               </td>
               <td colspan="4" align="center">
                  <span class="answer">617</span>
               </td>
               <td colspan="2" align="center">
                  <span class="answer">ANTIPOLO</span>
               </td>
            </tr>
            <tr>
               <td colspan="4" align="center">
                  House/Block/Lot No.
               </td>
               <td colspan="2" align="center">
                  Street
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2" align="top">
                  <span>10.</span>
               </td>
               <td class="bgGrayLabel" rowspan="2" align="top">
                  <span>GSIS NO.</span>
               </td>
               <td colspan="4" rowspan="2">
                  <span class="answer">200-250-9226</span>
               </td>
               <td colspan="4" align="center">
                  <span class="answer">GAGALANGIN</span>
               </td>
               <td colspan="2" align="center">
                  <span class="answer">167</span>
               </td>
            </tr>
            <tr>
               <td colspan="4" align="center">
                  Subdivision/Village
               </td>
               <td colspan="2" align="center">
                  Barangay
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">
                  <span>11.</span>
               </td>
               <td class="bgGrayLabel" rowspan="2">
                  <span>PAG-IBIG NO.</span>
               </td>
               <td colspan="4" rowspan="2">
                  <span class="answer">1050-0262-4197</span>
               </td>
               <td colspan="4" align="center">
                  <span class="answer">MANILA</span>
               </td>
               <td colspan="2" align="center">
                  <span class="answer">METRO MANILA</span>
               </td>
            </tr>
            <tr>
               <td colspan="4" align="center">
                  City/Municipality
               </td>
               <td colspan="2" align="center">
                  Province
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span>12.</span>
               </td>
               <td class="bgGrayLabel">
                  <span>PHILHEALTH NO.</span>
               </td>
               <td colspan="4">
                  <span class="answer">19-000239392-5</span>
               </td>
               <td class="bgGrayLabel" colspan="2" align="center">
                  <span>ZIP CODE</span>
               </td>
               <td colspan="6">
                  <span class="answer">1013</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span>13.</span>
               </td>
               <td class="bgGrayLabel">
                  <span>SSS NO.</span>
               </td>
               <td colspan="4">
                  <span class="answer">N/A</span>
               </td>
               <td class="bgGrayLabel" colspan="2">
                  <span>19. TELEPHONE NO.</span>
               </td>
               <td colspan="6">
                  <span class="answer">N/A</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span>14.</span>
               </td>
               <td class="bgGrayLabel">
                  <span>TIN NO.</span>
               </td>
               <td colspan="4">
                  <span class="answer">141-501-416</span>
               </td>
               <td class="bgGrayLabel" colspan="2">
                  <span>20. MOBILE NO.</span>
               </td>
               <td colspan="6">
                  <span class="answer">0995-1703268</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  <span>15.</span>
               </td>
               <td class="bgGrayLabel">
                  <span>AGENCY EMPLOYEE NO.</span>
               </td>
               <td colspan="4">
                  <span class="answer">N/A</span>
               </td>
               <td class="bgGrayLabel" colspan="2">
                  <span>21. E-MAIL ADDRESS (if any)</span>
               </td>
               <td colspan="6">
                  <span class="answer">N/A</span>
               </td>
            </tr>
            <tr>
               <td colspan="14" class="bgGray" style="border-top:2px solid #000;border-bottom:2px solid #000;">
                  <span style="font-family:Arial Narrow;font-size:11pt;font-weight:bold;">
                     <i>II. FAMILY BACKGROUND</i>
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  22.
               </td>
               <td class="bgGrayLabel">
                  SPOUSE'S SURNAME
               </td>
               <td colspan="6">
                  <span class="answer">BERNARDINO</span>
               </td>
               <td class="bgGrayLabel" colspan="4" align="center">
                  23. NAME OF CHILD (Write full name and list all)
               </td>
               <td class="bgGrayLabel b-left" colspan="2" align="center">
                  DATE OF BIRTH (mm/dd/yyyy)
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" colspan="2"></td>
               <td colspan="4" rowspan="2" valign="bottom">
                  <span class="answer">BERNARDO</span>
               </td>
               <td class="bgGrayLabel" colspan="2">
                  NAME EXTENSION (JR., SR)
               </td>
               <td colspan="4" rowspan="2">
                  <span class="answer">LALAINE P. BERNARDINO</span>
               </td>
               <td colspan="2" rowspan="2">
                  <span class="answer">01/02/1983</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  FIRST NAME
               </td>
               <td class="bgGrayLabel" colspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  MIDDLE NAME
               </td>
               <td colspan="6">
                  <span class="answer">ALBANO</span>
               </td>
               <td colspan="4">
                  <span class="answer">LILIBETH P. BERNARDINO</span>
               </td>
               <td colspan="2">
                  <span class="answer">05/03/1984</span<>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  OCCUPATION
               </td>
               <td colspan="6">
                  <span class="answer">N/A</span>
               </td>
               <td colspan="4">
                  <span class="answer">BERNARDO JR. P. BERNARDINO</span>
               </td>
               <td colspan="2">
                  <span class="answer">07/16/1985</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  EMPLOYER/BUSINESS NAME
               </td>
               <td colspan="6">
                  <span class="answer">N/A</span>
               </td>
               <td colspan="4">
                  <span class="answer">LEONARD P. BERNARDINO</span>
               </td>
               <td colspan="2">
                  <span class="answer">02/25/1987</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  BUSINESS ADDRESS
               </td>
               <td colspan="6">
                  <span class="answer">N/A</span>
               </td>
               <td colspan="4">
                  <span class="answer">LOWEL O. BERNARDINO</span>
               </td>
               <td colspan="2">
                  <span class="answer">06/04/1988</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  TELEPHONE NO.
               </td>
               <td colspan="6">
                  <span class="answer">N/A</span>
               </td>
               <td colspan="4">
                  <span class="answer">LEONEL P. BERNARDINO</span>
               </td>
               <td colspan="2">
                  <span class="answer">09/07/1991</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel">
                  24.
               </td>
               <td class="bgGrayLabel">
                  FATHER'S SURNAME
               </td>
               <td colspan="6">
                  <span class="answer">PICACHE</span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td colspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" colspan="2"></td>
               <td colspan="4" rowspan="2" valign="bottom">
                  <span class="answer">BERNARDO</span>
               </td>
               <td class="bgGrayLabel" colspan="2">
                  NAME EXTENSION (JR., SR)
               </td>
               <td colspan="4" rowspan="2">
                  <span class="answer"></span>
               </td>
               <td colspan="2" rowspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  FIRST NAME
               </td>
               <td class="bgGrayLabel" colspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  MIDDLE NAME
               </td>
               <td colspan="6">
                  <span class="answer">ROQUE</span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td colspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel">25.</td>
               <td class="bgGrayLabel">
                  MOTHER'S MAIDEN NAME
               </td>
               <td colspan="6">
                  <span class="answer"></span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td colspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  SURNAME
               </td>
               <td colspan="6">
                  <span class="answer">CELSO</span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td colspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  FIRST NAME
               </td>
               <td colspan="6">
                  <span class="answer">AURORA</span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td colspan="2">
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  MIDDLE NAME
               </td>
               <td colspan="6">
                  <span class="answer">DELA CRUZ</span>
               </td>
               <td colspan="6" class="bgGrayLabel" align="center" style="color:#FF0000;">
                  <i>(Continue on Separate sheet of necessary)</i>
               </td>
            </tr>
            <tr>
               <td colspan="14" class="bgGray" style="border-top:2px solid #000;border-bottom:2px solid #000;">
                  <span style="font-family:Arial Narrow;font-size:11pt;font-weight:bold;">
                     <i>III. EDUCATIONAL BACKGROUND</i>
                  </span>
               </td>
            </tr>
            <tr style="border:1px solid #000;">
               <td rowspan="3" class="bgGrayLabel" align="center">26.</td>
               <td rowspan="3" class="bgGrayLabel" align="center" style="border-left:1px solid gray;">LEVEL</td>
               <td rowspan="3" colspan="3" class="bgGrayLabel" align="center">
                  NAME OF SCHOOL
                  <br>
                  (Write in full)
               </td>
               <td rowspan="3" colspan="4" class="bgGrayLabel" align="center">
                  BASIC EDUCATION/DEGREE/COURSE
                  <br>
                  (Write in full)
               </td>
               <td rowspan="2" colspan="2" class="bgGrayLabel" align="center">
                  PERIOD OF
                  <br>
                  ATTENDANCE
               </td>
               <td rowspan="3" class="bgGrayLabel" align="center">
                  HIGHEST
                  <br>
                  LEVEL/
                  <br>
                  UNITS EARNED
                  <br>
                  (if not graduated)
               </td>
               <td rowspan="3" class="bgGrayLabel" align="center">
                  YEAR
                  <br>
                  GRADUATED
               </td>
               <td rowspan="3" class="bgGrayLabel" align="center">
                  SCHOLARSHIP/
                  <br>
                  ACADEMIC
                  <br>
                  HONORS
                  <br>
                  RECEIVED
               </td>
            </tr>
            <tr></tr>
            <tr style="border:1px solid #000;">
               <td class="bgGrayLabel" align="center">From</td>
               <td class="bgGrayLabel" align="center">To</td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  ELEMENTARY
               </td>
               <td colspan="3">
                  <span class="answer"></span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  SECONDARY
               </td>
               <td colspan="3">
                  <span class="answer"></span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  VOCATIONAL/
                  <br>
                  TRADE COURSE
               </td>
               <td colspan="3">
                  <span class="answer"></span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  COLLEGE
               </td>
               <td colspan="3">
                  <span class="answer"></span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel"></td>
               <td class="bgGrayLabel">
                  GRADUATE STUDIES
               </td>
               <td colspan="3">
                  <span class="answer"></span>
               </td>
               <td colspan="4">
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
            </tr>
            <tr style="border-top:2px solid #000;border-bottom:2px solid #000;">
               <td colspan="14" class="bgGrayLabel" align="center" style="color:#FF0000;">
                  <i>(Continue on separate sheet if necessary)</i>
               </td>
            </tr>
            <tr>
               <td colspan="2" class="bgGrayLabel" align="center">
                  <strong>SIGNATURE</strong>
               </td>
               <td colspan="4"></td>
               <td colspan="1" class="bgGrayLabel" align="center">
                  <strong>DATE</strong>
               </td>
               <td colspan="4"></td>
               <td colspan="3">
                  <i>CS FORM 212 (Revised 2017), Page 1 of 4</i>
               </td>
            </tr>
         </table>
      </div>
      <br class="noPrint">
      <div id="Eligibility" class="page-- nextpage">
         <table border=1 cellspacing=0 cellpadding=1>
            <tr align="center" class="ruler">
               <td width="20px">A</td>
               <td width="42px">B</td>
               <td width="68px">C</td>
               <td width="74px">D</td>
               <td width="22px">E</td>
               <td width="80px">F</td>
               <td width="50px">G</td>
               <td width="50px">H</td>
               <td width="*px">I</td>
               <td width="55px">J</td>
               <td width="55px">K</td>
               <td width="70px">L</td>
               <td width="70px">M</td>
            </tr>
            <tr>
               <td colspan="13" class="bgGray tabTitle_td">
                  <span class="tabTitle">
                     <i>IV. CIVIL SERVICE ELIGIBILITY</i>
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=2>27.</td>
               <td class="bgGrayLabel" align="center" rowspan=2 colspan=4>
                  CAREER SERVICE/ RA 1080 (BOARD/ BAR) <br>UNDER SPECIAL LAWS/ CES/ CSEE<br>
                  BARANGAY ELIGIBILITY / DRIVER'S LICENSE
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=2 >
                  RATING<br>(If Applicable)
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=2 colspan=2>
                  DATE OF EXAMINATION / <br>CONFERMENT
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=2 colspan=3>
                  PLACE OF EXAMINATION / <br>CONFERMENT
               </td>
               <td class="bgGrayLabel" align="center" valign="center" colspan=2>
                  LICENSE (if applicable)
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=2>NUMBER</td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=2>Date of Validatiy</td>
            </tr>
            <tr>
            </tr>
            <tr>
               <td colspan=5>
                  <span class="answer">R.A 6850 TEACHER ELIGIBILITY MAGNA - CARTA</span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td colspan=2>
                  <span class="answer"></span>
               </td>
               <td colspan=3>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
               <td>
                  <span class="answer"></span>
               </td>
            </tr>
            <?php
               for ($j=1;$j<=9;$j++) {
            ?>
                  <tr>
                     <td colspan=5>
                        <span class="answer">&nbsp;</span>
                     </td>
                     <td>
                        <span class="answer">&nbsp;</span>
                     </td>
                     <td colspan=2>
                        <span class="answer"></span>
                     </td>
                     <td colspan=3>
                        <span class="answer"></span>
                     </td>
                     <td>
                        <span class="answer"></span>
                     </td>
                     <td>
                        <span class="answer"></span>
                     </td>
                  </tr>
            <?php
               }
            ?>
         </table>
      </div>
      <br class="noPrint">
      <div id="WorkExp" class="page-- nextpage">
         <table border=1 cellspacing=0 cellpadding=1>
            <tr align="center" class="ruler">
               <td width="20px">A</td>
               <td width="42px">B</td>
               <td width="68px">C</td>
               <td width="74px">D</td>
               <td width="22px">E</td>
               <td width="80px">F</td>
               <td width="50px">G</td>
               <td width="50px">H</td>
               <td width="*px">I</td>
               <td width="55px">J</td>
               <td width="55px">K</td>
               <td width="70px">L</td>
               <td width="70px">M</td>
            </tr>
            <tr>
               <td colspan="13" class="bgGray tabTitle_td">
                  <span class="tabTitle">
                     <i>V. WORK EXPERIENCE</i>
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="13" class="bgGray">
                  <span class="tabTitle" style="font-size:10pt;">
                     <i>
                        (Include private employment.  Start from your recent work) Description of duties should be indicated in the attached Work Experience sheet.
                     </i>
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=2>28.</td>
               <td class="bgGrayLabel" align="center" rowspan=2 colspan=2>
                  INCLUSIVE DATES<br>(mm/dd/yyyy)
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=3 colspan=3>
                  POSITION TITLE<br>(Write in full/do not abbreviate)
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=3 colspan=3>
                  DEPARTMENT / AGENCY / OFFICE / COMPANY<br>
                  (Write in full/Do not abbreviate)
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=3>
                  MONTHLY SALARY
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=3>
                  SALARY/ JOB/ PAY GRADE (if applicable)& STEP  (Format "00-0")/ INCREMENT
               </td>
               <td class="bgGrayLabel" align="center" valign="middle" rowspan=3>
                  STATUS OF APPOINTMENT
               </td>
               <td class="bgGrayLabel" align="center" valign="center" rowspan=3>
                  GOV'T SERVICE<br>(Y/N)
               </td>
            </tr>
            <tr>
            </tr>
            <tr style="height:40px;">
               <td class="bgGrayLabel" align="center" valign="middle" colspan=2>From</td>
               <td class="bgGrayLabel" align="center" valign="center">To</td>
            </tr>







         </table>
      </div>
      <br class="noPrint">




      <div id="C-4" class="page-- lastpage">
         <table border=1 cellspacing=0 cellpadding=1>
            <tr align="center" style="height:1px;background:black;">
               <td width="20px"></td>
               <td width="139px"></td>
               <td width="180px"></td>
               <td width="14px"></td>
               <td width="215px"></td>
               <td width="17px"></td>
               <td width="55px"></td>
               <td width="11px"></td>
               <td width="15px"></td>
               <td width="148px"></td>
               <td width="23px"></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="7">34.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="3">
                  Are you related by consanguinity or affinity to the appointing or recommending authority, or to the
                  <br>
                  chief of bureau or office or to the person who has immediate supervision over you in the Office,
                  <br>
                  Bureau or Department where you will be appointed,
               </td>
               <td colspan="6" rowspan="3"></td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr>
               <td class="bgGrayLabel" colspan="4" rowspan="2">
                  a. within the third degree?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" colspan="4" rowspan="2">
                  b. within the fourth degree (for Local Government Unit - Career Employees)?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="6">35.</td>
               <td colspan="4" rowspan="2" class="bgGrayLabel">
                  a. Have you ever been found guilty of any administration offense?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" colspan="4" rowspan="4">
                  b. Have you been criminally charged before any court?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td></td>
               <td colspan="4">
                  Date Filled:
               </td>
            </tr>
            <tr>
               <td></td>
               <td></td>
               <td colspan="4">
                  Status of Case/s: ___________________
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">36.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2">
                  Have you ever been convicted of any crime or violation of any law, decree, ordinance, or regulation by
                  any court or tribunal?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">37.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  Have you ever been separated from the service in any of the following modes: resignation, retirement,
                  dropped from
                  <br>
                  the rolls, dismissal, termination, end of term, finished contract or phased out (abolition) in the
                  public or private sector?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">38.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  a. Have you ever been candidate in a national or local election held within the last year (except
                  Barangay Election)?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2"></td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  b. Have you resigned from the government service during the three (3)-month period before the last
                  <br>
                  election to promote/actively campaign for a national or local candidate?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">39.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  Have you acquired the status of an immigrant or permanent resident of another country?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="3"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="8">40.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2">
                  Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons (RA 7277);
                  and (c) Solo
                  <br>
                  Parents Welfare Act of 2000 (RA 8972), please answer the following items:
               </td>
               <td colspan="6" rowspan="2"></td>
            </tr>
            <tr></tr>
            <tr>
               <td colspan="4" class="bgGrayLabel" rowspan="2">
                  a. Are you a member of any indigenous group?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="2"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td colspan="4" class="bgGrayLabel" rowspan="2">
                  b. Are you a person with disability?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="2"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td colspan="4" class="bgGrayLabel" rowspan="2">
                  c. Are you a solo parent?
               </td>
               <td></td>
               <td colspan="3" align="center">
                  <span style="font-family:'Wingdings';">o</span> YES &nbsp
                  <span style="font-family:'Wingdings';">o</span> NO
               </td>
               <td colspan="2"></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="4">
                  If YES, give details:
                  <br>
                  <span class="answer">_______________________</span>
               </td>
               <td></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">41.</td>
               <td class="bgGrayLabel" colspan="6">
                  REFERENCES <i style="color:#FF0000">(Person not related by consanguinity or affinity to applicant / appointee)</i>
               </td>
               <td colspan="4"></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" colspan="4" align="center">NAME</td>
               <td class="bgGrayLabel" colspan="1" align="center">ADDRESS</td>
               <td class="bgGrayLabel" colspan="2" align="center">TEL NO.</td>
               <td colspan="4" rowspan="6"></td>
            </tr>
            <tr>
               <td colspan="4">
                  <span class="answer">&nbsp;</span>
               </td>
               <td colspan="1">
                  <span class="answer">&nbsp;</span>
               </td>
               <td colspan="2">
                  <span class="answer">&nbsp;</span>
               </td>
            </tr>
            <tr>
               <td colspan="4">
                  <span class="answer">&nbsp;</span>
               </td>
               <td colspan="1">
                  <span class="answer">&nbsp;</span>
               </td>
               <td colspan="2">
                  <span class="answer">&nbsp;</span>
               </td>
            </tr>
            <tr>
               <td colspan="4">
                  <span class="answer">&nbsp;</span>
               </td>
               <td colspan="1">
                  <span class="answer">&nbsp;</span>
               </td>
               <td colspan="2">
                  <span class="answer">&nbsp;</span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" rowspan="3" align="center">42.</td>
               <td class="bgGrayLabel" colspan="6" rowspan="3">
                  I declare under the oath that I have personally accomplished this Personal Data Sheet which is a true,
                  correct and complete statement pursuant to the provisions of pertinent laws, rules and regualations of
                  the Republic of the Philippines. I authorize the agency head/ authorized representative to verify/
                  validate the contents stated herein. I agree that any misrepresentation made in this document and its
                  attachment shall cause the filing of administrative/criminal case/s against me.
               </td>
            </tr>
            <tr></tr>
            <tr>
               <td colspan="4" align="center">PHOTO</td>
            </tr>
            <tr>
            <td colspan="11"></td>
            </tr>
            <tr>
               <td rowspan="6"></td>
               <td colspan="2" class="bgBlueLabel">
                  Government Issued ID (i.e. Passport, GSIS, SSS, PRC, Driver's License, etc.)
                  <br>
                  <strong><i>PLEASE INDICATE ID Number and Date of Issuance</i></strong>
               </td>
               <td rowspan="6"></td>
               <td colspan="3" rowspan="3" style="height:70px;"></td>
               <td colspan="2" rowspan="6"></td>
               <td rowspan="5"></td>
               <td rowspan="6"></td>
            </tr>
            <tr>
               <td>Government Issued ID:</td>
               <td> </td>
            </tr>
            <tr>
               <td rowspan="2">ID/License/Passport No. :</td>
               <td rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td colspan="3" align="center" class="bgBlueLabel"><i>Signature (Sign inside the box)</i></td>
            </tr>
            <tr>
               <td rowspan="2">Date/Place of Issuance:</td>
               <td rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td colspan="3">
                  <!--<table width="100%" style="margin-bottom:-5px;">
                     <tr>
                        <td></td>
                     </tr>
                     <tr>
                        <td colspan="3" class="bgBlueLabel" align="center">
                           Date Accomplished
                        </td>
                     </tr>
                  </table>-->
                  <div style="height:10px;">&nbsp;</div>
                  <div class="bgBlueLabel" align="center" style="">Date Accomplished</div>
               </td>
               <td align="center" class="bgBlueLabel">Right Thumbmark</td>
            </tr>
            <tr>
               <td colspan="11" align="center">
                  SUBSCRIBED AND SWORN to before me this ___________________ , affiant exhibiting his/her validly issued
                  government ID as indicated above.
               </td>
            </tr>
            <tr><td colspan="11"></td></tr>
            <tr>
               <td colspan="3" rowspan="2"></td>
               <td colspan="2" style="height:50px;"></td>
               <td colspan="6" rowspan="2"></td>
            </tr>
            <tr>
               <td colspan="2" align="center" class="bgBlueLabel">Person Administering Oath</td>
            </tr>
            <tr><td colspan="11"></td></tr>
         </table>
      </div>
   </body>
</html>
<?php
/*
   echo '<table border=1>' . "\n";
   foreach ($workSheet->getRowIterator() as $row) {
     echo '<tr>' . "\n";
     $cellIterator = $row->getCellIterator();
     // This loops all cells, even if it is not set.
     // By default, only cells that are set will be iterated.
     $cellIterator->setIterateOnlyExistingCells(false);
     foreach ($cellIterator as $cell) {
      $excelDate = $cell->getValue(); // gives you a number like 44444, which is days since 1900
      $stringDate = PHPExcel_Style_NumberFormat::toFormattedString($excelDate, 'MM-DD-YYYY');
      echo '<td>'.$stringDate.'</td>'."\n";
     }
     echo '</tr>' . "\n";
   }
   echo '</table>' . "\n";
   */


   /*function toNumber($dest)
   {
       if ($dest)
           return ord(strtolower($dest)) - 96;
       else
           return 0;
   }

   function myFunction($s,$x,$y){
      $x = toNumber($x);
      return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
   }


   $inputFileType = "Excel2007";
   $inputFileName = "pds/MAMIPDS2.xlsx";
   $objReader = PHPExcel_IOFactory::createReader($inputFileType);
   $objPHPExcel = $objReader->load($inputFileName);
   $objPHPExcel->setActiveSheetIndex(0);
   $sheetData = $objPHPExcel->getActiveSheet();
   echo var_dump($sheetData);

   $cellData = myFunction($sheetData,'C','8');
   echo "<br><br><br>ERWIN TEST >>> ".$cellData;
   */

   /*
   $file = "pds/MAMIPDS.xlt";
   $inputFileType = PHPExcel_IOFactory::identify($file->getRealPath());
   $objReader = PHPExcel_IOFactory::createReader($inputFileType);
   $objReader->setLoadSheetsOnly("MySheet");
   $objPHPExcel = $objReader->load($file->getRealPath());
   $rows = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
   echo $rows;
   */
?>

